package com.nps.controllers;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.nps.exceptions.ForbiddenAccessException;
import com.nps.exceptions.SessionExpiredException;

@ControllerAdvice
public class CustomExceptionController {
	
	@ExceptionHandler(ForbiddenAccessException.class)
	public ModelAndView handleForbiddenAccessException(ForbiddenAccessException e) {
		return new ModelAndView("common/403");
	}
	
	@ExceptionHandler(SessionExpiredException.class)
	public ModelAndView handleSessionExpiredException(SessionExpiredException e) {
		System.out.println("Session Expired.");
		return new ModelAndView(new RedirectView("landing-page"));
	}
}
