package com.nps.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.nps.constants.CustomHttpStatus;

@Controller
public class CustomErrorController implements ErrorController{
	
	@Override
	public String getErrorPath( ) {
		return "/error";
	}
	
	@GetMapping("/error")
	public ModelAndView displayErrorPage(HttpServletRequest request) {
		int httpErrorCode = getErrorCode(request);
		return new ModelAndView((httpErrorCode == CustomHttpStatus.NOT_FOUND ? "common/404" : "common/server-error"));
	}
	
	private int getErrorCode(HttpServletRequest request) {
		return (Integer) request.getAttribute("javax.servlet.error.status_code");
	}
	
}
