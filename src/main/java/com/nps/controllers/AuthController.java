package com.nps.controllers;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.nps.domains.User;
import com.nps.handlers.SessionHandler;
import com.nps.service.interfaces.UserService;

@RestController
public class AuthController {
	
	@Autowired
	private UserService userService;
	
	@GetMapping("login")
	public ModelAndView displayLoginPage(HttpServletRequest request) {
		return new ModelAndView("portal/login");
	}
	
	
	@PostMapping("login")
	public ModelAndView login(HttpServletRequest request, @RequestBody User user) {
		User theUser = this.userService.loginUser(user);
		SessionHandler.createSession(request, theUser);
		return redirectByUserType(user);
	}
	
	
	@GetMapping("logout")
	public ModelAndView logout(HttpServletRequest request) throws ServletException {
		SessionHandler.destroySession(request);
		return new ModelAndView(new RedirectView("login"));
	}
	
	
	private ModelAndView redirectByUserType(User user) {
		if(user.getUserType().equals("SA")) {
			return new ModelAndView(new RedirectView("admin.dashboard"));
		}else if(user.getUserType().equals("A")) {
			return new ModelAndView(new RedirectView("admin.dashboard"));
		}else {
			return new ModelAndView(new RedirectView("portal.dashboard"));
		}
	}
	
}
