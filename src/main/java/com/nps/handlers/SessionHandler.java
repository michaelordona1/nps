package com.nps.handlers;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.nps.domains.User;
import com.nps.exceptions.ForbiddenAccessException;
import com.nps.exceptions.SessionExpiredException;


public class SessionHandler {

	private static final String USER = "user";
	private static final int TIMEOUT = 30;
	private static final String SESSION_EXPIRED_MESSAGE = "Session has expired";
	private static final String FORBIDDEN_MESSAGE = "Access is forbidden";
	
	public static void createSession(HttpServletRequest request, User user) {
		request.getSession(true).setAttribute(USER, user);
		request.getSession().setMaxInactiveInterval(TIMEOUT*60);
	}
	
	public static User getSessionUser(HttpServletRequest request) throws SessionExpiredException {
		HttpSession session = request.getSession();
		if(session.getAttribute(USER) != null) {
			return (User) session.getAttribute(USER);
		}else {
			throw new SessionExpiredException(SESSION_EXPIRED_MESSAGE);
		}
	}
	
	public static void checkSessionUserAccess(HttpServletRequest request, String... userTypes) throws ForbiddenAccessException {
		HttpSession session = request.getSession();
		User sessionUser = (User) session.getAttribute(USER);

		int ctr = 0;
		for (String userType : userTypes) {
			if (sessionUser.getUserType().equals(userType)) {
				ctr++;
			}
		}
		if(ctr == 0) {
			throw new ForbiddenAccessException(FORBIDDEN_MESSAGE);
		}
	}
	
	public static void destroySession(HttpServletRequest request) throws ServletException {
		request.getSession().invalidate();
	}
	
} 
