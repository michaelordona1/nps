package com.nps.service.interfaces;


import com.nps.domains.User;


public interface UserService {

	User loginUser(User user);

}
