package com.nps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;


@SpringBootApplication
public class NpsApplication extends SpringBootServletInitializer {
	
	public static void main(String[] args) {
		SpringApplication.run(NpsApplication.class, args);
	}
}
