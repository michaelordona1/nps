/**
 * 
 */
package com.nps.exceptions;

public class NpsException extends Exception {

	private static final long serialVersionUID = 7459293151900260630L;

	public NpsException() {
		super();
	}
	
	public NpsException (String message) {
		super(message);
	}
	
	public NpsException (String message, Throwable cause) {
		super(message, cause);
	}
	
	public NpsException (Throwable cause) {
		super(cause);
	}
}
