package com.nps.domains;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="surveyemployee")
public class SurveyEmployee {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter private int surveyId;
	@Getter @Setter private int employeeId;
	@Getter @Setter private int status;
	@Transient @Getter @Setter private String updatedAt;
	@Transient @Getter @Setter private String createdAt;
}
