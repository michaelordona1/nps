package com.nps.domains;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="employee")
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter private int employeeId;
	@Getter @Setter private int userId;
	@Getter @Setter private int positionId;
	@Getter @Setter private String firstName;
	@Getter @Setter private String middleName;
	@Getter @Setter private String lastName;
	@Getter @Setter private String email;
	@Getter @Setter private String mobileNumber;
	@Getter @Setter private String address;
	@Getter @Setter private int postalCode;
	@Getter @Setter private int profilePictureUrl;
	@Transient @Getter @Setter private String updatedAt;
	@Transient @Getter @Setter private String createdAt;
	
	@Transient @Getter @Setter private String fullName;
}
