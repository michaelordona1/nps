package com.nps.domains;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="company")
public class Company {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter private int companyId;
	@Getter @Setter private String name;
	@Getter @Setter private String description;
	@Transient @Getter @Setter private String updatedAt;
	@Transient @Getter @Setter private String createdAt;
}
