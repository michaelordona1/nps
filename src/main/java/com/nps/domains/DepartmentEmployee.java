package com.nps.domains;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="departmentemployee")
public class DepartmentEmployee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter private int departmentId;
	@Getter @Setter private int divisionId;
	@Getter @Setter private int employeeId;
	@Getter @Setter private boolean isManager;
	@Transient @Getter @Setter private String updatedAt;
	@Transient @Getter @Setter private String createdAt;	
}
