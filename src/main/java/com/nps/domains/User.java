package com.nps.domains;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="user")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter private int userId;
	@Getter @Setter private String userType;
	@Getter @Setter private String username;
	@Getter @Setter private String password;
	@Getter @Setter private boolean status;
	@Transient @Getter @Setter private String updatedAt;
	@Transient @Getter @Setter private String createdAt;
}
