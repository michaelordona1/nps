package com.nps.domains;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="question")
public class Question {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter private int questionId;
	@Getter @Setter private String questionType;
	@Getter @Setter private String name;
	@Getter @Setter private String description;
	@Getter @Setter private boolean status;
	@Transient @Getter @Setter private String updatedAt;
	@Transient @Getter @Setter private String createdAt;
}
