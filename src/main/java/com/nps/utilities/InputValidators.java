package com.nps.utilities;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;


@Component
public class InputValidators {
	public static final Pattern EMAIL_ADDRESS_REGEX = 
		    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE),
		    NO_SPECIAL_CHARS_EX_DOT_REGEX = Pattern.compile("^[-a-zA-Z'\\\\. ]*$");
	
	public boolean isNumeric(String str) {
	    for (char c : str.toCharArray())
	    {
	        if (!Character.isDigit(c)) return false;
	    }
	    return true;
	}
	
	public boolean noSpecialCharsExceptDot(String str) {
		Matcher matcher = NO_SPECIAL_CHARS_EX_DOT_REGEX.matcher(str);
		return !matcher.find();
	}
	
	public File multipartToFile(MultipartFile multipart, String fileName) throws IllegalStateException, IOException {
	    File convFile = new File(System.getProperty("java.io.tmpdir")+"/"+fileName);
	    multipart.transferTo(convFile);
	    return convFile;
	}
	
	public boolean validateEmail(String emailStr) {
        Matcher matcher = EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
	}
	
	public String getFileExtension(String fileName) {
	    String name = fileName;
	    int lastIndexOf = name.lastIndexOf(".");
	    if (lastIndexOf == -1) {
	        return ""; // empty extension
	    }
	    return name.substring(lastIndexOf);
	}
	
	public String generateUsername(String firstName, String lastName) {
		List<String> invalidCharacters = new ArrayList<>();
		invalidCharacters.add(" ");
		invalidCharacters.add(".");
		
		for(String invChar: invalidCharacters) {
			firstName = firstName.replace(invChar, "");
			lastName = lastName.replace(invChar, "");
		}
		return firstName + "." + lastName;
	}
	
	public String generatedPassword() {
		int leftLimit = 97;
	    int rightLimit = 122;
	    int targetStringLength = 10;
	    Random random = new Random();
	    StringBuilder buffer = new StringBuilder(targetStringLength);
	    for (int i = 0; i < targetStringLength; i++) {
	        int randomLimitedInt = leftLimit + (int) 
	          (random.nextFloat() * (rightLimit - leftLimit + 1));
	        buffer.append((char) randomLimitedInt);
	    }
	    return buffer.toString();
	}
}
