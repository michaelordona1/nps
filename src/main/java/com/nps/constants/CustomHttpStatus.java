package com.nps.constants;

public class CustomHttpStatus {
	
	public static final int
	OK							=200,
	CREATED						=201,
	UNAUTHORIZED				=401,
	FORBIDDEN					=403,
	NOT_FOUND					=404,
	SESSION_EXPIRED				=440,
	SERVER_ERROR				=500;	
}
