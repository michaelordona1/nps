$(function(){
	defineVariables();
	initPageSettings();
	initContTypeDropdown();
	initClassMaterialsTbl();
	initCourseMaterialsTbl();
	initClassInfo();
	attachListenerToCancelBtn();
	attachListenerToContentTypeDropdown();
	attachListenerToDeleteMaterialsButton();
	attachListenerToSaveMaterialButton();
	attachListenerToUploadPhotoLink();
	//attachCopyBtnListener();
	attachListenerToUploadPhoto();
});

function defineVariables() {
	checkedLists = [];
}

function initContTypeDropdown() {
	_.each(ContentType.LIST,function(contentType) {
		$("#contentTypeDropdown").append($("<option/>").attr({"value" : contentType.id})
			.html(contentType.description));
	});
}

function initClassInfo() {
	if($("#deliveryMethod").val() == DeliveryMethod.INDIVIDUAL){
		$("#deliveryMethodLabel").html("Individual");
	} else if($("#deliveryMethod").val() == DeliveryMethod.MODULAR){
		$("#deliveryMethodLabel").html("Modular");
	} else if($("#deliveryMethod").val() == DeliveryMethod.CLASSTRAINING){
		$("#deliveryMethodLabel").html("Class Training");
	}
}

function initClassMaterialsTbl() {
	var classId = parseInt(GetURLParameter('classId'));
	pagination.initiate({
    	url: "classinfo/classmaterial/pages",
    	locator: "classMaterials",
    	paginationDom: "#classMaterialTblPagination",
    	tableBodyDom: "#classMaterialTblBody",
    	className: "paginationjs-theme-blue",
    	pageSize: 5,
    	ajax:{data:{classId:classId},type:"GET"},
    	functionName: "createClassMaterialsRow"
    });
}

function initCourseMaterialsTbl() {
	var courseId = $("#tbCourseId").val();
	pagination.initiate({
    	url: "courseinfo/coursematerial/pages",
    	locator: "courseMaterials",
    	paginationDom: "#courseMaterialTblPagination",
    	tableBodyDom: "#courseMaterialTblBody",
    	className: "paginationjs-theme-blue",
    	pageSize: 5,
    	ajax:{data:{courseId:courseId},type:"GET"},
    	functionName: "createCourseMaterialsRow"
    });
}

function initPreloader(status) {
	if(status=="show") {
		$("#materialsModalBtn").hide();
		$("#progressCon").show();
	} else {
		$("#materialsModalBtn").show();
		$("#preloader").hide();
		$("#progressCon").hide();
	}
}

function initPageSettings() {
	$("#urlDiv").hide();
	$("#preloader").hide();
}

function createCourseMaterialsRow(courseMaterial) {
	var visibility = "";
	if(courseMaterial.viewStatus == Status.ACTIVE){
		visibility = $("<i/>").attr("class","material-icons").html("visibility");
	} else if(courseMaterial.viewStatus == Status.INACTIVE){
		visibility = $("<i/>").attr("class","material-icons").html("visibility_off");
	}
	
	$("#courseMaterialTblBody").append($("<tr/>")
		.append($("<td/>")
					.append($("<span/>").attr("style","padding:10px;"))
					.append($("<a/>").attr("href", "courseinfo/coursematerials/downloadmaterial?materialId=" 
					+ courseMaterial.courseMaterialId)
						.append($("<i/>").attr("class","material-icons").html("file_download")).css("display", courseMaterial.contentType == ContentType.EMBED ? "none" : ""))

					.append(
						$("<a>").attr("href","#!")
								.bind("click",function(){
									$("#viewEmbeddedPowerPoint").modal("open");
									$("#embeddedPowerPointTitle").html(courseMaterial.fileLabel)
									
									$("#documentViewerCon").empty();
									
									if(courseMaterial.contentType == ContentType.EMBED){
										$("#documentViewerCon")
										.append($("<center>")
												.append($("<iframe>")
															.attr({
																"id":"documentViewerFrame", 
																"width":"650", 
																"height":"450",
																"src":courseMaterial.contentUrlEncoded
															})
														)
										)
									}else if(courseMaterial.contentType == ContentType.DOCUMENT){
										$("#documentViewerCon")
										.append($("<center>")
												.append($("<iframe allowfullscreen webkitallowfullscreen>")
															.attr({
																"id":"documentViewerFrame", 
																"width":"650", 
																"height":"450",
																"src":"https://docs.google.com/viewer?url=" + 
																encodeURIComponent(courseMaterial.contentUrlEncoded) + 
																"&embedded=true"
															})
												)
										)
										.append("<div style='width: 40px; height: 40px; position: absolute; opacity: 0; right: 14px; top: 14px;'> </div>")
									}else if(courseMaterial.contentType == ContentType.VIDEO){
										$("#documentViewerCon")
										.append($("<center>")
												.append(
														$("<video autoplay controls controlsList='nodownload' oncontextmenu='return false;'>")
														.attr({
															"id":"documentViewerFrame",
															"width": "650", 
															"height": "450"
														})		
														.append(
																$("<source>").attr({"src":courseMaterial.contentUrlEncoded})
														)
												)
										);
									}
									
								})
								.append($("<i>").addClass("material-icons").html("remove_red_eye"))
				)	
												
						
		)
		.append($("<td/>").html(courseMaterial.fileLabel))
		.append($("<td/>").html(getContentTypeDesc(courseMaterial.contentType)))
		.append($("<td/>").html(courseMaterial.fileName))
		.append($("<td/>")
		.append(visibility)));
}

/*function attachCopyBtnListener() {
	var classId = GetURLParameter("classId");
	$("#copyBtn").click(function () {
		var $temp = $("<input>");
		var trainingUrl = window.location.origin + "/preview-class?classId=" + classId;
		$("body").append($temp);
		$temp.val(trainingUrl).select();
		document.execCommand("copy");
		$temp.remove();
		alertify.success("Copied");
	});
}*/

function createClassMaterialsRow(classMaterial) {
	var visibility = "";
	if(classMaterial.viewStatus == Status.ACTIVE){
		visibility = $("<i/>").attr("class","material-icons").html("visibility");
	} else if(classMaterial.viewStatus == Status.INACTIVE){
		visibility = $("<i/>").attr("class","material-icons").html("visibility_off");
	}
	$("#classMaterialTblBody").append($("<tr/>")
		.append($("<td/>")
			.append($("<label/>").attr("style","margin-left:10px !important;").css("display", $("#userType").val() == 1 ? "" : "none")
				.append($("<input/>").attr({"type":"checkbox"})
				.prop("checked",checkIfClassMaterialExist(checkedLists,classMaterial))
				.bind("change",function() {
					if($(this).prop("checked") != true) {
						if(checkIfClassMaterialExist(checkedLists,classMaterial)) {
							deleteClassMaterialFromCheckList(classMaterial);
						}
					} else {
						if(!checkIfClassMaterialExist(checkedLists,classMaterial)) {
							checkedLists.push(classMaterial);
						}
					}
				}))
				.append($("<span/>").attr("style","padding:10px;")))
				.append($("<a/>").attr("href","classinfo/classmaterials/downloadmaterial?materialId=" + classMaterial.classMaterialId)
					.append($("<i/>").attr("class","material-icons").css("display", classMaterial.contentType == ContentType.EMBED ? "none" : "").html("file_download"))
					.append($("<a>").attr("href","#!").css("display", classMaterial.contentType == ContentType.VIDEO ? "" : "none")
							.append($("<i>").addClass("material-icons").html("file_copy"))
							.bind("click", function(){
								var $temp = $("<input>");
								var origin = window.location.origin;
								var root = window.location.pathname.split("/")[1];
								var materialUrl = "";
								if(origin.indexOf("localhost") !== -1){  //if not equal to -1, then word localhost found
									materialUrl = origin + "/" + root + "/classmaterials/" + classMaterial.classMaterialId + "/video";
								}else{ //for uat/prod
									materialUrl = origin + "/classmaterials/" + classMaterial.classMaterialId + "/video";
								}
								$("body").append($temp);
								$temp.val(materialUrl).select();
								document.execCommand("copy");
								$temp.remove();
								alertify.success("Copied");
							})
					)
				)
				.append(
					$("<a>").attr("href","#!")
							.bind("click",function(){
								$("#viewEmbeddedPowerPoint").modal("open");
								$("#embeddedPowerPointTitle").html(classMaterial.fileLabel)
								
								$("#documentViewerCon").empty();
								
								if(classMaterial.contentType == ContentType.EMBED){
									$("#documentViewerCon")
									.append($("<center>")
											.append($("<iframe allowfullscreen webkitallowfullscreen>")
														.attr({
															"id":"documentViewerFrame", 
															"width":"650", 
															"height":"450",
															"src":classMaterial.contentUrl
														})
													)
									)
								}else if(classMaterial.contentType == ContentType.DOCUMENT){
									$("#documentViewerCon")
									.append($("<center>")
											.append($("<iframe allowfullscreen webkitallowfullscreen>")
														.attr({
															"id":"documentViewerFrame", 
															"width":"650", 
															"height":"450",
															"src":"https://docs.google.com/viewer?url=" + 
															encodeURIComponent(classMaterial.contentUrlEncoded) + 
															"&embedded=true"
														})
											)
									)
									.append("<div style='width: 40px; height: 40px; position: absolute; opacity: 0; right: 14px; top: 14px;'> </div>")
								}else if(classMaterial.contentType == ContentType.VIDEO){
									$("#documentViewerCon")
									.append($("<center>")
											.append(
													$("<video autoplay controls controlsList='nodownload' oncontextmenu='return false;'>")
													.attr({
														"id":"documentViewerFrame",
														"width": "650", 
														"height": "450"
													})		
													.append(
															$("<source>").attr({"src":classMaterial.contentUrlEncoded})
													)
											)
									);
								}
								
							})
							.append($("<i>").addClass("material-icons").html("remove_red_eye"))
				)						
		)
		.append($("<td/>").html(classMaterial.fileLabel))
		.append($("<td/>").html(getContentTypeDesc(classMaterial.contentType)))
		.append($("<td/>").html(classMaterial.fileName))
		.append($("<td/>")
			.append($("<a/>").attr({"data-id":classMaterial.classMaterialId,"data-status":classMaterial.viewStatus})
			.bind("click",function(){updateStatus($(this).data("id"),$(this).data("status"));})
				.append(visibility))));
}

function getContentTypeDesc(contentTypeValue) {
	var result = "";
	_.each(ContentType.LIST,function(contentType) {
		if(contentTypeValue==contentType.id) {
			result=contentType.description;
		}
	});
	return result;
}

function checkIfClassMaterialExist(classMaterials,classMaterial) {
	var result = false;
	_.each(classMaterials,function(cmObj) {
		if(cmObj.classMaterialId == classMaterial.classMaterialId) {
			result = true;
		}
	});
	return result;
}

function deleteClassMaterialFromCheckList(classMaterial) {
	var result = false;
	_.each(checkedLists,function(cmObj) {
		if(cmObj.classMaterialId == classMaterial.classMaterialId) {
			checkedLists = deleteItem(classMaterial);
		}
	});
	return result;
}

function deleteItem(classMaterial) {
	var resultArray = [];
	_.each(checkedLists,function(cmObj) {
		if(cmObj.classMaterialId != classMaterial.classMaterialId) {
			resultArray.push(cmObj);
		}
	});
	return resultArray;
}

function attachListenerToContentTypeDropdown() {
	$("#contentTypeDropdown").on("change",function() {
		if($(this).val()==ContentType.URL) {
			$("#urlDiv").show();
			$("#mediaUploadDiv").hide();
		}if($(this).val()==ContentType.EMBED) {
			$("#urlDiv").show();
			$("#mediaUploadDiv").hide();
		}  else {
			$("#urlDiv").hide();
			$("#mediaUploadDiv").show();
		}
	});
}

function attachListenerToDeleteMaterialsButton() {
	$("#btnDeleteMaterials").on("click",function() {
		$("#btnDeleteMaterials").attr("disabled","disabled");
		$("#btnDeleteMaterials").html("Deleting Materials...");
		if(checkedLists.length == 0){
			alertify.dismissAll();
			alertify.warning("Please select at least 1 class material to delete.");
			$("#btnDeleteMaterials").removeAttr("disabled");
			$("#btnDeleteMaterials").html("<i class='material-icons left'>delete</i>Delete Materials");
		} else {
			alertify.dismissAll();
			deleteClassMaterials();
		}
	});
}

function attachListenerToSaveMaterialButton() {
	$("#saveClassMaterials").on("click",function() {
		$("#saveClassMaterials").attr("disabled","disabled");
		$("#saveClassMaterials").html("Saving...");
		var classMaterials = getClassMaterialsData();
		alertify.dismissAll();
		if(validateClassMaterialFields()) {
			createClassMaterial(classMaterials);
		} else {
			$("#saveClassMaterials").removeAttr("disabled");
			$("#saveClassMaterials").html("<i class='material-icons left'>save</i>Save");
		}
	});
} 

function attachListenerToCancelBtn(){
	$("#cancelBtn").on("click",function() {
		$("#contentTypeDropdown").val(0).trigger("change");
		$("#fileLabel").val("");
		$("#fileupload").val("");
		$("#urlTextBox").val("");
		$("#viewStatus").prop("checked","");
	});
}

function getClassMaterialsData() {
	var classMaterials = {};
	classMaterials.classId = parseInt($("#classId").val());
	classMaterials.contentType = parseInt($("#contentTypeDropdown").val());
	classMaterials.contentUrl = $("#urlTextBox").val();
	classMaterials.viewStatus = getCheckBoxValue("#viewStatus");
	classMaterials.fileLabel = $("#fileLabel").val();
	return classMaterials;
}

function validateClassMaterialFields() {
	var result = true;
	if($("#contentTypeDropdown").val()==null) {
		alertify.warning("Please choose a content type.");
		result = false;
	}
	if($("#contentTypeDropdown").val()!=ContentType.URL && $("#contentTypeDropdown").val()!=ContentType.EMBED && $("#fileupload").val()=="") {
		alertify.warning("Please choose a file to upload.");
		result = false;
	}
	if($("#contentTypeDropdown").val()==ContentType.URL && $("#contentTypeDropdown").val()==ContentType.EMBED && $("#urlTextBox").val()=="") {
		alertify.warning("Please specify a URL.");
		result = false;
	}
	if($("#fileLabel").val() == "") {
		alertify.warning("Please specify a file label.");
		result = false;
	}
	return result;
}

function clearMaterialModalFields() {
	$("#contentTypeDropdown").val(0).trigger("change");
	$("#urlTextBox").val("");
	$("#viewStatus").prop("checked","");
	$("#fileLabel").val("");
	$("#fileupload").val("");
}

function createClassMaterial(classMaterial) {
	$.when(ajax.create("classinfo/classmaterial",classMaterial)).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				if(classMaterial.contentType!=ContentType.URL && classMaterial.contentType!=ContentType.EMBED) {
					uploadMediaFile(response.classMaterialId);
				} else{
					alertify.success("Successfully created!");
					setTimeout(function(){$("#learningMaterialModal").modal("close");},1000);
					initClassMaterialsTbl();
					clearMaterialModalFields();
					$(window).unbind('beforeunload');
				}
				break;
		}
	});
}

function uploadMediaFile(classMaterialId) {
	initPreloader("show");
	$.when(ajax.uploadWithProgressHandler("classinfo/classmaterial/"+classMaterialId+"/mediaFile","mediaUploadFrm"))
		.done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				alertify.success("Successfully created!");
				setTimeout(function(){
					initPreloader("hide");
					$("#learningMaterialModal").modal("close");
					$("#saveClassMaterials").removeAttr("disabled");
					$("#saveClassMaterials").html("<i class='material-icons left'>save</i>Save");
				},3000);
				initClassMaterialsTbl();
				clearMaterialModalFields();
				$(window).unbind('beforeunload');
				break;
			case HttpStatus.FILE_FORMAT_IS_INVALID:
				initPreloader("hide");
				alertify.warning("File format is invalid. Please try another one.");
				break;
			case HttpStatus.FILE_SIZE_IS_INVALID:
				initPreloader("hide");
				alertify.warning("File size is invalid.");
				break;
		}
	});
}

function updateStatus(classMaterialId,viewStatus) {
	var classMaterial = {};
	classMaterial.classMaterialId = parseInt(classMaterialId);
	classMaterial.viewStatus = (viewStatus==Status.ACTIVE)?Status.INACTIVE:Status.ACTIVE;
	$.when(ajax.customUpdate("classinfo/classmaterial/"+classMaterialId+"/viewstatus",classMaterial)).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				initClassMaterialsTbl();
				
				break;
		}
	});
}

function deleteClassMaterials() {
	$.when(ajax.customUpdate("classinfo/classmaterial/deleteclassmaterials",checkedLists)).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				alertify.success("Successfully Deleted.");
				$("#btnDeleteMaterials").removeAttr("disabled");
				$("#btnDeleteMaterials").html("<i class='material-icons left'>delete</i>Delete Materials");
				initClassMaterialsTbl();
				break;
		}
	});
}

function attachListenerToUploadPhotoLink() {
	$("#uploadPhotoLink").on("click",function() {
		$("#fileUpload").trigger("click");
	});
}

function attachListenerToUploadPhoto() {
	$("#fileUpload").on("change",function(e) {
		if(validateFile()) {
			handleRefreshEvent();
			updateClassPhoto();
		}
	});
}

function validateFile() {
	var result = true;
	if($("#fileUpload").get(0).files.length==0) {
		result = false;
	}
	if(!validateFileType("#fileUpload","IMAGE")) {
		alertify.warning("Invalid File Type. Please Try another one.");
		result = false;
	} 
	if(!validateFileSize("#fileUpload","IMAGE")) {
		result = false;
	}
	return result;
}

function updateClassPhoto() {
	blockUI("Uploading Media File..");
	var classId = $("#classId").val();
	$.when(ajax.upload("classinfo/"+classId+"/classPhotoUrl","uploadPhotoFrm")).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				alertify.success("Successfully Updated!");
				$("#classPhoto").attr("src",response.classInfo.classPhotoUrl);
				$(window).unbind('beforeunload');
				$.unblockUI();
				break;
		}
	});
}