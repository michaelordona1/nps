$(function() {
	initializeSchedules();
});

function initializeSchedules() {
	var events = [];
	$.when(ajax.fetch("classinfo/trainee/calendar")).done(function(response) {
		if(response.status == HttpStatus.SUCCESS){
			var schedules = response.scheduleList;
			if (schedules.length == 0) {
				alertify.warning("No enrolled classes found.");
			} 
			
			_.each(schedules, function(schedule){
				var event = {};
				event.title = schedule.classCode;
				event.start = schedule.startDate;
				event.id = schedule.classId;
				events.color = "#008080";
				events.push(event);
			});
			$("#trainingCalendar").empty();
			initializeCalendarWithListener("trainingCalendar", events);
			
		}
	}); 
}

function initializeCalendarWithListener(dom, data) {
	var calendarDom = document.getElementById(dom);
	var calendar = new FullCalendar.Calendar(calendarDom, {
	  plugins: ['interaction', 'dayGrid', 'timeGrid'],
	  themeSystem: 'bootstrap',
	  defaultView: 'dayGridMonth',
	  header: {
		  left: 'prev,next today',
		  center: 'title',
		  right: 'dayGridMonth'
	  },
	  events: data,
	  eventClick: function(info) {
		$.when(ajax.fetch(`classinfo/${info.event.id}/fetchdetails`)).done(function(response) {
			var classInfo = response.classDetail;
			$("#courseName").html(classInfo.courseName);
			$("#className").html(classInfo.className);
			$("#trainerName").html(classInfo.trainerName);
			$("#locationName").html(classInfo.location);
			$("#schedule").html(info.event.scheduleStartTime);
		});
	  }
	});
	calendar.render();
}