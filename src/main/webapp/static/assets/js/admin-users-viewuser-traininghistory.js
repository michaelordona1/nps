/**
 * 
 */
$(function(){
	initUserLinks();
	initUserTrainingHistory();
	attachListenerToSearchTrainingHistory();
});

function initUserLinks() {
	var userId = GetURLParameter('id');
	$("#viewUserLink").attr("href", "admin.viewuser?id="+userId);
	$("#tfJobroleLink").attr("href", "admin.viewuser.jobrole?id="+userId);
	$("#tfUsergroupLink").attr("href", "admin.viewuser.usergroup?id="+userId);
	$("#tfTrainingHistoryLink").attr("href", "admin.viewuser.traininghistory?id="+userId);
}

function initUserTrainingHistory() {
	var userId = GetURLParameter('id');
	var pageSize = 10;
    $("#trainingHistoryTblPagination").pagination({     
        dataSource: "users/"+userId+"/traininghistory/pages",
        locator: 'trainingHistories',
        totalNumberLocator: function(response) {            
            return response.totalRecords;
        },
        className: 'paginationjs-theme-blue',
        pageSize: pageSize,     
        callback: function(data) {            
            $("#trainingHistoryTblBody").empty(); 
            _.each(data, function(userTrainingHistory) {
	            populateTrainingFlowList(userTrainingHistory);
            });         
        }
    });
}

function searchUserTrainingHistory(keyword) {
	var userId = GetURLParameter('id');
	var pageSize = 10;
	$("#trainingHistoryTblPagination").pagination({     
		dataSource: "users/"+userId+"/traininghistory/search",
		locator: 'trainingHistories',
		totalNumberLocator: function(response) {            
			return response.totalRecords;
		},
		ajax: {data: {keyword : keyword}, type: "GET"},
		className: 'paginationjs-theme-blue',
		pageSize: pageSize,     
		callback: function(data) {                      
			$("#trainingHistoryTblBody").empty(); 
            _.each(data, function(userTrainingHistory) {
	            populateTrainingFlowList(userTrainingHistory);
            });           
		}
	});
}

function populateTrainingFlowList(userTrainingHistory) {
	var userId = GetURLParameter('id');
	var deliveryMethod = (userTrainingHistory.deliveryMethod > 0 ? DeliveryMethod.LIST[userTrainingHistory.deliveryMethod-1].name : "") ;
	//var userTrainingStatus = TrainingStatus.LIST[userTrainingHistory.trainingStatus-1].description;
	
	var downloadLink = "";
	if(userTrainingHistory.statusDisplay == "Completed"){
		downloadLink = $("<i/>").attr("class","material-icons").html("file_download");
	}
	
	$("#trainingHistoryTblBody").append($("<tr/>")
		.append($("<td/>")
				.append($("<a/>").attr({"href": "training-history/"+userTrainingHistory.trainingHistoryId+"/certificate", "id":"certificate_"+userTrainingHistory.classSessionId})
					.append(downloadLink)))
		.append($("<td/>").html(userTrainingHistory.courseTitle))
		.append($("<td/>").html(deliveryMethod))
		.append($("<td/>").html(userTrainingHistory.categoryName))
		.append($("<td/>").html(userTrainingHistory.className))
		.append($("<td/>").html(userTrainingHistory.trainerName))
		.append($("<td/>").html(userTrainingHistory.statusDisplay))
		.append($("<td/>").html(userTrainingHistory.completionDate)));
}

function attachListenerToSearchTrainingHistory() {
	$("#searchTrainingHistory").on("keyup",function(e) {
		var keycode = (event.keyCode ? event.keyCode : event.which);
		
		if(keycode == 13) {
			var keyword = $(this).val();
			
			if(keyword.length == 0){
				initUserTrainingHistory();
			}else{
				searchUserTrainingHistory(keyword);
			}
		}
		
		if(keycode == 8 && $("#searchTrainingHistory").val().length == 0) {
			initUserTrainingHistory();
		}
		return true;
	});
}
