$(function() {
	initPageSettings();
	attachListenerToRetakeBtn();
});

function initPageSettings() {
	var classId=$("#hdnClassId").val();
	var examId=$("#hdnExamId").val();
	var courseId = GetURLParameter("courseId");
	if (!courseId) {
		$.when(ajax.fetch("classinfo/"+classId+"/classexam/"+examId)).done(function(response) {
			switch(response.status) {
				case HttpStatus.SUCCESS:
					renderHtmlComponents(response.classExam);
					break;
			}
		});
	} else {
		$.when(ajax.fetch("courseinfo/courseexam/" + examId + "?courseId=" + courseId)).done(function(response) {
			switch(response.status) {
				case HttpStatus.SUCCESS:
					renderHtmlComponents(response.courseExam);
					break;
			}
		});
	}
	
}

function renderHtmlComponents(classExam) {
	var classId=$("#hdnClassId").val();
	var examId=$("#hdnExamId").val();
	var courseId = $("#hdnCourseId").val();
	initOfficialScore(classExam.examRetake);
	if(classExam.isShowCorrectAnswer) {
		$("#reviewBtnCon").append($("<a/>").addClass("bttn btn waves-light waves-effect")
			.attr({"href":"exam-review-home?classId="+classId+"&courseId="+courseId+"&examId="+examId,"id":"reviewBtn"}).css("width","100%").html("Review"));
	}
	if(classExam.isShowBreakDown==IsShowBreakdown.TRUE) {
		$("#showBreakDownCon").show();
		initScoreBreakdown();
	} else {
		$("#showBreakDownCon").hide();
	}
	if(classExam.isShowScore==IsShowScore.TRUE) {
		$("#scoreCon").show();
	} else {
		$("#scoreCon").hide();
	}
	if(classExam.examRetake!=ExamRetake.NO_RETAKES) {
		$("#officialScoreCon").show();
		$("#retakeCon").show();
		
		if(classExam.examRetake == ExamRetake.ONLY_WHEN_FAILED){
			if($("#hdnStatus").val() == "Failed"){
				$("#retakeExamBtnCon").show();
			}else{
				$("#retakeExamBtnCon").hide();
			}
		}else{
			$("#retakeExamBtnCon").show();
		}
	
		
		$("#retakeTxt").html("Retake: Allowed");
		$("#retakePropCon").show();
		initAttemptHistory();
		initConditionDesc(classExam.examRetake);
	} else {
		$("#officialScoreCon").hide();
		$("#retakeCon").hide();
		$("#retakeExamBtnCon").hide();	
		$("#retakeTxt").html("Retake: None");
		$("#retakePropCon").hide();
	}
	if(classExam.allowAttachment==AllowAttachment.TRUE) {
		$("#attachmentCon").show();
	} else {
		$("#attachmentCon").hide();
	}
}

function initConditionDesc(examRetake) {
	if(examRetake==ExamRetake.ONLY_WHEN_FAILED) {
		$("#conditionTxt").html("Condition: Only When Failed");
	} else if(examRetake==ExamRetake.EVEN_IF_PASSED) {
		$("#conditionTxt").html("Condition: Even If Passed");
	} else {
		$("#conditionTxt").html("");
	}
}

function initScoreBreakdown() {
	var classId=$("#hdnClassId").val();
	var examId=$("#hdnExamId").val();
	$.when(ajax.fetchWithData("classexam/"+classId+"/employeeexamprogresslist",{examId:examId})).done(
		function(response) {
			switch(response.status) {
				case HttpStatus.SUCCESS:
					populateScoreBreakdown(response.employeeExamProgressList);
					break;
			}	
	});
}

function populateScoreBreakdown(employeeExamProgressList) {
	_.each(employeeExamProgressList,function(employeeExamProgress) {
		$("#scoreBreakdownTblBody").append($("<tr/>")
			.append($("<td/>").html(employeeExamProgress.sectionName))
			.append($("<td/>").html(employeeExamProgress.totalItems))
			.append($("<td/>").html(employeeExamProgress.score)));
	});
}

function initAttemptHistory() {
	var classId=$("#hdnClassId").val();
	var examId=$("#hdnExamId").val();
	var courseId = GetURLParameter("courseId");
	$.when(ajax.fetchWithData("employeeexamprogress/attempthistory",
		{
			examId:examId,
			classId:classId,
			courseId:courseId
		}))
		.done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				populateAttemptHistory(response.employeeExamProgressList);
				break;
		}
	});
}

function populateAttemptHistory(employeeExamProgressList) {
	_.each(employeeExamProgressList,function(employeeExamProgress) {
		$("#attemptHistoryTblBody").append($("<tr/>")
			.append($("<td/>").html(employeeExamProgress.createdAt))
			.append($("<td/>").html(employeeExamProgress.score))
			.append($("<td/>").html(employeeExamProgress.percentage+"%"))
			.append($("<td/>").html(employeeExamProgress.statusDesc)))
	});
}

function initOfficialScore(examRetake) {
	var examId=$("#hdnExamId").val();
	var classId=$("#hdnClassId").val();
	var courseId = GetURLParameter("courseId");
	$.when(ajax.fetchWithData("employeeexamsummary/officialscore",
		{classId:classId,
			examId:examId,
			examRetake:examRetake,
			courseId:courseId
		})).done(
		function(response) {
			switch(response.status) {
				case HttpStatus.SUCCESS:
					var employeeExamProgress=response.employeeExamProgress;
					populateOfficialScoreBreakdown(employeeExamProgress.employeeExamProgressList);
					$("#officialScore").html(employeeExamProgress.totalScore);
					$("#officialPercentage").html(employeeExamProgress.percentage+"%");
					$("#officialStatus").html(employeeExamProgress.statusDesc);
					break;
			}
	});
}

function populateOfficialScoreBreakdown(employeeExamProgressList) {
	_.each(employeeExamProgressList,function(employeeExamProgress) {
		$("#OfficialScoreBreakdownTblBody").append($("<tr/>")
			.append($("<td/>").html(employeeExamProgress.sectionName))
			.append($("<td/>").html(employeeExamProgress.totalItems))
			.append($("<td/>").html(employeeExamProgress.score)));
	});
}

function attachListenerToRetakeBtn() {
	var courseId = GetURLParameter("courseId");
	var classId = $("#hdnClassId").val();
	var examId = $("#hdnExamId").val();
	$("#retakeExamBtn").on("click",function() {
		var employeeExamSummary={};
		employeeExamSummary.examId=$("#hdnExamId").val();
		employeeExamSummary.classId=$("#hdnClassId").val();
		employeeExamSummary.courseId=courseId;
		$.when(ajax.create("employeeexamsummary",employeeExamSummary)).done(function(response) {
			switch(response.status) {
				case HttpStatus.SUCCESS:
					!courseId
					?	redirect(`exam-home?classId=${classId}&examId=${examId}`,1000)
					:	redirect(`exam-home?classId=${classId}&courseId=${courseId}&examId=${examId}`,1000)
					break;
			}
		})
	});
}