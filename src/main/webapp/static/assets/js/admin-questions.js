$(function() {
	initTopicDropDown();
	initDifficultyLevelDropDown();
	initQuestionTypeDropDown();
	initQuestionsTbl();
	initSortableItems();
	attachListenerToSearchQuestionBtn();
	attachListenerToSortLink();
	attachListenerToResetBtn();
	initLightBox();
});

function initLightBox(){
	lightbox.option({
	      'resizeDuration': 0,
	      'wrapAround': true
	    })	
}

function initSortableItems() {
	//Billy pakiayos hindi ako sure if
	//pwede pagsamahin na lang para hindi paulit ulit
	//ginamit ko siya sa ordering, and matching 
	$("#orderingAnswer").sortable({
		items:"#sortable",
	});
}

function initTopicDropDown() {
	$.when(ajax.fetch("trainingtopics")).done(function(response) {
		populateTopicDropDown(response.trainingTopics);
	});
}

function populateTopicDropDown(trainingTopics) {
	_.each(trainingTopics,function(trainingTopic) {
		$("#topicDropDown").append($("<option/>").attr({"value" : trainingTopic.topicId})
				.html(trainingTopic.topicDesc));
	});
}

function initDifficultyLevelDropDown() {
	$.when(ajax.fetch("difficultylevels")).done(function(response) {
		populateDIfficultyDropDown(response.difficultyLevels);
	});
}

function populateDIfficultyDropDown(difficultyLevels) {
	_.each(difficultyLevels,function(difficultyLevel) {
		$("#difficultyDropDown").append($("<option/>").attr({"value" : difficultyLevel.difficultyId})
				.html(difficultyLevel.difficultyName));
	});
}

function initQuestionTypeDropDown() {
	$.when(ajax.fetch("questiontypes")).done(function(response) {
		populateQuestionTypeDropDown(response.questionTypes);
	});
}

function populateQuestionTypeDropDown(questionTypes) {
	_.each(questionTypes,function(questionType) {
		$("#questionTypeDropDown").append($("<option/>").attr({"value" : questionType.questionTypeId})
				.html(questionType.questionTypeDesc));
	});
}

function initQuestionsTbl() {
	pagination.initiate({
    	url: "questions/pages",
    	locator: "questions",
    	paginationDom: "#questionsPagination",
    	tableBodyDom: "#questionsTblBody",
    	className: "paginationjs-theme-blue",
    	pageSize: 10,
    	ajax:{data:{
    		questionType:$("#questionTypeDropDown").val(),
    		topic:$("#topicDropDown").val(),
    		difficultyLevel:$("#difficultyDropDown").val(),
    		status: $("#statusDropDown").val(),
    		sortName:"createdAt",
    		sortDir:"ASC"
    		},type:"POST"},
    	functionName: "createQuestionsRow"
    });
}

function createQuestionsRow(question) {
	var url = question.mediaUrl;
	var modalAssignment = "";
	if(question.questionImages.length == 0){
		modalAssignment = "withoutPicture"
	}else{
		console.log(question);
		console.log("size: " + question.questionImages.length);
		modalAssignment = "withPicture"
	}
	
	$("#questionsTblBody").append($("<tr/>")
		.append($("<td/>").addClass("min")
			.append($("<a/>").addClass("waves-effect waves-light")
			.attr({"href":"#!"})
			.bind("click",function() {
				$("#viewQuestionModal"+modalAssignment).modal("open");
				initQuestionDetails(question, modalAssignment);
			})
				.append($("<i/>").addClass("material-icons").html("pageview")))
			.append($("<a/>").addClass("waves-effect waves-light").attr("href","admin.question?action=update&questionId="+question.questionId)
				.append($("<i/>").addClass("material-icons").html("edit"))))
		.append($("<td/>").addClass("mid").html(question.label))
		.append($("<td/>").addClass("mid").html(question.topicDesc))
		.append($("<td/>").addClass("mid").html(question.difficultyName))
		.append($("<td/>").addClass("mid").html(question.questionTypeDesc))
		.append($("<td/>").addClass("mid")
			.append($("<a/>").addClass("waves-effect waves-light")
				.append($("<i/>").addClass("material-icons").html((question.status != 0)? "check":"close"))))
	)
}

function populateQuestionPreview(question,modalAssignment) {
	$("#questionContent"+modalAssignment).empty();
	$("#modalPicCon").empty();
	_.each(question.questionImages, function(questionImage){
		$("#modalPicCon")
			.append($("<div>").addClass("row")
				.append($("<img>").addClass("imgPos").attr("src",questionImage.mediaUrl)
					.bind("click", function(){
						window.open("portal.image?id="+questionImage.questionImageId);
					})
				)
			)
	})
	$("#questionContent"+modalAssignment).html(question.content);
}

function attachListenerToSearchQuestionBtn() {
	$("body").on("click","#searchQuestionBtn",function() {
		pagination.initiate({
	    	url: "questions/pages",
	    	locator: "questions",
	    	paginationDom: "#questionsPagination",
	    	tableBodyDom: "#questionsTblBody",
	    	className: "paginationjs-theme-blue",
	    	pageSize: 10,
	    	ajax:{data:{
	    		questionType:$("#questionTypeDropDown").val(),
	    		topic:$("#topicDropDown").val(),
	    		difficultyLevel:$("#difficultyDropDown").val(),
	    		status: $("#statusDropDown").val(),
	    		sortName:"createdAt",
	    		sortDir:"ASC"
	    		},type:"POST"},
	    	functionName: "createQuestionsRow"
	    });
	});
}

function attachListenerToSortLink() {
	$("body").on("click",".sort",function(e) {
		e.preventDefault();
		pagination.initiate({
	    	url: "questions/pages",
	    	locator: "questions",
	    	paginationDom: "#questionsPagination",
	    	tableBodyDom: "#questionsTblBody",
	    	className: "paginationjs-theme-blue",
	    	pageSize: 10,
	    	ajax:{data:{
	    		questionType:$("#questionTypeDropDown").val(),
	    		topic:$("#topicDropDown").val(),
	    		difficultyLevel:$("#difficultyDropDown").val(),
	    		status: $("#statusDropDown").val(),
	    		sortName:$(this).data("sortname"),
	    		sortDir:$(this).data("sortdir")
	    		},type:"POST"},
	    	functionName: "createQuestionsRow"
	    });
		return false;
	});
}

function attachListenerToResetBtn() {
	$("body").on("click","#resetBtn",function() {
		$("#questionTypeDropDown").val("0").trigger("change");
		$("#topicDropDown").val("0").trigger("change");
		$("#difficultyDropDown").val("0").trigger("change");
		$("#statusDropDown").val("2").trigger("change");
		initQuestionsTbl();
	});
}

function initQuestionDetails(question, modalAssignment) {
	$(".questionsDiv").css("display","none");
	$.when(ajax.fetch("question/" + question.questionId)).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				$.when(populateQuestionDetails(response.question, modalAssignment)).done(function(response1) {
					populateQuestionPreview(response.question,modalAssignment);
				});
				
				break;
		}
	});
}

function populateQuestionDetails(question, modalAssignment) {
	if(question.questionTypeId == QuestionType.MULTIPLE_CHOICE){
		populateChoices(question.choices, modalAssignment);
		$("#choiceDiv"+modalAssignment).css("display","");
	
	}else if(question.questionTypeId == QuestionType.FILL_IN_THE_BLANK){
		fillBlanksAnswers = question.fillInTheBlanksAnswers;
		populateFillInTheBlanks(fillBlanksAnswers, modalAssignment);
		$("#fillBlanksDiv"+modalAssignment).css("display","");
		
	}else if(question.questionTypeId == QuestionType.TRUE_OR_FALSE){
		$("#trueFalseDiv"+modalAssignment).css("display","");
		$("#trueFalse"+modalAssignment).html(question.answer);
	
	}else if(question.questionTypeId == QuestionType.ESSAY){
		$("#essayDiv"+modalAssignment).css("display","");
	
	}else if(question.questionTypeId == QuestionType.IDENTIFICATION){
		$("#identificationDiv"+modalAssignment).css("display","");
		$("#identification"+modalAssignment).html(question.answer);
	
	}else if(question.questionTypeId == QuestionType.ENUMERATION){
		populateEnumerationAnswers(question.enumerationAnswers, modalAssignment);
		$("#enumerationDiv"+modalAssignment).css("display","");
	
	}else if(question.questionTypeId == QuestionType.ORDERING){
		$("#orderingDiv"+modalAssignment).css("display","");
		populateOrderingAnswers(question.orderingAnswers, modalAssignment);
	
	}else if(question.questionTypeId == QuestionType.MATCHING){
		$("#matchingDiv"+modalAssignment).css("display","");
		analogyAnswerList = question.analogyAnswers;
		populateAnalogyAnswers(question.analogyAnswers, modalAssignment);
	
	}else if(question.questionTypeId == QuestionType.FORM){
		
	
	}else if(question.questionTypeId == QuestionType.ASSESSMENT){
		$("#assessmentCriteria"+modalAssignment).css("display","");
	}
}

function populateFillInTheBlanks(fillBlanksAnswers, modalAssignment) {
	$("#fillBlanksDivAnswer"+modalAssignment).html("");
	_.each(fillBlanksAnswers,function(fillBlanksAnswer) {
		$("#fillBlanksDivAnswer"+modalAssignment)
			.append($("<ul/>").attr("style","list-style-type:disc")
				.append($("<li/>").html("<b>> "+fillBlanksAnswer.answer+"<b/>")));
		
	});
}

function populateOrderingAnswers(orderingAnswers, modalAssignment) {
	var orderingCtr = 1;
	$('#orderingDivAnswers'+modalAssignment).html("");
	_.each(orderingAnswers,function(orderingAnswer) {
		$("#orderingDivAnswers"+modalAssignment)
			.append($("<ul/>").attr("style","list-style-type:disc")
					.append($("<li/>").html("<b>" + orderingCtr + ". " +orderingAnswer.answer+"<b/>")));
		orderingCtr++;
	});
}

function populateAnalogyAnswers(analogyAnswers, modalAssignment) {
	_.each(analogyAnswers, function(analogyAnswer){
		$("#matchingDivAnswers"+modalAssignment)
		.append($("<ul/>").attr("style","list-style-type:disc")
				.append($("<li/>").html("<b> "+analogyAnswer.givenA+"<b/>" + " : " + "<b> "+analogyAnswer.givenB+"<b/>")));
	})
}

function populateEnumerationAnswers(enumerationAnswers, modalAssignment) {
	$("#enumerationDivAnswer"+modalAssignment).html("");
	_.each(enumerationAnswers,function(enumerationAnswer) {
		$("#enumerationDivAnswer"+modalAssignment)
			.append($("<ul/>").attr("style","list-style-type:disc")
				.append($("<li/>").html("<b>> "+enumerationAnswer.answer+"<b/>")));
		
	});
}

function populateChoices(choices, modalAssignment) {
	$("#multipleChoiceDiv"+modalAssignment).html("");
	_.each(choices,function(choice) {
		if(choice.choiceType != 0){
			$("#multipleChoiceDiv"+modalAssignment)
				.append($("<ul/>").attr("style","list-style-type:disc")
					.append($("<li/>").html("<b>> "+choice.choiceDesc+"<b/>")));
		}
		
	});
}

function attachListenerToQuestionTypeDropdown() {
//	$("#questionTypeDropDown").on("change",function() {
//		switch(parseInt($(this).val())) {
//			case QuestionType.MULTIPLE_CHOICE:
//				tinymce.remove();
//				initTinyMceEditor("#texteditor",500);
//				displayContainer("#multipleChoiceCon");
//				if($("#action").val()=="update"){setContent("texteditor",$("#contentHdn").val());}
//				if($("#action").val()=="create"){createMultipleChoiceCon(4);}
//				break;
//			case QuestionType.TRUE_OR_FALSE:
//				tinymce.remove();
//				initTinyMceEditor("#texteditor",500);
//				displayContainer("#trueOrFalseCon");
//				if($("#action").val()=="update"){setContent("texteditor",$("#contentHdn").val());}
//				break;
//			case QuestionType.IDENTIFICATION:
//				tinymce.remove();
//				initTinyMceEditor("#texteditor",500);
//				displayContainer("#identificationCon");
//				if($("#action").val()=="update"){setContent("texteditor",$("#contentHdn").val());}
//				break;
//			case QuestionType.ENUMERATION:
//				tinymce.remove();
//				initTinyMceEditor("#texteditor",500);
//				displayContainer("#enumerationCon");
//				if($("#action").val()=="update"){setContent("texteditor",$("#contentHdn").val());}
//				break;
//			case QuestionType.ORDERING:
//				tinymce.remove();
//				initTinyMceEditor("#texteditor",500);
//				displayContainer("#OrderingCon");
//				if($("#action").val()=="update"){setContent("texteditor",$("#contentHdn").val());}
//				break;
//			case QuestionType.MATCHING:
//				tinymce.remove();
//				initTinyMceEditor("#texteditor",500);
//				displayContainer("#matchingCon");
//				setContent("texteditor",$("#contentHdn").val());
//				break;
//			case QuestionType.ASSESSMENT:
//				tinymce.remove();
//				initTinyMceEditor("#texteditor",500);
//				displayContainer("#AssessmentCon");
//				if($("#action").val()=="update"){setContent("texteditor",$("#contentHdn").val());}
//				break;
//			case QuestionType.ESSAY:
//				tinymce.remove();
//				initTinyMceEditor("#texteditor",500);
//				displayContainer("#essayCon");
//				if($("#action").val()=="update"){setContent("texteditor",$("#contentHdn").val());}
//				break;
//			case QuestionType.FILL_IN_THE_BLANK:
//				tinymce.remove();
//				initCustomTinyMceEditor();
//				if($("#action").val()=="update"){setContent("texteditor",$("#contentHdn").val());}
//				break;
//			default:
//				initTinyMceEditor("#texteditor",500);
//				displayContainer("#essayCon");
//				if($("#action").val()=="update"){setContent("texteditor",$("#contentHdn").val());}
//				break;
//		}
//	});
}