$(function() {
	removeTabIndex();
	defineGlobalVariables();
	updateTraineeStatusAfterEndDate();
	initUserGroupDropdown();
	initJobRoleDropdown();
	initTraineeTbl();
	initApprovedTraineeTbl();
	initPendingTraineeTbl();
	initGradeSheetHeader();
	initClassEmployeeAssessmentsList();
	
	attachListenerToSortName();
	attachListenerToSortJob();
	attachListenerToSortUserGroup();
	attachListenerToSortDateEnrolled();
	attachListenerToSortDateCompleted();
	attachListenerToSortTrainingStatus();
	attachListenerToSortRequestDate();
	attachListenerToSortApprovedDate();
	attachListenerToSortApprovalStatus();
	
	attachListenerToFilterGroup();
	attachListenerToJobRoleDropdown();
	attachListenerToUserGroupDropdown();
	attachListenerToSearchTxtBox();
	attachListenerToCheckAll();
	attachListenerToAddTraineeBtn();
	attachListenerToCancelTraineeBtn();
	attachListenerToCompleteChkBox();
	attachListenerToSetAsCompletedBtn();
	attachListenerToUploadPhotoLink();
	attachListenerToUploadPhoto();
	attachListenerToPrintApprovedTraineeBtn();
	attachListenerToPrintGradingSheetBtn();
	attachListenerToGenerateCertificatesBtn();
});


function updateTraineeStatusAfterEndDate(){
	var classId = $("#classId").val();
	var obj = {};
	obj.classId = classId;
	ajax.customUpdate("classinfo/"+classId+"/class-employees/training-status", obj);
}


function defineGlobalVariables() {
	employeeInfoCheckedList = [];
	employeeInfoList = [];
	approvedTraineeList = [];
	pendingTraineeList = [];
	completedTraineeList = [];
	gradingComponentsList = [];
	classEmployeeAssessmentList = [];
	employeeExamSummariesList = [];
	sortDirection = "";
	statusTimeout = undefined;
}


function attachListenerToSortName(){
	$("#sortByNameApproved").click(function(){
		var classId = $("#classId").val();
		sortColumnName = "NAME";
		sortDirection = (sortDirection == "DESC" ? "ASC" : "DESC");
		sortApprovedTraineeTbl(classId, sortColumnName, sortDirection);
	});
	
	$("#sortByNamePending").click(function(){
		var classId = $("#classId").val();
		sortColumnName = "NAME";
		sortDirection = (sortDirection == "DESC" ? "ASC" : "DESC");
		sortPendingTraineeTbl(classId, sortColumnName, sortDirection);
	});	
}


function attachListenerToSortJob(){
	$("#sortByJobApproved").click(function(){
		var classId = $("#classId").val();
		sortColumnName = "JOB";
		sortDirection = (sortDirection == "DESC" ? "ASC" : "DESC");
		sortApprovedTraineeTbl(classId, sortColumnName, sortDirection);
	});	
	
	$("#sortByJobPending").click(function(){
		var classId = $("#classId").val();
		sortColumnName = "JOB";
		sortDirection = (sortDirection == "DESC" ? "ASC" : "DESC");
		sortPendingTraineeTbl(classId, sortColumnName, sortDirection);
	});		
}

function attachListenerToSortUserGroup(){
	$("#sortByUserGroupApproved").click(function(){
		var classId = $("#classId").val();
		sortColumnName = "USER_GROUP";
		sortDirection = (sortDirection == "DESC" ? "ASC" : "DESC");
		sortApprovedTraineeTbl(classId, sortColumnName, sortDirection);
	});	
	
	$("#sortByUserGroupPending").click(function(){
		var classId = $("#classId").val();
		sortColumnName = "USER_GROUP";
		sortDirection = (sortDirection == "DESC" ? "ASC" : "DESC");
		sortPendingTraineeTbl(classId, sortColumnName, sortDirection);
	});	
}

function attachListenerToSortDateEnrolled(){
	$("#sortByDateEnrolledApproved").click(function(){
		var classId = $("#classId").val();
		sortColumnName = "DATE_ENROLLED";
		sortDirection = (sortDirection == "DESC" ? "ASC" : "DESC");
		sortApprovedTraineeTbl(classId, sortColumnName, sortDirection);
	});	
}

function attachListenerToSortDateCompleted(){
	$("#sortByDateCompletedApproved").click(function(){
		var classId = $("#classId").val();
		sortColumnName = "DATE_COMPLETED";
		sortDirection = (sortDirection == "DESC" ? "ASC" : "DESC");
		sortApprovedTraineeTbl(classId, sortColumnName, sortDirection);
	});	
}

function attachListenerToSortTrainingStatus(){
	$("#sortByTrainingStatusApproved").click(function(){
		var classId = $("#classId").val();
		sortColumnName = "TRAINING_STATUS";
		sortDirection = (sortDirection == "DESC" ? "ASC" : "DESC");
		sortApprovedTraineeTbl(classId, sortColumnName, sortDirection);
	});	
}

function attachListenerToSortRequestDate(){
	$("#sortByRequestDatePending").click(function(){
		var classId = $("#classId").val();
		sortColumnName = "REQUEST_DATE";
		sortDirection = (sortDirection == "DESC" ? "ASC" : "DESC");
		sortPendingTraineeTbl(classId, sortColumnName, sortDirection);
	});			
}

function attachListenerToSortApprovedDate(){
	$("#sortByApprovedDatePending").click(function(){
		var classId = $("#classId").val();
		sortColumnName = "APPROVED_DATE";
		sortDirection = (sortDirection == "DESC" ? "ASC" : "DESC");
		sortPendingTraineeTbl(classId, sortColumnName, sortDirection);
	});		
}

function attachListenerToSortApprovalStatus(){
	$("#sortByApprovedStatusPending").click(function(){
		var classId = $("#classId").val();
		sortColumnName = "APPROVAL_STATUS";
		sortDirection = (sortDirection == "DESC" ? "ASC" : "DESC");
		sortPendingTraineeTbl(classId, sortColumnName, sortDirection);
	});		
}

function attachListenerToSortLastName(){
	$("#sortByLastName").click(function(){
		var classId = $("#classId").val();
		sortColumnName = "LAST_NAME";
		sortDirection = (sortDirection == "DESC" ? "ASC" : "DESC");
		sortTraineeTbl(classId, sortColumnName, sortDirection);
	});		
}

function attachListenerToSortFirstName(){
	$("#sortByFirstName").click(function(){
		var classId = $("#classId").val();
		sortColumnName = "FIRST_NAME";
		sortDirection = (sortDirection == "DESC" ? "ASC" : "DESC");
		sortTraineeTbl(classId, sortColumnName, sortDirection);
	});		
}

function attachListenerToSortEmployeeCode(){
	$("#sortByEmployeeCode").click(function(){
		var classId = $("#classId").val();
		sortColumnName = "EMPLOYEE_CODE";
		sortDirection = (sortDirection == "DESC" ? "ASC" : "DESC");
		sortTraineeTbl(classId, sortColumnName, sortDirection);
	});		
}

function attachListenerToSortJobRole(){
	$("#sortByJobRole").click(function(){
		var classId = $("#classId").val();
		sortColumnName = "JOB_ROLE";
		sortDirection = (sortDirection == "DESC" ? "ASC" : "DESC");
		sortTraineeTbl(classId, sortColumnName, sortDirection);
	});		
}

function sortApprovedTraineeTbl(classId, sortColumnName, sortDirection){
	$("#AppprovedTraineePagination").pagination({     
        dataSource: "classinfo/trainees/sort",
        locator: "classEmployees",
        totalNumberLocator: function(response) {            
            return response.totalRecords;
        },
        ajax: {data:{
    		classId:classId,
			approvalStatus: ApprovalStatus.APPROVED,
    		sortColumnName: sortColumnName,
    		sortDirection: sortDirection,},
    		type:"POST"
		},	
        className: "paginationjs-theme-blue",
        pageSize: 5,     
        callback: function(data) { 
            $("#ApprovedTraineeTblBody").html("");
            approvedTraineeList = data;
            _.each(data, function(classEmployee) {
            	createApprovedTraineeRow(classEmployee);
            });
        }
    });	
}

function sortPendingTraineeTbl(classId, sortColumnName, sortDirection){
	$("#pendingTraineePagination").pagination({     
        dataSource: "classinfo/trainees/sort",
        locator: "classEmployees",
        totalNumberLocator: function(response) {            
            return response.totalRecords;
        },
        ajax: {data:{
    		classId:classId,
			approvalStatus: ApprovalStatus.ALL_APPROVAL_STATUS,
    		sortColumnName: sortColumnName,
    		sortDirection: sortDirection,},
    		type:"POST"
		},	
        className: "paginationjs-theme-blue",
        pageSize: 5,     
        callback: function(data) { 
            $("#pendingTraineeTblBody").html("");
            pendingTraineeList = data;
            _.each(data, function(classEmployee) {
            	createPendingTraineeRow(classEmployee);
            });
        }
    });	
}

function sortTraineeTbl(classId, sortColumnName, sortDirection) {
	$("#traineePagination").pagination({     
        dataSource: "employeeinfo/trainees/sort",
        locator: "employeeInfos",
        totalNumberLocator: function(response) {            
            return response.totalRecords;
        },
        ajax: {data:{
    		classId:classId,
    		sortColumnName: sortColumnName,
    		sortDirection: sortDirection,},
    		type:"POST"
		},
        className: "paginationjs-theme-blue",
        pageSize: 5,     
        callback: function(data) { 
            $("#traineeTblBody").html("");
            employeeInfoList = data;
            createTraineeHeader("jobrole");
            _.each(data, function(employeeInfo) {
            	createTraineeRow(employeeInfo,"jobrole");
            });
        }
    });
}

function attachListenerToPrintApprovedTraineeBtn(){
	$("#printApprovedTraineeBtn").click(function(){
		var classId = $("#classId").val();
		$("#printApprovedTraineeBtn").attr("href", "classinfo/"+classId+"/classemployee/trainee/approvalstatus/"+ApprovalStatus.APPROVED+"/report");
	})
}

function attachListenerToPrintGradingSheetBtn(){
	$("#printGradingSheetBtn").click(function(){
		var classId = $("#classId").val();
		var courseId = $("#courseId").val();
		$("#printGradingSheetBtn").attr("href", "classinfo/"+classId+"/courseinfo/"+courseId+"/classemployee/traineesgradingsheet/report");
	});
}

function removeTabIndex() {
	$("#addTraineesModal").removeAttr("tabindex");
}

function initUserGroupDropdown() {
	$.when(ajax.fetch("usergroups")).done(function(response) {
		var usergroups = response.usergroups;
		populateUserGroupDropdown(usergroups);
	});
}

function populateUserGroupDropdown(userGroups) {
	$("#userGroupDropdown").html("");
	$("#userGroupDropdown").append($("<option/>").prop({
		"disabled":true,"selected":true,"value":"0"}).html("Choose an option"));
	_.each(userGroups,function(userGroup) {
		$("#userGroupDropdown").append($("<option/>").attr({"value":userGroup.userGroupId})
			.html(userGroup.userGroupName));
	});
}

function initJobRoleDropdown() {
	$.when(ajax.fetch("jobroles")).done(function(response) {
		var jobRoles = response.jobRoles;
		populateJobRoleDropdown(jobRoles);
	});
}

function populateJobRoleDropdown(jobRoles) {
	$("#jobRoleDropdown").html("");
	$("#jobRoleDropdown").append($("<option/>").prop({
		"disabled":true,"selected":true,"value":"0"}).html("Choose an option"));
	_.each(jobRoles,function(jobRole) {
		$("#jobRoleDropdown").append($("<option/>").attr({"value":jobRole.jobRoleId}).html(jobRole.jobName));
	})
}

function initTraineeTbl() {
	var classId = $("#classId").val();
	$("#traineePagination").pagination({     
        dataSource: "employeeinfo/trainees/pages",
        locator: "employeeInfos",
        totalNumberLocator: function(response) {            
            return response.totalRecords;
        },
        ajax: {data:{classId:classId},type:"POST"},
        className: "paginationjs-theme-blue",
        pageSize: 5,     
        callback: function(data) { 
            $("#traineeTblBody").html("");
            employeeInfoList = data;
            createTraineeHeader("jobrole");
            _.each(data, function(employeeInfo) {
            	createTraineeRow(employeeInfo,"jobrole");
            });
        }
    });
}

function createTraineeHeader(filterType) {
	$("#traineeTblHeader").html("");
	$("#traineeTblHeader").append($("<tr/>")
		.append($("<th/>")
			.append($("<label/>").css("margin-left","15px !important")
				.append($("<input/>").addClass("checkAll").attr({"type":"checkbox"})
					.prop("checked",(countNoOfCheckedObj(employeeInfoList)==employeeInfoList.length)?true:false))
				.append($("<span/>"))))
		.append($("<th/>").html("Last Name")
			.append($("<a/>").attr({"href":"#!", "id":"sortByLastName"}).addClass("sort-by")))
		.append($("<th/>").html("First Name")
			.append($("<a/>").attr({"href":"#!", "id":"sortByFirstName"}).addClass("sort-by")))
		.append($("<th/>").html("Employee Code")
			.append($("<a/>").attr({"href":"#!", "id":"sortByEmployeeCode"}).addClass("sort-by")))
		.append($("<th/>").html((filterType=="jobrole")?"Job Role":"User Group")
			.append($("<a/>").attr({"href":"#!", "id":"sortByJobRole"}).addClass("sort-by"))));
			
			
	attachListenerToSortLastName();
	attachListenerToSortFirstName();
	attachListenerToSortEmployeeCode();
	attachListenerToSortJobRole();
}

function countNoOfCheckedObj(employeeInfos) {
	var count = 0;
	_.each(employeeInfos,function(employeeInfo) {
		_.each(employeeInfoCheckedList,function(checkedEmployeeInfo) {
			if(checkedEmployeeInfo.employeeId==employeeInfo.employeeId) {
				count++;
			}
		})
	});
	return count;
}

function createTraineeRow(employeeInfo,filterType) {
	$("#traineeTblBody").append($("<tr/>")
		.append($("<td/>")
			.append($("<label/>").css("margin-left","15px !important")
				.append($("<input/>").addClass("employeeChkBox").attr({
					"type":"checkbox",
					"data-id":employeeInfo.employeeId})
				.prop("checked",checkIfObjExist(employeeInfo.employeeId)).bind("change",function() {
					var currentId = $(this).data("id");
					if($(this).prop("checked")) {
						employeeInfoCheckedList.push(getEmployeeInfoObj(currentId));
					} else {
						employeeInfoCheckedList = removeEmployeeInfoObj(currentId);
						$(".checkAll").prop("checked",false);
					}
				})).append($("<span/>"))))
		.append($("<td/>").html(employeeInfo.lastName))
		.append($("<td/>").html(employeeInfo.firstName))
		.append($("<td/>").html(employeeInfo.employeeCode))
		.append($("<td/>").html((filterType=="jobrole")?employeeInfo.jobName:employeeInfo.userGroupName)));
}

function getEmployeeInfoObj(employeeInfoId) {
	var employeeInfoObj = {};
	_.each(employeeInfoList,function(employeeInfo) {
		if(employeeInfo.employeeId == employeeInfoId) {
			employeeInfo.classId = $("#classId").val();
			employeeInfo.role=Role.TRAINEE;
			employeeInfoObj = employeeInfo;
		}
	});
	return employeeInfoObj;
}

function removeEmployeeInfoObj(employeeId) {
	var tempEmployeeInfoArray = [];
	_.each(employeeInfoCheckedList,function(employeeInfo) {
		if(employeeInfo.employeeId!=employeeId) {
			tempEmployeeInfoArray.push(employeeInfo);
		}
	});
	return tempEmployeeInfoArray;
}

function checkIfObjExist(employeeId) {
	var result = false;
	_.each(employeeInfoCheckedList,function(employeeInfo) {
		if(employeeInfo.employeeId==employeeId) {
			result = true;
		}
	});
	return result;
}

function refreshTraineeStatus(classId, classEmployeeId){
	return new Promise((resolve, reject) => {
		$.when(ajax.fetch("classinfo/"+classId+"/classemployees/"+classEmployeeId)).done(function(response){
			resolve(response.classEmployee.trainingStatus);
		});
	})
}

function initApprovedTraineeTbl() {
	var classId = $("#classId").val();
	$("#AppprovedTraineePagination").pagination({     
        dataSource: "classinfo/"+classId+"/classemployee/trainee/approvalstatus/"+ApprovalStatus.APPROVED,
        locator: "classEmployees",
        totalNumberLocator: function(response) {            
            return response.totalRecords;
        },
        className: "paginationjs-theme-blue",
        pageSize: 5,     
        callback: function(data) { 
            $("#ApprovedTraineeTblBody").html("");
            approvedTraineeList = data;
            _.each(data, function(classEmployee) {
            	createApprovedTraineeRow(classEmployee);
            });
        }
    });
}

function createApprovedTraineeRow(classEmployee) {
	var deliveryMethod=$("#deliveryMethod").val();
	$("#ApprovedTraineeTblBody").append($("<tr/>")
		.append($("<td/>")
			.append($("<label/>").css("margin-left","10px !important").css("display", $("#userType").val() == 1 || $("#loggedInId").val() == $("#trainerId").val()  ? "" : "none")
				.append((deliveryMethod==DeliveryMethod.CLASSTRAINING)?
					$("<input/>").addClass("completeChkBox").attr({"type":"checkbox","data-id":classEmployee.classEmployeeId})
					.prop("checked",checkIfExistInCompletedTraineeList(classEmployee.classEmployeeId)):"")
				.append($("<span/>")))
			.append($("<a/>").attr({"href":"#!","data-id":classEmployee.classEmployeeId}).css("display", $("#userType").val() == 1 || $("#loggedInId").val() == $("#trainerId").val()  ? "" : "none")
				.append($("<i/>").addClass("material-icons")
				.bind("click",function() {
					var message = "Are you sure you want to remove this trainee in this class?"
					alertify.confirm("Delete Trainee", message, function(){ 
						removeClassEmployee(classEmployee.classEmployeeId);
					}, function(){}).set("labels",{ok:"Yes", cancel:"No"});
				}).html("delete"))))
		.append($("<td/>").html(classEmployee.fullName))
		.append($("<td/>").html(classEmployee.jobName))
		.append($("<td/>").html((classEmployee.userGroupName==null)?"NONE":classEmployee.userGroupName))
		.append($("<td/>").html(classEmployee.enrolledDate))
		.append($("<td/>").html((classEmployee.completedDate=="")?"N/A":classEmployee.completedDate))
		.append($("<td/>")
			.append($("<select/>").addClass("trainingStatus")
				.attr({"id":"trainingStatus_"+classEmployee.classEmployeeId,"data-id":classEmployee.classEmployeeId, "disabled": ($("#userType").val() == 1 || $("#loggedInId").val() == $("#trainerId").val()  ? false : "disabled")})
				.bind("change",function() {
					classEmployee.trainingStatus = $("#trainingStatus_"+classEmployee.classEmployeeId).val();
					classEmployee.comment = $("#comment_"+classEmployee.classEmployeeId).val();
					$("#gradeStatus_"+classEmployee.classEmployeeId).val(checkIfPassOrFailed(classEmployee.classEmployeeId, classEmployee.trainingStatus));
					
					$("input[id^='componentEmp"+classEmployee.classEmployeeId+"Comp']").each(function(index, component){
						var comp =  $(this).attr('id');
						var gComponentId = comp.slice(comp.indexOf("Comp") + "Comp".length);
						var traineeGrade1 = getTraineeGrade(classEmployee.classEmployeeId, gComponentId);
						var theValue1 = ((classEmployee.trainingStatus != TrainingStatus.NO_SHOW && 
												classEmployee.trainingStatus != TrainingStatus.CANCELLED) && traineeGrade1 > 0 ? traineeGrade1 : 0); 
						$(this).val(theValue1);
					})
					
					$("#finalGrade_"+classEmployee.classEmployeeId).val(
															calculateFinalGrade(classEmployee.classEmployeeId, classEmployee.trainingStatus));
															
					updateClassEmployee(classEmployee);
					
				})
				.append($("<option/>").attr("value","0").prop({"selected":true,"disabled":true})
					.html("Choose an option"))))
		.append($("<td/>")
			.append($("<input/>").addClass("commentTxtBox").attr({
				"type":"text",
				"value":classEmployee.comment,
				"data-id":classEmployee.classEmployeeId,
				"id":"comment_"+classEmployee.classEmployeeId,
				"disabled": ($("#userType").val() == 1 || $("#loggedInId").val() == $("#trainerId").val()  ? false : "disabled")})
			.bind("keyup",function() {
				classEmployee.trainingStatus = $("#trainingStatus_"+classEmployee.classEmployeeId).val();
				classEmployee.comment = $(this).val();
				updateClassEmployee(classEmployee);
			}))));
	refreshTraineeStatus(classEmployee.classId, classEmployee.classEmployeeId)
	.then((traineeStatus) => { 
		$("#trainingStatus_"+classEmployee.classEmployeeId).val(traineeStatus); 
	});		
	appendTrainingStatusOptions(classEmployee.classEmployeeId,classEmployee.trainingStatus);
}

function checkIfExistInCompletedTraineeList(classEmployeeId) {
	var result = false;
	_.each(completedTraineeList,function(classEmployee) {
		if(classEmployee.classEmployeeId == classEmployeeId) {
			result = true;
		}
	});
	return result;
}

function updateClassEmployee(classEmployee) {
	var classId = $("#classId").val();
	$.when(ajax.customUpdate("classinfo/"+classId+"/classemployee/"+classEmployee.classEmployeeId,classEmployee))
		.done(function(response) {
			switch(response.status) {
				case HttpStatus.SUCCESS:
					break;
			}
	});
}

function removeClassEmployee(classEmployeeId) {
	var classId = $("#classId").val();
	$.when(ajax.remove("classinfo/"+classId+"/classemployee/",classEmployeeId))
		.done(function(response) {
			switch(response.status) {
				case HttpStatus.SUCCESS:
					initApprovedTraineeTbl();
					initTraineeTbl();
					break;
			}
	});
}

function appendTrainingStatusOptions(classEmployeeId,trainingStatus) {
	_.each(TrainingStatus.LIST,function(trainingStatusObj) {
		$("#trainingStatus_"+classEmployeeId)
			.append(new Option(trainingStatusObj.description, trainingStatusObj.id, false, (trainingStatus==trainingStatusObj.id)?true:false ))
	});
	$("#trainingStatus_"+classEmployeeId).select2();
}

function initPendingTraineeTbl() {
	var classId = $("#classId").val();
	$("#pendingTraineePagination").pagination({     
        dataSource: "classinfo/"+classId+"/classemployee/trainee/approvalstatus/"+ApprovalStatus.ALL_APPROVAL_STATUS,
        locator: "classEmployees",
        totalNumberLocator: function(response) {            
            return response.totalRecords;
        },
        className: "paginationjs-theme-blue",
        pageSize: 5,     
        callback: function(data) { 
            $("#pendingTraineeTblBody").html("");
            pendingTraineeList = data;
            _.each(data, function(classEmployee) {
            	createPendingTraineeRow(classEmployee);
            });
        }
    });
}

function createPendingTraineeRow(classEmployee) {
	$("#pendingTraineeTblBody").append($("<tr/>")
		.append($("<td/>")
			.append($("<a/>").attr("href","#!").css("display", $("#userType").val() == 1 || $("#loggedInId").val() == $("#trainerId").val()  ? "" : "none")
				.bind("click",function() {
					updateApprovalStatus(classEmployee.classEmployeeId,ApprovalStatus.APPROVED);
				}).append($("<i/>").addClass("material-icons")
					.html("thumb_up")))
		.append($("<a/>").attr("href","#!").css("display", $("#userType").val() == 1 || $("#loggedInId").val() == $("#trainerId").val()  ? "" : "none")
				.bind("click",function() {
					updateApprovalStatus(classEmployee.classEmployeeId,ApprovalStatus.DISAPPROVED);
				}).append($("<i/>").addClass("material-icons")
					.html("thumb_down"))))
		.append($("<td/>").html(classEmployee.fullName))
		.append($("<td/>").html(classEmployee.jobName))
		.append($("<td/>").html((classEmployee.userGroupName==null)?"NONE":classEmployee.userGroupName))
		.append($("<td/>").html(classEmployee.requestDate))
		.append($("<td/>").html(classEmployee.approvedDate))
		.append($("<td/>").html(getApprovalStatusDesc(classEmployee.approvalStatus))));
}

function updateApprovalStatus(classEmployeeId,approvalStatus) {
	var classId = $("#classId").val();
	$.when(ajax.customUpdate("classinfo/"+classId+"/classemployee/"+classEmployeeId+"/approvalstatus/"+approvalStatus))
		.done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				initApprovedTraineeTbl();
				initPendingTraineeTbl();
				break;
		}
	});
}

function getApprovalStatusDesc(approvalStatus) {
	var result = "";
	switch(approvalStatus) {
		case ApprovalStatus.APPROVED:
			result = "APPROVED";
			break;
		case ApprovalStatus.DISAPPROVED:
			result = "DISAPPROVED";
			break;
		case ApprovalStatus.ONGOING:
			result = "PENDING";
			break;
	}
	return result;
}

function initGradeSheetHeader() {
	var courseId = $("#courseId").val();
	$.when(ajax.fetch("courseinfo/"+courseId+"/gradingcomponents")).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:  
				gradingComponentsList=response.gradingComponents;
				populateGradingSheetHeader(response.gradingComponents);
				break;
		}
	});
}

function populateGradingSheetHeader(gradingComponents) {
	gradingComponents.push(setGradingComponentObj("Job"));
	gradingComponents.push(setGradingComponentObj("Name"));
	_.each(gradingComponents.reverse(),function(gradingComponent) {
		$("#gradingSheetTblHeader").append($("<th/>").html(gradingComponent.componentDesc).append($("<a>").addClass("sort-by").attr("href","#!")));
	});
	$("#gradingSheetTblHeader").append($("<th/>").html("Final Grade").append($("<a>").addClass("sort-by").attr("href","#!")))
		.append($("<th/>").html("Status").append($("<a>").addClass("sort-by").attr("href","#!")));
	initGradingSheetBody();
}

function setGradingComponentObj(componentDesc) {
	var gradingComponent = {};
	gradingComponent.gradingComponentId = 0;
	gradingComponent.componentDesc = componentDesc;
	return gradingComponent;
}

function initGradingSheetBody() {
	var classId = $("#classId").val();
	$("#gradingSheetPagination").pagination({     
        dataSource: "classinfo/"+classId+"/classemployee/trainee/approvalstatus/"+ApprovalStatus.APPROVED,
        locator: "classEmployees",
        totalNumberLocator: function(response) {            
            return response.totalRecords;
        },
        className: "paginationjs-theme-blue",
        pageSize: 5,    
        callback: function(data) { 
            $("#gradingSheetTblBody").html("");
            _.each(data, function(classEmployee) {
            	createGradingSheetRow(classEmployee);
            });
        }
    });
}

function createGradingSheetRow(classEmployee) {
	var passingGrade = 0;
	var trainingStatus = classEmployee.trainingStatus;
	$("#gradingSheetTblBody").append($("<tr/>").attr("id","employeeNo_"+classEmployee.classEmployeeId)
		.append($("<td/>").html(classEmployee.fullName))
		.append($("<td/>").html(classEmployee.jobName)));
	
		$.when(ajax.fetch("classinfo/"+classEmployee.classId+"/trainee/"+classEmployee.classEmployeeId+"/gradingsheet")).done(function(response){
			switch(response.status){
				case HttpStatus.SUCCESS:
					employeeExamSummariesList=response.employeeExamSummaries;
					
				_.each(gradingComponentsList,function(gradingComponent) {
					if(gradingComponent.gradingComponentId != 0) {
						passingGrade = gradingComponent.passingGrade;				
					
						_.each(employeeExamSummariesList, function(employeeExamSummary){
							if(gradingComponent.gradingComponentId == employeeExamSummary.gradingComponentId &&
									classEmployee.employeeId == employeeExamSummary.employeeId){
								
								var traineeGrade = getTraineeGrade(classEmployee.classEmployeeId,gradingComponent.gradingComponentId);
								var theValue = ((trainingStatus != TrainingStatus.NO_SHOW && 
												trainingStatus != TrainingStatus.CANCELLED) && traineeGrade > 0 ? traineeGrade : 0); 
								
								var obj = {};
								obj.classId = classEmployee.classId;
								obj.score = employeeExamSummary.score;
								obj.totalItems = employeeExamSummary.totalItems;
								obj.percentage = gradingComponent.percentage;
								obj.componentDesc = gradingComponent.componentDesc;
								obj.examType = employeeExamSummary.examType;

								//Automatic updating of grades for exams
								if(employeeExamSummary.examType > 0){
									 computeGrade(obj)
									 	.then((theGrade) => { 
									 		calculatedGrade = theGrade;
									 		updateClassEmployeeAssessment(classEmployee.classEmployeeId,gradingComponent.gradingComponentId,calculatedGrade);
											$("#componentEmp"+classEmployee.classEmployeeId+"Comp"+gradingComponent.gradingComponentId).val(calculatedGrade)
									 	})
									
									$("#finalGrade_"+classEmployee.classEmployeeId).val(
											calculateFinalGrade(classEmployee.classEmployeeId, trainingStatus));
									$("#gradeStatus_"+classEmployee.classEmployeeId).val(
										checkIfPassOrFailed(classEmployee.classEmployeeId,trainingStatus));							
								}
								
								$("#employeeNo_"+classEmployee.classEmployeeId).append($("<td/>")
										.append($("<input/>").addClass("numbersOnly").attr({
											"type":"text",
											"id":"componentEmp"+classEmployee.classEmployeeId+"Comp"+gradingComponent.gradingComponentId,
											"data-employeeid":classEmployee.classEmployeeId,
											"data-componentid":gradingComponent.gradingComponentId,
											"data-percentage":gradingComponent.percentage,
											"data-value": theValue,
											"value":  theValue,
											"disabled": ($("#userType").val() == 1 || $("#loggedInId").val() == $("#trainerId").val()  ? false : "disabled")})
										.bind("change",function(e) {
											var $this = $(this);
											var value=$this.val();
											var employeeId = $(this).data("employeeid");
											var componenId = $(this).data("componentid");
											obj.score = value;
											
											if(employeeExamSummary.examType == 0){
												setTimeout(function() {
													if($this.val() != $this.attr("data-value")){
														computeGrade(obj)
															.then((theGrade) => {
																calculatedGrade = theGrade;
																$this.val(calculatedGrade);
																updateClassEmployeeAssessment(employeeId,componenId,calculatedGrade);
															})
														$("#finalGrade_"+classEmployee.classEmployeeId).val(
															calculateFinalGrade(classEmployee.classEmployeeId, trainingStatus));
														$("#gradeStatus_"+classEmployee.classEmployeeId).val(
															checkIfPassOrFailed(classEmployee.classEmployeeId,trainingStatus));
													}
												},1000);
											}
											
											
										})
									));		
								
							} //END if statement
						}) //END employeeExamSummaryList FOR LOOP	
					
					}
				});	
				
				$("#employeeNo_"+classEmployee.classEmployeeId).append($("<td/>")
					.append($("<input/>").attr({
						"type":"text",
						"data-employeeid":classEmployee.classEmployeeId,
						"id":"finalGrade_"+classEmployee.classEmployeeId,
						"value":calculateFinalGrade(classEmployee.classEmployeeId, trainingStatus)})
						.prop("disabled",true)));
				$("#employeeNo_"+classEmployee.classEmployeeId).append($("<td/>")
						.append($("<input/>").attr({
							"type":"text",
							"data-employeeid":classEmployee.classEmployeeId,
							"id":"gradeStatus_"+classEmployee.classEmployeeId,
							"value":checkIfPassOrFailed(classEmployee.classEmployeeId,trainingStatus)})
							.prop("disabled",true)));									
					
					break;
			}
		});			
			
}

function checkIfPassOrFailed(classEmployeeId,trainingStatus) {
	var finalGrade = parseFloat($("#finalGrade_"+classEmployeeId).val());
	var finalGradeStatus;
	if(trainingStatus == TrainingStatus.ENROLLED || trainingStatus == TrainingStatus.CANCELLED || trainingStatus == TrainingStatus.NO_SHOW){
		finalGradeStatus = "N/A";
	}else if(trainingStatus == TrainingStatus.INCOMPLETE || trainingStatus == TrainingStatus.FAILED){
		finalGradeStatus = "Failed";
	}else{
		finalGradeStatus = "Passed";
	}
	return finalGradeStatus;
}

function calculateFinalGrade(employeeId, trainingStatus) {
	var finalGrade=0;
	_.each(gradingComponentsList,function(gradingComponent) {
		if(gradingComponent.gradingComponentId != 0) {
			var employeeGrade = $("#componentEmp"+employeeId+"Comp"+gradingComponent.gradingComponentId).val();
			finalGrade=finalGrade+parseFloat(employeeGrade);
		}
	});
	if(finalGrade > 100){notify("Final Grade must not exceed 100","warning")}	
	
	if(trainingStatus == TrainingStatus.CANCELLED || trainingStatus == TrainingStatus.NO_SHOW){
		finalGrade = 0;
	}
	
	return (finalGrade > 0 ? finalGrade.toFixed(2) : 0);
}

function computeGrade(obj) {
	var classEmployeeAssessment = {};
	classEmployeeAssessment.classId = obj.classId;
	classEmployeeAssessment.score = parseInt(obj.score);
	classEmployeeAssessment.totalItems = obj.totalItems;
	classEmployeeAssessment.gradingComponent = {componentDesc: obj.componentDesc, percentage: obj.percentage};
	classEmployeeAssessment.examType = obj.examType;
	
	console.log(classEmployeeAssessment);
	
	return new Promise((resolve, reject) => {
		$.when(ajax.create("classinfo/"+obj.classId+"/classemployeeassessments/grading-computation", classEmployeeAssessment)).done(function(response){
			var calculatedGrade = response.computedGrade;
			var fixedGrade = (!Number.isNaN(calculatedGrade) ? (calculatedGrade > 0 ? calculatedGrade.toFixed(2) : 0) : 0);
			resolve(fixedGrade);
		})	
	})
} 
	
function updateClassEmployeeAssessment(employeeId,gradingComponentId,score) {
	var classId = $("#classId").val();
	var classEmployeeAssessment = {};
	classEmployeeAssessment.classId = classId;
	classEmployeeAssessment.employeeId = employeeId;
	classEmployeeAssessment.gradingComponentId = gradingComponentId;
	classEmployeeAssessment.score = score;
	
	$.when(ajax.create("classinfo/"+classId+"/classemployeeassessment",classEmployeeAssessment)).done(
		function(response) {
			switch(response.status) {
				case HttpStatus.SUCCESS:
					break;
			}
	});
}

function initClassEmployeeAssessmentsList() {
	var classId = $("#classId").val();
	$.when(ajax.fetch("classinfo/"+classId+"/classemployeeassessments")).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				classEmployeeAssessmentList = response.classEmployeeAssessments;
				break;
		}
	})
}

function getTraineeGrade(employeeId,gradingComponentId) {
	var grade = null;
	_.each(classEmployeeAssessmentList,function(classEmployeeAssessment) {
		if(classEmployeeAssessment.employeeId==employeeId && 
			classEmployeeAssessment.gradingComponentId==gradingComponentId) {
			grade = classEmployeeAssessment.score;
		}
	});
	return (grade > 0 ? grade.toFixed(2) : 0);
}

function attachListenerToFilterGroup() {
	$("input[name='filterGroup']").on("change",function() {
		if($("#jobRoleRdBtn").prop("checked")) {
			$("#jobRoleDropdown").prop("disabled",false);
			$("#userGroupDropdown").prop("disabled",true);
			$("#userGroupDropdown").val("0").trigger("change");
		} else {
			$("#userGroupDropdown").prop("disabled",false);
			$("#jobRoleDropdown").prop("disabled",true);
			$("#jobRoleDropdown").val("0").trigger("change");
		}
	});
}

function attachListenerToJobRoleDropdown() {
	$("#jobRoleDropdown").on("change",function() {
		if($("#jobRoleDropdown").val()==null && $("#userGroupDropdown").val()==null) {
			initTraineeTbl();
		} else {
			searchEmployeeInfo();
		}
	});
}

function attachListenerToUserGroupDropdown() {
	$("#userGroupDropdown").on("change",function() {
		if($("#jobRoleDropdown").val()==null && $("#userGroupDropdown").val()==null) {
			initTraineeTbl();
		} else {
			searchEmployeeInfo();
		}
	});
}

function attachListenerToSearchTxtBox() {
	$("#searchTrainee").on("keyup",function() {
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == 13) {
			searchEmployeeInfo();
		}
		
		if(keycode == 8 && $("#searchTrainee").val().length == 0) {
			initTraineeTbl();
		}
	});
	
	$("#clearField").on("click",function() {
		initTraineeTbl();
	});
} 

function searchEmployeeInfo() {
	var classId = $("#classId").val();
	$("#traineePagination").pagination({     
        dataSource: "employeeinfo/trainees/search",
        locator: "employeeInfos",
        totalNumberLocator: function(response) {            
            return response.totalRecords;
        },
        ajax: {data:{
        	classId:classId,
        	keyword:$("#searchTrainee").val(),
        	jobRoleId:($("#jobRoleDropdown").val()==null)?0:$("#jobRoleDropdown").val(),
        	userGroupId:($("#userGroupDropdown").val()==null)?0:$("#userGroupDropdown").val()},
        	type:"POST"},
        className: "paginationjs-theme-blue",
        pageSize: 10,     
        callback: function(data) {            
            var filterType=($("#jobRoleRdBtn").prop("checked")?"jobrole":"usergroup");
            $("#traineeTblBody").html("");
            createTraineeHeader(filterType);
            employeeInfoList = data;
            _.each(data, function(employeeInfo) {
            	createTraineeRow(employeeInfo,filterType);
            });
        }
    });
}

function attachListenerToCheckAll() {
	$("body").on("change",".checkAll",function() {
		if($(this).prop("checked")) {
			$("#traineeTblBody").find("input[type=checkbox]").prop("checked",true).trigger("change");
		} else {
			$("#traineeTblBody").find("input[type=checkbox]").prop("checked",false).trigger("change");
		}
	})
}

function attachListenerToAddTraineeBtn() {
	$("#addTraineeBtn").on("click",function() {
		if(employeeInfoCheckedList.length != 0) {
			$("#addTraineeBtn").attr("disabled","disabled");
			$("#addTraineeBtn").html("Saving...");
			createClassEmployees();
		} else {
			alertify.warning("Please Select atleast 1 employee.")
		}
	});
}

function createClassEmployees() {
	var classId = $("#classId").val();
	$.when(ajax.create("classinfo/"+classId+"/classemployee/trainee",employeeInfoCheckedList))
		.done(function(response) {
			switch(response.status) {
				case HttpStatus.SUCCESS:
					alertify.success("Successfully Added!");
					employeeInfoCheckedList = [];
					$("#addTraineesModal").modal("close");
					initApprovedTraineeTbl();
					initGradeSheetHeader();
					$("#searchTrainee").val("");
					initUserGroupDropdown();
					initJobRoleDropdown();
					initTraineeTbl();
					
					/*$("#addTraineeBtn").empty();
					$("#addTraineeBtn").attr("disabled",false);
					$("#addTraineeBtn").append($("<i>").addClass("material-icons left").html("save")).append("Save");*/
					
					setTimeout(function(){redirect("admin.classtrainee?classId="+classId);}, 3000);
					break;
			}
		});
}

function attachListenerToCancelTraineeBtn() {
	$("#cancelTraineeBtn").on("click",function() {
		employeeInfoCheckedList = [];
		$("#searchTrainee").val("");
		initUserGroupDropdown();
		initJobRoleDropdown();
		initTraineeTbl();
	});
}

function attachListenerToCompleteChkBox() {
	$("body").on("change",".completeChkBox",function() {
		if($(this).prop("checked")) {
			var classEmployee = {};
			classEmployee.classEmployeeId=$(this).data("id");
			classEmployee.trainingStatus = TrainingStatus.COMPLETED;
			completedTraineeList.push(classEmployee);
		} else {
			completedTraineeList = removeFromCompleteTraineeList($(this).data("id"));
		}
	});
}

function removeFromCompleteTraineeList(classEmployeeId) {
	var tempTraineeList = [];
	_.each(completedTraineeList,function(classEmployee) {
		if(classEmployee.classEmployeeId != classEmployeeId) {
			tempTraineeList.push(classEmployee);
		}
	});
	return tempTraineeList;
}

function attachListenerToSetAsCompletedBtn() {
	$("#setAsCompletedBtn").on("click",function() {
		updateTrainingStatus(completedTraineeList);
	});
}

function updateTrainingStatus(classEmployees) {
	var classId = $("#classId").val();
	$.when(ajax.customUpdate("classinfo/"+classId+"/trainingstatus",classEmployees)).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				alertify.success("Successfully set as completed!");
				initApprovedTraineeTbl();
				completedTraineeList = [];
				break;
		}
	})
}

function attachListenerToUploadPhotoLink() {
	$("#uploadPhotoLink").on("click",function() {
		$("#fileUpload").trigger("click");
	});
}

function attachListenerToUploadPhoto() {
	$("#fileUpload").on("change",function(e) {
		if(validateFile()) {
			handleRefreshEvent();
			updateClassPhoto();
		}
	});
}

function validateFile() {
	var result = true;
	if($("#fileUpload").get(0).files.length==0) {
		result = false;
	}
	if(!validateFileType("#fileUpload","IMAGE")) {
		alertify.warning("Invalid File Type. Please Try another one.");
		result = false;
	} 
	if(!validateFileSize("#fileUpload","IMAGE")) {
		result = false;
	}
	return result;
}

function updateClassPhoto() {
	blockUI("Uploading Media File..");
	var classId = $("#classId").val();
	$.when(ajax.upload("classinfo/"+classId+"/classPhotoUrl","uploadPhotoFrm")).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				alertify.success("Successfully Updated!");
				$("#classPhoto").attr("src",response.classInfo.classPhotoUrl);
				$(window).unbind('beforeunload');
				$.unblockUI();
				break;
		}
	});
}

function attachListenerToGenerateCertificatesBtn(){
	$("#generateCertificatesBtn").click(function(){
		var classId = $("#classId").val();
		
		if($("#countTrainees").val() > 0){
			location.href = "zip/"+classId;
			//$.when(ajax.fetch("zip/"+classId)).done(function(){});
			alertify.success("Success!");
		}else{
			alertify.error("No trainees are found to generate certificate");
		}
		
		
	})
}