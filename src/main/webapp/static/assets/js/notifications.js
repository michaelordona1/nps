$(function(){
	//initStomp();
	countUnreadNotifications();
	fetchNotifications();
	attachListenerToLogoutBtn();
})

var client;
var totalUnreadNofications = 0;

/*
function initStomp(){
	var origin = window.location.origin;
	var root = window.location.pathname.split("/")[1];
	var url = origin + ((origin.indexOf("localhost") !== -1) ? ("/" + root + "/notifications") :  "/notifications");
	
	//var socket = new SockJS(url);
	var socket = new SockJS(url);
	client = Stomp.over(socket);
	client.debug = null;
	
	client.connect({}, function (frame) {
		client.send("/app/start", {}, JSON.stringify({
			jobroleId: JOB_ROLE_ID,
			userGroupId: USER_GROUP_ID,
			userId: USER_ID
		}));
		
		// "/user/" prefix tells Spring that the client only wants to listen to his messages.
	    client.subscribe('/user/notification/training-flow', function (response) {
			$("#noNotifLi").remove();
			var object = JSON.parse(response.body);			
			totalUnreadNofications++;
			countUnreadNotifications();
			createNotificationRow(object.notification);
	    });
	});
}
*/

function fetchNotifications(){
	$.when(ajax.fetch("users/"+USER_ID+"/notifications")).done(function(response){
		var notifications = response.notifications;
		_.each(notifications, function(notification){
			createFetchedNotificationRow(notification);
		})
	})
}

function attachListenerToLogoutBtn(){
	$("#logoutBtn").click(function(){
		//client.send("/app/stop", {});
		//client.disconnect();
		location.href="logout";
	})
}

function createFetchedNotificationRow(notification){
	$("#notificationUl")
	.append($("<li>").addClass("collection-item avatar")
		.append($("<img>").addClass("circle").attr("src","static/assets/images/usersimage/user.png"))
		//.append($("<span>").addClass("title").append($("<b>").html("TITLE")))
		.append($("<p>").css("color", (notification.readAt != null) ? "grey" : "black")
				.append($("<small>").css({"color": notification.color, 
											"background-color": notification.color, 
											"margin-right": "5px", 
											"border-radius": "100%" 
											}).html("::::"))
				.append(notification.message)
		)
		.append($("<p>").addClass("right-align").css("color", (notification.readAt != null) ? "grey" : "black").html(" - " + moment(notification.createdAt).fromNow()))
		.one("mouseover", function(){
			var obj = {};
			obj.notificationId = notification.notificationId;
			obj.readAt = notification.readAt;
			markAsRead(obj, this);
		})
	)	
}

/*
function createNotificationRow(notification){
	$("#notificationUl")
	.prepend($("<li>").addClass("collection-item avatar")
		.append($("<img>").addClass("circle").attr("src","static/assets/images/usersimage/user.png"))
		//.append($("<span>").addClass("title").append($("<b>").html("TITLE")))
		.append($("<p>").html(notification.message))
		.append($("<p>").addClass("right-align").html(" - " + moment(notification.createdAt).fromNow()))
		.one("mouseover", function(){
			var obj = {};
			obj.notificationId = notification.notificationId;
			obj.readAt = notification.readAt;
			markAsRead(obj, this);
		})
	)
	//.append($("<li>").append($("<span>").addClass("center-align").css("padding", "15px !important").html("Show more>>>")))
}
*/

function countUnreadNotifications(){
	$.when(ajax.fetch("users/"+USER_ID+"/total-unread-notifications")).then(function(response){
		$("#notificationBadge").html(totalUnreadNofications + response.totalUnreadCount);
	})
}

function markAsRead(obj, element){
	$(element).find("p").css("color","grey");
	
	if(obj.notificationId != 0 && (obj.readAt == null || obj.readAt.trim() == "")){
		$.when(ajax.customUpdate("notification/"+obj.notificationId, obj)).then(function(){
			countUnreadNotifications();
		})
	}else{
		if(totalUnreadNofications >= 1){
			totalUnreadNofications--;
			countUnreadNotifications();
		}
	}	
}
