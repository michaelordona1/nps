$(function () {
	defineVariables();
	initPageSettings();
	initTimer();
	initQuestionList();
	attachListenerToNextBtn();
	attachListenerToPrevBtn();
	attachListenerToQuestionNoLink();
	attachListenerToChoicesRadioBtn();
	attachListenerToFinishBtn();
	attachListenerToIdenticationTxtBox();
	attachListenerToTrueOrFalseRdBtn();
	initLightBox();
});

function defineVariables() {
	questionList = [];
	matchingQuestionList = [];
	mapMatchingShuffledQuestionList = new Map();
	noOfQuestions = 0;
	prevQuestionNo = 0;
	nextQuestionNo = 2;
	employeeExamAnswerList = [];
	choiceList = [];
	correctMultipleChoiceAnswersCtr = 0;
	currentQuestionNo=0;
}

function initTimer() {
	var classId = $("#hdnClassId").val();
	var examId = $("#hdnExamId").val();
	var sectionOrder = $("#hdnSectionOrder").val();
	$.when(ajax.fetchWithData("classinfo/" + classId + "/employeeexamprogress",
		{ examId: examId, sectionOrder: sectionOrder })).done(function (response) {
			switch (response.status) {
				case HttpStatus.SUCCESS:
					var employeeExamProgress = response.employeeExamProgress;
					var timeLeft = moment(employeeExamProgress.timeLeft, "hh:mm:ss");
					startTimer({
						dom: "#timer",
						hours: timeLeft.hours(),
						minutes: timeLeft.minutes(),
						seconds: timeLeft.seconds(),
						functionName: "processTimesUpRedirection"
					})
					break;
			}
		});
}

function processTimesUpRedirection() {
	var classId = $("#hdnClassId").val();
	var examId = $("#hdnExamId").val();
	alertify.alert("Warning", " Your time is up! All your answers will be submitted. Click OK to continue. ", function () {
		createEmployeeExamAnswers();
		redirect("exam-home?classId=" + classId + "&examId=" + examId, 1500);
		$(window).unbind('beforeunload');
	});
}

function initPageSettings() {
	handleRefreshEvent();
}

function initQuestionList() {
	var examId = $("#hdnExamId").val();
	var sectionOrder = $("#hdnSectionOrder").val();
	$.when(ajax.fetch("examinfos/" + examId + "/examquestions/" + sectionOrder)).done(function (response) {
		switch (response.status) {
			case HttpStatus.SUCCESS:
				questionList = response.examQuestions;
				var lastQuestionId = 0;
				let iterator = 0;

				_.each(questionList, function (question) {

					if (question.questionTypeId == QuestionType.ORDERING
						|| question.questionTypeId == QuestionType.MATCHING
						|| question.questionTypeId == QuestionType.ENUMERATION
						|| question.questionTypeId == QuestionType.FILL_IN_THE_BLANK) {

						if (lastQuestionId != question.questionId) {
							lastQuestionId = question.questionId;
							iterator++;
						}

					} else {
						iterator++;
					}
					noOfQuestions = iterator;
				});
				initQuestionNumberCon();
				$("#totalQuestions").html(noOfQuestions);
				initChoicesList();
				break;
		}
	});
}

function initQuestion(questionNo) {
	var question = getQuestionData(questionNo);
	currentQuestionNo = questionNo;
	renderNavigationButtons(questionNo);
	$("#currentQuestionNo").html(questionNo);
	$("#hdnExamQuestionId").val(question.examQuestionId);
	$("#hdnIsRandomized").val(question.isRandomized);
	
	switch (question.questionTypeId) {
		case QuestionType.MULTIPLE_CHOICE:
			initQuestionImage("#multipleChoicePicCon", question.questionId);
			populateMultipleChoiceCon(question);
			break;
		case QuestionType.IDENTIFICATION:
			initQuestionImage("#identificationPicCon", question.questionId);
			populateIdentification(question);
			break;
		case QuestionType.FILL_IN_THE_BLANK:
			initQuestionImage("#fillInTheBlanksPicCon", question.questionId);
			populateFillInTheBlanks(questionNo);
			break;
		case QuestionType.TRUE_OR_FALSE:
			initQuestionImage("#trueFalsePicCon", question.questionId);
			populateTrueOrFalseCon(question);
			break;
		case QuestionType.ESSAY:
			initQuestionImage("#essayPicCon", question.questionId);
			populateEssay(question);
			break;
		case QuestionType.MATCHING: 
			initQuestionImage("#matchingPicCon", question.questionId);
			populateMatching(questionNo);
			break;
		case QuestionType.ORDERING:
			initQuestionImage("#orderingPicCon", question.questionId);
			populateOrdering(questionNo);
			break;
		case QuestionType.ENUMERATION:
			initQuestionImage("#enumerationPicCon", question.questionId);
		
			populateEnumeration(questionNo);
			break;
	}
}

function initQuestionImage(dom, questionId) {
	/*showQuestionImageCon(dom, mediaUrl);
	$(".questionPic").attr("src", mediaUrl)
					.bind("click", function(){
						window.open($(this).attr("src"))
				    });*/
	
	$(dom).empty();
	$.when(ajax.fetch("question/"+questionId+"/images")).done(function(response){
		showQuestionImageCon(dom, response.questionImages.length);
		_.each(response.questionImages, function(image){
			$(dom).append(
				$("<img>").addClass("imgPos")
							.attr({"src": image.mediaUrl, "id": "image-"+image.questionImageId})
							.bind("click", function(){
								window.open("portal.image?id="+image.questionImageId);
						    })
			)
		})
	})
	
}

function showQuestionImageCon(dom, questionImagesSize) {
	/*$("#multipleChoicePicCon").hide();
	$("#identificationPicCon").hide();
	$("#enumerationPicCon").hide();
	$("#orderingPicCon").hide();
	$("#fillInTheBlanksPicCon").hide();
	$("#trueFalsePicCon").hide();
	$("#enumerationCon").hide();
	$("#matchingPicCon").hide();
	$("#essayPicCon").hide();*/
	
	if (questionImagesSize > 0) {
		$(dom).show();
	}else{
		$(dom).hide();
	}
}

function showQuestionDiv(dom) {
	$("#multipleChoiceCon").hide();
	$("#identificationCon").hide();
	$("#trueOrFalseCon").hide();
	$("#orderingCon").hide();
	$("#fillInTheBlanksCon").hide();
	$("#enumerationCon").hide();
	$("#essayCon").hide();
	$("#matchingCon").hide();

	$(dom).show();
}

function renderNavigationButtons(currentQuestionNo) {
	if (currentQuestionNo == 1) {
		$("#prevBtn").hide();
	} else {
		$("#prevBtn").show();
	}
	if (currentQuestionNo >= noOfQuestions) {
		$("#nextBtn").hide();
	} else {
		$("#nextBtn").show();
	}
}

function getQuestionData(questionNo) {
	var questionObj = {};
	var iterator = 0;
	var lastQuestionId = 0;
	_.each(questionList, function (question) {
		if (lastQuestionId != question.questionId) {
			lastQuestionId = question.questionId;
			iterator++;
		}
		if (iterator == questionNo) {
			questionObj = question;
		}
	});
	return questionObj;
}

function initQuestionNumberCon() {
	$("#questionNumbersContainer").append(createQuestionNumberCon());
}

function createQuestionNumberCon() {
	var min = 1;
	var max = 8;
	var ctr = 1;
	var html = [];
	html.push("<tr>");
	while (noOfQuestions >= ctr) {
		if (min <= ctr && ctr <= max) {
			html.push("<td><a href='#!' class='questionNoLink' id='questionNo-"+ctr+"' data-ctr='" + ctr + "'>" + ctr + "</a></td>");
			ctr++;
		} else {
			html.push("</tr>");
			html.push("<tr>");
			min += 1;
			max += 8;
		}
	}
	return html.join(" ");
}

function attachListenerToNextBtn() {
	$("#nextBtn").on("click", function () {
		initQuestion(nextQuestionNo);
		nextQuestionNo++;
		prevQuestionNo++;
	});
}

function attachListenerToPrevBtn() {
	$("#prevBtn").on("click", function () {
		initQuestion(prevQuestionNo);
		nextQuestionNo--;
		prevQuestionNo--;
	});
}

function createQuestionAnswer() {
	var questionTypeId = parseInt($("#hdnQuestionType").val());
	var tempEmployeeExamAnswerList = [];
	if (questionTypeId == QuestionType.MULTIPLE_CHOICE) {
		tempEmployeeExamAnswerList = recordMultipleChoiceAnswer();
	} else if (questionTypeId == QuestionType.IDENTIFICATION) {
		tempEmployeeExamAnswerList = recordIdentificationAnswer();
	} else if (questionTypeId == QuestionType.ORDERING) {
		tempEmployeeExamAnswerList = rec
	}
	return tempEmployeeExamAnswerList;
}

function attachListenerToQuestionNoLink() {
	$("body").on("click", ".questionNoLink", function () {
		var questionNo = parseInt($(this).data("ctr"));
		prevQuestionNo = questionNo - 1;
		nextQuestionNo = questionNo + 1;
		initQuestion(questionNo);
	});
}

function attachListenerToFinishBtn() {
	$("#finishBtn").on("click", function () {
		alertify.confirm("Finish Section", "Are you sure you want to finish this section?.",
			function(){
				$("#finishBtn").attr("disabled","disabled");
				$("#finishBtn").html("Saving...");
				createEmployeeExamAnswers();
			},
			null
		);
	});
}

function createEmployeeExamAnswers() {
	var classId = $("#hdnClassId").val();
	var examId = $("#hdnExamId").val();
	var courseId = $("#hdnCourseId").val();
	var employeeExamProgress = {};
	employeeExamProgress.employeeExamAnswers = employeeExamAnswerList;
	employeeExamProgress.timeLeft = timer.getTimeValues().toString();
	employeeExamProgress.classId = classId;
	employeeExamProgress.examQuestionId = $("#hdnExamQuestionId").val();
	employeeExamProgress.examId = $("#hdnExamId").val();
	employeeExamProgress.sectionOrder = $("#hdnSectionOrder").val();
	
	$.when(ajax.create("classinfo/" + classId + "/employeeexamanswers", employeeExamProgress)).done(
		function (response) {
			switch (response.status) {
				case HttpStatus.SUCCESS:
					$(window).unbind('beforeunload');
					!courseId 
						? redirect("exam-home?classId=" + classId + "&examId=" + examId, 1500)
						: redirect("exam-home?classId=" + classId + "&examId=" + examId + "&courseId=" + courseId, 1500)
					
					break;
			}
		}
	);
}

function removeEmployeeExamAnswerObj(questionId) {
	var tempEmployeeExamAnswer = [];
	_.each(employeeExamAnswerList, function (employeeExamAnswer) {
		if (employeeExamAnswer.questionId != questionId) {
			tempEmployeeExamAnswer.push(employeeExamAnswer);
		}
	});
	return tempEmployeeExamAnswer;
}
//-----------------------------------------------Start: Multiple Choice-------------------------------------------//
function populateMultipleChoiceCon(question) {
	showQuestionDiv("#multipleChoiceCon");
	$("#multipleChoiceQuestion").html("");
	var html = [];
	html.push("<span>" + question.content + "<span/>");
	$("#multipleChoiceQuestion").append(html.join(" "));
	$("#hdnQuestionId").val(question.questionId);
	$("#hdnQuestionType").val(question.questionTypeId);
	fetchMultipleChoiceChoices(question.questionId);
}

function initChoicesList() {
	$.when(ajax.fetch("choices")).done(function (response) {
		switch (response.status) {
			case HttpStatus.SUCCESS:
				choiceList = response.choices;
				initQuestion(1);
				break;
		}
	});
}

function fetchMultipleChoiceChoices(questionId) {
	var tempChoices = [];
	correctMultipleChoiceAnswersCtr = 0;
	_.each(choiceList, function (choice) {
		if (choice.questionId == questionId) {
			tempChoices.push(choice);
			if(choice.choiceType){
				correctMultipleChoiceAnswersCtr++;
			}
		}
	});
	populateMultipleChoiceChoices(tempChoices);
}

function populateMultipleChoiceChoices(choices) {
	$("#multipleChoiceChoices").html("");
	var choiceCtr=0;
	_.each(choices, function (choice) {
		var html = [];
		html.push("<label>");
		html.push(getMultipleChoiceRadioBtn(choice, choiceCtr));
		html.push("<span>" + choice.choiceDesc + "<span/>");
		html.push("<label/><br>")
		$("#multipleChoiceChoices").append(html.join(" "));
		choiceCtr++;
	});
}

function getMultipleChoiceRadioBtn(choice, choiceCtr) {
	var questionId = parseInt($("#hdnQuestionId").val());
	var radioBtnHtml = "";
	
	if(correctMultipleChoiceAnswersCtr > 1){
		radioBtnHtml =  "<input type='checkbox' id='choiceId-"+choiceCtr+"' name='choices[]' data-name='"+choice.choiceDesc+"' class='choice' value='"+choice.choiceId+"'>";
	}else{
		radioBtnHtml = "<input type='radio' name='choices[]' data-name='" + choice.choiceDesc + "' class='choice' value='" + choice.choiceId + "'>";
	}
	
	if (checkIfAnswerExist(questionId)) {
		_.each(employeeExamAnswerList, function (employeeExamAnswer) {
			if (employeeExamAnswer.choiceId == choice.choiceId && employeeExamAnswer.questionId == questionId) {
				if(correctMultipleChoiceAnswersCtr > 1){
					radioBtnHtml =  "<input type='checkbox' name='choices[]' data-name='"+choice.choiceDesc+"' class='choice' value='"+choice.choiceId+"' checked>";
				}else{
					radioBtnHtml = "<input type='radio' name='choices[]' data-name='" + choice.choiceDesc + "' value='" + choice.choiceId + "' checked>";
				}
			}
		})
	}
	return radioBtnHtml;
}

function recordMultipleChoiceAnswer() {
	var questionId = parseInt($("#hdnQuestionId").val());
	var tempEmployeeExamAnswerList = employeeExamAnswerList;
	
	if (checkIfAnswerExist(questionId)) {
		tempEmployeeExamAnswerList = removeEmployeeExamAnswerObj(questionId);
	} 
	tempEmployeeExamAnswerList = tempEmployeeExamAnswerList.concat(getMultipleChoiceAnswerData());
	
	if(tempEmployeeExamAnswerList.length > 0){
		$("#questionNo-"+currentQuestionNo).css("font-weight", "bold");
	}
	
	return tempEmployeeExamAnswerList;
}

function validateMultipleChoiceAnswer(questionType) {
	var result = true;
	if ($("input[name='choices[]']:checked").val() == undefined) {
		alertify.warning("Please chooose an answer.");
		result = false;
	}
	return result;
}

function getMultipleChoiceAnswerData() {
	var data = [];
	$("input[name='choices[]']:checked").each(function(index,element){
		var employeeExamAnswer = {};
		employeeExamAnswer.classId = $("#hdnClassId").val();
		employeeExamAnswer.examId = $("#hdnExamId").val();
		employeeExamAnswer.questionId = $("#hdnQuestionId").val();
		employeeExamAnswer.choiceId = $(this).val();
		employeeExamAnswer.answer = $(this).data("name");
		employeeExamAnswer.examQuestionId = $("#hdnExamQuestionId").val();
		employeeExamAnswer.questionType = $("#hdnQuestionType").val();
		employeeExamAnswer.sectionOrder = $("#hdnSectionOrder").val();
		employeeExamAnswer.isRandomized = $("#hdnIsRandomized").val();
		data.push(employeeExamAnswer);
		$("input[type='checkbox']:not(:checked)").attr('disabled', (index < (correctMultipleChoiceAnswersCtr - 1) ? false : true ));
	})
	return data;
}

function checkIfAnswerExist(questionId) {
	var result = false;
	_.each(employeeExamAnswerList, function (employeeExamAnswer) {
		if (employeeExamAnswer.questionId == questionId) {
			result = true;
		}
	});
	return result;
}

function attachListenerToChoicesRadioBtn() {
	$("body").on("change", "input[name='choices[]']", function () {
		employeeExamAnswerList = createQuestionAnswer();
		enableFinishBtn();
	})
}
//-----------------------------------------------End: Multiple Choice-------------------------------------------//
//-----------------------------------------------Start: Identification-------------------------------------------//
function populateIdentification(question) {
	showQuestionDiv("#identificationCon");
	$("#identificationQuestion").html("");
	$("#identificationQuestion").append("<span/>" + question.content + "</span>");
	$("#identificationAnswerTxtBox").val("");
	$("#hdnQuestionId").val(question.questionId);
	$("#hdnQuestionType").val(question.questionTypeId);
	if(question.mediaUrl != null){
		$("#identificationMedia").attr("src",question.mediaUrl);
	}
		
	if (checkIfAnswerExist(question.questionId)) {
		_.each(employeeExamAnswerList, function (employeeExamAnswer) {
			if (employeeExamAnswer.questionId == question.questionId) {
				$("#identificationAnswerTxtBox").val(employeeExamAnswer.answer);
			}
		});
	}
}

function attachListenerToIdenticationTxtBox() {
	$("body").on("keyup", "#identificationAnswerTxtBox", function () {
		setTimeout(function () {
			employeeExamAnswerList = createQuestionAnswer();
		}, 1000);
		enableFinishBtn();
	});
}

function recordIdentificationAnswer() {
	var questionId = parseInt($("#hdnQuestionId").val());
	var tempEmployeeExamAnswerList = employeeExamAnswerList;
	if (checkIfAnswerExist(questionId)) {
		tempEmployeeExamAnswerList = removeEmployeeExamAnswerObj(questionId);
		tempEmployeeExamAnswerList.push(getIdentificationAnswerData());
	} else {
		tempEmployeeExamAnswerList.push(getIdentificationAnswerData());
	}
	if(tempEmployeeExamAnswerList.length > 0){
		$("#questionNo-"+currentQuestionNo).css("font-weight", "bold");
	}
	return tempEmployeeExamAnswerList;
}

function getIdentificationAnswerData(questionId) {
	var employeeExamAnswer = {};
	employeeExamAnswer.classId = $("#hdnClassId").val();
	employeeExamAnswer.examId = $("#hdnExamId").val();
	employeeExamAnswer.questionId = $("#hdnQuestionId").val();
	employeeExamAnswer.answer = $("#identificationAnswerTxtBox").val();
	employeeExamAnswer.questionType = $("#hdnQuestionType").val();
	employeeExamAnswer.sectionOrder = $("#hdnSectionOrder").val();
	employeeExamAnswer.isRandomized = $("#hdnIsRandomized").val();
	employeeExamAnswer.examQuestionId = $("#hdnExamQuestionId").val();
	return employeeExamAnswer;
}
//-----------------------------------------------End: Identification-------------------------------------------//
//-----------------------------------------------Start: True Or False-------------------------------------------//
function populateTrueOrFalseCon(question) {
	showQuestionDiv("#trueOrFalseCon");
	$("#hdnQuestionId").val(question.questionId);
	$("#trueOrFalseQuestionCon").html("");
	$("#trueOrFalseQuestionCon").html(question.content);
	$("#hdnQuestionType").val(question.questionTypeId);
	$("#trueRdBtn").prop("checked", false);
	$("#falseRdBtn").prop("checked", false);
	if (checkIfAnswerExist(question.questionId)) {
		_.each(employeeExamAnswerList, function (employeeExamAnswer) {
			if (employeeExamAnswer.questionId == question.questionId) {
				if (employeeExamAnswer.answer == "true") {
					$("#trueRdBtn").prop("checked", true);
				} else {
					$("#falseRdBtn").prop("checked", true);
				}
			}
		})
	}
}

function attachListenerToTrueOrFalseRdBtn() {
	$("input[name='trueOrFalseRdBtn']").on("change", function () {
		employeeExamAnswerList = recordTrueOrFalseAnswer();
		enableFinishBtn();
	})
}

function recordTrueOrFalseAnswer() {
	var questionId = parseInt($("#hdnQuestionId").val());
	var tempEmployeeExamAnswerList = employeeExamAnswerList;
	if (checkIfAnswerExist(questionId)) {
		tempEmployeeExamAnswerList = removeEmployeeExamAnswerObj(questionId);
		tempEmployeeExamAnswerList.push(getTrueOrFalseAnswerData());
	} else {
		tempEmployeeExamAnswerList.push(getTrueOrFalseAnswerData());
	}
	
	if(tempEmployeeExamAnswerList.length > 0){
		$("#questionNo-"+currentQuestionNo).css("font-weight", "bold");
	}
	
	return tempEmployeeExamAnswerList;
}

function getTrueOrFalseAnswerData() {
	var employeeExamAnswer = {};
	employeeExamAnswer.classId = $("#hdnClassId").val();
	employeeExamAnswer.examId = $("#hdnExamId").val();
	employeeExamAnswer.questionId = $("#hdnQuestionId").val();
	employeeExamAnswer.answer = $("input[name='trueOrFalseRdBtn']:checked").val();
	employeeExamAnswer.questionType = $("#hdnQuestionType").val();
	employeeExamAnswer.sectionOrder = $("#hdnSectionOrder").val();
	employeeExamAnswer.isRandomized = $("#hdnIsRandomized").val();
	employeeExamAnswer.examQuestionId = $("#hdnExamQuestionId").val();
	return employeeExamAnswer;
}
//-----------------------------------------------End: True Or False-------------------------------------------//
//-----------------------------------------------Start: Ordering----------------------------------------------//
function populateOrdering(questionNo) {
	showQuestionDiv("#orderingCon");

	var shuffledOrderingList = [];
	var orderingQuestionList = [];
	let iterator = 0;
	var savedQuestionId = 0;
	var questionId = 0;
	_.each(questionList, function (question) {
		if (savedQuestionId != question.questionId) {
			savedQuestionId = question.questionId;
			iterator++;
		}
		if (iterator == questionNo) {
			questionId = question.questionId;
			shuffledOrderingList.push(question);
			$("#orderingMedia").attr("src",question.mediaUrl);
		}
	});
	$("#hdnQuestionId").val(questionId);
	orderingQuestionList = employeeExamAnswerList.filter(question => question.questionId == questionId);
	if (Array.isArray(orderingQuestionList) && orderingQuestionList.length) {
		var lastOrderingList = [];
		_.each(orderingQuestionList, function(question){
			let selectedQuestion = shuffledOrderingList.find(element => question.answer == element.orderingId);
			lastOrderingList.push(selectedQuestion);
		});
		shuffledOrderingList = lastOrderingList.slice();
	} else {
		shuffledOrderingList = shuffle(shuffledOrderingList);
	}
	$("#sortable").empty();
	_.each(shuffledOrderingList, function (question) {
		$("#orderingContent").html(question.content)
		$("#hdnQuestionType").val(question.questionTypeId);
		$("#sortable")
			.append($("<li/>").attr({
				"data-id": question.orderingId,
				"data-text": question.orderingAnswer
			})
				.append($(`<span><div class="sortItem"> ${question.orderingAnswer}</div></span>`)
				)
			)
	});
	listenerOrderData();
}

function listenerOrderData() {

	$("#sortable").sortable({
		update: function (event, ui) {
			var order = $(this).sortable("toArray", { "attribute": "data-id"});
			employeeExamAnswerList = employeeExamAnswerList.filter(emp => emp.questionId != $("#hdnQuestionId").val());
			_.each(order, function (answer) {
				employeeExamAnswerList.push(getOrderingAnswerData(answer));
			});
			if(employeeExamAnswerList.length > 0){
				$("#questionNo-"+currentQuestionNo).css("font-weight", "bold");
			}
			enableFinishBtn();
		}
	});
}

function getOrderingAnswerData(orderingAnswerId) {
	var employeeExamAnswer = {};
	let result = questionList.find(question => question.orderingId == orderingAnswerId);
	employeeExamAnswer.classId = $("#hdnClassId").val();
	employeeExamAnswer.examId = $("#hdnExamId").val();
	employeeExamAnswer.questionId = $("#hdnQuestionId").val();
	employeeExamAnswer.answer = orderingAnswerId;
	employeeExamAnswer.questionLabel = result.orderingAnswer;
	employeeExamAnswer.questionType = $("#hdnQuestionType").val();
	employeeExamAnswer.sectionOrder = $("#hdnSectionOrder").val();
	employeeExamAnswer.isRandomized = $("#hdnIsRandomized").val();
	employeeExamAnswer.examQuestionId = $("#hdnExamQuestionId").val();
	return employeeExamAnswer;
}

//-----------------------------------------------End: Ordering------------------------------------------------//
//-----------------------------------------------Start: Enumeration-------------------------------------------//
function populateEnumeration(questionNo) {
	showQuestionDiv("#enumerationCon");
	var enumerationQuestionList = [];
	var savedEnumerationAnswerList = [];
	let iterator = 0;
	var savedQuestionId = 0;
	var questionId = 0;
	_.each(questionList, function (question) {
		if (savedQuestionId != question.questionId) {
			savedQuestionId = question.questionId;
			iterator++;
		}
		if (iterator == questionNo) {
			questionId = question.questionId;
			enumerationQuestionList.push(question);
			$("#hdnQuestionType").val(question.questionTypeId);
			$("#enumerationContent").html(question.content);
			$("#enumerationMedia").attr("src",question.mediaUrl);
		}
	});
	$("#hdnQuestionId").val(questionId);
	savedEnumerationAnswerList = employeeExamAnswerList.filter(question => question.questionId == questionId);
	
	if (Array.isArray(savedEnumerationAnswerList) && savedEnumerationAnswerList.length) {
		enumerationQuestionList = savedEnumerationAnswerList.slice();
	} 

	$("#enumerationColumn").empty();
	employeeExamAnswerList = employeeExamAnswerList.filter(exam => exam.questionId !== questionId)
	_.each(enumerationQuestionList, function (question) {
		var enumeration = {};
		var enumerationId = question.enumerationId;
		enumeration = getEnumerationAnswerData(!question.enumerationAnswer ? "" : question.answer, enumerationId);
	
		
		if (savedEnumerationAnswerList.length == 0) {
			employeeExamAnswerList.push(enumeration);
		}

		$("#enumerationColumn")
			.append($("<input/>").attr({
				"data-id": enumerationId,
				"type" : "text",
				"id" : "enumerationInput_" + enumerationId,
				"value" : question.enumerationAnswer == "" ? "" : question.answer,
				"class" : "enumerationField"
				})
			)
	});
	listenerEnumerationData();
}

function listenerEnumerationData() {
	$(".enumerationField").on("change", function(){
		var enumerationId = $(this).data("id");
		var enumerationUserAnswer = $("#enumerationInput_" + enumerationId).val();
		var enumeration = {};
		enumeration = getEnumerationAnswerData(enumerationUserAnswer,  enumerationId);
		var findIndex = employeeExamAnswerList.findIndex(e => e.enumerationId == enumerationId);
		employeeExamAnswerList[findIndex]  = enumeration;
		
		if(employeeExamAnswerList.length > 0){
			$("#questionNo-"+currentQuestionNo).css("font-weight", "bold");
		}
		
		enableFinishBtn();
	})
}

function getEnumerationAnswerData(answer, id) {
	var employeeExamAnswer = {};
	employeeExamAnswer.classId = $("#hdnClassId").val();
	employeeExamAnswer.examId = $("#hdnExamId").val();
	employeeExamAnswer.questionId = $("#hdnQuestionId").val();
	employeeExamAnswer.answer = answer;
	employeeExamAnswer.enumerationId = id;
	employeeExamAnswer.questionType = $("#hdnQuestionType").val();
	employeeExamAnswer.sectionOrder = $("#hdnSectionOrder").val();
	employeeExamAnswer.isRandomized = $("#hdnIsRandomized").val();
	employeeExamAnswer.examQuestionId = $("#hdnExamQuestionId").val();
	return employeeExamAnswer;
}
//-----------------------------------------------End: Enumeration---------------------------------------------//
//-----------------------------------------------Start: Matching----------------------------------------------//
function populateMatching(questionNo) {
	showQuestionDiv("#matchingCon");
	$("#matchingQuestions").empty();
	$("#matchingQuestionsCompare").empty();
	
	var savedMatchingAnswerList = [];
	var A = 65;
	var tempMatchingQuestionList = [];
	mapMatchingShuffledQuestionList = new Map();
	let iterator = 0;
	var savedQuestionId = 0;
	var questionId = 0;
	var matchingTypeCharEnumerator = A;


	_.each(questionList, function (question) {

		if (savedQuestionId != question.questionId) {
			savedQuestionId = question.questionId;
			iterator++;
		}

		if (iterator == questionNo) {
			questionId = question.questionId;
			$("#hdnQuestionType").val(question.questionTypeId);
			$("#matchingQuestionCon").html(question.content);
			tempMatchingQuestionList.push(question);
			$("#matchingQuestions").append($("<li/>")
				.append($("<input/>").attr({
					"type":"text",
					"class":"spanWidth matchingTxtBox",
					"maxlength":"1",
					"id":question.analogyId
					})
				)
				.append($("<span/>").html(question.givenA))
			);
		}
	});
	savedMatchingAnswerList = employeeExamAnswerList.filter(exam => exam.questionId == questionId); 
	$("#hdnQuestionId").val(questionId);

	if (matchingQuestionList.length === 0) {
		matchingQuestionList = tempMatchingQuestionList.slice();
		matchingQuestionList = shuffle(matchingQuestionList);
	}
	attachListenerMatchingTextBox();

	_.each(shuffle(tempMatchingQuestionList), function(questionObj) {

		if (!questionObj.givenBMediaUrl) {
			$("#matchingQuestionsCompare").append($("<li/>")
				.append($("<span/>").html(questionObj.givenB))
			);
		} else {
			$("#matchingQuestionsCompare").append($("<li/>")
				.append($("<img/>").addClass("fixSmallImg").attr({"src" : questionObj.givenBMediaUrl}))
			);
		}
		mapMatchingShuffledQuestionList.set(matchingTypeCharEnumerator, questionObj);
		matchingTypeCharEnumerator++;
	});

	if (savedMatchingAnswerList.length > 0) {

		_.each(savedMatchingAnswerList, function(matchingAnswer) {
			var key = getByValue(mapMatchingShuffledQuestionList, matchingAnswer.answer);
			
			if (key.length !== 1) {
				$(`#${matchingAnswer.analogyId}`).val(String.fromCharCode(key));
			} else {
				$(`#${matchingAnswer.analogyId}`).val(matchingAnswer.answer);
			}	
		}) ;
	}
}

function attachListenerMatchingTextBox() {
	$(".matchingTxtBox").change(function (event) { 
		var matchingTypeAnswer = {};
		var analogyId = event.target.id;
		var answer = event.target.value;
		var answerFromMap;

		var getMatchingTypeAnswerFromMap = mapMatchingShuffledQuestionList.get(answer.toUpperCase().charCodeAt(0));
		
		if (!getMatchingTypeAnswerFromMap) {
			matchingTypeAnswer = getMatchingAnswerData(answer, analogyId);
		} else {
			matchingTypeAnswer = getMatchingAnswerData(getMatchingTypeAnswerFromMap.givenB, analogyId);
		}
		var findIndex = employeeExamAnswerList.findIndex(index => index.analogyId == analogyId);
		if (findIndex < 0) {
			employeeExamAnswerList.push(matchingTypeAnswer);
		} else {
			employeeExamAnswerList[findIndex] = matchingTypeAnswer;
		}
		
		if(employeeExamAnswerList.length > 0){
			$("#questionNo-"+currentQuestionNo).css("font-weight", "bold");
		}
		
		enableFinishBtn();
	});
}


function getMatchingAnswerData(answer, analogyId) {
	var employeeExamAnswer = {};
	employeeExamAnswer.classId = $("#hdnClassId").val();
	employeeExamAnswer.examId = $("#hdnExamId").val();
	employeeExamAnswer.questionId = $("#hdnQuestionId").val();
	employeeExamAnswer.answer = answer;
	employeeExamAnswer.analogyId = analogyId;
	employeeExamAnswer.questionType = $("#hdnQuestionType").val();
	employeeExamAnswer.sectionOrder = $("#hdnSectionOrder").val();
	employeeExamAnswer.isRandomized = $("#hdnIsRandomized").val();
	employeeExamAnswer.examQuestionId = $("#hdnExamQuestionId").val();
	return employeeExamAnswer;
}
//-----------------------------------------------End: Matching------------------------------------------------//
//-----------------------------------------------Start: Essay----------------------------------------------//
function populateEssay(question) {
	showQuestionDiv("#essayCon");
	var questionId = 0;
	$("#essayMedia").attr("src",question.mediaUrl);
	$("#essayQuestionCon").html(question.content);
	questionId = question.questionId;
	$("#hdnQuestionType").val(question.questionTypeId);
	$("#essayForm").empty();
	$("#essayForm").append($("<div/>").attr({"id":"essayAnswer_" + questionId,"class":"txtarea"}));
	$("#hdnQuestionId").val(questionId);
	initTinyMceExamEditor(".txtarea" , 500);
	employeeExamAnswer = employeeExamAnswerList.filter(exam => exam.questionId == questionId);
	if(employeeExamAnswer.length > 0) {
		tinymce.activeEditor.setContent(employeeExamAnswer[0].answer, {format: 'html'});
	}
}

function getEssayAnswerData(answer) {
	var employeeExamAnswer = {};
	employeeExamAnswer.classId = $("#hdnClassId").val();
	employeeExamAnswer.examId = $("#hdnExamId").val();
	employeeExamAnswer.questionId = $("#hdnQuestionId").val();
	employeeExamAnswer.answer = answer;
	employeeExamAnswer.questionType = $("#hdnQuestionType").val();
	employeeExamAnswer.sectionOrder = $("#hdnSectionOrder").val();
	employeeExamAnswer.isRandomized = $("#hdnIsRandomized").val();
	employeeExamAnswer.examQuestionId = $("#hdnExamQuestionId").val();
	return employeeExamAnswer;
}
//-----------------------------------------------End: Essay------------------------------------------------//
//-----------------------------------------------Start: Fill in the Blanks------------------------------------//
function populateFillInTheBlanks(questionNo) {
	$("#fillInTheBlanksQuestions").empty();
	showQuestionDiv("#fillInTheBlanksCon");
	var fillInTheBlanksQuestionList = [];
	let iterator = 0;
	var savedQuestionId = 0;
	var questionId = 0;
	
	_.each(questionList, function(question) {

		if (savedQuestionId != question.questionId) {
			savedQuestionId = question.questionId;
			iterator++;
		}

		if (iterator == questionNo) {
			if (isEmpty($('#fillInTheBlanksQuestions'))) {
				$("#fillInTheBlanksMedia").attr("src",question.mediaUrl);
				//$("#fillInTheBlanksQuestions").html(question.content);

				var dom = document.getElementById("fillInTheBlanksQuestions");
				dom.innerHTML = question.content;
				
				$(dom).find("input").each(function(inputIndex, inputValue){
					$(inputValue).attr("readonly",false);
					$(inputValue).val("");
					$(inputValue).addClass("styleWidth");
				})
				
				$(dom).find("select").each(function(selectIndex, selectValue){
					var tes=[];
					$(this).find("option").each(function(optionIndex, optionValue){
						tes.push(optionValue);
					});
					$(this).empty();
					$(this).append(shuffle(tes));
				})				
				
			}	
			questionId = question.questionId;
			var componentCode = `#${question.fillIntheBlanksAnswerComponentCode}`;
			fillInTheBlanksQuestionList.push(question);
			$("#hdnQuestionType").val(question.questionTypeId);
			$("#fillInTheBlanksImageMedia").attr("src",question.mediaUrl);
			$("#fillInTheBlanksSpan").html(question.instruction);
			$("#fillInTheBlanksSpan").html(question.instruction);
			$(componentCode).removeAttr("readonly", "selected");
			$("option").removeAttr("selected");
			$(componentCode).val("");

			if (question.fillIntheBlanksAnswerChoiceType == ChoiceType.MULTIPLE_CHOICE) {
				$(componentCode).addClass("selectWidthStyle");
				attachListenerDropDownFillInTheBlanks(componentCode, questionList.length);
				
			} else {
				$(componentCode).addClass("styleWidth");
				attachListenerTextBoxFillInTheBlanks(componentCode, questionList.length);
				
			}
			$(componentCode).trigger("contentChanged");
		}
	});
	$("#hdnQuestionId").val(questionId);
	employeeExamAnswerList = employeeExamAnswerList.filter(exam => exam.questionId !== questionId)
	_.each(employeeExamAnswerList, function (question) {
		$(`#${question.componentCode}`).val(question.answer);
	});
}

function attachListenerTextBoxFillInTheBlanks(dom, length) {
	$(dom).keyup(function (el) { 
		var componentCode = el.target.id;
		var answer = el.target.value;
	
		var fillInTheBlankAnswer = {};
		fillInTheBlankAnswer = getFillInTheBlanksAnswerData(componentCode, answer);
		var findIndex = employeeExamAnswerList.findIndex(index => index.componentCode == componentCode);
		if (findIndex < 0) {
			employeeExamAnswerList.push(fillInTheBlankAnswer);
		} else {
			employeeExamAnswerList[findIndex] = fillInTheBlankAnswer;
		}
		
		if(employeeExamAnswerList.length > 0){
			$("#questionNo-"+currentQuestionNo).css("font-weight", "bold");
		}			
		
		enableFinishBtn();
		
		
	});
}

function attachListenerDropDownFillInTheBlanks(dom, length) {
	$(dom).change(function (el) { 
		var componentCode = el.target.id;
		var answer = el.target.value;
	
		var fillInTheBlank = {};
		fillInTheBlank = getFillInTheBlanksAnswerData(componentCode, answer);
		var findIndex = employeeExamAnswerList.findIndex(index => index.componentCode == componentCode);
		if (findIndex < 0) {
			employeeExamAnswerList.push(fillInTheBlank);
		} else {
			employeeExamAnswerList[findIndex] = fillInTheBlank;
		}
		
		if(employeeExamAnswerList.length > 0 ){
			$("#questionNo-"+currentQuestionNo).css("font-weight", "bold");
		}			
		
		enableFinishBtn();
		
	});
}

function getFillInTheBlanksAnswerData(componentCode, answer) {
	var employeeExamAnswer = {};
	employeeExamAnswer.classId = $("#hdnClassId").val();
	employeeExamAnswer.examId = $("#hdnExamId").val();
	employeeExamAnswer.questionId = $("#hdnQuestionId").val();
	employeeExamAnswer.answer = answer;
	employeeExamAnswer.componentCode = componentCode;
	employeeExamAnswer.questionType = $("#hdnQuestionType").val();
	employeeExamAnswer.sectionOrder = $("#hdnSectionOrder").val();
	employeeExamAnswer.isRandomized = $("#hdnIsRandomized").val();
	employeeExamAnswer.examQuestionId = $("#hdnExamQuestionId").val();
return employeeExamAnswer;
}
//-----------------------------------------------End: Fill in the Blanks--------------------------------------//

function isEmpty(dom) {
	return !$.trim(dom.html());
}

function initTinyMceExamEditor(dom,height) {
	return tinymce.init({
		  selector: dom,
		  plugins: Editor.PLUGINS,
		  toolbar: Editor.TOOLBAR,
		  theme: 'silver',
		  menubar: Editor.MENUBAR,
		  paste_as_text: true,
		  height: height,
		  branding:false,
		  setup: function(editor) {
			editor.on('keyup', function(e) {
				var essayAnswer = {};
				essayAnswer = getEssayAnswerData(e.srcElement.innerHTML);
				var findIndex = employeeExamAnswerList.findIndex(index => index.questionId == $("#hdnQuestionId").val());
				if (findIndex < 0) {
					employeeExamAnswerList.push(essayAnswer);
				} else {
					employeeExamAnswerList[findIndex] = essayAnswer;
				}
				
				if(employeeExamAnswerList.length > 0){
					$("#questionNo-"+currentQuestionNo).css("font-weight", "bold");
				}
				
				enableFinishBtn();
				
			});
		  }
	});
}

function getByValue(map, searchValue) {
	for (let [key, value] of map.entries()) {
		if (value.givenB == searchValue) {
			return key;
		} 
	}
	return searchValue;
}

function initLightBox(){
	lightbox.option({
	      'resizeDuration': 0,
	      'wrapAround': true
	    })	
}

function enableFinishBtn(){
	var countUnansweredQuestions = 0;
	
	$(".questionNoLink").each(function(index, value){
		if(value.style.fontWeight != "bold"){
			countUnansweredQuestions++;
		}
	})
	
	// if no remaining unanswered questions left, enable finish btn
	if(countUnansweredQuestions == 0){
		$("#finishBtn").attr("disabled", false);
	}
}