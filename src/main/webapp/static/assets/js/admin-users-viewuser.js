/**
 * 
 */

$(function(){
	initUserDetails();
	initButtons();
	attachAddUserAttachmentListener();
	fetchUserAttachment();
});

function initButtons(){
	$("#userResetPassword").unbind("click").click(function(){
		var username = $("#usernameLabel").html();
		var userId = GetURLParameter('id');
		var user = {};
		user.userId = userId;
		user.username = username;
		$.when(ajax.update("users/password/",username, user)).done(function(response) {
			if(response.status == HttpStatus.SUCCESS){
				alertify.alert('Reset Password', 'Password has successfully changed and it was sent to your email.');
			}
		});
	});
}

function initUserDetails(){
	var userId = GetURLParameter('id');
	$("#viewUserLink").attr("href", "admin.viewuser?id="+userId);
	$("#tfJobroleLink").attr("href", "admin.viewuser.jobrole?id="+userId);
	$("#tfUsergroupLink").attr("href", "admin.viewuser.usergroup?id="+userId);
	$("#tfTrainingHistoryLink").attr("href", "admin.viewuser.traininghistory?id="+userId);
	$("#editUserProfile").attr("href", "admin.userpage?action=EDIT&id="+userId);
	
	$.when(ajax.fetchObj("users/",userId)).done(function(response) {
		if(response.status == HttpStatus.SUCCESS){
			var userDetails = response.user;
			populateUserDetails(userDetails);
		}
	}); 
}

function populateUserDetails(userDetails) {
	var userId = GetURLParameter('id');
	var utid = userDetails.userTypeId;
	var hired = new Date(userDetails.dateHired).toDateString("MM-dd-yyyy");
	var created = new Date(userDetails.createdAt).toDateString("MM-dd-yyyy hh:MM:ss TT");
	var lastModified = new Date(userDetails.lastModifiedAt).toDateString("MM-dd-yyyy hh:MM:ss TT");
	$("#employeeNoLabel").html(userDetails.employeeCode);
	$("#fullNameLabel").html(userDetails.fullName);
	$("#userTypeLabel").html(userDetails.userTypeDesc);
	$("#usernameLabel").html(userDetails.username);
	$("#jobRoleLabel").html(userDetails.jobName);
	$("#hireDateLabel").html(userDetails.dateHired);
	$("#mobileNoLabel").html(userDetails.mobileNo);
	$("#emailLabel").html(userDetails.email);
	$("#dateCreatedLabel").html(created);
	$("#dateModifiedLabel").html(lastModified);
	$("#modifiedByLabel").html(userDetails.modifiedBy);
	$("#userProfilePicture").attr("src", 
		(userDetails.photoUrl.length === 0)?
		"static/assets/images/usersimage/user.png":userDetails.photoUrl);
	
	if(userDetails.status == "1"){
		$("#statusLabel").html("Active");
	} else if(userDetails.status == "2"){
		$("#statusLabel").html("Inactive");
	}
}

function validateAttachment() {
	var result = true;

	if($("#fileUpload").get(0).files.length==0) {
		result = false;
		alertify.warning("Please enter a file.");
		return;
		
	}

	if (($("#userType_"+id).val() || 0) == 0) {
		result = false;
		alertify.warning("Please enter a file name.");
		return;
	}

	return result;
}
	var userId = GetURLParameter('id');

function attachAddUserAttachmentListener() {
	var userId = GetURLParameter('id');
	$("#saveLocationBtn").on("click", function(){
		if (validateFile()) {
			var fileLabel = $("#fileName").val();
			var fileDesc = $("#fileDescription").val();

			if (!fileLabel) {
				alertify.warning("Please enter a file name.");
			} else {
				$.when(ajax.upload(`user/userattachment/${userId}/userfile`, "uploadMediaForm")).done(function(response) {
					switch(response.status) {
						case HttpStatus.COMPONENT_EXISTS:
							alertify.error("Same file Name is Existing.");
							break;
						case HttpStatus.SUCCESS:
							alertify.success("User attachment is Successfully saved.");
							fetchUserAttachment();
							clearModalValues();
							$("#userModal").modal("close");
							break;
						case HttpStatus.SESSION_EXPIRED:
							alertify.error(Message.SESSION_EXPIRED_MESSAGE);
							break;
						case HttpStatus.QUERY_FAILED:
							alertify.error(Message.QUERY_FAILED_MESSAGE);
							break;
						case HttpStatus.UNHANDLED_ERROR:
							alertify.error(Message.UNHANDLED_ERROR_MESSAGE);
							break;
					}
				});
			}
			
			
		}
	});
}

function fetchUserAttachment() {
	var userId = GetURLParameter('id');
	$.when(ajax.fetch(`user/fetchattachment/${userId}`)).done(function(response){
		initPaginationAttachment(response.userAttachmentList);
	});
}

function validateFile() {
	var result = true;
	
	if($("#fileUpload").get(0).files.length==0) {
		result = false;
		alertify.warning("Please enter a file.");
		return;
	}
	return result;
}

function populateUserAttachmentTable(attachmentList) {
	_.each(attachmentList, function(attachment, index) {
		$("#attachmentTblBody").append($("<tr/>")
			.append($("<td/>").addClass("min")
				.append($("<a/>").addClass("waves-effect waves-light").attr({
					"id":"attachment_" + attachment.userAttachmentId,
					"data-id":attachment.userAttachmentId,
					"href":"user/downloadattachment/userattachmentid?userAttachmentId="+attachment.userAttachmentId
					})
					.append($("<i/>").addClass("material-icons").html("file_download"))
				)
				.append($("<a/>").addClass("waves-effect waves-light").attr({
					"id": "attachmentId_" + attachment.userAttachmentId,
					"data-id":attachment.userAttachmentId})
					.bind("click", function() {
						var attachmentId = $(this).data("id");
						var userId = GetURLParameter('id');
						$.when(ajax.remove("user/userattachment/"+ userId + "/deleteAttachment?attachmentId=", attachmentId)).done(function(response){
							switch(response.status) {
								case HttpStatus.SUCCESS:
									fetchUserAttachment();
									break;
								case HttpStatus.COMPONENT_EXISTS:
									alertify.error("File is not deleted.");
									break;
								case HttpStatus.SESSION_EXPIRED:
									alertify.error(Message.SESSION_EXPIRED_MESSAGE);
									break;
								case HttpStatus.QUERY_FAILED:
									alertify.error(Message.QUERY_FAILED_MESSAGE);
									break;
								case HttpStatus.UNHANDLED_ERROR:
									alertify.error(Message.UNHANDLED_ERROR_MESSAGE);
									break;
							}
						});
					})
					.append($("<i/>").addClass("material-icons").html("delete"))
				)
			)
			.append($("<td/>").addClass("mid").html(attachment.fileTitle))
			.append($("<td/>").addClass("mid").html(attachment.fileDescription))
			.append($("<td/>").addClass("mid").html(attachment.fileTitle))
		)
	});
}

function initPaginationAttachment(attachmentList) {
	var pageSize = 10;
	$("#attachmentPagination").pagination({
		dataSource: attachmentList,
		className: 'paginationjs-theme-blue',
		pageSize: pageSize,
		callback: function (data) {
			$("#attachmentTblBody").empty();
			populateUserAttachmentTable(data);
		}
	});
}

function clearModalValues() {
	$("#fileName").val("");
	$("#fileDescription").val("");
	$("#fileUpload").val("");
	$("#mediaDisplayTxt").val("");
	var $el = $('#fileUpload'); 
	$el.wrap('<form>').closest( 
	  'form').get(0).reset(); 
	$el.unwrap(); 
}
