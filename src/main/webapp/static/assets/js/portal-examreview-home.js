$(function(){
	initPageSettings();
	initExamSectionTbl();
})

function initPageSettings() {
	$("#examType").html(getExamTypeDesc($("#hdnExamType").val()));
}

function getExamTypeDesc(examType) {
	var examTypeDesc="";
	_.each(ExamTypes,function(examTypeObj) {
		if(examTypeObj.id==examType) {
			examTypeDesc=examTypeObj.description;
		}
	});
	return examTypeDesc;
}

function initExamSectionTbl() {
	var examId=$("#hdnExamId").val();
	var classId=$("#hdnClassId").val();
	$.when(ajax.fetch("classinfo/"+classId+"/examinfo/"+examId+"/examquestions/totalpoints")).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				populateExamSectionTbl(response.examQuestions);
				break;
		}
	});
}

function populateExamSectionTbl(examQuestions) {
	var examId = $("#hdnExamId").val();
	var classId = $("#hdnClassId").val();
	$("#examSectionTbl").append(
		$("<tr/>"))
	_.each(examQuestions,function(examQuestion) {
		$("#examSectionTbl").append($("<tr/>")
			.append($("<td/>").append($("<a>").attr("href","exam-review?classId="+classId+"&examId="+examId+"&order="+examQuestion.sectionOrder).html(examQuestion.sectionName)))	
			.append($("<td/>").html(examQuestion.instruction))
			.append($("<td/>").html(examQuestion.totalPoints))
			.append($("<td/>").html(getEmployeeExamStatusDesc(examQuestion.status))));
	});	
}

function getEmployeeExamStatusDesc(status) {
	var result="";
	if(SessionStatus.NOT_YET_TAKEN==status || status==null) {
		result="Not Yet Taken"
	} else {
		result="Finished";
	}
	return result;
}