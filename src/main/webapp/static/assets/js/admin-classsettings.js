/**
 * 
 */
$(function(){
	defineGlobalVariables();
	defineList();
	initHighPositionEmployees();
	initClassInfo();
	attachListenerOnCertificate();
	attachListenerOnSelfRegistration();
	attachListenerOnViewingRestrictions();
	attachListenerOnAccessRestrictions();
	attachListenerToAddClassIpAddressBtn();
	attachListenerToAddClassIpAddressModalBtn();
	attachListenerToSaveSettingsBtn();
	attachListenerToUploadPhotoLink();
	attachListenerToUploadPhoto();
	attachListenerToAddSignatoryBtn();
});

function defineGlobalVariables(){
	signatoryCtr = 0;
	highPositionEmployees = [];
	left = 0;
	middle = 0;
	right = 0;
	prevValue = 0;
	prevValue2 = 0;
}

function initSelect2(dom){
	$(dom).select2({placeholder: "Choose An Option"});
}

function attachListenerToRemoveSignatoryBtn(btn){
	btn.closest('li').remove();
}

function initHighPositionEmployees(){
	$.when(ajax.fetch("high-position-employees")).done(function(response){
		highPositionEmployees = response.employeeInfos;
		initClassSignatories();
	})
}

function appendEmployeesToSignatoryDrpDwn(dom){
	$(dom).append(new Option("Choose An Option", null))
	_.each(highPositionEmployees, function(employee){
		$(dom).append(new Option(employee.fullName + "(" + employee.jobName + ")", employee.employeeId))
	})
}

function initClassSignatories(){
	$.when(ajax.fetch("classinfo/"+parseInt(GetURLParameter('classId'))+"/class-signatories")).done(function(response){
		signatories = response.classSignatories;
		
		$("#classSignatoryList").empty();
		
		_.each(signatories, function(signatory){
			signatoryCtr++;
			
			$("#classSignatoryList")
			.append($("<li>").addClass("row")
				.append($("<div>").addClass("col s12 m6")
					.append($("<select>").attr({"id":'signatory-'+signatoryCtr, "disabled": ($("#userType").val() == 1 || $("#loggedInId").val() == $("#trainerId").val()  ? false : "disabled")}))
				)
				.append($("<div>").addClass("col s12 m3").css("display", $("#userType").val() == 1 || $("#loggedInId").val() == $("#trainerId").val()  ? "" : "none")
					.append($("<a>").addClass("btn-floating btn-small waves-effect waves-light bttn")
						.append($("<i>").addClass("material-icons").html("remove"))
						.bind("click", function(){
							attachListenerToRemoveSignatoryBtn($(this));
						})
					)
				)
			)
			
			initSelect2('#signatory-'+signatoryCtr);
			appendEmployeesToSignatoryDrpDwn('#signatory-'+signatoryCtr);
			$('#signatory-'+signatoryCtr).select2().val(signatory.employeeId).trigger('change');
		})
		
	})
}

function attachListenerToAddSignatoryBtn(){
	$("#addSignatoryBtn").click(function(){
		signatoryCtr++;
		
		if($("#classSignatoryList li").length + 1 <= 3){
			$("#classSignatoryList")
			.append($("<li>").addClass("row")
				.append($("<div>").addClass("col s12 m6")
					.append($("<select>").attr("id",'signatory-'+signatoryCtr)
						
					)
				)
				.append($("<div>").addClass("col s12 m3")
					.append($("<a>").addClass("btn-floating btn-small waves-effect waves-light bttn")
						.append($("<i>").addClass("material-icons").html("remove"))
						.bind("click", function(){
							attachListenerToRemoveSignatoryBtn($(this));
						})
					)
				)
			)
			initSelect2('#signatory-'+signatoryCtr);
			appendEmployeesToSignatoryDrpDwn('#signatory-'+signatoryCtr);
		}else{
			alertify.warning("Up to 3 signatories only");
		}
		
	})
}

function defineList() {
	checkIpAddress = [];
}

function initClassInfo() {
	if($("#deliveryMethod").val() == DeliveryMethod.INDIVIDUAL){
		$("#modularDisplay").css("display","");
		$("#deliveryMethodLabel").html("Individual");
	} else if($("#deliveryMethod").val() == DeliveryMethod.MODULAR){
		$("#modularDisplay").css("display","");
		$("#deliveryMethodLabel").html("Modular");
	} else if($("#deliveryMethod").val() == DeliveryMethod.CLASSTRAINING){
		$("#modularDisplay").css("display","none");
		$("#deliveryMethodLabel").html("Class Training");
	}
	
	if($("#deliveryMethod").val() == DeliveryMethod.INDIVIDUAL){
		$("#modularDisplay").css("display","");
		$("#deliveryMethodLabel").html("Individual");
	} else if($("#deliveryMethod").val() == DeliveryMethod.MODULAR){
		$("#modularDisplay").css("display","");
		$("#deliveryMethodLabel").html("Modular");
	}
	
	if($("#chWithCertificate").attr("data-withcertificate") == WithCertificate.TRUE){
		$("#chWithCertificate").prop("checked",true).trigger("change");
		$("#withCertificate").css("display","");
		$("#classSignatoriesDiv").css("display","");
		if($("#withCertificate").attr("data-template") == CertificateType.SYSTEMTEMPLATE) {
			$("#raSystemTemplate").prop("checked",true);
			$("#customUploadDiv").css("display","none");
		} else if($("#withCertificate").attr("data-template") == CertificateType.CUSTOMTEMPLATE) {
			$("#raCustomTemplate").prop("checked",true);
			$("#customUploadDiv").css("display","");
		}
		
		if($("#isDownloadable").attr("data-downloadable") == CertificateDownloadable.TRUE){
			$("#chIsDownloadable").prop("checked",true).trigger("change");
		} else if($("#isDownloadable").attr("data-downloadable") == CertificateDownloadable.FALSE){
			$("#chIsDownloadable").prop("checked",false).trigger("change");
		}
	} else {
		$("#withCertificate").css("display","none");
		$("#customUploadDiv").css("display","none");
		$("#classSignatoriesDiv").css("display","none");
	}
	
	if($("#chIsSelfRegister").attr("data-selfregister") == IsSelfRegister.TRUE){
		$("#chIsSelfRegister").prop("checked",true).trigger("change");
		if($("#selfRegistrationDiv").attr("data-selfregistertype") == SelfRegisterType.OPENINDEFINITELY) {
			$("#raOpenIndefinite").prop("checked",true);
			$("#specifiedPeriodDiv").css("display","none");
		} else if($("#selfRegistrationDiv").attr("data-selfregistertype") == SelfRegisterType.SPECIFIEDPERIOD) {
			$("#raOpenSpecifiedPeriod").prop("checked",true);
			$("#specifiedPeriodDiv").css("display","");
		}
	} else {
		$("#selfRegistrationDiv").css("display","none");
		$("#specifiedPeriodDiv").css("display","none");
	}
	
	if($("#viewRestrictionDiv").attr("data-viewrestriction") == ViewRestrictions.TRAININGDURATION) {
		$("#raAccessTrainingDurationOnly").prop("checked",true);
		$("#specifiedRestrictionsDiv").css("display","none");
	} else if($("#viewRestrictionDiv").attr("data-viewrestriction") == ViewRestrictions.INDEFINITE) {
		$("#raAccessIndefinitely").prop("checked",true);
		$("#specifiedRestrictionsDiv").css("display","none");
	} else if($("#viewRestrictionDiv").attr("data-viewrestriction") == ViewRestrictions.SPECIFIEDPERIOD) {
		$("#raAccessSpecifiedPeriod").prop("checked",true);
		$("#specifiedRestrictionsDiv").css("display","");
	}else{
		$("#specifiedRestrictionsDiv").css("display","none");
	}
	
	if($("#accessRestrictionTypeDiv").attr("data-accesstype") == AccessRestrictionType.PUBLIC) {
		$("#raIpPublic").prop("checked",true);
		$("#btnClassIPModal").attr("disabled","disabled");
	} else if($("#accessRestrictionTypeDiv").attr("data-accesstype") == AccessRestrictionType.PRIVATE) {
		$("#raIpPrivate").prop("checked",true);
		$("#btnClassIPModal").removeAttr("disabled");
	}
}

function attachListenerOnCertificate() {
	$("#chWithCertificate").on("change",function() {
		if($(this).prop("checked") == true) {
			$("#chWithCertificate").attr("data-withcertificate",WithCertificate.TRUE);
			$("#withCertificate").css("display","");
			$("#classSignatoriesDiv").css("display","");
			if($("#raSystemTemplate").prop("checked") != true) {
				$("#customUploadDiv").css("display","");
			} else {
				$("#customUploadDiv").css("display","none");
			}
		} else {
			$("#chWithCertificate").attr("data-withcertificate",WithCertificate.FALSE);
			$("#customUploadDiv").css("display","none");
			$("#withCertificate").css("display","none");
			$("#classSignatoriesDiv").css("display","none");
		}
	});
	
	$("#raSystemTemplate").on("change", function () {
		$("#withCertificate").attr("data-template", CertificateType.SYSTEMTEMPLATE);
		$("#customUploadDiv").css("display","none");
	});
	
	$("#raCustomTemplate").on("change", function () {
		$("#withCertificate").attr("data-template",CertificateType.CUSTOMTEMPLATE);
		$("#customUploadDiv").css("display","");
	});
	
	$("#chIsDownloadable").on("change", function () {
		if($("#chIsDownloadable").prop("checked") == true) {
			$("#isDownloadable").attr("data-downloadable",CertificateDownloadable.TRUE);
		} else {
			$("#isDownloadable").attr("data-downloadable",CertificateDownloadable.FALSE);
		}
	});
}

function attachListenerOnSelfRegistration() {
	$("#chIsSelfRegister").on("change",function() {
		if($(this).prop("checked") == true) {
			$("#chIsSelfRegister").attr("data-selfregister",IsSelfRegister.TRUE);
			$("#selfRegistrationDiv").css("display","");
			if($("#raOpenIndefinite").prop("checked") == true) {
				$("#specifiedPeriodDiv").css("display","none");
			} else if($("#raOpenSpecifiedPeriod").prop("checked") == true) {
				$("#specifiedPeriodDiv").css("display","");
			}
		} else {
			$("#chIsSelfRegister").attr("data-selfregister",IsSelfRegister.FALSE);
			$("#selfRegistrationDiv").css("display","none");
			$("#specifiedPeriodDiv").css("display","none");
		}
	});
	
	$("#raOpenIndefinite").on("change", function () {
		$("#selfRegistrationDiv").attr("data-selfregistertype",SelfRegisterType.OPENINDEFINITELY);
		$("#specifiedPeriodDiv").css("display","none");
	});
	
	$("#raOpenSpecifiedPeriod").on("change", function () {
		$("#selfRegistrationDiv").attr("data-selfregistertype",SelfRegisterType.SPECIFIEDPERIOD);
		$("#specifiedPeriodDiv").css("display","");
	});
}

function attachListenerOnViewingRestrictions() {
	$("#raAccessTrainingDurationOnly").on("change", function () {
		$("#viewRestrictionDiv").attr("data-viewrestriction",ViewRestrictions.TRAININGDURATION);
		$("#specifiedRestrictionsDiv").css("display","none");
	});
	
	$("#raAccessIndefinitely").on("change", function () {
		$("#viewRestrictionDiv").attr("data-viewrestriction",ViewRestrictions.INDEFINITE);
		$("#specifiedRestrictionsDiv").css("display","none");
	});
	
	$("#raAccessSpecifiedPeriod").on("change", function () {
		$("#viewRestrictionDiv").attr("data-viewrestriction",ViewRestrictions.SPECIFIEDPERIOD);
		$("#specifiedRestrictionsDiv").css("display","");
	});
}

function attachListenerOnAccessRestrictions() {
	$("#raIpPublic").on("change", function () {
		$("#accessRestrictionTypeDiv").attr("data-accesstype",AccessRestrictionType.PUBLIC);
		$("#btnClassIPModal").attr("disabled","disabled");
	});
	
	$("#raIpPrivate").on("change", function () {
		$("#accessRestrictionTypeDiv").attr("data-accesstype",AccessRestrictionType.PRIVATE);
		$("#btnClassIPModal").removeAttr("disabled");
	});
}

function initClassIPAddressTbl() {
	var classId = parseInt(GetURLParameter('classId'));
	var pageSize = 5;
	$("#classIpTblPagination").pagination({     
		dataSource: "classinfo/"+classId+"/classipaddress/pages",
		locator: 'classIpAddresses',
		totalNumberLocator: function(response) {            
			return response.totalRecords;
		},
		className: 'paginationjs-theme-blue',
		pageSize: pageSize,     
		callback: function(data) {                      
			$("#classIpTblBody").empty(); 
			_.each(data, function(classipaddress) {
				createClassIpAddressRow(classipaddress);
			});         
		}
	});
}

function initIPAddressTbl() {
	var classId = parseInt(GetURLParameter('classId'));
	var pageSize = 5;
	$("#ipaddressPagination").pagination({     
		dataSource: "ipaddresses/pages/class/"+classId,
		locator: 'ipaddresses',
		totalNumberLocator: function(response) {            
			return response.totalRecords;
		},
		className: 'paginationjs-theme-blue',
		pageSize: pageSize,     
		callback: function(data) {                      
			$("#ipaddressTblBody").empty(); 
			_.each(data, function(ipaddress) {
				createIpAddressRow(ipaddress);
			});         
		}
	});
}
function createClassIpAddressRow(classipaddress) {
	$("#classIpTblBody").append($("<tr/>")
		.append($("<td/>").addClass("mins")
			.append($("<a/>").addClass("waves-light waves-effect").attr({
				"href":"#!",
				"data-classipaddressid":classipaddress.classIpId,
				"data-classid":classipaddress.classId})
				.bind("click",function() {
					deleteClassIpAddress($(this).data("classipaddressid"));
				})
				.append($("<i/>").addClass("material-icons").html("delete"))))
		.append($("<td/>").attr("style","text-align:left;")
		.html(classipaddress.ipAddress)));
}

function createIpAddressRow(ipaddress) {
	var classId = parseInt(GetURLParameter('classId'));
	$("#ipaddressTblBody").append($("<tr/>")
		.append($("<td/>").addClass("mins")
			.append($("<label/>")
				.append($("<input/>").attr({"type":"checkbox"})
				.prop("checked",checkIfItemExist(ipaddress.ipAddressId))
				.bind("change",function() {
					if($(this).prop("checked") != true) {
						if(checkIfItemExist(ipaddress.ipAddressId)) {
							deleteClassIpFromCheckList(ipaddress);
						}
					} else {
						if(!checkIfItemExist(ipaddress.ipAddressId)) {
							ipaddress.classId = classId;
							checkIpAddress.push(ipaddress);
						}
					}
				}))
				.append($("<span/>"))))
		.append($("<td/>").attr("style","text-align:left;")
		.html(ipaddress.ipAddress)));
}

function checkIfItemExist(ipAddressId) {
	var result = false;
	_.each(checkIpAddress,function(ipAddressInfo) {
		if(ipAddressInfo.ipAddressId==ipAddressId) {
			result=true;
		}
	});
	return result;
}

function deleteClassIpFromCheckList(ipAddressInfo) {
	var result = false;
	_.each(checkIpAddress,function(ipObj) {
		if(ipObj.ipAddressId == ipAddressInfo.ipAddressId) {
			checkIpAddress = deleteItem(ipAddressInfo);
		}
	});
	return result;
}

function deleteItem(ipAddressInfo) {
	var resultArray = [];
	_.each(checkIpAddress,function(ipObj) {
		if(ipObj.ipAddressId != ipAddressInfo.ipAddressId) {
			resultArray.push(ipObj);
		}
	});
	return resultArray;
}

function attachListenerToAddClassIpAddressBtn() {
	$("#addClassIPAddressBtn").on("click",function() {
		if(checkIpAddress.length == 0) {
			alertify.warning("Please select at least one IP Address from the table.");
		} else {
			createClassIpAddress(checkIpAddress);
		}
	});
}

function attachListenerToSaveSettingsBtn() {
	$("#btnSaveSettings").on("click",function() {
		var pass = true;
		var message = "";
		
		if(($("#raOpenSpecifiedPeriod").is(":checked") && $("#chIsSelfRegister").is(":checked")) || $("#raAccessSpecifiedPeriod").is(":checked")    ){
			if($("#tbSpecifiedStart").val().trim() == "" || $("#tbSpecifiedEnd").val().trim() == ""){
				message = "Self Register Start and End Date is required!";
				alertify.warning(message);
				pass = false;
			}
			else if($("#tbAccessSpecifiedStart").val().trim() == "" || $("#tbAccessSpecifiedEnd").val().trim() == ""){
				message = "Access Start and End Date is required!";
				alertify.warning(message);
				pass = false;
			}else{
				pass = true;
			}
		}
		
		if(pass){
			$("#btnSaveSettings").attr("disabled","disabled");
			$("#btnSaveSettings").html("Saving...");
			updateClassSettings();
		}
	});
}

function populateClassSettings() {
	classinfo = {};
	
	classinfo.classId = parseInt(GetURLParameter('classId'));
	classinfo.classDuration = $("#tbClassDuration").val();
	classinfo.isSelfRegister = $("#chIsSelfRegister").attr("data-selfregister");
	classinfo.selfRegisterType = $("#selfRegistrationDiv").attr("data-selfregistertype");
	classinfo.selfRegisterStartDate = $("#tbSpecifiedStart").val().trim() == "" ? null : $("#tbSpecifiedStart").val();
	classinfo.selfRegisterEndDate = $("#tbSpecifiedEnd").val().trim() == "" ? null : $("#tbSpecifiedEnd").val();
	classinfo.withCertificate = $("#chWithCertificate").attr("data-withcertificate");
	classinfo.certificateTemplateType = $("#withCertificate").attr("data-template");
	classinfo.certificateUrl = $("#fuCustomCertificate").val();
	classinfo.isCertificateDownloadable = $("#isDownloadable").attr("data-downloadable");
	classinfo.viewRestrictionType = $("#viewRestrictionDiv").attr("data-viewrestriction");
	classinfo.accessRestrictionType = $("#accessRestrictionTypeDiv").attr("data-accesstype");
	classinfo.accessStartDate = $("#tbAccessSpecifiedStart").val().trim() == "" ? null : $("#tbAccessSpecifiedStart").val();
	classinfo.accessEndDate = $("#tbAccessSpecifiedEnd").val().trim() == "" ? null : $("#tbAccessSpecifiedEnd").val();
	
	
	var rawArray=[];
	$("#classSignatoryList li").each(function(row, li){
		rawArray[row]={
			"classId" : parseInt(GetURLParameter('classId')),
			"employeeId" : $(li).find('select[id^="signatory-"]').val(),
			"orderNo": row+1
		}	
	})
	
	var classSignatoriesArray = rawArray.filter((cs) => { return cs.employeeId != null &&  cs.employeeId != "null" });
	
	classinfo.classSignatories = classSignatoriesArray;
	
	return classinfo;
}

function attachListenerToAddClassIpAddressModalBtn() {
	$("#btnClassIPModal").on("click",function() {
		initClassIPAddressTbl();
		initIPAddressTbl();
	});
}

function updateClassSettings() {
	var classinfo = populateClassSettings();
	var classId = parseInt(GetURLParameter('classId'));
	
	$.when(ajax.customUpdate("classinfo/"+classinfo.classId+"/classsettings",classinfo)).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				alertify.success("Successfully saved!");
				setTimeout(function(){ redirect("admin.classsettings?classId="+classinfo.classId); }, 3000);
		}
	});
}

function createClassIpAddress(classIpAddress) {
	var classId = parseInt(GetURLParameter('classId'));
	$.when(ajax.customUpdate("classinfo/"+classId+"/classipaddresses",classIpAddress)).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				alertify.success("Successfully added!");
				defineList();
				initClassIPAddressTbl();
				initIPAddressTbl();
				break;
		}
	});
}

function deleteClassIpAddress(classIpId) {
	var classId = parseInt(GetURLParameter('classId'));
	$.when(ajax.remove("classinfo/"+classId+"/classipaddress/",classIpId)).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				defineList();
				initClassIPAddressTbl();
				initIPAddressTbl();
				break;
		}
	});
}

function attachListenerToUploadPhotoLink() {
	$("#uploadPhotoLink").on("click",function() {
		$("#fileUpload").trigger("click");
	});
}

function attachListenerToUploadPhoto() {
	$("#fileUpload").on("change",function(e) {
		if(validateFile()) {
			handleRefreshEvent();
			updateClassPhoto();
		}
	});
}

function validateFile() {
	var result = true;
	if($("#fileUpload").get(0).files.length==0) {
		result = false;
	}
	if(!validateFileType("#fileUpload","IMAGE")) {
		alertify.warning("Invalid File Type. Please Try another one.");
		result = false;
	} 
	if(!validateFileSize("#fileUpload","IMAGE")) {
		result = false;
	}
	return result;
}

function updateClassPhoto() {
	blockUI("Uploading Media File..");
	var classId = $("#classId").val();
	$.when(ajax.upload("classinfo/"+classId+"/classPhotoUrl","uploadPhotoFrm")).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				alertify.success("Successfully Updated!");
				$("#classPhoto").attr("src",response.classInfo.classPhotoUrl);
				$(window).unbind('beforeunload');
				$.unblockUI();
				break;
		}
	});
}