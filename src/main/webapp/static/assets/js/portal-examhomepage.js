$(function() {
	initPageSettings();
	initStarExamBtnSectionName();
	initExamSectionTbl();
	initFileUploadDisplay();
	attachListenerToFileUpload();
});
function initPageSettings() {
	$("#examType").html(getExamTypeDesc($("#hdnExamType").val()));
}

function initStarExamBtnSectionName() {
	var classId = parseInt($("#hdnClassId").val());
	var examId = parseInt($("#hdnExamId").val());
	var courseId = GetURLParameter("courseId");
	console.log("check courseId" + courseId);
	$.when(ajax.fetch("classexam/"+classId+"/classemployeeexam/"+examId+"/nextsection")).done(
		function(response) {
			switch(response.status) {
				case HttpStatus.SUCCESS:
					var examQuestion = response.examQuestion;
					if(examQuestion.totalRecords != 0) {
						$("#startExamBtn").html("Start "+examQuestion.sectionName);
						!courseId 
							? $("#startExamBtn").attr("href",`exam-main?classId=${classId}&examId=${examQuestion.examId}&sectionOrder=${examQuestion.sectionOrder}`)
							: $("#startExamBtn").attr("href",`exam-main?classId=${classId}&courseId=${courseId}&examId=${examQuestion.examId}&sectionOrder=${examQuestion.sectionOrder}`)
						
					} else {
						$("#startExamBtn").attr("href","preview-class?classId="+classId);
						$("#startExamBtn").html("Back to class");
					}
					break;
			}
		}
	)
}

function initFileUploadDisplay() {
	if($("#hdnAllowAttachment").val()==AllowAttachment.TRUE) {
		$("#fileUploadCon").show();
	} else {
		$("#fileUploadCon").hide();
	}
}

function getExamTypeDesc(examType) {
	var examTypeDesc="";
	_.each(ExamTypes,function(examTypeObj) {
		if(examTypeObj.id==examType) {
			examTypeDesc=examTypeObj.description;
		}
	});
	return examTypeDesc;
}

function initExamSectionTbl() {
	var examId=$("#hdnExamId").val();
	var classId=$("#hdnClassId").val();
	$.when(ajax.fetch("classinfo/"+classId+"/examinfo/"+examId+"/examquestions/totalpoints")).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				populateExamSectionTbl(response.examQuestions);
				break;
		}
	});
}

function populateExamSectionTbl(examQuestions) {
	$("#examSectionTbl").append(
		$("<tr/>"))
	_.each(examQuestions,function(examQuestion) {
		$("#examSectionTbl").append($("<tr/>")
			.append($("<td/>").html(examQuestion.sectionName))
			.append($("<td/>").html(examQuestion.instruction))
			.append($("<td/>").html(examQuestion.totalPoints))
			.append($("<td/>").html(getEmployeeExamStatusDesc(examQuestion.status))));
	});	
}

function getEmployeeExamStatusDesc(status) {
	console.log(status);
	var result="";
	if(SessionStatus.NOT_YET_TAKEN==status || status==null) {
		result="Not Yet Taken";
	}else if(SessionStatus.IN_PROGRESS == status){
		result="In Progress";
	}else if(SessionStatus.TAKEN == status){
		result="Taken";
	}else {
		result="Finished";
	}
	return result;
}

function attachListenerToFileUpload() {
	$("#fileUpload").on("change",function() {
		if(validateFileUpload()) {
			handleRefreshEvent();
			uploadAttachmentFile();
		}
	});
}

function validateFileUpload() {
	var result = true;
	if($("#fileUpload").get(0).files.length==0) {
		result = false;
	}
	if(!validateFileType("#fileUpload","IMAGE") && !validateFileType("#fileUpload","ARCHIVE")) {
		alertify.warning("Invalid File Type. Please Try another one.");
		result = false;
	}
	return result;
}

function uploadAttachmentFile() {
	blockUI("Uploading Media File..");
	var classId =$("#hdnClassId").val();
	var examId =$("#hdnExamId").val();
	$.when(ajax.upload("classexam/"+classId+"/classemployeeexam/"+examId+"/attachmentUrl","fileUploadFrm")).done(
		function(response) {
			switch(response.status) {
				case HttpStatus.SUCCESS:
					alertify.success("Successfully Uploaded!");
					$(window).unbind('beforeunload');
					$.unblockUI();
					break;
			}
	});
}