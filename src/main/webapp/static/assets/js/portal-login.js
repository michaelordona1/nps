/**
 * 
 */

$(function() {
	loginFailedCheck();
	attachListenerToResetBtn();
});

function loginFailedCheck() {
	if(GetURLParameter("logincode") == "incorrect"){
		alertify.alert("Login Failed", "Login Credentials are incorrect. Please try again.");
	}
}

function attachListenerToResetBtn(){
	$("#resetBtn").click(function(){
		var username = $("#username").val();
		
		if(username.trim() != ""){
			var user = {};
			user.username = username;
			$.when(ajax.update("users/password/",username, user)).done(function(response) {
				if(response.status == HttpStatus.SUCCESS){
					$("#chgePassModal").modal('close');
					alertify.alert('Reset Password', 'Password has successfully changed and it was sent to your email.');
				}
			});	
		}else{
			alertify.warning("Username cannot be empty.");
		}
		
		
		
			
	})
}