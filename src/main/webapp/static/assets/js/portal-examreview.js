$(function () {
	defineVariables();
	//initPageSettings();
	initQuestionList();
	attachListenerToNextBtn();
	attachListenerToPrevBtn();
	attachListenerToQuestionNoLink();
});

function defineVariables() {
	questionList = [];
	matchingQuestionList = [];
	mapMatchingShuffledQuestionList = new Map();
	noOfQuestions = 0;
	prevQuestionNo = 0;
	nextQuestionNo = 2;
	employeeExamAnswerList = [];
	choiceList = [];
	correctMultipleChoiceAnswersCtr = 0;
	currentQuestionNo=0;
	examId = $("#hdnExamId").val();
	sectionOrder = $("#hdnSectionOrder").val();
	classId = $("#hdnClassId").val();
	courseId =$("#hdnCourseId").val();
}


/*function initPageSettings() {
	handleRefreshEvent();
}*/

function initQuestionList() {
	$.when(ajax.fetch("examinfos/" + examId + "/examquestions/" + sectionOrder)).done(function (response) {
		switch (response.status) {
			case HttpStatus.SUCCESS:
				questionList = response.examQuestions;
				var lastQuestionId = 0;
				let iterator = 0;

				_.each(questionList, function (question) {

					if (question.questionTypeId == QuestionType.ORDERING
						|| question.questionTypeId == QuestionType.MATCHING
						|| question.questionTypeId == QuestionType.ENUMERATION
						|| question.questionTypeId == QuestionType.FILL_IN_THE_BLANK) {

						if (lastQuestionId != question.questionId) {
							lastQuestionId = question.questionId;
							iterator++;
						}

					} else {
						iterator++;
					}
					noOfQuestions = iterator;
				});
				initQuestionNumberCon();
				$("#totalQuestions").html(noOfQuestions);
				initChoicesList();
				break;
		}
	});
}

function initQuestion(questionNo) {
	var question = getQuestionData(questionNo);
	currentQuestionNo = questionNo;
	renderNavigationButtons(questionNo);
	$("#currentQuestionNo").html(questionNo);
	$("#hdnExamQuestionId").val(question.examQuestionId);
	$("#hdnIsRandomized").val(question.isRandomized);
	
	$.when(ajax.fetch("class/"+classId+"/exam/"+examId+"/question/"+question.questionId+"/reviewer")).done(function(response){
		employeeExamAnswerList = response.employeeExamAnswers;
	
		switch (question.questionTypeId) {
			case QuestionType.MULTIPLE_CHOICE:
				initQuestionImage("#multipleChoicePicCon", question.questionId, "#choiceLightBox");
				populateMultipleChoiceCon(question);
				break;
			case QuestionType.IDENTIFICATION:
				initQuestionImage("#identificationPicCon", question.questionId, "#identificationLightBox");
				populateIdentification(question);
				break;
			case QuestionType.FILL_IN_THE_BLANK:
				initQuestionImage("#fillInTheBlanksPicCon", question.questionId, "#fillInTheBlanksLightBox");
				populateFillInTheBlanks(questionNo);
				break;
			case QuestionType.TRUE_OR_FALSE:
				initQuestionImage("#trueFalsePicCon", question.questionId, "#trueOrFalseLightBox");
				populateTrueOrFalseCon(question);
				break;
			case QuestionType.ESSAY:
				initQuestionImage("#essayPicCon", question.questionId, "#essayLightBox");
				populateEssay(question);
				break;
			case QuestionType.MATCHING: 
				initQuestionImage("#matchingPicCon", question.questionId);
				populateMatching(questionNo);
				break;
			case QuestionType.ORDERING:
				initQuestionImage("#orderingPicCon", question.questionId, "#orderingLightBox");
				populateOrdering(questionNo);
				break;
			case QuestionType.ENUMERATION:
				initQuestionImage("#enumerationPicCon", question.questionId, "#enumerationLightBox");
				populateEnumeration(questionNo);
				break;
		}
	
	})

	
	
}

function initQuestionImage(dom, questionId, lightBoxDom) {
	$(dom).empty();
	$.when(ajax.fetch("question/"+questionId+"/images")).done(function(response){
		showQuestionImageCon(dom, response.questionImages.length);
		_.each(response.questionImages, function(image){
			$(dom).append(
				$("<img>").addClass("imgPos")
							.attr({"src": image.mediaUrl, "id": "image-"+image.questionImageId})
							.bind("click", function(){
								window.open($(this).attr("src"))
						    })
			)
		})
	})
}

function showQuestionImageCon(dom, questionImagesSize) {
	/*$("#multipleChoicePicCon").hide();
	$("#identificationPicCon").hide();
	$("#enumerationPicCon").hide();
	$("#orderingPicCon").hide();
	$("#fillInTheBlanksPicCon").hide();
	$("#trueFalsePicCon").hide();
	$("#enumerationCon").hide();
	$("#matchingPicCon").hide();
	$("#essayPicCon").hide();*/
	
	if (questionImagesSize > 0) {
		$(dom).show();
	}else{
		$(dom).hide();
	}
}

function showQuestionDiv(dom) {
	$("#multipleChoiceCon").hide();
	$("#identificationCon").hide();
	$("#trueOrFalseCon").hide();
	$("#orderingCon").hide();
	$("#fillInTheBlanksCon").hide();
	$("#enumerationCon").hide();
	$("#essayCon").hide();
	$("#matchingCon").hide();

	$(dom).show();
}

function renderNavigationButtons(currentQuestionNo) {
	if (currentQuestionNo == 1) {
		$("#prevBtn").hide();
	} else {
		$("#prevBtn").show();
	}
	if (currentQuestionNo >= noOfQuestions) {
		$("#nextBtn").hide();
	} else {
		$("#nextBtn").show();
	}
}

function getQuestionData(questionNo) {
	var questionObj = {};
	var iterator = 0;
	var lastQuestionId = 0;
	_.each(questionList, function (question) {
		if (lastQuestionId != question.questionId) {
			lastQuestionId = question.questionId;
			iterator++;
		}
		if (iterator == questionNo) {
			questionObj = question;
		}
	});
	return questionObj;
}

function initQuestionNumberCon() {
	$("#questionNumbersContainer").append(createQuestionNumberCon());
}

function createQuestionNumberCon() {
	var min = 1;
	var max = 8;
	var ctr = 1;
	var html = [];
	html.push("<tr>");
	while (noOfQuestions >= ctr) {
		if (min <= ctr && ctr <= max) {
			html.push("<td><a href='#!' class='questionNoLink' id='questionNo-"+ctr+"' data-ctr='" + ctr + "'>" + ctr + "</a></td>");
			ctr++;
		} else {
			html.push("</tr>");
			html.push("<tr>");
			min += 1;
			max += 8;
		}
	}
	return html.join(" ");
}

function attachListenerToNextBtn() {
	$("#nextBtn").on("click", function () {
		initQuestion(nextQuestionNo);
		nextQuestionNo++;
		prevQuestionNo++;
	});
}

function attachListenerToPrevBtn() {
	$("#prevBtn").on("click", function () {
		initQuestion(prevQuestionNo);
		nextQuestionNo--;
		prevQuestionNo--;
	});
}

function createQuestionAnswer() {
	var questionTypeId = parseInt($("#hdnQuestionType").val());
	var tempEmployeeExamAnswerList = [];
	if (questionTypeId == QuestionType.MULTIPLE_CHOICE) {
		tempEmployeeExamAnswerList = recordMultipleChoiceAnswer();
	} else if (questionTypeId == QuestionType.IDENTIFICATION) {
		tempEmployeeExamAnswerList = recordIdentificationAnswer();
	} else if (questionTypeId == QuestionType.ORDERING) {
		tempEmployeeExamAnswerList = rec
	}
	return tempEmployeeExamAnswerList;
}

function attachListenerToQuestionNoLink() {
	$("body").on("click", ".questionNoLink", function () {
		var questionNo = parseInt($(this).data("ctr"));
		prevQuestionNo = questionNo - 1;
		nextQuestionNo = questionNo + 1;
		initQuestion(questionNo);
	});
}

//-----------------------------------------------Start: Multiple Choice-------------------------------------------//
function populateMultipleChoiceCon(question) {
	
	var employeeExamAnswerList2 = employeeExamAnswerList.filter(answer => answer.questionId == question.questionId);
	
	showQuestionDiv("#multipleChoiceCon");
	$("#multipleChoiceQuestion").html("");
	var html = [];
	html.push("<span>" + question.content + "<span/>");
	$("#multipleChoiceQuestion").append(html.join(" "));
	$("#hdnQuestionId").val(question.questionId);
	$("#hdnQuestionType").val(question.questionTypeId);
	fetchMultipleChoiceChoices(question.questionId);
}

function initChoicesList() {
	$.when(ajax.fetch("choices")).done(function (response) {
		switch (response.status) {
			case HttpStatus.SUCCESS:
				choiceList = response.choices;
				initQuestion(1);
				break;
		}
	});
}

function fetchMultipleChoiceChoices(questionId) {
	var tempChoices = [];
	correctMultipleChoiceAnswersCtr = 0;
	_.each(choiceList, function (choice) {
		if (choice.questionId == questionId) {
			tempChoices.push(choice);
			if(choice.choiceType){
				correctMultipleChoiceAnswersCtr++;
			}
		}
	});
	populateMultipleChoiceChoices(tempChoices);
}

function populateMultipleChoiceChoices(choices) {
	$("#multipleChoiceChoices").html("");
	var choiceCtr=0;
	_.each(choices, function (choice) {
		var html = [];
		html.push("<label>");
		html.push(getMultipleChoiceRadioBtn(choice, choiceCtr));
		html.push("<label/><br>")
		$("#multipleChoiceChoices").append(html.join(" "));
		choiceCtr++;
	});
}

function getMultipleChoiceRadioBtn(choice, choiceCtr) {
	var questionId = parseInt($("#hdnQuestionId").val());
	var radioBtnHtml = "";
	
	if(correctMultipleChoiceAnswersCtr > 1){
		radioBtnHtml =  "<input type='checkbox' id='choiceId-"+choiceCtr+"' name='choices[]' class='choice' value='"+choice.choiceId+"'> " +
						"<span style='color:"+(choice.choiceType ? "blue" : "red")+"'>"+choice.choiceDesc+"</span>";
	}else{
		radioBtnHtml = "<input type='radio' name='choices[]' class='choice' value='" + choice.choiceId + "'> " +
						"<span style='color:"+(choice.choiceType ? "blue" : "red")+"'>"+choice.choiceDesc+"</span>";
	}
	
	_.each(employeeExamAnswerList, function (employeeExamAnswer) {
		if (employeeExamAnswer.choiceId == choice.choiceId && employeeExamAnswer.questionId == questionId) {
			if(correctMultipleChoiceAnswersCtr > 1){
				radioBtnHtml =  "<input type='checkbox' name='choices[]'  class='choice' value='"+choice.choiceId+"' checked> " +
								"<span style='color:"+(choice.choiceType ? "blue" : "red")+"'>"+choice.choiceDesc+"</span>";
			}else{
				radioBtnHtml = "<input type='radio' name='choices[]' value='" + choice.choiceId + "' checked> " +
								"<span style='color:"+(choice.choiceType ? "blue" : "red")+"'>"+choice.choiceDesc+"</span>";
			}
		}
	})
	return radioBtnHtml;
}

//-----------------------------------------------End: Multiple Choice-------------------------------------------//
//-----------------------------------------------Start: Identification-------------------------------------------//
function populateIdentification(question) {
	showQuestionDiv("#identificationCon");
	$("#identificationQuestion").html("");
	$("#identificationQuestion").append("<span/>" + question.content + "</span>");
	$("#identificationAnswerTxtBox").val("");
	$("#hdnQuestionId").val(question.questionId);
	$("#hdnQuestionType").val(question.questionTypeId);
	if(question.mediaUrl != null){
		$("#identificationMedia").attr("src",question.mediaUrl);
	}
	_.each(employeeExamAnswerList, function (employeeExamAnswer) {
		if (employeeExamAnswer.questionId == question.questionId) {
			$("#identificationAnswerTxtBox").val(employeeExamAnswer.answer).css("color", employeeExamAnswer.isCorrect ? "blue" : "red");
			
		}
	});
}
//-----------------------------------------------End: Identification-------------------------------------------//
//-----------------------------------------------Start: True Or False-------------------------------------------//
function populateTrueOrFalseCon(question) {
	showQuestionDiv("#trueOrFalseCon");
	$("#hdnQuestionId").val(question.questionId);
	$("#trueOrFalseQuestionCon").html("");
	$("#trueOrFalseQuestionCon").html(question.content);
	$("#hdnQuestionType").val(question.questionTypeId);
	$("#trueRdBtn").prop("checked", false);
	$("#falseRdBtn").prop("checked", false);
	_.each(employeeExamAnswerList, function (employeeExamAnswer) {
		if (employeeExamAnswer.questionId == question.questionId) {
			if (employeeExamAnswer.answer == "true") {
				$("#trueRdBtn").prop("checked", true);
			} else {
				$("#falseRdBtn").prop("checked", true);
			}
			$("#falseSpan").css("color", "grey"); 
			$("#trueSpan").css("color", "grey");
			$("#"+employeeExamAnswer.answer+"Span").css("color", (employeeExamAnswer.isCorrect ? "blue" : "red"));
		}
	})
}
//-----------------------------------------------End: True Or False-------------------------------------------//
//-----------------------------------------------Start: Ordering----------------------------------------------//
function populateOrdering(questionNo) {
	showQuestionDiv("#orderingCon");

	var shuffledOrderingList = [];
	var orderingQuestionList = [];
	let iterator = 0;
	var savedQuestionId = 0;
	var questionId = 0;
	var index=0;
	_.each(questionList, function (question) {
		if (savedQuestionId != question.questionId) {
			savedQuestionId = question.questionId;
			iterator++;
		}
		if (iterator == questionNo) {
			questionId = question.questionId;
			shuffledOrderingList.push(question);
			$("#orderingMedia").attr("src",question.mediaUrl);
		}
	});
	$("#hdnQuestionId").val(questionId);
	orderingQuestionList = employeeExamAnswerList.filter(question => question.questionId == questionId);
	
	if (Array.isArray(orderingQuestionList) && orderingQuestionList.length) {
		var lastOrderingList = [];
		_.each(orderingQuestionList, function(question){
			let selectedQuestion = shuffledOrderingList.find(element => question.answer == element.orderingId);
			lastOrderingList.push(selectedQuestion);
		});
		shuffledOrderingList = lastOrderingList.slice();
	} else {
		shuffledOrderingList = shuffle(shuffledOrderingList);
	}
	
	$("#sortable").empty();
	_.each(shuffledOrderingList, function (question) {
		$("#orderingContent").html(question.content)
		$("#hdnQuestionType").val(question.questionTypeId);
		$("#sortable")
			.append($("<li/>").attr({
				"data-id": question.orderingId,
				"data-text": question.orderingAnswer
			}).css("color", orderingQuestionList[index].isCorrect ? "blue" : "red")
				.append($(`<span><div class="sortItem"> ${question.orderingAnswer}</div></span>`))
			)
		index++;
	});
}


//-----------------------------------------------End: Ordering------------------------------------------------//
//-----------------------------------------------Start: Enumeration-------------------------------------------//
function populateEnumeration(questionNo) {
	showQuestionDiv("#enumerationCon");
	var enumerationQuestionList = [];
	var savedEnumerationAnswerList = [];
	let iterator = 0;
	var savedQuestionId = 0;
	var questionId = 0;
	var index = 0;
	_.each(questionList, function (question) {
		if (savedQuestionId != question.questionId) {
			savedQuestionId = question.questionId;
			iterator++;
		}
		if (iterator == questionNo) {
			questionId = question.questionId;
			enumerationQuestionList.push(question);
			$("#hdnQuestionType").val(question.questionTypeId);
			$("#enumerationContent").html(question.content);
			$("#enumerationMedia").attr("src",question.mediaUrl);
		}
	});
	$("#hdnQuestionId").val(questionId);
	savedEnumerationAnswerList = employeeExamAnswerList.filter(question => question.questionId == questionId);
	
	if (Array.isArray(savedEnumerationAnswerList) && savedEnumerationAnswerList.length) {
		enumerationQuestionList = savedEnumerationAnswerList.slice();
	} 

	$("#enumerationColumn").empty();
	
	_.each(savedEnumerationAnswerList, function(employeeExamAnswer){
		$("#enumerationColumn")
			.append($("<input/>").attr({
				"data-id": index,
				"type" : "text",
				"id" : "enumerationInput_" + index,
				"value" : employeeExamAnswer.answer,
				"class" : "enumerationField"
				})
				.css("color",employeeExamAnswer.isCorrect ? "blue" : "red")
			)		
		index++;
	})
	
}
//-----------------------------------------------End: Enumeration---------------------------------------------//
//-----------------------------------------------Start: Matching----------------------------------------------//
function populateMatching(questionNo) {
	showQuestionDiv("#matchingCon");
	$("#matchingQuestions").empty();
	$("#matchingQuestionsCompare").empty();
	
	var savedMatchingAnswerList = [];
	var A = 65;
	var tempMatchingQuestionList = [];
	mapMatchingShuffledQuestionList = new Map();
	let iterator = 0;
	var savedQuestionId = 0;
	var questionId = 0;
	var matchingTypeCharEnumerator = A;
	var index=0;

	_.each(questionList, function (question) {

		if (savedQuestionId != question.questionId) {
			savedQuestionId = question.questionId;
			iterator++;
		}

		if (iterator == questionNo) {
			questionId = question.questionId;
			$("#hdnQuestionType").val(question.questionTypeId);
			$("#matchingQuestionCon").html(question.content);
			tempMatchingQuestionList.push(question);
			$("#matchingQuestions").append($("<li/>")
				.append($("<input/>").attr({
					"type":"text",
					"class":"spanWidth matchingTxtBox",
					"id": "matching-" + index
					})
				)
				.append($("<span/>").html(question.givenA))
			);
			console.log(index);
			index++;
		}
	});
	savedMatchingAnswerList = employeeExamAnswerList.filter(exam => exam.questionId == questionId); 
	$("#hdnQuestionId").val(questionId);

	if (matchingQuestionList.length === 0) {
		matchingQuestionList = tempMatchingQuestionList.slice();
		matchingQuestionList = shuffle(matchingQuestionList);
	}

	_.each(shuffle(tempMatchingQuestionList), function(questionObj) {

		if (!questionObj.givenBMediaUrl) {
			$("#matchingQuestionsCompare").append($("<li/>")
				.append($("<span/>").html(questionObj.givenB))
			);
		} else {
			$("#matchingQuestionsCompare").append($("<li/>")
				.append($("<img/>").addClass("fixSmallImg").attr({"src" : questionObj.givenBMediaUrl}))
			);
		}
		mapMatchingShuffledQuestionList.set(matchingTypeCharEnumerator, questionObj);
		matchingTypeCharEnumerator++;
	});
	
	index = 0;
	if (savedMatchingAnswerList.length > 0) {
		_.each(savedMatchingAnswerList, function(matchingAnswer) {
			var key = getByValue(mapMatchingShuffledQuestionList, matchingAnswer.answer);
			$("#matching-"+index).val(String.fromCharCode(key)).css("color", matchingAnswer.isCorrect ? "blue" : "red");
			index++;
		}) ;
	}
}


//-----------------------------------------------End: Matching------------------------------------------------//
//-----------------------------------------------Start: Essay----------------------------------------------//
function populateEssay(question) {
	showQuestionDiv("#essayCon");
	$("#essayMedia").attr("src",question.mediaUrl);
	$("#essayQuestionCon").html(question.content);
	$("#essayForm").empty();
	$("#essayForm").append($("<div/>").attr({"id":"essayAnswer_" + question.questionId,"class":"txtarea"}));
	initTinyMceExamEditor(".txtarea" , 500);
	employeeExamAnswer = employeeExamAnswerList.filter(exam => exam.questionId == question.questionId);
	tinymce.activeEditor.setContent(employeeExamAnswer[0].answer, {format: 'html'});
}

//-----------------------------------------------End: Essay------------------------------------------------//
//-----------------------------------------------Start: Fill in the Blanks------------------------------------//
function populateFillInTheBlanks(questionNo) {
	$("#fillInTheBlanksQuestions").empty();
	showQuestionDiv("#fillInTheBlanksCon");
	var fillInTheBlanksQuestionList = [];
	let iterator = 0;
	var savedQuestionId = 0;
	var questionId = 0;
	var dom;
	var index = 0;
	
	_.each(questionList, function(question) {
		if (savedQuestionId != question.questionId) {
			savedQuestionId = question.questionId;
			iterator++;
		}

		if (iterator == questionNo) {
			if (isEmpty($('#fillInTheBlanksQuestions'))) {
				$("#fillInTheBlanksMedia").attr("src",question.mediaUrl);

				dom = document.getElementById("fillInTheBlanksQuestions");
				dom.innerHTML = question.content;
				
				$(dom).find("input").each(function(inputIndex, inputValue){
					$(inputValue).attr("readonly",false);
					$(inputValue).val("");
				})
				
				$(dom).find("select").each(function(selectIndex, selectValue){
					var tes=[];
					$(this).find("option").each(function(optionIndex, optionValue){
						tes.push(optionValue);
					});
					$(this).empty();
					$(this).append(shuffle(tes));
				})				
				
			}	
			questionId = question.questionId;
			
			var componentCode = `#${question.fillIntheBlanksAnswerComponentCode}`;
			fillInTheBlanksQuestionList.push(question);
			$("#hdnQuestionType").val(question.questionTypeId);
			$("#fillInTheBlanksImageMedia").attr("src",question.mediaUrl);
			$("#fillInTheBlanksSpan").html(question.instruction);
			$("#fillInTheBlanksSpan").html(question.instruction);
			$(componentCode).removeAttr("readonly", "selected");
			$("option").removeAttr("selected");
			$(componentCode).val("");

			$(componentCode).trigger("contentChanged");
		}
	});
	
	
	$("#hdnQuestionId").val(questionId);
	var test2 = employeeExamAnswerList.filter(answer => answer.questionId == questionId);
	_.each(fillInTheBlanksQuestionList, function(question){
		if($("#fillInTheBlanksQuestions").find("table").length > 0){
			$("#fillInTheBlanksQuestions").find("tr").each(function(trIndex, theTr){
				$(this).find("td").each(function(tdIndex, tdValue){
					$(tdValue).find("input").each(function(theInput, theValue){
						$(this).attr("disabled",true);
						$(this).val(test2[index].answer).css("color", test2[index].isCorrect ? "blue" : "red");
						index++;
					})
					$(tdValue).find("select").each(function(theInput, theValue){
						$(this).attr("disabled",true);
						$(this).val(test2[index].answer).css("color", test2[index].isCorrect ? "blue" : "red");
						index++;
					})
				})
			})
		}else{
			
			/*$(dom).find("[id="+question.fillIntheBlanksAnswerComponentCode+"]").each(function(theIndex, theValue){
				if($(theValue).is("input")){
					$(theValue).attr("readonly",true);
					$(theValue).val(test2[index].answer).css("color", test2[index].isCorrect ? "blue" : "red");
					$(theValue).addClass("styleWidth");
				}else{
					var tes=[];
					$(theValue).find("option").each(function(optionIndex, optionValue){
						tes.push(optionValue);
					});
					$(theValue).empty();
					$(theValue).append(shuffle(tes));
					$(theValue).val(test2[index].answer).css("color", test2[index].isCorrect ? "blue" : "red");
				}
				index++;
			})	*/
			
			$(dom).find("input").each(function(inputIndex, inputValue){
				if(inputValue.id == question.fillIntheBlanksAnswerComponentCode){
					$(this).attr("disabled",true);
					$(this).val(test2[index].answer).css("color", test2[index].isCorrect ? "blue" : "red");
					index++;
				}
			})
			
			$(dom).find("select").each(function(selectIndex, selectValue){
				if(selectValue.id == question.fillIntheBlanksAnswerComponentCode){
					var tes=[];
					$(this).find("option").each(function(optionIndex, optionValue){
						tes.push(optionValue);
					});
					$(this).attr("disabled",true);
					$(this).empty();
					$(this).append(shuffle(tes));
					$(this).val(test2[index].answer).css("color", test2[index].isCorrect ? "blue" : "red");
					index++;
				}
			})		
		}
	})
	
	
}


//-----------------------------------------------End: Fill in the Blanks--------------------------------------//

function isEmpty(dom) {
	return !$.trim(dom.html());
}

function initTinyMceExamEditor(dom,height) {
	return tinymce.init({
		  selector: dom,
		  plugins: Editor.PLUGINS,
		  toolbar: Editor.TOOLBAR,
		  theme: 'silver',
		  menubar: Editor.MENUBAR,
		  paste_as_text: true,
		  height: height,
		  branding:false,
		  readonly : 1
	});
}

function getByValue(map, searchValue) {
	for (let [key, value] of map.entries()) {
		if (value.givenB == searchValue) {
			return key;
		} 
	}
	return searchValue;
}


