$(function() {
	defineVariables();
	//initClassSchedule();
	populateScheduleDropdown();
	initLearningMaterialsSlickSettings();
	initLearningMaterialsSlider();
	initExamSlickSettings();
	initExamSlider();
	initEvaluationSlickSettings();
	initEvaluationSlider();
	initLearningPaths();
	attachListenerToContentUrlLink();
	attachListenerToFinishBtn();
});
function defineVariables() {
	scheduleSet = [];
	noOfLearningMaterials = 0;
	noOfExams = 0;
	noOfEvaluations = 0;
	learningPathList=[];
	allEvaluations=[];
	allLearningPaths=[];
	slideToShow = 5;
	slideToScroll = 3;
}

/*function initClassSchedule() {
	var classId=$("#classId").val();
	$.when(ajax.fetch("classinfo/"+classId+"/classschedule")).done(function(response) {
		var classInfo = response.classInfo;
		switch(classInfo.scheduleType) {
			case ScheduleType.BLOCK:
				populateClassBlockSchedule(classInfo.classBlockSchedule);
				break;
			case ScheduleType.SET:
				populateClassSetSchedule(classInfo.classSetSchedules);
				break;
		}
	});
}

function populateClassBlockSchedule(classBlockSchedule) {
	$("#classScheduleDiv").append($("<p>").html(classBlockSchedule.startDate+" "+classBlockSchedule.startTime
			+" to "+classBlockSchedule.endDate+" "+classBlockSchedule.endTime));
}

function populateClassSetSchedule(classSetSchedules) {
	_.each(classSetSchedules, function(classSetSchedule) {
		$("#classScheduleDiv").append($("<p>").html(classSetSchedule.startDate + " " + classSetSchedule.startTime
		+ " to " + classSetSchedule.endDate + " " + classSetSchedule.endTime));
	});
}*/

function initClassScheduleList() {
	var pageSize = 10;
	$("#idSchedulePagination").pagination({
		dataSource: scheduleSet,
		className: 'paginationjs-theme-blue',
		pageSize: pageSize,
		callback: function (data) {
			$("#classScheduleTblBody").empty();
			addSavedRecordValues(data);
		}
	});
}

function addSavedRecordValues(scheduleList) {
	_.each(scheduleList, function (schedule) {
		$("#classScheduleTblBody").append($("<tr/>")
			.append($("<td/>").html(schedule.date))
			.append($("<td/>").html(schedule.startTime))
			.append($("<td/>").html(schedule.endTime)));

	});
}

function populateScheduleDropdown() {
	var classId=$("#classId").val();
	var classSchedule = "";
	var startDate = "";
	var endDate = "";
	var date1 = "";
	var date2 = "";
	var startTime = "";
	var endTime = "";

	var month = new Array();
	month[0] = "January"; month[1] = "February"; month[2] = "March"; month[3] = "April";
	month[4] = "May"; month[5] = "June"; month[6] = "July"; month[7] = "August";
	month[8] = "September"; month[9] = "October"; month[10] = "November";
	month[11] = "December";

	$.when(ajax.fetch("classinfo/" + classId + "/classschedule")).done(function (response) {
		if (response.status == HttpStatus.SUCCESS) {
			if (response.classInfo.scheduleType == ScheduleType.BLOCK) {
				classSchedule = response.classInfo.classBlockSchedule;
				startDate = classSchedule.startDate;
				endDate = classSchedule.endDate;
				date1 = new Date(startDate);
				date2 = new Date(endDate);
				startTime = classSchedule.startTime;
				endTime = classSchedule.endTime;
				time1 = new Date(date1.toDateString() + " " + startTime);
				time2 = new Date(date2.toDateString() + " " + endTime);

				$("#classScheduleTblBody").html("");
				var dateDiff = time2 - time1
				var mins = Math.floor(dateDiff / 60000);
				totalAverageTime = mins;
				//showAverageTrainingTimeLabel();
				for (var d = date1; d <= date2; d.setDate(d.getDate() + 1)) {

					var dayStartTime = "";
					var dayEndTime = "";
					var startTimeHr = parseInt(startTime.substring(0, 2));
					var startTimeMin = startTime.substring(3, 5);
					var endTimeHr = parseInt(endTime.substring(0, 2));
					var endTimeMin = endTime.substring(3, 5);

					if (startTimeHr == 0) {
						dayStartTime = `12:${startTimeMin}:00 AM`;
					} else if (startTimeHr > 12) {
						dayStartTime = `${convertNumbertoString(startTimeHr - 12)}:${startTimeMin}:00 PM`;
					} else if (startTimeHr == 12) {
						dayStartTime = `${startTimeHr}:${startTimeMin}:00 PM`;
					} else {
						dayStartTime = `${convertNumbertoString(startTimeHr)}:${startTimeMin}:00 AM`
					}

					if (endTimeHr == 0) {
						dayEndTime = `12:${endTimeMin}:00 AM`
					} else if (endTimeHr > 12) {
						dayEndTime = `${convertNumbertoString(endTimeHr - 12)}:${endTimeMin}:00 PM`
					} else if (endTimeHr == 12) {
						dayEndTime = `${endTimeHr}:${endTimeMin}:00 PM`
					} else {
						dayEndTime = `${convertNumbertoString(endTimeHr)}:${endTimeMin}:00 AM`
					}
					var date = month[date1.getMonth()] + " " + date1.getDate() + ", " + date1.getFullYear();
					$
					var schedule = setSchedule(date, dayStartTime, dayEndTime);
					scheduleSet.push(schedule);
					$("#scheduleDropdown").append($("<option/>").attr("value", `${date1.getFullYear}-${date1.getMonth}-${date1.getDate}`).html(date));
				}
				initClassScheduleList();
				$("#scheduleDropdown").trigger("contentChanged");

			} else if (response.classInfo.scheduleType == ScheduleType.SET) {
				classSchedule = response.classInfo.classSetSchedules;
				$("#classScheduleTblBody").html("");

				_.each(classSchedule, function (classSetSchedule) {

					startDate = classSetSchedule.startDate;
					endDate = classSetSchedule.endDate;
					date1 = new Date(startDate);
					date2 = new Date(endDate);
					startTime = classSetSchedule.startTime;
					endTime = classSetSchedule.endTime;
					var dateDiff = endTime - startTime
					var mins = Math.floor(dateDiff / 60000);
					totalAverageTime = mins;
					//showAverageTrainingTimeLabel();
					for (var d = date1; d <= date2; d.setDate(d.getDate() + 1)) {
						var dayStartTime = "";
						var dayEndTime = "";
						var startTimeHr = parseInt(startTime.substring(0, 2));
						var startTimeMin = startTime.substring(3, 5);
						var endTimeHr = parseInt(endTime.substring(0, 2));
						var endTimeMin = endTime.substring(3, 5);

						if (startTimeHr == 0) {
							dayStartTime = String(`12:${startTimeMin}:00 AM`);
						} else if (startTimeHr > 13) {
							dayStartTime = String(`0${convertNumbertoString(startTimeHr - 12)}:${startTimeMin}:00 PM`);
						} else if (startTimeHr == 12) {
							dayStartTime = String(`${startTimeHr}:${startTimeMin}:00 PM`);
						} else {
							dayStartTime = String(`${startTimeHr}:${startTimeMin}:00 AM`)
						}

						if (endTimeHr == 0) {
							dayEndTime = `12:${endTimeMin}:00 AM`
						} else if (endTimeHr > 13) {
							dayEndTime = `${convertNumbertoString(endTimeHr - 12)}:${endTimeMin}:00 PM`
						} else if (endTimeHr == 12) {
							dayEndTime = `${endTimeHr}:${endTimeMin}:00 PM`
						} else {
							dayEndTime = `${endTimeHr}:${endTimeMin}:00 AM`
						}
						var date = month[date1.getMonth()] + " " + date1.getDate() + ", " + date1.getFullYear();
						var schedule = setSchedule(date, dayStartTime, dayEndTime);
						$("#scheduleDropdown").append($("<option/>").attr("value", `${date1.getFullYear()}-${date1.getMonth()}-${date1.getDate()}`).html(date));

						scheduleSet.push(schedule);
					}
					initClassScheduleList();
					$("#scheduleDropdown").trigger("contentChanged");
				});

			} else if (response.classInfo.scheduleType == ScheduleType.SERIES) {

				$.when(ajax.fetch("classinfo/" + classId + "/classscheduled")).done(function (response) {
					if (response.status == HttpStatus.SUCCESS) {
						if ($("#scheduleType").val() == ScheduleType.SERIES) {
							classSchedules = response.classInfo.classSeriesSchedules;
							$("#classScheduleTblBody").html("");
							_.each(classSchedules,function(classSchedule) {
								startDate = classSchedule.startDate;
								endDate = classSchedule.endDate;
								date1 = new Date(startDate);
								date2 = new Date(endDate);
								startTime = classSchedule.startTime;
								endTime = classSchedule.endTime;
								time1 = new Date(date1.toDateString()+" "+startTime);
								time2 = new Date(date2.toDateString()+" "+endTime);
								
								var dayStartTime = "";
								var dayEndTime = "";
								var startTimeHr = parseInt(startTime.substring(0, 2));
								var startTimeMin = startTime.substring(3, 5);
								var endTimeHr = parseInt(endTime.substring(0, 2));
								var endTimeMin = endTime.substring(3, 5);

								if (startTimeHr == 0) {
									dayStartTime = `12:${startTimeMin}:00 AM`;
								} else if (startTimeHr > 12) {
									dayStartTime = `${convertNumbertoString(startTimeHr - 12)}:${startTimeMin}:00 PM`;
								} else if (startTimeHr == 12) {
									dayStartTime = `${startTimeHr}:${startTimeMin}:00 PM`;
								} else {
									dayStartTime = `${convertNumbertoString(startTimeHr)}:${startTimeMin}:00 AM`
								}

								if (endTimeHr == 0) {
									dayEndTime = `12:${endTimeMin}:00 AM`
								} else if (endTimeHr > 12) {
									dayEndTime = `${convertNumbertoString(endTimeHr - 12)}:${endTimeMin}:00 PM`
								} else if (endTimeHr == 12) {
									dayEndTime = `${endTimeHr}:${endTimeMin}:00 PM`
								} else {
									dayEndTime = `${convertNumbertoString(endTimeHr)}:${endTimeMin}:00 AM`
								}
								
								$("#classScheduleTblBody").append($("<tr/>")
									.append($("<td/>").html(month[date1.getMonth()]+" "+date1.getDate()+", "+date1.getFullYear()))
									.append($("<td/>").html(dayStartTime))
									.append($("<td/>").html(dayEndTime)));
							});
						}
					}
				});
			}
		}
	});
	
	
}

function convertNumbertoString(d) {
	return (d < 10) ? '0' + d.toString() : d.toString();
}

function setSchedule(date, startTime, endTime) {
	schedule = {};
	schedule.date = date;
	schedule.startTime = startTime;
	schedule.endTime = endTime;
	return schedule;
}

function initLearningMaterialsSlickSettings() {
	var learningMaterialSlider = $("#learningMaterialsSlider").slick({
		infinite:false,
		slidesToShow:slideToShow,
		slidesToScroll:slideToScroll,
		prevArrow: "<img class='slick-prev' src='static/assets/images/slick-button/prev-button.png'>",
		nextArrow: "<img class='slick-next' src='static/assets/images/slick-button/next-button.png'>"
	});
	
	$("#learningMaterialsSlider .slick-prev").hide();
	
	learningMaterialSlider.on("afterChange", function(event, slick, currentSlide){
		if(currentSlide === 0){
			$("#learningMaterialsSlider .slick-prev").hide();
			$("#learningMaterialsSlider .slick-next").show();
		}else{
			$("#learningMaterialsSlider .slick-prev").show();
		}
		
		var push = slideToShow - slideToScroll;
		var result = (slick.slideCount - currentSlide) <= push ? true : false;
		
		if(result){
			$("#learningMaterialsSlider .slick-next").hide();
		}
	})	
}

function initExamSlickSettings() {
	var examSlider = $("#classExamSlider").slick({
		infinite:false,
		slidesToShow:slideToShow,
		slidesToScroll:slideToScroll,
		prevArrow: "<img class='slick-prev' src='static/assets/images/slick-button/prev-button.png'>",
		nextArrow: "<img class='slick-next' src='static/assets/images/slick-button/next-button.png'>"
	});
	
	$("#classExamSlider .slick-prev").hide();
	
	
	examSlider.on("afterChange", function(event, slick, currentSlide){
		if(currentSlide === 0){
			$("#classExamSlider .slick-prev").hide();
			$("#classExamSlider .slick-next").show();
		}else{
			$("#classExamSlider .slick-prev").show();
		}
		
		var push = slideToShow - slideToScroll;
		var result = (slick.slideCount - currentSlide) <= push ? true : false;
		
		if(result){
			$("#classExamSlider .slick-next").hide();
		}
	})	
}

function initEvaluationSlickSettings() {
	var evaluationSlider = $("#evaluationSlider").slick({
		infinite:false,
		slidesToShow:slideToShow,
		slidesToScroll:slideToScroll,
		prevArrow: "<img class='slick-prev' src='static/assets/images/slick-button/prev-button.png'>",
		nextArrow: "<img class='slick-next' src='static/assets/images/slick-button/next-button.png'>"
	});
	
	$("#evaluationSlider .slick-prev").hide();
	
	
	evaluationSlider.on("afterChange", function(event, slick, currentSlide){
		if(currentSlide === 0){
			$("#evaluationSlider .slick-prev").hide();
			$("#evaluationSlider .slick-next").show();
		}else{
			$("#evaluationSlider .slick-prev").show();
		}
		
		var push = slideToShow - slideToScroll;
		var result = (slick.slideCount - currentSlide) <= push ? true : false;
		
		if(result){
			$("#evaluationSlider .slick-next").hide();
		}
	})	
}

function initLearningMaterialsSlider() {
	createClassMaterialSlides();
}

function createClassMaterialSlides() {
	var classId=$("#classId").val();
	$.when(ajax.fetch("classinfo/"+classId+"/classmaterials")).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				var classMaterials = response.classMaterials;
				populateLearningMaterialsSlider(classMaterials);
				noOfLearningMaterials+=classMaterials.length;
				displayNoSlideText("#learningMaterialsSlider","There is no learning material for this class.",
					noOfLearningMaterials);
				break;
		}
	});
}

function displayNoSlideText(dom,message,listSize) {
	if(listSize==0) {
		$(dom).append("<center><p>"+message+"</p></center>");
	}
}

function populateLearningMaterialsSlider(materialsList) {
	_.each(materialsList,function(materialObject) {
		$("#learningMaterialsSlider").slick("slickAdd",createLearningMaterialSlideDiv(materialObject));
	});
}

function createLearningMaterialSlideDiv(material) {
	var divHtml = [];
	divHtml.push("<div>")
	divHtml.push(createDivLink(material));
    divHtml.push("<div class='divcard' style='margin: 5px;''>")
    divHtml.push("<h6 style='margin: 0;'><b>"+material.fileLabel+"</b></h6>")
    divHtml.push("<p><label><span>"+getContentType(material.contentType)+"</span></label></p>");
	divHtml.push("</div></a>");
	divHtml.push("</div>");
	return divHtml.join(" ");
}

function createDivLink(material) {
	var html = [];
	var classId=$("#classId").val();
	if(material.contentType != ContentType.URL) {
		html.push("<a href='preview-material?classId="+classId+"&classMaterialId="+material.classMaterialId+"' ");
		html.push("class='viewMaterialsLink' data-type='"+material.contentType+"'>");
	} else {
		html.push("<a href='"+material.contentUrl+"' class='viewMaterialsLink' target='_blank'>");
	}
	return html.join(" ");
}


function getContentType(contentType) {
	var contentTypeDesc="";
	switch(contentType) {
		case ContentType.DOCUMENT:
			contentTypeDesc="Document";
			break;
		case ContentType.VIDEO:
			contentTypeDesc="Video";
			break;
		case ContentType.AUDIO:
			contentTypeDesc="Audio";
			break;
		case ContentType.IMAGE:
			contentTypeDesc="Image";
			break;
		case ContentType.FLASH:
			contentTypeDesc="Flash";
			break;
		case ContentType.URL:
			contentTypeDesc="URL";
			break;									
	}
	return contentTypeDesc;
}

function initExamSlider() {
	createClassExamSlides();
}

function createClassExamSlides() {
	var result = $.Deferred();
	var classId = $("#classId").val();
	$.when(ajax.fetch("classinfo/"+classId+"/classexams")).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				var classExams = response.classExams;
				
				if($("#trainingStatus").val() == TrainingStatus.COMPLETED){
					classExams = classExams.filter(exam => exam.remarks != "NOT YET STARTED");
				}
				
				populateExamSlider(classExams);
				
				
				result.resolve("Success");
				noOfExams+=classExams.length;
				displayNoSlideText("#classExamSlider","There is no exam for this class.",noOfExams);
				break;
		}
	});
	return result.promise();
}

function populateExamSlider(examList) {
	_.each(examList,function(examObj) {
		$("#classExamSlider").slick("slickAdd",createExamSlideDiv(examObj));
	});
}

function createExamSlideDiv(examObj) {
	var classId=$("#classId").val();
	var examHtml = [];
	var noOfRetake=examObj.noOfRetake-examObj.noOfTimesTaken;

	!examObj.courseId 
		?	examHtml.push(`<a href="exam-home?classId=${classId}&examId=${examObj.examId}">`) 
		:   examHtml.push(`<a href="exam-home?classId=${classId}&examId=${examObj.examId}&courseId=${examObj.courseId}">`)
	examHtml.push("<div class='divcard' style='margin: 5px;''>");
	examHtml.push("<h6 style='margin: 0;''><b>"+examObj.title+"</b></h6>("+getExamTypeDesc(examObj.examType)+")");
	examHtml.push("<p><label><span>Duration: "+examObj.duration+" minutes</span></label></p>");
	examHtml.push("<p><label><span>Passing Score: "+examObj.passingScore+"</span></label></p>");
	examHtml.push("<p><label><span>No Of Retake Left: "+noOfRetake+"</span></label></p>");
	examHtml.push("<p><label><span>Status: "+examObj.remarks+"</span></label></p></div></div>");
	return examHtml.join(" ");
}

function getExamTypeDesc(examType) {
	var examTypeDesc="";
	_.each(ExamTypes,function(examTypeObj) {
		if(examTypeObj.id==examType) {
			examTypeDesc=examTypeObj.description;
		}
	});
	return examTypeDesc;
}

function initEvaluationSlider() {
	var classId = $("#classId").val();
	var result=$.Deferred();
	//$.when(createCourseEvaluationSlides()).then(function(status) {
		//Todo
		$.when(ajax.fetch("classInfo/classevaluations/list?classId="+ classId)).done(function(response) {
			switch(response.status) {
				case HttpStatus.SUCCESS:
					var classEvaluations=response.classevaluations;
					
					if($("#trainingStatus").val() == TrainingStatus.COMPLETED){
						classEvaluations = classEvaluations.filter(evaluationObj => evaluationObj.employeeEvaluationId > 0);
					}
					
					populateEvaluationSlider(classEvaluations);
					noOfEvaluations+=classEvaluations.length;
					result.resolve("Success");
					if (noOfEvaluations === 0) {
						displayNoSlideText("#evaluationSlider","There is no evaluation for this class.",noOfEvaluations);
					}
					break;
			}
		});
	//});
	return result.promise();
}

function createCourseEvaluationSlides() {
	var courseId = $("#courseId").val();
	var result=$.Deferred();
	$.when(ajax.fetch("courseinfo/"+courseId+"/courseevaluations/list")).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				var courseEvaluations=response.courseevaluations;
				
				if($("#trainingStatus").val() == TrainingStatus.COMPLETED){
					courseEvaluations = courseEvaluations.filter(evaluationObj => evaluationObj.employeeEvaluationId > 0);
				}				
				
				populateEvaluationSlider(courseEvaluations);
				noOfEvaluations+=courseEvaluations.length;
				result.resolve("Success");
				break;
		}
	});
	return result.promise();
}

function populateEvaluationSlider(evaluationList) {
	allEvaluations = evaluationList;
	_.each(evaluationList,function(evaluationObj) {
		$("#evaluationSlider").slick("slickAdd",createEvaluationSlideDiv(evaluationObj));
	});
}

function createEvaluationSlideDiv(evaluationObj) {
	var htmlDiv=[];
	var classId=$("#classId").val();
	var courseId=$("#courseId").val();
	htmlDiv.push("<div>");
	if (evaluationObj.courseEvaluationId > 0 ) {
		htmlDiv.push(`<a href='class-evaluation-test?classId=${classId}&courseEvaluationId=${evaluationObj.evaluationId}&courseId=${courseId}'>`);
	} else {
		htmlDiv.push(`<a href='class-evaluation-test?classId=${classId}&classEvaluationId=${evaluationObj.evaluationId}'>`);
	}
	htmlDiv.push("<div class='divcard' style='margin: 5px;''>");
	htmlDiv.push("<h6 style='margin: 0;''><b>"+evaluationObj.title+"</b></h6>");
	htmlDiv.push("<p><label><span>Status: "+((evaluationObj.employeeEvaluationId > 0)?"SUBMITTED":"NOT YET STARTED")+"</span></label></p>");
	htmlDiv.push("</div></div>");
	return htmlDiv.join(" ");
}

function initLearningPaths() {
	var classId=$("#classId").val();
	$.when(ajax.fetch("classinfo/"+classId+"/learningpaths")).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				learningPathList=response.learningPaths;
				var learningPaths = response.learningPaths;
				
				if($("#trainingStatus").val() == TrainingStatus.COMPLETED){
					learningPaths = learningPaths.filter(learningPath => learningPath.sessionStatus == SessionStatus.FINISHED);
				}			
				
				populateLearningPathSection(response.learningPathSections,learningPaths);
				break;
		}
	})			
}

function populateLearningPathSection(learningPathSections,learningPaths) {
	allLearningPaths = learningPaths;
	_.each(learningPathSections,function(learningPathSection) {
		$("#learningPathCon").append($("<table/>").addClass("striped highlight")
			.append($("<thead/>").append($("<tr/>")
			.append($("<th>").attr("colspan","4")
				.append($("<span/>").addClass("left").html(learningPathSection.sectionTitle)))))
			.append($("<tbody/>").attr("id","sectionNo_"+learningPathSection.learningPathSectionId)));
	});
	populateLearningPath(learningPaths);
	
	if($("#trainingStatus").val() != TrainingStatus.COMPLETED){
		enableFinishBtn();
	}
}

function populateLearningPath(learningPaths) {
	_.each(learningPaths,function(learningPath) {
		$("#sectionNo_"+learningPath.learningPathSectionId)
			.append((learningPath.itemType==ItemType.MATERIAL)?
				getLearningPathMaterialsRow(learningPath):getLearningPathExamRow(learningPath));
	});
}

function getLearningPathMaterialsRow(learningPath) {
	var html=[];
	html.push("<tr style='background-color: "+(getSessionStatusValue(learningPath.sessionStatus) == "DONE" ? "#DEDEDE" : "")+"' >");
	html.push("<td>"+learningPath.fileLabel+"("+getContentType(learningPath.contentType)+")"+"</td>");
	html.push("<td>"+((learningPath.lastAccessed!=null)?"Last Accessed:"+learningPath.lastAccessed:"")+"</td/>");
	html.push("<td>"+((learningPath.sessionStatus!=SessionStatus.NOT_YET_TAKEN)?
		"Total Time Spent:"+learningPath.timeSpent+" minutes":"")+"</td>");
	html.push("<td>"+getSessionStatusValue(learningPath.sessionStatus)+"</td>");
	html.push("<td>"+getLearningPathActionBtn(learningPath)+"</td>");
	html.push("<tr/>");
	return html.join(" ");
}

function getSessionStatusValue(sessionStatus) {
	var sessionStatusDesc="";
	if(sessionStatus==SessionStatus.FINISHED) {
		sessionStatusDesc="DONE";
	} else if(sessionStatus==SessionStatus.IN_PROGRESS) {
		sessionStatusDesc="In Progress";
	} else {
		sessionStatusDesc="Not Yet Taken";
	}
	return sessionStatusDesc;
}

function getLearningPathExamRow(learningPath) {
	var html=[];
	html.push("<tr style='background-color: "+(getSessionStatusValue(learningPath.sessionStatus) == "DONE" ? "#DEDEDE" : "")+"' >");
	html.push("<td>"+learningPath.title+"("+getExamType(learningPath.examType)+")</td>");
	html.push("<td>Duration: "+learningPath.duration + " minutes</td>");
	html.push("<td>Passing Score: "+learningPath.passingScore+"%</td>");
	html.push("<td>No. Of Retake :"+learningPath.noOfRetake+"</td>");
	html.push("<td>"+getLearningPathActionBtn(learningPath)+"</td>");
	html.push("<tr/>");
	return html.join(" ");
}

function getExamType(examType) {
	var result = "";
	_.each(ExamTypes,function(examTypeObj) {
		if(examTypeObj.id==examType) {
			result=examTypeObj.description;
		}
	});
	return result;
}

function getLearningPathActionBtn(learningPath) {
	var html = [];
	var classId=$("#classId").val();
	var lastSubOrderNo=getLastItemSubOrderNo(learningPath.subOrderNo);
	var lastLearningPathItem=getLearningPathObj(learningPath.learningPathSectionId,lastSubOrderNo);
	
	if(learningPath.sessionStatus==SessionStatus.FINISHED) {
		if(learningPath.contentType ==ContentType.URL && learningPath.sessionStatus==SessionStatus.FINISHED) {
			html.push("<a href='"+learningPath.contentUrl+"' data-id="+learningPath.learningPathId);
			html.push("class='contentUrlLink' ><i class='material-icons'>play_arrow</i></a>");
		} else {
			if(learningPath.itemType == ItemType.MATERIAL){
				html.push("<a href='preview-material?classId="+classId+"&learningPathId="+learningPath.learningPathId+"'");
				html.push("data-id="+learningPath.learningPathId+">")
				html.push("<i class='material-icons'>play_arrow</i></a>");				
			}else{
				if(!learningPath.courseId){
					html.push(`<a href="exam-home?classId=${classId}&examId=${learningPath.examId}"`);
					html.push("class='bttn btn waves-effect waves-light'>"+(learningPath.sessionStatus==SessionStatus.FINISHED ? "View" : "Start")+"</a>");	
				}else{
					html.push(`<a href="exam-home?classId=${classId}&examId=${learningPath.examId}&courseId=${learningPath.courseId}"`);
					html.push("class='bttn btn waves-effect waves-light'>"+(learningPath.sessionStatus==SessionStatus.FINISHED ? "View" : "Start")+"</a>");	
				}				
			}
		}
		
	} else if(lastLearningPathItem.sessionStatus==SessionStatus.FINISHED || learningPath.subOrderNo==1) {
		if(learningPath.contentType!=ContentType.URL) {
			if(learningPath.itemType == ItemType.MATERIAL){
				html.push("<a href='preview-material?classId="+classId+"&learningPathId="+learningPath.learningPathId+"'");
				html.push("class='bttn btn waves-effect waves-light'>Start</a>");					
			}else{
				if(!learningPath.courseId){
					html.push(`<a href="exam-home?classId=${classId}&examId=${learningPath.examId}"`);
					html.push("class='bttn btn waves-effect waves-light'>"+(learningPath.sessionStatus==SessionStatus.FINISHED ? "View" : "Start")+"</a>");	
				}else{
					html.push(`<a href="exam-home?classId=${classId}&examId=${learningPath.examId}&courseId=${learningPath.courseId}"`);
					html.push("class='bttn btn waves-effect waves-light'>"+(learningPath.sessionStatus==SessionStatus.FINISHED ? "View" : "Start")+"</a>");	
				}
			}
		} else {
			html.push("<a href='"+learningPath.contentUrl+"' data-id="+learningPath.learningPathId);
			html.push("class='contentUrlLink bttn btn waves-effect waves-light'>Start</a>");
		}
		
	} 
	
	
	return html.join(" ");	
}

function getLastItemSubOrderNo(subOrderNo) {
	var lastItemSubOrderNo=subOrderNo-1;
	return ((lastItemSubOrderNo)==0)?1:lastItemSubOrderNo;
}

function getLearningPathObj(sectionId,subOrderNo) {
	var learningPathObj={};
	_.each(learningPathList,function(learningPath){
		if(learningPath.subOrderNo==subOrderNo && learningPath.learningPathSectionId==sectionId) {
			learningPathObj=learningPath;
		}
	});
	return learningPathObj;
}

function attachListenerToContentUrlLink() {
	$("body").on("click",".contentUrlLink",function() {
		var learningPathId=parseInt($(this).data("id"));
		createLearningPathSessionTimer(learningPathId);
	});
}

function createLearningPathSessionTimer(learningPathId) {
	var classId=$("#classId").val();
	$.when(ajax.customUpdate("classinfo/"+classId+"/learningpathsessiontimer/"+learningPathId)).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
			break;
		}
	})
} 

function attachListenerToFinishBtn() {
	$("#finishBtn").on("click",function() {
		alertify.confirm("Finish Class", "Are you sure all examinations are taken and all learning materials are finished?",
				function(){
					if(validateClassLearningPath()) {
						updateEmployeeTrainingStatus();
						var classId=$("#classId").val();
						//redirect to in-progress tab
						redirect("mytraining-inprogress",300);
					}
				},
				null
		);
	});
}

function updateEmployeeTrainingStatus() {
	var classId=$("#classId").val();
	$.when(ajax.customUpdate("classinfo/"+classId+"/classemployee/trainingStatus")).done(
		function(response) {
			switch(response.status) {
				case HttpStatus.SUCCESS:
					alertify.success("Congratulations! You have successfully finished this class");
					break;
				case HttpStatus.TIME_SPENT_IS_NOT_ENOUGH:
					alertify.warning(Message.TIME_SPENT_IS_NOT_ENOUGH);
					break;
			}
	});
}

function validateClassLearningPath() {
	var result=true;
	if(!checkCompletedClassMaterials()) {
		alertify.warning("Please finish all class materials first.");
		result=false;
	}
	return result;
}

function checkCompletedClassMaterials() {
	var result=true;
	_.each(learningPathList,function(learningPath) {
		if(learningPath.itemType==ItemType.MATERIAL && learningPath.sessionStatus!=SessionStatus.FINISHED) {
			result=false;
		}
	});
	return result;
}

function enableFinishBtn(){
	var countUnfinishedLearningPath = allLearningPaths.filter(item => item.sessionStatus === 0).length;
	var countAnsweredEvaluations = allEvaluations.filter(item => item.employeeEvaluationId > 0 ).length;
	
	if($("#evaluationRequired").val() == 0 || $("#evaluationRequired").val() == 2 || !$("#evaluationRequired").val()) {
		$("#finishBtn").attr("disabled", (countUnfinishedLearningPath == 0 ? false : true));
	}else{
		$("#finishBtn").attr("disabled",(countUnfinishedLearningPath == 0 && countAnsweredEvaluations > 0 ? false : true));
	}
	
}