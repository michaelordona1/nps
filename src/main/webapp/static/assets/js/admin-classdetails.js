// * 
// */

$(function(){
	defineGlobalList();
	
	initClassFields();
	allowNumbersOnly();
	initCourseInfoFields();
	populateClassFields();
	populateScheduleDropdown();
	initLearningPathTbl();
	attachListenerToScheduleTypeDropdownChanged();
	attachListenerToMonthlyRadioButtonChanged();
	attachListenerToRecurrenceDropdownChanged();
	attachListenerToDaysCheckBox();
	attachListenerToOccurenceDropDown();
	attachListenerToClassSchedule();
	attachListenerToAttendeeScale();
	attachListenerToAddSetSchedule();
	attachListenerToSaveButton();
	attachListenerToAssignCounter();
	attachListenerToUploadPhotoLink();
	attachListenerToUploadPhoto();
});

function defineGlobalList() {
	tempClassSchedules = [];
	tempDays = [];
	tempCheckedDays = [];
	tempChecklist = [];
	tempSeriesParameter = [];
	setCounter = 1;
}


function initClassFields() {
	var classId = $("#classId").val();
	$.when(ajax.fetch("locations/status/" + Status.ACTIVE)).done(function(response) {
		populateLocationDropdown(response.locations);
	});
	$.when(ajax.fetch("employeeinfo/trainers/status/"+Status.ACTIVE)).done(function(response) {
		populateEmployeeDropdown(response.employeeInfos);
		$.when(ajax.fetch("classinfo/"+classId+"/classtrainer")).done(function(response) {
			var classEmployee = response.classEmployee;
			$("#trainerDropdown").val(classEmployee.employeeId).trigger("change");
		})
	})
	
}

function initCourseInfoFields() {
	var courseId = $("#courseId").val();
	$.when(ajax.fetch("courseinfo/" + courseId)).done(function(response) {
		if(response.status == HttpStatus.SUCCESS){
			var courseInfo = response.courseInfo;
			$("#divCourseDescription").html(courseInfo.description);
			$("#divCourseObjective").html(courseInfo.objective);
			$("#courseId").val();
		}
	});
}

function populateLocationDropdown(locations) {
	$("#locationDropdown").html("");
	$("#locationDropdown").append($("<option/>").attr({"selected":"","disabled":"disabled"}));
	_.each(locations,function(location) {
	$("#locationDropdown").append($("<option/>").attr("value",location.locationId).html(location.locationName));
	})
	$("#locationDropdown").val($("#locationId").val());
}

function populateEmployeeDropdown(employeeInfos) {
	$("#trainerDropdown").html("");
	$("#trainerDropdown").append($("<option/>").attr({"selected":"","disabled":"disabled"}));
	_.each(employeeInfos,function(employeeInfo) {
		$("#trainerDropdown").append($("<option/>").attr("value",employeeInfo.employeeId).html(employeeInfo.fullName));
	})
}

function populateClassFields() {
	
	$("#scheduleTypeDropdown").val($("#scheduleType").val()).trigger("change");
	
	if($("#deliveryMethod").val() == DeliveryMethod.INDIVIDUAL){
		$("#mainLearningPath").css("display","none");
		$("#mainChecklist").css("display","none");
		$("#deliveryMethodLabel").html("Individual");
	} else if($("#deliveryMethod").val() == DeliveryMethod.MODULAR){
		$("#mainChecklist").css("display","none");
		$("#deliveryMethodLabel").html("Modular");
	} else if($("#deliveryMethod").val() == DeliveryMethod.CLASSTRAINING){
		$("#mainLearningPath").css("display","none");
		$("#deliveryMethodLabel").html("Classroom Training");
	}
	
	if($("#isSelfRegister").val() == IsSelfRegister.TRUE){
		$("#selfRegCheckBox").prop("checked",true).trigger('change');
	} else if($("#isSelfRegister").val() == IsSelfRegister.FALSE){
		$("#selfRegCheckBox").prop("checked",false).trigger('change');
	}
	if($("#withCertificate").val() == WithCertificate.TRUE){
		$("#certificateCheckBox").prop("checked",true).trigger('change');
	} else if($("#withCertificate").val() == WithCertificate.FALSE){
		$("#certificateCheckBox").prop("checked",false).trigger('change');
	}
	if($("#exam").val() == WithExam.TRUE){
		$("#examCheckbox").prop("checked",true).trigger('change');
	} else if($("#exam").val() == WithExam.FALSE){
		$("#examCheckbox").prop("checked",false).trigger('change');
	}
	
	if($("#scheduleTypeDropdown").val() == "1"){
		$("#blockSchedule").css("display","");
		$("#setSchedule").css("display","none");
		$("#seriesSchedule").css("display","none");
	}else if($("#scheduleTypeDropdown").val() == "2"){
		$("#blockSchedule").css("display","none");
		$("#setSchedule").css("display","");
		$("#seriesSchedule").css("display","none");
	}else if($("#scheduleTypeDropdown").val() == "3"){
		$("#blockSchedule").css("display","none");
		$("#setSchedule").css("display","none");
		$("#seriesSchedule").css("display","");
	}
}

function initLearningPathTbl() {
	$.when(initLearningPathSections()).then(function(status) {
		initLearningPathSubItems();
	});
}

function initLearningPathSections() {
	var result = $.Deferred();
	$.when(fetchLearningPathSection()).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				populateLearningPathSection(response.learningPathSections);
				learningPathSectionList = response.learningPathSections;
				result.resolve("Success");
				break;
		}
	});
	return result.promise();
}

function initLearningPathSubItems() {
	$.when(fetchLearningPathSubItems()).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				learningPathList=response.learningPaths;
				populateLearningPath(response.learningPaths);
				break;
		}
	});
}

function fetchLearningPathSection() {
	var courseId=$("#courseId").val();
	return $.ajax({
		url: "courseinfo/"+courseId+"/learningpathsections",
		type: "GET",
		dataType: "json",
		contentType: "application/json",
		mimeType: "application/json"
	}).done(function(response) {
		switch(response.status) {
			case HttpStatus.SESSION_EXPIRED:
				alertify.error(Message.SESSION_EXPIRED_MESSAGE);
				break;
			case HttpStatus.QUERY_FAILED:
				alertify.error(Message.QUERY_FAILED_MESSAGE);
				break;
			case HttpStatus.UNHANDLED_ERROR:
				alertify.error(Message.UNHANDLED_ERROR_MESSAGE);
				break;
		}
	});
}

function reArrangeSectionOrderNo(learningPathSectionId,learningPathSectionList) {
	var ctr=1;
	var tempSectionArray=[];
	_.each(learningPathSectionList,function(learningPathSection) {
		if(learningPathSection.learningPathSectionId!=learningPathSectionId) {
			learningPathSection.sectionOrderNo=ctr;
			tempSectionArray.push(learningPathSection);
			ctr++;
		}
	});
	var courseInfo = {};
	courseInfo.learningPathSections=tempSectionArray;
	updateLearningPathSectionOrder(courseInfo);
}

function initLearningPathSubItems() {
	$.when(fetchLearningPathSubItems()).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				learningPathList=response.learningPaths;
				populateLearningPath(response.learningPaths);
				break;
		}
	});
}

function fetchLearningPathSubItems() {
	var courseId=$("#courseId").val();
	return $.ajax({
		url: "courseinfo/"+courseId+"/learningpaths",
		type: "GET",
		dataType: "json",
		contentType: "application/json",
		mimeType: "application/json"
	}).done(function(response) {
		switch(response.status) {
			case HttpStatus.SESSION_EXPIRED:
				alertify.error(Message.SESSION_EXPIRED_MESSAGE);
				break;
			case HttpStatus.QUERY_FAILED:
				alertify.error(Message.QUERY_FAILED_MESSAGE);
				break;
			case HttpStatus.UNHANDLED_ERROR:
				alertify.error(Message.UNHANDLED_ERROR_MESSAGE);
				break;
		}
	});
}

function getExamTypeDesc(examTypeValue) {
	var examTypeDesc="";
	_.each(ExamTypes,function(examType) {
		if(examType.id==examTypeValue) {
			examTypeDesc=examType.description;
		}
	});
	return examTypeDesc;
}

function getContentTypeDesc(contentTypeValue) {
	var contentTypeDesc="";
	_.each(ContentType.LIST,function(contentType) {
		if(contentType.id==contentTypeValue) {
			contentTypeDesc=contentType.description;
		}
	});
	return contentTypeDesc;
}

function populateLearningPathSection(learningPathSections) {
	$("#learningPathCon").html("");
	_.each(learningPathSections,function(learningPathSection) {
		$("#learningPathCon").append($("<div/>").css("margin-top","20px")
			.append($("<table/>").addClass("highlight striped")
				.append($("<thead>")
					.append($("<tr/>")
						.append($("<th/>").addClass("large").attr("colspan","2").css("text-align","left")
							.html(learningPathSection.sectionTitle))))
			.append($("<tbody>").attr("id","section_"+learningPathSection.learningPathSectionId))));
	});
}

function populateLearningPath(learningPaths) {
	_.each(learningPaths,function(learningPath) {
			$("#section_"+learningPath.learningPathSectionId)
			.append($("<tr/>")
				.append($("<td/>").addClass("min").css("text-align","left").html(
					(learningPath.itemType==ItemType.EXAM)?learningPath.title:learningPath.fileLabel))
				.append($("<td/>").html(
					(learningPath.itemType==ItemType.EXAM)?
						getExamTypeDesc(learningPath.examType):getContentTypeDesc(learningPath.contentType))));
	});
}

function initClassSetScheduleList(classSetSchedule) {
	meridiemStart = "";
	meridiemEnd = "";
	startDate = classSetSchedule.startDate;
	endDate = classSetSchedule.endDate;
	startTime = classSetSchedule.startTime;
	endTime = classSetSchedule.endTime;
	date1 = new Date(startDate);
	date2 = new Date(endDate);
	time1 = new Date(date1.toDateString()+" "+startTime);
	time2 = new Date(date2.toDateString()+" "+endTime);

	setSchedule = {};
	if(time1.getHours() > 12)
	{
		setSchedule.ScheduleHrStart = parseInt(time1.getHours()) - 12;
		setSchedule.ScheduleMeridiemStart = "PM";
	}
	else
	{
		setSchedule.ScheduleHrStart = parseInt(time1.getHours());
		if(time1.getHours() == 12){
			setSchedule.ScheduleMeridiemStart = "PM";
		} else {
			setSchedule.ScheduleMeridiemStart = "AM";
		}
	}
	
	if(time2.getHours() > 12)
	{
		setSchedule.ScheduleHrEnd = parseInt(time2.getHours()) - 12;
		setSchedule.ScheduleMeridiemEnd = "PM";
	}
	else
	{
		setSchedule.ScheduleHrEnd = parseInt(time2.getHours());
		if(time2.getHours() == 12){
			setSchedule.ScheduleMeridiemEnd = "PM";
		} else {
			setSchedule.ScheduleMeridiemEnd = "AM";
		}
	}
	
	setSchedule.setCounter = setCounter;
	setSchedule.ScheduleDateStart = date1.getFullYear()+"-"+("0" + (date1.getMonth() + 1)).slice(-2)+"-"+("0" + (date1.getDate())).slice(-2);
	setSchedule.ScheduleDateEnd = date2.getFullYear()+"-"+("0" + (date2.getMonth() + 1)).slice(-2)+"-"+("0" + (date2.getDate())).slice(-2);
	setSchedule.ScheduleMinStart = (time1.getMinutes() < 10 ? "0" : "")+time1.getMinutes();
	setSchedule.ScheduleMinEnd = (time2.getMinutes() < 10 ? "0" : "")+time2.getMinutes();

	setClassSchedule(setSchedule);
	createSetScheduleRow(setSchedule);
	setCounter++;
}

function convertNumbertoString(d) {
	return (d < 10) ? '0' + d.toString() : d.toString();
}


function populateScheduleDropdown() {
	var classId = parseInt(GetURLParameter('classId'));
	
	$.when(ajax.fetch("classinfo/" + classId + "/classscheduled")).done(function (response) {
		if (response.status == HttpStatus.SUCCESS) {
//			if ($("#scheduleType").val() == ScheduleType.BLOCK) {
//				classSchedule = response.classInfo.classBlockSchedule;
//				console.log(classSchedule);
//			} else if ($("#scheduleType").val() == ScheduleType.SET) {
//				classSchedule = response.classInfo.classSetSchedules;
//				console.log(classSchedule);
//			} 
			if ($("#scheduleType").val() == ScheduleType.SERIES) {
				classSchedules = response.classInfo.classSeriesSchedules;
				$("#classScheduleTblBody").empty();
				_.each(classSchedules,function(classSchedule) {
					startDate = classSchedule.startDate;
					endDate = classSchedule.endDate;
					date1 = new Date(startDate);
					date2 = new Date(endDate);
					startTime = classSchedule.startTime;
					endTime = classSchedule.endTime;
					time1 = new Date(date1.toDateString()+" "+startTime);
					time2 = new Date(date2.toDateString()+" "+endTime);
					
					var dayStartTime = "";
					var dayEndTime = "";
					var startTimeHr = parseInt(startTime.substring(0, 2));
					var startTimeMin = startTime.substring(3, 5);
					var endTimeHr = parseInt(endTime.substring(0, 2));
					var endTimeMin = endTime.substring(3, 5);

					if (startTimeHr == 0) {
						dayStartTime = `12:${startTimeMin}:00 AM`;
					} else if (startTimeHr > 12) {
						dayStartTime = `${convertNumbertoString(startTimeHr - 12)}:${startTimeMin}:00 PM`;
					} else if (startTimeHr == 12) {
						dayStartTime = `${startTimeHr}:${startTimeMin}:00 PM`;
					} else {
						dayStartTime = `${convertNumbertoString(startTimeHr)}:${startTimeMin}:00 AM`
					}

					if (endTimeHr == 0) {
						dayEndTime = `12:${endTimeMin}:00 AM`
					} else if (endTimeHr > 12) {
						dayEndTime = `${convertNumbertoString(endTimeHr - 12)}:${endTimeMin}:00 PM`
					} else if (endTimeHr == 12) {
						dayEndTime = `${endTimeHr}:${endTimeMin}:00 PM`
					} else {
						dayEndTime = `${convertNumbertoString(endTimeHr)}:${endTimeMin}:00 AM`
					}
					
					$("#classScheduleTblBody").append($("<tr/>")
						.append($("<td/>").html(month[date1.getMonth()]+" "+date1.getDate()+", "+date1.getFullYear()))
						.append($("<td/>").html(startTime))
						.append($("<td/>").html(endTime)));
				});
			}
		}
	});
	
	var classSchedule = "";
	var startDate = "";
	var endDate = "";
	var endAfter = "";
	var date1 = "";
	var date2 = "";
	var time1 = "";
	var time2 = "";
	var startTime = "";
	var endTime = "";
	var recurrencePattern = 0;
	
	var month = new Array();
	month[0] = "January"; month[1] = "February"; month[2] = "March"; month[3] = "April";
	month[4] = "May"; month[5] = "June"; month[6] = "July"; month[7] = "August";
	month[8] = "September"; month[9] = "October"; month[10] = "November";
	month[11] = "December";

	$.when(ajax.fetch("classinfo/"+classId+"/classschedule")).done(function(response) {
		if(response.status == HttpStatus.SUCCESS){
			if($("#scheduleType").val() == ScheduleType.BLOCK){
				classSchedule = response.classInfo.classBlockSchedule;
				startDate = classSchedule.startDate;
				endDate = classSchedule.endDate;
				date1 = new Date(startDate);
				date2 = new Date(endDate);
				startTime = classSchedule.startTime;
				endTime = classSchedule.endTime;
				time1 = new Date(date1.toDateString()+" "+startTime);
				time2 = new Date(date2.toDateString()+" "+endTime);
				
				
				$("#blockDateStartPicker").val(date1.getFullYear()+"-"+("0" + (date1.getMonth() + 1)).slice(-2)+"-"+("0" + (date1.getDate())).slice(-2));
				$("#blockDateEndPicker").val(date2.getFullYear()+"-"+("0" + (date2.getMonth() + 1)).slice(-2)+"-"+("0" + (date2.getDate())).slice(-2));
				
				if(time1.getHours() > 12)
				{
					$("#blockStartHr").val((parseInt(time1.getHours()) - 12));
					$("#blockMeridiemFrom").val("PM").trigger("change");
				}
				else
				{
					$("#blockStartHr").val(parseInt(time1.getHours()));
					if(time1.getHours() == 12){
						$("#blockMeridiemFrom").val("PM").trigger("change");
					} else {
						$("#blockMeridiemFrom").val("AM").trigger("change");
					}
				}
				
				if(time2.getHours() > 12)
				{
					$("#blockEndHr").val((parseInt(time2.getHours()) - 12));
					$("#blockMeridiemTo").val("PM").trigger("change");
				}
				else
				{
					$("#blockEndHr").val(parseInt(time2.getHours()));
					if(time2.getHours() == 12){
						$("#blockMeridiemTo").val("PM").trigger("change");
					} else {
						$("#blockMeridiemTo").val("AM").trigger("change");
					}
				}

				$("#blockStartMin").val((time1.getMinutes() < 10 ? "0" : "")+time1.getMinutes());
				$("#blockEndMin").val((time1.getMinutes() < 10 ? "0" : "")+time2.getMinutes());
				
				$("#classScheduleTblBody").html("");

				for(var d = date1; d <= date2; d.setDate(d.getDate() + 1)){
					$("#classScheduleTblBody").append($("<tr/>")
						.append($("<td/>").html(month[date1.getMonth()]+" "+date1.getDate()+", "+date1.getFullYear()))
						.append($("<td/>").html(startTime))
						.append($("<td/>").html(endTime)));

				}
			}else if($("#scheduleType").val() == ScheduleType.SET){
				classSchedule = response.classInfo.classSetSchedules;
				$("#classScheduleTblBody").html("");
				
				_.each(classSchedule,function(classSetSchedule) {
					initClassSetScheduleList(classSetSchedule);
					
					startDate = classSetSchedule.startDate;
					endDate = classSetSchedule.endDate;
					date1 = new Date(startDate);
					date2 = new Date(endDate);
					startTime = classSetSchedule.startTime;
					endTime = classSetSchedule.endTime;
					
					for(var d = date1; d <= date2; d.setDate(d.getDate() + 1)){
						$("#classScheduleTblBody").append($("<tr/>")
							.append($("<td/>").html(month[date1.getMonth()]+" "+date1.getDate()+", "+date1.getFullYear()))
							.append($("<td/>").html(startTime))
							.append($("<td/>").html(endTime)));
					}
				});
				
			}else if($("#scheduleType").val() == ScheduleType.SERIES){
				classSchedules = response.classInfo.classSeriesSchedules;
				classParameters = response.classInfo.classSeriesParameters;
				_.each(classSchedules,function(classSchedule) {
					startDate = classSchedule.startDate;
					endDate = classSchedule.endDate;
					date1 = new Date(startDate);
					date2 = new Date(endDate);
					startTime = classSchedule.startTime;
					endTime = classSchedule.endTime;
					time1 = new Date(date1.toDateString()+" "+startTime);
					time2 = new Date(date2.toDateString()+" "+endTime);
				});
				_.each(classParameters,function(classParameter) {
					endAfter = classParameter.noOfOccurrence;
					recurrencePattern = classParameter.recurrencePattern;
					$("#recurrenceDropdown").val(recurrencePattern).trigger("change");
					day = classParameter.day;
					if(recurrencePattern === 1){
						if(day == 1){
							$("#chMonSeries").prop("checked",true);
							checkDays("#chMonSeries",1);
						}else if(day == 2){
							$("#chTueSeries").prop("checked",true);
							checkDays("#chTueSeries",2);
						}else if(day == 3){
							$("#chWedSeries").prop("checked",true);
							checkDays("#chWedSeries",3);
						}else if(day == 4){
							$("#chThuSeries").prop("checked",true);
							checkDays("#chThuSeries",4);
						}else if(day == 5){
							$("#chFriSeries").prop("checked",true);
							checkDays("#chFriSeries",5);
						}else if(day == 6){
							$("#chSatSeries").prop("checked",true);
							checkDays("#chSatSeries",6);
						}else if(day == 7){
							$("#chSunSeries").prop("checked",true);
							checkDays("#chSunSeries",7);
						}
					}else if(recurrencePattern === 2){
						if (classParameter.week === 0) {
							$("#dateRadio").prop("checked", true).trigger("change");
							$("#dayTextBox").val(classParameter.day);
							$("#offsetTextBox").val(classParameter.monthOffset);
						} else {
							$("#dayRadio").prop("checked", true).trigger("change");
							$("#dayDropDown").val(classParameter.day).trigger("change");
							$("#monthlyOffsetTextBox").val(classParameter.monthOffset);
							$("#weekDropDown").val(classParameter.week).trigger("change");
						}
					}
				});
				
				$("#scheduleStartDate").val(date1.getFullYear()+"-"+("0" + (date1.getMonth() + 1)).slice(-2)+"-"+("0" + (date1.getDate())).slice(-2));
				
				if(time1.getHours() > 12)
				{
					$("#seriesStartHr").val((parseInt(time1.getHours()) - 12));
					$("#seriesMeridiemStartDropDown").val("PM").trigger("change");
				}
				else
				{
					$("#seriesStartHr").val(parseInt(time1.getHours()));
					if(time1.getHours() == 12){
						$("#seriesMeridiemStartDropDown").val("PM").trigger("change");
					} else {
						$("#seriesMeridiemStartDropDown").val("AM").trigger("change");
					}
				}
				
				if(time2.getHours() > 12)
				{
					$("#seriesEndHr").val((parseInt(time2.getHours()) - 12));
					$("#seriesMeridiemEndDropDown").val("PM").trigger("change");
				}
				else
				{
					$("#seriesEndHr").val(parseInt(time2.getHours()));
					if(time2.getHours() == 12){
						$("#seriesMeridiemEndDropDown").val("PM").trigger("change");
					} else {
						$("#seriesMeridiemEndDropDown").val("AM").trigger("change");
					}
				}

				$("#seriesStartMin").val((time1.getMinutes() < 10 ? "0" : "")+time1.getMinutes());
				$("#seriesEndMin").val((time1.getMinutes() < 10 ? "0" : "")+time2.getMinutes());
				
				if(endDate == null){
					$("#endTypeDropDown").val("endAfter").trigger("change");
					$("#noOfOccurrence").val(endAfter);
				} else {
					$("#endTypeDropDown").val("endBy").trigger("change");
					$("#scheduleEndDate").val(date2.getFullYear()+"-"+("0" + (date2.getMonth() + 1)).slice(-2)+"-"+("0" + (date2.getDate())).slice(-2));
				}

			}
		}
	});
}



function attachListenerToAddSetSchedule() {
	$("#addSetScheduleSubmit").on("click",function(e) {
		var incField = 0;
		if($("#setDateStartPicker").val().length == 0){
			$("#setScheduleStartLabel").css("color","red");
			incField = 1;
		}else{$("#setScheduleStartLabel").css("color","");}
		
		if($("#setDateEndPicker").val().length == 0){
			$("#setScheduleEndLabel").css("color","red");
			incField = 1;
		}else{$("#setScheduleEndLabel").css("color","");}
		if(incField == 1){
			alertify.dismissAll();
			alertify.warning("Please fill up the required fields.");
			$("#addSetScheduleSubmit").removeAttr("disabled");
			$("#addSetScheduleSubmit").html("<i class='material-icons left'>save</i>Save");
		}else{
			setSchedule = {};
			setSchedule.setCounter = parseInt($("#addSetScheduleSubmit").attr("data-counterid"));
			setSchedule.ScheduleDateStart = $("#setDateStartPicker").val();
			setSchedule.ScheduleDateEnd = $("#setDateEndPicker").val();
			setSchedule.ScheduleHrStart = $("#setStartHr").val();
			setSchedule.ScheduleMinStart = $("#setStartMin").val();
			setSchedule.ScheduleMeridiemStart = $("#setMeridiemFrom").val();
			setSchedule.ScheduleHrEnd = $("#setEndHr").val();
			setSchedule.ScheduleMinEnd = $("#setEndMin").val();
			setSchedule.ScheduleMeridiemEnd = $("#setMeridiemTo").val();
			
			if($("#addSetScheduleSubmit").attr("data-action") == "EDIT"){
				var month = new Array();
				month[0] = "January"; month[1] = "February"; month[2] = "March"; month[3] = "April";
				month[4] = "May"; month[5] = "June"; month[6] = "July"; month[7] = "August";
				month[8] = "September"; month[9] = "October"; month[10] = "November";
				month[11] = "December";
				
				var date1 = new Date($("#setDateStartPicker").val());
				var date2 = new Date($("#setDateEndPicker").val());
				
				var currentCounter = $("#addSetScheduleSubmit").attr("data-counterid");
				$("#setListDateStart_"+currentCounter).html(month[date1.getMonth()]+" "+date1.getDate()+", "+date1.getFullYear());
				$("#setListDateEnd_"+currentCounter).html(month[date2.getMonth()]+" "+date2.getDate()+", "+date2.getFullYear());

				deleteSetScheduleFromList(currentCounter);
				setClassSchedule(setSchedule);
				$("#addSetScheduleSubmit").attr("data-action","");
				$("#addScheduleModal").modal('close');
//				createSetScheduleRow(setSchedule);
			} else {
				setClassSchedule(setSchedule);
				createSetScheduleRow(setSchedule);
				setCounter++;
				$("#addScheduleModal").modal('close');
			}
			
		}
	});
}

function attachListenerToScheduleTypeDropdownChanged() {
	$("#scheduleTypeDropdown").on("change",function(e) {
		if($(this).val() == "1"){
			$("#blockSchedule").css("display","");
			$("#setSchedule").css("display","none");
			$("#seriesSchedule").css("display","none");
		}else if($(this).val() == "2"){
			$("#blockSchedule").css("display","none");
			$("#setSchedule").css("display","");
			$("#seriesSchedule").css("display","none");
		}else if($(this).val() == "3"){
			$("#blockSchedule").css("display","none");
			$("#setSchedule").css("display","none");
			$("#seriesSchedule").css("display","");
		}
	});
}

function attachListenerToMonthlyRadioButtonChanged() {
	 $("input:radio[name=monthlyType]").change(function () {
         if (this.value == "Date") {
        	 $("#dateDiv").css("display","");
        	 $("#dayDiv").css("display","none");
         }
         if (this.value == "Day") {
        	 $("#dateDiv").css("display","none");
        	 $("#dayDiv").css("display","");
         }
     });
}

function attachListenerToRecurrenceDropdownChanged() {
	$("#recurrenceDropdown").on("change",function(e) {
		if($(this).val() == "1"){
			$("#weeklyDiv").css("display","");
			$("#monthlyDiv").css("display","none");
			$("#dateDiv").css("display","none");
			$("#dayDiv").css("display","none");
			$("#dateRadio").prop("checked",false);
			$("#dayRadio").prop("checked",false);
		}else if($(this).val() == "2"){
			$("#weeklyDiv").css("display","none");
			$("#monthlyDiv").css("display","");
			$("#dateDiv").css("display","none");
			$("#dayDiv").css("display","none");
			$("#dateRadio").prop("checked",false);
			$("#dayRadio").prop("checked",false);
			attachListenerToUncheckDays();
		}
	});
	$("#recurrenceDropdown").trigger("change");
}

function attachListenerToUncheckDays() {
	$("#chMonSeries").prop("checked",false);
	$("#chTueSeries").prop("checked",false);
	$("#chWedSeries").prop("checked",false);
	$("#chThuSeries").prop("checked",false);
	$("#chFriSeries").prop("checked",false);
	$("#chSatSeries").prop("checked",false);
	$("#chSunSeries").prop("checked",false);
}

function attachListenerToOccurenceDropDown() {
	$("#endTypeDropDown").on("change",function(e) {
		if($(this).val() == "endAfter"){
			$("#scheduleEndDate").hide();
			$("#noOfOccurrence").show();
			$("#scheduleEndDate").val("");
		}else if($(this).val() == "endBy"){
			$("#scheduleEndDate").show();
			$("#noOfOccurrence").hide();
			$("#noOfOccurrence").val(0);
		}
	});
}

function attachListenerToAttendeeScale() {
	$("#minAttendees").on("focusout",function(e) {
		var numOnly = e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57);
		var minScale = parseInt($("#minAttendees").val());
		var maxScale = parseInt($("#maxAttendees").val());
		if(minScale > maxScale || minScale == maxScale || minScale < 0){
			$("#minAttendees").val(maxScale-1);
		}
	});
	
	$("#maxAttendees").on("focusout",function(e) {
		var numOnly = e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57);
		var minScale = parseInt($("#minAttendees").val());
		var maxScale = parseInt($("#maxAttendees").val());
		if(minScale > maxScale || minScale == maxScale || minScale < 0){
			$("#maxAttendees").val(minScale+1);
		}
	});
}

function attachListenerToClassSchedule() {
	var date1 = new Date($("#blockDateStartPicker").val());
	var date2 = new Date($("#blockDateEndPicker").val());
}

function attachListenerToAssignCounter() {
	$("#addScheduleModal").on("click",function() {
		if($("#addSetScheduleSubmit").attr("data-action") != "EDIT"){
			$("#addSetScheduleSubmit").attr("data-counterid",setCounter);
		}
	});
}

function createSetScheduleRow(setSchedule) {
	var month = new Array();
	month[0] = "January"; month[1] = "February"; month[2] = "March"; month[3] = "April";
	month[4] = "May"; month[5] = "June"; month[6] = "July"; month[7] = "August";
	month[8] = "September"; month[9] = "October"; month[10] = "November";
	month[11] = "December";
	
	var date1 = new Date(setSchedule.ScheduleDateStart);
	var date2 = new Date(setSchedule.ScheduleDateEnd);
	
	$("#setScheduleTblBody").append($("<tr/>").attr({"id":"setSchedule_"+setSchedule.setCounter})
		.append($("<td/>").attr({"id":"setListDateStart_"+setSchedule.setCounter,"class":"mid"}).html(month[date1.getMonth()]+" "+date1.getDate()+", "+date1.getFullYear()))
		.append($("<td/>").attr({"id":"setListDateEnd_"+setSchedule.setCounter,"class":"mid"}).html(month[date2.getMonth()]+" "+date2.getDate()+", "+date2.getFullYear()))
		.append($("<td/>").attr({"class":"mins"})
		 .append($("<a/>").attr({"href":"#!","data-editid":setSchedule.setCounter}).css("display", $("#userType").val() == 1 || $("#loggedInId").val() == $("#trainerId").val()  ? "" : "none")
		  .bind("click",function(){
			  var currentSetList = setSchedule;
			  editClassSetSchedule($(this).attr("data-editid"));
			  $("#addSetScheduleSubmit").attr("data-counterid",$(this).attr("data-editid"));
			  $("#addSetScheduleSubmit").attr("data-action","EDIT");
			  $("#addScheduleModal").modal('open');
		  })
		  .append($("<i/>").attr({"class":"material-icons"}).html("edit")))
		 .append($("<a/>").attr({"href":"#!","data-deleteid":setSchedule.setCounter}).css("display", $("#userType").val() == 1 || $("#loggedInId").val() == $("#trainerId").val()  ? "" : "none")
		  .bind("click",function(){
			  deleteSetScheduleFromList($(this).attr("data-deleteid"));
			  $("#setSchedule_"+$(this).attr("data-deleteid")).remove();
		  })
		  .append($("<i/>").attr("class","material-icons").html("delete")))));
}

//classSetSchedule Delete
function deleteSetScheduleFromList(currentId) {
	var result = false;
	_.each(tempClassSchedules,function(tempClassSchedule) {
		if(tempClassSchedule.setCounter == currentId) {
			tempClassSchedules = deleteSetSchedule(currentId);
		}
	});
	return result;
}

function deleteSetSchedule(currentId) {
	var resultArray = [];
	_.each(tempClassSchedules,function(tempClassSchedule) {
		if(tempClassSchedule.setCounter != currentId) {
			resultArray.push(tempClassSchedule);
		}
	});
	return resultArray;
}

//classSetSchedule Edit
function editClassSetSchedule(currentId) {
	_.each(tempClassSchedules,function(setSchedule) {
		if(setSchedule.setCounter == currentId) {
			meridiemStart = "";
			meridiemEnd = "";
			startDate = setSchedule.startDate;
			endDate = setSchedule.endDate;
			startTime = setSchedule.startTime;
			endTime = setSchedule.endTime;
			date1 = new Date(startDate);
			date2 = new Date(endDate);
			time1 = new Date(date1.toDateString()+" "+startTime);
			time2 = new Date(date2.toDateString()+" "+endTime);

			setSchedule = {};
			if(time1.getHours() > 12)
			{
				$("#setStartHr").val(parseInt(time1.getHours() - 12));
				$("#setMeridiemFrom").val("PM").trigger("change");
			}
			else
			{
				if(time1.getHours() == 12){
					$("#setStartHr").val(time1.getHours());
					$("#setMeridiemFrom").val("PM").trigger("change");
				} else if(time1.getHours() == 0){
					$("#setStartHr").val(parseInt(time1.getHours()+12));
					$("#setMeridiemFrom").val("AM").trigger("change");
				} else {
					$("#setStartHr").val(time1.getHours());
					$("#setMeridiemFrom").val("AM").trigger("change");
				}
			}
			
			if(time2.getHours() > 12)
			{
				$("#setEndHr").val(parseInt(time2.getHours() - 12));
				$("#setMeridiemTo").val("PM").trigger("change");
			}
			else
			{
				if(time2.getHours() == 12){
					$("#setEndHr").val(time2.getHours());
					$("#setMeridiemTo").val("PM").trigger("change");
				} else if(time2.getHours() == 0){
					$("#setEndHr").val(parseInt(time2.getHours()+12));
					$("#setMeridiemTo").val("AM").trigger("change");
				} else {
					$("#setEndHr").val(time2.getHours());
					$("#setMeridiemTo").val("AM").trigger("change");
				}
			}
			
			$("#setDateStartPicker").val(date1.getFullYear()+"-"+("0" + (date1.getMonth() + 1)).slice(-2)+"-"+("0" + (date1.getDate())).slice(-2));
			$("#setStartMin").val(time1.getMinutes() < 10 ? "0"+time1.getMinutes() : ""+time1.getMinutes());
			
			$("#setDateEndPicker").val(date2.getFullYear()+"-"+("0" + (date2.getMonth() + 1)).slice(-2)+"-"+("0" + (date2.getDate())).slice(-2));
			$("#setEndMin").val(time2.getMinutes() < 10 ? "0"+time2.getMinutes() : ""+time2.getMinutes());
		}
	});
}

function setClassSchedule(setSchedule) {
	var classSchedule = {};
	var classId = parseInt($("#classId").val());
	var dateStart = setSchedule.ScheduleDateStart;
	var hrStart = parseInt(setSchedule.ScheduleHrStart);
	var minStart = setSchedule.ScheduleMinStart;
	var meridiemStart = setSchedule.ScheduleMeridiemStart;
	
	var dateEnd = setSchedule.ScheduleDateEnd;
	var hrEnd = parseInt(setSchedule.ScheduleHrEnd);
	var minEnd = setSchedule.ScheduleMinEnd;
	var meridiemEnd = setSchedule.ScheduleMeridiemEnd;
	
	if(meridiemStart == "PM" && hrStart < 12)
	{
		hrStart = hrStart + 12;
	}
	else if(meridiemStart == "AM" && hrStart == 12)
	{
		hrStart = hrStart - 12;
	}
	
	if(meridiemEnd == "PM" && hrEnd < 12)
	{
		hrEnd = hrEnd + 12;
	}
	else if(meridiemEnd == "AM" && hrEnd == 12)
	{
		hrEnd = hrEnd - 12;
	}

	var timeStart = hrStart+":"+minStart+":00";
	var timeEnd = hrEnd+":"+minEnd+":00";
	
	classSchedule.setCounter = setSchedule.setCounter;
	classSchedule.classId = classId;
	classSchedule.startDate = dateStart;
	classSchedule.endDate = dateEnd;
	classSchedule.startTime = timeStart;
	classSchedule.endTime = timeEnd;
	
	tempClassSchedules.push(classSchedule);
}

function attachListenerToSaveButton() {
	$("#btnClassSave").on("click",function(e) {
		$("#btnClassSave").attr("disabled","disabled");
		$("#btnClassSave").html("Saving...");
		validateClassInfoFields();
	});
}

function convertCourseNameToInitials(name) {
	var matches = name.match(/\b(\w)/g);
	var acronym = matches.join('');
	return acronym.toUpperCase();
}

function validateClassInfoFields() {
	var incField = 0;
	
	if($("#classNameTxtbox").val().length == 0){
		$("#nameLabel").css("color","red");
		incField = 1;
	}else{$("#nameLabel").css("color","");}
	
	if($("#locationDropdown").val() == 0 || $("#locationDropdown").val() == null){
		$("#locationLabel").css("color","red");
		incField = 1;
	}else{$("#locationLabel").css("color","");}
	
	if($("#trainerDropdown").val() == 0 || $("#trainerDropdown").val() == null){
		$("#trainerLabel").css("color","red");
		incField = 1;
	}else{$("#trainerLabel").css("color","");}
	
	if($("#minAttendees").val().length == 0){
		$("#minLabel").css("color","red");
		incField = 1;
	}else{$("#minLabel").css("color","");}
	
	if($("#maxAttendees").val().length == 0){
		$("#maxLabel").css("color","red");
		incField = 1;
	}else{$("#maxLabel").css("color","");}
	
	if($("#scheduleTypeDropdown").val() == 0 || $("#scheduleTypeDropdown").val() == null){
		$("#schedLabel").css("color","red");
		incField = 1;
	}else{$("#schedLabel").css("color","");}
	
	if(incField != 1){
		$("#blockSDLabel").css("color","");
		$("#blockEDLabel").css("color","");
		$("#setSchedLabel").css("color","");
		var minScale = parseInt($("#minAttendees").val());
		var maxScale = parseInt($("#maxAttendees").val());
		if(minScale > maxScale){
			$("#minLabel").css("color","red");
			$("#maxLabel").css("color","red");
			$("#minAttendees").val("");
			$("#maxAttendees").val("");
			alertify.dismissAll();
			alertify.warning("Minimum value cannot be greater than Maximum value.");
			$("#btnClassSave").removeAttr("disabled");
			$("#btnClassSave").html("<i class='material-icons left'>save</i>Save");
		} else if(minScale == maxScale){
			$("#minLabel").css("color","red");
			$("#maxLabel").css("color","red");
			$("#minAttendees").val("");
			$("#maxAttendees").val("");
			alertify.dismissAll();
			alertify.warning("Minimum and Maximum value cannot be equal.");
			$("#btnClassSave").removeAttr("disabled");
			$("#btnClassSave").html("<i class='material-icons left'>save</i>Save");
		} else if(minScale < 0 || maxScale < 0){
			$("#minLabel").css("color","red");
			$("#maxLabel").css("color","red");
			$("#minAttendees").val("");
			$("#maxAttendees").val("");
			alertify.dismissAll();
			alertify.warning("Minimum and Maximum value must be a positive number.");
			$("#btnClassSave").removeAttr("disabled");
			$("#btnClassSave").html("<i class='material-icons left'>save</i>Save");
		} else if($("#scheduleTypeDropdown").val() == "1" && $("#blockDateStartPicker").val() == ""){
			$("#blockSDLabel").css("color","red");
			alertify.dismissAll();
			alertify.warning("Please Specify Start Date.");
			$("#btnClassSave").removeAttr("disabled");
			$("#btnClassSave").html("<i class='material-icons left'>save</i>Save");
		} else if($("#scheduleTypeDropdown").val() == "1" && $("#blockDateEndPicker").val() == ""){
			$("#blockEDLabel").css("color","red");
			alertify.dismissAll();
			alertify.warning("Please Specify End Date.");
			$("#btnClassSave").removeAttr("disabled");
			$("#btnClassSave").html("<i class='material-icons left'>save</i>Save");
		} else if($("#scheduleTypeDropdown").val() == "2" && tempClassSchedules.length == 0){
			$("#setSchedLabel").css("color","red");
			alertify.dismissAll();
			alertify.warning("Please add at least 1 set schedule date.");
			$("#btnClassSave").removeAttr("disabled");
			$("#btnClassSave").html("<i class='material-icons left'>save</i>Save");
		} else if($("#scheduleTypeDropdown").val() == "3" && 
				($("#scheduleStartDate").val() == "" || $("#endTypeDropDown").val() == null)){
			$("#seriesScheduleStarts").css("color","red");
			alertify.dismissAll();
			alertify.warning("Please Specify Start Date and End Type.");
			$("#btnClassSave").removeAttr("disabled");
			$("#btnClassSave").html("<i class='material-icons left'>save</i>Save");
		} else if($("#scheduleTypeDropdown").val() == "3" && $("#endTypeDropDown").val() == "endBy" &&
				($("#scheduleStartDate").val() == "" || $("#scheduleEndDate").val() == "")){
			alertify.dismissAll();
			$("#seriesScheduleStarts").css("color","red");
			alertify.warning("Please Specify Start and End Date.");
			$("#btnClassSave").removeAttr("disabled");
			$("#btnClassSave").html("<i class='material-icons left'>save</i>Save");
		} else if($("#scheduleTypeDropdown").val() == "3" && 
				$("#recurrenceDropdown").val() == "1" && tempCheckedDays.length == 0){
			alertify.dismissAll();
			$("#seriesScheduleStarts").css("color","red");
			alertify.warning("Please Specify Start and End Date.");
			$("#btnClassSave").removeAttr("disabled");
			$("#btnClassSave").html("<i class='material-icons left'>save</i>Save");
		} else {
			if($("#scheduleTypeDropdown").val() == "1"){
				$("#blockSDLabel").css("color","");
				$("#blockEDLabel").css("color","");
				setSchedule = {};
				setSchedule.ScheduleDateStart = $("#blockDateStartPicker").val();
				setSchedule.ScheduleDateEnd = $("#blockDateEndPicker").val();
				setSchedule.ScheduleHrStart = $("#blockStartHr").val();
				setSchedule.ScheduleMinStart = $("#blockStartMin").val();
				setSchedule.ScheduleMeridiemStart = $("#blockMeridiemFrom").val();
				setSchedule.ScheduleHrEnd = $("#blockEndHr").val();
				setSchedule.ScheduleMinEnd = $("#blockEndMin").val();
				setSchedule.ScheduleMeridiemEnd = $("#blockMeridiemTo").val();
				setClassSchedule(setSchedule);
			} else if($("#scheduleTypeDropdown").val() == "3"){
				$("#seriesScheduleStarts").css("color","");
				setSchedule = {};
				setSchedule.ScheduleDateStart = $("#seriesStartDate").val();
				setSchedule.ScheduleEndType = $("#endTypeDropDown").val();
				setSchedule.ScheduleDateEnd = $("#seriesEndDate").val();
				setSchedule.ScheduleDateAfter = $("#seriesEndAfter").val();
				setSchedule.ScheduleHrStart = $("#seriesStartHr").val();
				setSchedule.ScheduleMinStart = $("#seriesStartMin").val();
				setSchedule.ScheduleMeridiemStart = $("#seriesMeridiemStartDropDown").val();
				setSchedule.ScheduleHrEnd = $("#seriesEndHr").val();
				setSchedule.ScheduleMinEnd = $("#seriesEndMin").val();
				setSchedule.ScheduleMeridiemEnd = $("#seriesMeridiemEndDropDown").val();
				setClassSeries(setSchedule);
			}
			submitClassDetails();
		}
		
	} else{
		alertify.dismissAll();
		alertify.warning("Please fill up the required fields.");
		$("#btnClassSave").removeAttr("disabled");
		$("#btnClassSave").html("<i class='material-icons left'>save</i>Save");
	}
}

function setClassInfo() {
	var classinfo = {};
	classinfo.classId = parseInt($("#classId").val());
	classinfo.locationId = parseInt($("#locationDropdown").val());
	classinfo.className = $("#classNameTxtbox").val();
	var scheduleType = $("#scheduleTypeDropdown").val();
	
	if ($('#selfRegCheckBox').is(':checked')) {
		classinfo.isSelfRegister = 1;
	} else if(!$('#selfRegCheckBox').is(':checked')){
		classinfo.isSelfRegister = 0;
	}
	if ($('#certificateCheckBox').is(':checked')) {
		classinfo.withCertificate = 1;
	} else if(!$('#certificateCheckBox').is(':checked')){
		classinfo.withCertificate = 0;
	}
	if ($('#examCheckbox').is(':checked')) {
		classinfo.withExam = 1;
	} else if(!$('#examCheckbox').is(':checked')){
		classinfo.withExam = 0;
	}
	classinfo.minAttendee = parseInt($("#minAttendees").val());
	classinfo.maxAttendee = parseInt($("#maxAttendees").val());
	classinfo.scheduleType = scheduleType;

	return classinfo;
}

function setClassEmployee() {
	var classEmployee = {};
	classEmployee.classId = parseInt($("#classId").val());
	classEmployee.employeeId = parseInt($("#trainerDropdown").val());
	classEmployee.role = 1;
	return classEmployee;
}

function setDayToInteger(day) {
	var convertedValue;
	switch(day) {
		case "Monday":
			convertedValue = 1;
			break;
		case "Tuesday":
			convertedValue = 2;
			break;
		case "Wednesday":
			convertedValue = 3;
			break;
		case "Thursday":
			convertedValue = 4;
			break;
		case "Friday":
			convertedValue = 5;
			break;
		case "Saturday":
			convertedValue = 6;
			break;
		case "Sunday":
			convertedValue = 7;
			break;
	}
}

function setClassSeriesParameterWeekly(selectedDay) {
	var classSeriesParameter = {};
	classSeriesParameter.classId = parseInt($("#classId").val());
	classSeriesParameter.recurrencePattern = parseInt($(recurrenceDropdown).val());
	classSeriesParameter.noOfOccurrence = $("#noOfOccurrence").val();
	classSeriesParameter.day = selectedDay;
	classSeriesParameter.week = 0;
	classSeriesParameter.monthOffSet = 0;

	return classSeriesParameter;
}

function setClassSeriesParameterMonthly() {
	var monthlyType = $(`input[name='monthlyType']:checked`).val();
	var classSeriesParameter = {};
	classSeriesParameter.classId = parseInt($("#classId").val());
	classSeriesParameter.recurrencePattern = parseInt($(recurrenceDropdown).val());
	classSeriesParameter.noOfOccurrence = $("#noOfOccurrence").val();
	//check if empty
	if (monthlyType === "Date") {
		classSeriesParameter.day = $("#dayTextBox").val();
		classSeriesParameter.monthOffset = $("#offsetTextBox").val();
		classSeriesParameter.week = 0;
	} else if (monthlyType === "Day") {
		classSeriesParameter.day = $("#dayDropDown").val();
		classSeriesParameter.monthOffset = $("#monthlyOffsetTextBox").val();
		classSeriesParameter.week = $("#weekDropDown").val();
	}
	return classSeriesParameter;
}

//Class Set Schedule
function reorderClassSchedules() {
	var classSchedules = [];
	var currentId = 1;
	
	if($("#scheduleType").val() == ScheduleType.SET){
		for(currentId;currentId < setCounter;currentId++){
			_.each(tempClassSchedules,function(tempSchedule) {
				if(currentId == tempSchedule.setCounter) {
					classSchedules.push(tempSchedule);
				}
			});
		}
	} else {
		classSchedules = tempClassSchedules;
	}
	
	return classSchedules;
}

function submitClassDetails() {
	var classId = parseInt($("#classId").val());
	var classInfo = setClassInfo();
	var classEmployee = setClassEmployee();
	var classSchedules = reorderClassSchedules();
	var scheduleType = "";

	$.when(ajax.customUpdate("classinfo/updateclassdetails",classInfo)).done(function(response) {
		if(response.status == HttpStatus.SUCCESS){
			$.when(ajax.customUpdate("classemployee/updateclasstrainer",classEmployee)).done(function(response) {
				if(response.status == HttpStatus.SUCCESS){
					if(classInfo.scheduleType == "1"){
						scheduleType = "block";
					}else if(classInfo.scheduleType == "2"){
						scheduleType = "set";
					}else if(classInfo.scheduleType == "3"){
						scheduleType = "series";
					}
					var classSeriesParameterList = [];

					if (scheduleType === "series") {
						var recurrencePatternValue = $("#recurrenceDropdown").val();
						if (recurrencePatternValue === "1") {
							if (tempCheckedDays.length > 0) {

								_.each(tempCheckedDays, function(day) {
									var classSeriesWeeklyParam = setClassSeriesParameterWeekly(day.typeOfDay);
									classSeriesParameterList.push(classSeriesWeeklyParam);
								});
							} else {
								$("#btnClassSave").removeAttr("disabled");
								$("#codeLabel").css("color","red");
								$("#btnClassSave").html("<i class='material-icons left'>save</i>Save");
								alertify.dismissAll();
								alertify.warning("Empty selected check box!");
								return;
							}
						} else if (recurrencePatternValue === "2") {
							var classSeriesMonthlyParam = setClassSeriesParameterMonthly();
							classSeriesParameterList.push(classSeriesMonthlyParam);
							
						}
						var seriesSchedule = {};
						var seriesMeridiemStartValue = $("#seriesMeridiemStartDropDown").val();
						var seriesMeridiemEndValue = $("#seriesMeridiemEndDropDown").val();
						
						var startTimeHr = parseInt($("#seriesStartHr").val());

						if (seriesMeridiemStartValue === "AM") {
							if (startTimeHr === 12) {
								startTimeHr = startTimeHr - 12;
							}
						} else {
							if(startTimeHr !== 12){
								startTimeHr = startTimeHr + 12;
							}
						}
						
						var startTimeMin = $("#seriesStartMin").val();
						
						var endTimeHr = parseInt($("#seriesEndHr").val());
						if (seriesMeridiemEndValue === "AM") {
							if (endTimeHr === 12) {
								endTimeHr = endTimeHr - 12;
							}
						} else {
							if (endTimeHr !== 12) {
								endTimeHr = endTimeHr + 12;
							} 
						}
						var endTimeMin = $("#seriesEndMin").val();

						seriesSchedule.startDate = $("#scheduleStartDate").val();
						var seriesEndDate = $("#scheduleEndDate").val();
						if (!seriesEndDate) {
							seriesSchedule.endDate = null;
						} else {
							seriesSchedule.endDate = seriesEndDate;
						}
						seriesSchedule.startTime = `${startTimeHr}:${startTimeMin}`
						seriesSchedule.endTime = `${endTimeHr}:${endTimeMin}`
						seriesSchedule.classId = classId;
						classSchedules.push(seriesSchedule);
						$.when(ajax.customUpdate("classinfo/classschedules/seriesparameter", classSeriesParameterList)).done(function(response){
							if (response.status == HttpStatus.SUCCESS) {
							}
						});
					}
					$.when(ajax.customUpdate("classinfo/"+classId+"/classschedules/"+scheduleType,classSchedules)).done(function(response) {
						if(response.status == HttpStatus.SUCCESS){
							alertify.dismissAll();
							alertify.success("Successfully Updated.");
							setTimeout(function(){redirect("admin.classdetails?classId="+classId);}, 3000);
						}
					});
				}
			})
		}
	});
}

function checkIfDayExist(days,day) {
	var result = false;
	_.each(days,function(dayObj) {
		if(dayObj.typeOfDay == day) {
			result = true;
		}
	});
	return result;
}

function deleteDayFromCheckList(day) {
	var result = false;
	tempDays = tempCheckedDays;
	_.each(tempDays,function(dayObj) {
		if(dayObj.typeOfDay == day) {
			tempCheckedDays = deleteItem(day);
		}
	});
	return result;
}

function deleteItem(day) {
	var resultArray = [];
	_.each(tempCheckedDays,function(dayObj) {
		if(dayObj.typeOfDay != day) {
			resultArray.push(dayObj);
		}
	});
	return resultArray;
}

function attachListenerToDaysCheckBox(){
	$("#chMonSeries").on("change",function(e) {
		var checkedDay = 1;
		checkDays(this,checkedDay);
	});
	$("#chTueSeries").on("change",function(e) {
		var checkedDay = 2;
		checkDays(this,checkedDay);
	});
	$("#chWedSeries").on("change",function(e) {
		var checkedDay = 3;
		checkDays(this,checkedDay);
	});
	$("#chThuSeries").on("change",function(e) {
		var checkedDay = 4;
		checkDays(this,checkedDay);
	});
	$("#chFriSeries").on("change",function(e) {
		var checkedDay = 5;
		checkDays(this,checkedDay);
	});
	$("#chSatSeries").on("change",function(e) {
		var checkedDay = 6;
		checkDays(this,checkedDay);
	});
	$("#chSunSeries").on("change",function(e) {
		var checkedDay = 7;
		checkDays(this,checkedDay);
	});
}

function checkDays(dom,day) {
	
	if($(dom).prop("checked") != true) {
		if(checkIfDayExist(tempCheckedDays,day)) {
			deleteDayFromCheckList(day);
		}
	} else if(!checkIfDayExist(tempCheckedDays,day)) {
		tempcheckDay = {};
		tempcheckDay.typeOfDay = day;
		tempCheckedDays.push(tempcheckDay);
	}
}

function setClassSeries(setSchedule) {
	var date1 = new Date(setSchedule.ScheduleDateStart);
	var date2 = new Date(setSchedule.ScheduleDateEnd);
	
	var endType = "";

	//Weekly Recurrence
	if($("#recurrenceDropdown").val() == "1"){
		
		for (var d = date1; d <= date2; d.setDate(d.getDate() + 1)) {
			var weekday=new Array(7);
			weekday[0]="Sunday";
			weekday[1]="Monday";
			weekday[2]="Tuesday";
			weekday[3]="Wednesday";
			weekday[4]="Thursday";
			weekday[5]="Friday";
			weekday[6]="Saturday";
			
			tempDays = tempCheckedDays;
			
			_.each(tempDays,function(dayObj) {
				
				if(weekday[d.getDay()] == dayObj.typeOfDay) {
					var blockDate = {};
					var monthNumber = d.getMonth()+1;
					var dateNumber = d.getDate();
					var classId = parseInt($("#btnClassSave").attr("data-id"));
					var dateStart = d.getFullYear()+"-"+monthNumber+"-"+dateNumber;
					var hrStart = parseInt(setSchedule.ScheduleHrStart);
					var minStart = setSchedule.ScheduleMinStart;
					var meridiemStart = setSchedule.ScheduleMeridiemStart;
					
					var dateEnd = setSchedule.ScheduleDateEnd;
					var hrEnd = parseInt(setSchedule.ScheduleHrEnd);
					var minEnd = setSchedule.ScheduleMinEnd;
					var meridiemEnd = setSchedule.ScheduleMeridiemEnd;
					
					if(meridiemStart == "PM" && hrStart < 12)
					{
						hrStart = hrStart + 12;
					}
					else if(meridiemStart == "AM" && hrStart == 12)
					{
						hrStart = hrStart - 12;
					}
					
					if(meridiemEnd == "PM" && hrEnd < 12)
					{
						hrEnd = hrEnd + 12;
					}
					else if(meridiemEnd == "AM" && hrEnd == 12)
					{
						hrEnd = hrEnd - 12;
					}
					
					var timeStart = hrStart+":"+minStart+":00";
					var timeEnd = hrEnd+":"+minEnd+":00";
					
					blockDate.classId = classId;
					blockDate.startDate = dateStart;
					blockDate.endDate = dateStart;
					blockDate.startTime = timeStart;
					blockDate.endTime = timeEnd;
					
					tempClassSchedules.push(blockDate);
				}
			});
		} 
	} else if($("#recurrenceDropdown").val() == "2"){
		//Monthly Recurrence
		for (var d = date1; d <= date2; d.setDate(d.getMonth() + 1)) {
			var weekday=new Array(7);
			weekday[0]="Sunday";
			weekday[1]="Monday";
			weekday[2]="Tuesday";
			weekday[3]="Wednesday";
			weekday[4]="Thursday";
			weekday[5]="Friday";
			weekday[6]="Saturday";
			
			tempDays = tempCheckedDays;
			
			_.each(tempDays,function(dayObj) {
				
				if(weekday[d.getDay()] == dayObj.typeOfDay) {
					var blockDate = {};
					var monthNumber = d.getMonth()+1;
					var dateNumber = d.getDate();
					var classId = parseInt($("#btnClassSave").attr("data-id"));
					var dateStart = d.getFullYear()+"-"+monthNumber+"-"+dateNumber;
					var hrStart = parseInt(setSchedule.ScheduleHrStart);
					var minStart = setSchedule.ScheduleMinStart;
					var meridiemStart = setSchedule.ScheduleMeridiemStart;
					
					var dateEnd = setSchedule.ScheduleDateEnd;
					var hrEnd = parseInt(setSchedule.ScheduleHrEnd);
					var minEnd = setSchedule.ScheduleMinEnd;
					var meridiemEnd = setSchedule.ScheduleMeridiemEnd;
					
					if(meridiemStart == "PM" && hrStart < 12)
					{
						hrStart = hrStart + 12;
					}
					else if(meridiemStart == "AM" && hrStart == 12)
					{
						hrStart = hrStart - 12;
					}
					
					if(meridiemEnd == "PM" && hrEnd < 12)
					{
						hrEnd = hrEnd + 12;
					}
					else if(meridiemEnd == "AM" && hrEnd == 12)
					{
						hrEnd = hrEnd - 12;
					}
					
					var timeStart = hrStart+":"+minStart+":00";
					var timeEnd = hrEnd+":"+minEnd+":00";
					
					blockDate.classId = classId;
					blockDate.startDate = dateStart;
					blockDate.endDate = dateStart;
					blockDate.startTime = timeStart;
					blockDate.endTime = timeEnd;
					
					tempClassSchedules.push(blockDate);
				}
			});
		} 
	}
}

function attachListenerToUploadPhotoLink() {
	$("#uploadPhotoLink").on("click",function() {
		$("#fileUpload").trigger("click");
	});
}

function attachListenerToUploadPhoto() {
	$("#fileUpload").on("change",function(e) {
		if(validateFile()) {
			handleRefreshEvent();
			updateClassPhoto();
		}
	});
}

function validateFile() {
	var result = true;
	if($("#fileUpload").get(0).files.length==0) {
		result = false;
	}
	if(!validateFileType("#fileUpload","IMAGE")) {
		alertify.warning("Invalid File Type. Please Try another one.");
		result = false;
	} 
	if(!validateFileSize("#fileUpload","IMAGE")) {
		result = false;
	}
	return result;
}

function updateClassPhoto() {
	blockUI("Uploading Media File..");
	var classId = $("#classId").val();
	$.when(ajax.upload("classinfo/"+classId+"/classPhotoUrl","uploadPhotoFrm")).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				alertify.success("Successfully Updated!");
				$("#classPhoto").attr("src",response.classInfo.classPhotoUrl);
				$(window).unbind('beforeunload');
				$.unblockUI();
				break;
		}
	});
}