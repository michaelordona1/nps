/**
 * 
 */

$(function(){
	defineVariables();
	initEmployeeExamAnswers();
	
	
	attachListenerToQuestionNoLink();
	attachListenerToNextBtn();
	attachListenerToPrevBtn();
	attachListenerToSubmitBtn();
})

function defineVariables() {
	questionList = [];
	noOfQuestions = 0;
	prevQuestionNo = 0;
	nextQuestionNo = 2;
	employeeExamAnswerList = [];
	analogyAnswerList = [];
	fillInTheBlanksAnswersList = [];
	choiceList = [];
	sectionOrder = $("#hdnSectionOrder").val();
	examId = $("#hdnExamId").val();	
	courseId = $("#hdnCourseId").val();
	classId = $("#hdnClassId").val();
	correctMultipleChoiceAnswersCtr = 0;
	employeeId = $("#hdnEmployeeId").val();
	employeeExamAnswerHolder = [];
	retakeNo = $("#hdnRetakeNo").val();
	numbers = /^[0-9]+$/;
}

function attachListenerToQuestionNoLink() {
	$("body").on("click", ".questionNoLink", function () {
		var questionNo = parseInt($(this).data("ctr"));
		prevQuestionNo = questionNo - 1;
		nextQuestionNo = questionNo + 1;
		initQuestion(questionNo);
	});
}

function attachListenerToNextBtn() {
	$("#nextBtn").on("click", function () {
		initQuestion(nextQuestionNo);
		nextQuestionNo++;
		prevQuestionNo++;
	});
}

function attachListenerToPrevBtn() {
	$("#prevBtn").on("click", function () {
		initQuestion(prevQuestionNo);
		nextQuestionNo--;
		prevQuestionNo--;
	});
}

function attachListenerToSubmitBtn(){
	$("#submitBtn").click(function(){
		alertify.confirm("Submit", "Are you sure?",
			function(){
				var employeeExamProgress = {};
				employeeExamProgress.classId = classId;
				employeeExamProgress.employeeId = employeeId;
				employeeExamProgress.examId = examId;
				employeeExamProgress.sectionOrder = sectionOrder;
				employeeExamProgress.employeeExamAnswers = employeeExamAnswerHolder;
				employeeExamProgress.retakeNo = retakeNo;
				
				$.when(ajax.customUpdate("employeeexamprogress/exam/essay", employeeExamProgress)).done(function(response){
					if(response.status = HttpStatus.SUCCESS){
						alertify.success("SUCCESS!");
						redirect(`admin.classexam?classId=${classId}`,1000);
					}
				})
			},
			null
		);
	})
}

function renderNavigationButtons(currentQuestionNo) {
	if (currentQuestionNo == 1) {
		$("#prevBtn").hide();
	} else {
		$("#prevBtn").show();
	}
	if (currentQuestionNo >= noOfQuestions) {
		$("#nextBtn").hide();
	} else {
		$("#nextBtn").show();
	}
}

function initQuestionNumberCon() {
	$("#questionNumbersContainer").append(createQuestionNumberCon());
}

function createQuestionNumberCon() {
	var min = 1;
	var max = 5;
	var ctr = 1;
	var html = [];
	html.push("<tr>");
	while (noOfQuestions >= ctr) {
		if (min <= ctr && ctr <= max) {
			html.push("<td><a href='#!' class='questionNoLink' data-ctr='" + ctr + "'>" + ctr + "</a></td>");
			ctr++;
		} else {
			html.push("</tr>");
			html.push("<tr>");
			min += 5;
			max += 5;
		}
	}
	return html.join(" ");
}

function initEmployeeExamAnswers() {
	$.when(ajax.fetchWithData("classexam/"+examId+"/sectionOrder/"+sectionOrder+"/examreview", {classId: classId, courseId: courseId, employeeId: employeeId} )).done(function(response){
		var lastQuestionId = 0;
		_.each(response.employeeExamReviewerCommand.employeeExamAnswers, function(employeeExamAnswer){
			employeeExamAnswerList.push(employeeExamAnswer);
			noOfQuestions++;
		})
		initQuestionNumberCon();
		initQuestion(1);
		$("#totalQuestions").html(noOfQuestions);
	})
}



function showQuestionDiv(dom) {
	$("#essayCon").hide();
	$(dom).show();
}

function getQuestionData(questionNo){
	var questionObj = {};
	var iterator = 0;
	var lastQuestionId = 0;
	_.each(employeeExamAnswerList, function(question){
		if(lastQuestionId != question.questionId){
			lastQuestionId = question.questionId;
			iterator++;
		}
		if(iterator == questionNo){
			questionObj = question;
		}
	})
	return questionObj;
}

function initQuestionImage(dom, mediaUrl) {
	showQuestionImageCon(dom, mediaUrl);
	$(".questionPic").attr("src", mediaUrl);
}

function showQuestionImageCon(dom, mediaUrl) {
	$("#essayMedia").hide();
	if (mediaUrl != null) {
		$(dom).show();
	}
}

function initQuestion(questionNo){
	var question = getQuestionData(questionNo);
	renderNavigationButtons(questionNo);
	$("#currentQuestionNo").html(questionNo);
	$("#hdnExamQuestionId").val(question.examQuestionId);
	$("#hdnIsRandomized").val(question.isRandomized);
	
	switch(question.questionType){
		case QuestionType.ESSAY:
			populateEssay(question);
			break;
	}
}

function populateEssay(question) {
	
	var idx = employeeExamAnswerHolder.indexOf(question);
	
	showQuestionDiv("#essayCon");
	$("#essayMedia").attr("src",question.mediaUrl);
	$("#essayQuestionCon").html(question.content);
	$("#essayForm").empty();
	$("#essayForm").append($("<div/>").attr({"id":"essayAnswer_" + question.questionId,"class":"txtarea"}));
	$("#essayForm")
	.append(
			$("<div>").addClass("row col s12 m12")
				.append($("<div>").addClass("col s6 m6")
					.append($("<b>").html("Grammar/Spelling (20%) "))
					.append($("<input>").attr({"id":"grammar_"+question.questionId, "type":"number", "min":"0", "max":"20"}).val((employeeExamAnswerHolder[idx] != undefined ? employeeExamAnswerHolder[idx].grammar : question.grammar))
						.bind("keyup", function(){
							if(!this.value.match(numbers) || this.value < 0){this.value = 0;}
							if(this.value > 20){ this.value=20;}
							question.grammar = parseFloat(this.value);
						})
					)
				)
				.append($("<div>").addClass("col s6 m6")
					.append($("<b>").html("Content (50%) "))
					.append($("<input>").attr({"id":"content_"+question.questionId, "type":"number", "min":"0", "max":"50"}).val((employeeExamAnswerHolder[idx] != undefined ? employeeExamAnswerHolder[idx].essayContent : question.essayContent))
						.bind("keyup", function(){
							if(!this.value.match(numbers) || this.value < 0){this.value = 0;}
							if(this.value > 50){ this.value=50;}
							question.essayContent = parseFloat(this.value);
						})
					)
				)
	)
	.append(
			$("<div>").addClass("row col s12 m12")
				.append($("<div>").addClass("col s6 m6")
					.append($("<b>").html("Thoughts (15%) "))
					.append($("<input>").attr({"id":"thoughts_"+question.questionId, "type":"number", "min":"0", "max":"15"}).val((employeeExamAnswerHolder[idx] != undefined ? employeeExamAnswerHolder[idx].thoughts : question.thoughts))
						.bind("keyup", function(){
							if(!this.value.match(numbers) || this.value < 0){this.value = 0;}
							if(this.value > 15){ this.value=15;}
							question.thoughts = parseFloat(this.value);
						})	
					)
				)
				.append($("<div>").addClass("col s6 m6")
					.append($("<b>").html("Structure (15%) "))
					.append($("<input>").attr({"id":"structure_"+question.questionId, "type":"number", "min":"0", "max":"15"}).val((employeeExamAnswerHolder[idx] != undefined ? employeeExamAnswerHolder[idx].structure : question.structure))
						.bind("keyup", function(){
							if(!this.value.match(numbers) || this.value < 0){this.value = 0;}
							if(this.value > 15){ this.value=15;}
							question.structure = parseFloat(this.value);
						})	
					)
				)			
	)
	.append(
			$("<div>").addClass("col s12 m5")
				.append($("<label>")
					.append($("<input/>").attr({"type":"checkbox","id":"essayIsCorrect_"+question.questionId })
							.bind("click",function(){
								question.isCorrect = $(this).is(":checked") ? 1 : 0;
							})
							.prop("checked", question.isCorrect)
					)
					.append($("<span/>").html("Is correct?"))
				)
	)
	
	
	initTinyMceExamEditor(".txtarea" , 500);
	tinymce.activeEditor.setContent(question.employeeAnswer, {format: 'html'});
	
	
	// ONLY PUSH QUESTION TO ARRAY THAT DOESN'T EXIST YET IN ARRAY
	if(employeeExamAnswerHolder.indexOf(question) === -1){
		employeeExamAnswerHolder.push(question);
	}
	
}


function validateInput(input){
	if(!input.value.match(numbers) || input.value < 0){
		input.value = 0;
	}else{
		console.log("A MATCH");
	}
	return input.value;
}

function initTinyMceExamEditor(dom,height) {
	return tinymce.init({
		  selector: dom,
		  plugins: Editor.PLUGINS,
		  toolbar: Editor.TOOLBAR,
		  theme: 'silver',
		  menubar: Editor.MENUBAR,
		  paste_as_text: true,
		  height: height,
		  branding:false,
		  readonly : 1
	});
}
