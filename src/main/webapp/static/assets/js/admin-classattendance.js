/**
 * 
 */

$(function(){
	initClassInfo();
	defineVariables();
	attachListenerToScheduleListDropDown();
	attachListenerToUploadPhotoLink();
	attachListenerToUploadPhoto();
});

function defineVariables() {
	classTraineeList = [];
	classAttendanceList = [];
	scheduledLists = [];
	scheduledListArray = 1;
	allowDropdownChange = 0;
}

function initClassInfo() {
	if($("#deliveryMethod").val() == DeliveryMethod.INDIVIDUAL){
		$("#divAttendance1").css("display","none");
		$("#divAttendance2").css("display","");
		$("#deliveryMethodLabel").html("Individual");
	} else if($("#deliveryMethod").val() == DeliveryMethod.MODULAR){
		$("#divAttendance1").css("display","none");
		$("#divAttendance2").css("display","");
		$("#deliveryMethodLabel").html("Modular");
	} else if($("#deliveryMethod").val() == DeliveryMethod.CLASSTRAINING){
		$("#divAttendance1").css("display","");
		$("#divAttendance2").css("display","none");
		$("#deliveryMethodLabel").html("Class Training");
	}
	populateScheduleDropdown();
}

function populateScheduleDropdown() {
	var classId = parseInt(GetURLParameter('classId'));
	
	var classSchedule = "";
	var startDate = "";
	var endDate = "";
	var date1 = "";
	var date2 = "";
	var startTime = "";
	var endTime = "";
	
	var month = new Array();
	month[0] = "January"; month[1] = "February"; month[2] = "March"; month[3] = "April";
	month[4] = "May"; month[5] = "June"; month[6] = "July"; month[7] = "August";
	month[8] = "September"; month[9] = "October"; month[10] = "November";
	month[11] = "December";

	$.when(ajax.fetch("classinfo/"+classId+"/classschedule")).done(function(response) {
		if(response.status == HttpStatus.SUCCESS){
			if($("#scheduleType").val() == ScheduleType.BLOCK){
				classSchedule = response.classInfo.classBlockSchedule;
				startDate = classSchedule.startDate;
				endDate = classSchedule.endDate;
				date1 = new Date(startDate);
				date2 = new Date(endDate);
				startTime = classSchedule.startTime;
				endTime = classSchedule.endTime;
				
				$("#dropdownScheduleList").html("");
				$("#dropdownScheduleList").append($("<option/>")
					.attr({"selected":"","disabled":"disabled"}));
				for(var d = date1; d <= date2; d.setDate(d.getDate() + 1)){
					
					var monthStr = ("0" + (date1.getMonth() + 1)).slice(-2);
					var dateStr = ("0" + date1.getDate()).slice(-2);
					
					scheduledList = {};
					scheduledList.listId = scheduledListArray;
					scheduledList.classId = classId;
					scheduledList.dateAccessed = date1.getFullYear()+"-"+monthStr+"-"+dateStr;
					scheduledList.startTime = startTime;
					scheduledList.endTime = endTime;
					
					$("#dropdownScheduleList").append($("<option/>")
						.attr("value",scheduledListArray)
						.html(month[date1.getMonth()]+" "
								+date1.getDate()+","+date1.getFullYear()
								+" ("+startTime+"-"+endTime+")"));
					
					scheduledLists.push(scheduledList);
					scheduledListArray++;
				}
			}else if($("#scheduleType").val() == ScheduleType.SET){
				classSchedule = response.classInfo.classSetSchedules;
				$("#dropdownScheduleList").html("");
				_.each(classSchedule,function(classSetSchedule) {
					startDate = classSetSchedule.startDate;
					endDate = classSetSchedule.endDate;
					date1 = new Date(startDate);
					date2 = new Date(endDate);
					startTime = classSetSchedule.startTime;
					endTime = classSetSchedule.endTime;
					
					
					$("#dropdownScheduleList").append($("<option/>")
						.attr({"selected":"","disabled":"disabled"}));
					for(var d = date1; d <= date2; d.setDate(d.getDate() + 1)){
						
						var monthStr = ("0" + (date1.getMonth() + 1)).slice(-2);
						var dateStr = ("0" + date1.getDate()).slice(-2);
						
						scheduledList = {};
						scheduledList.listId = scheduledListArray;
						scheduledList.classId = classId;
						scheduledList.dateAccessed = date1.getFullYear()+"-"+monthStr+"-"+dateStr;
						scheduledList.startTime = startTime;
						scheduledList.endTime = endTime;

						$("#dropdownScheduleList").append($("<option/>")
							.attr("value",scheduledListArray)
							.html(month[date1.getMonth()]+" "
								+date1.getDate()+","+date1.getFullYear()
								+" ("+startTime+"-"+endTime+")"));
						
						scheduledLists.push(scheduledList);
						scheduledListArray++;
					}
				});
				
			}else if($("#scheduleType").val() == ScheduleType.SERIES){
				classSchedule = response.classInfo.classSeriesSchedule;
				startDate = classSchedule.startDate;
				endDate = classSchedule.endDate;
				date1 = new Date(startDate);
				date2 = new Date(endDate);
				startTime = classSchedule.startTime;
				endTime = classSchedule.endTime;
			}
		}
	});
}

function attachListenerToScheduleListDropDown() {
	$("#dropdownScheduleList").on("change", function() {
		var listId = $(this).val();
		_.each(scheduledLists,function(scheduledList) {
			if(scheduledList.listId == listId){
				
				updateClassAttendance(scheduledList.classId, scheduledList.dateAccessed);	
				
				tableCommand = {};
				tableCommand.listId = scheduledList.listId;
				tableCommand.classId = scheduledList.classId;
				tableCommand.dateAccessed = scheduledList.dateAccessed;
				tableCommand.startTime = scheduledList.startTime;
				tableCommand.endTime = scheduledList.endTime;

				if($("#deliveryMethod").val() == DeliveryMethod.INDIVIDUAL){
					initCBTAttendanceTbl(tableCommand);
				} else if($("#deliveryMethod").val() == DeliveryMethod.MODULAR){
					initCBTAttendanceTbl(tableCommand);
				} else if($("#deliveryMethod").val() == DeliveryMethod.CLASSTRAINING){
					$.when(ajax.fetchWithData("classinfo/"+scheduledList.classId+"/classattendance/list",tableCommand)).done(function(response) {
						var classAttendances = response.classAttendances;
						classAttendanceList = response.classAttendances;
						initApprovedTraineeTbl(tableCommand);
					});
				}
				var classId = $("#classId").val();
				var scheduleType = $("#scheduleType").val();
				var deliveryMethod = $("#deliveryMethod").val();
				var dateAccessed =  scheduledList.dateAccessed;
				$("#printClassAttendanceBtn").attr("href","classinfo/"+classId+"/classschedules/"+scheduleType+"/delivery/"+deliveryMethod+"/classattendance/date/"+dateAccessed+"/report");
			}
		});
	});
}

function createClassAttendanceList(classAttendance) {
	$("#tblAttendance2Body").append($("<tr/>")
		.append($("<td/>").html(classAttendance.fullName))
		.append($("<td/>").html(classAttendance.employeeCode))
		.append($("<td/>").html(classAttendance.userGroupName))
		.append($("<td/>").html(classAttendance.jobName))
		.append($("<td/>").html(classAttendance.attendanceStatus))
		.append($("<td/>")
			.append($("<input/>")
				.attr({"id":"remarks_"+classAttendance.classEmployeeId,
					"class":"traineeRemarks",
					"data-id":+classAttendance.classEmployeeId,
					"type":"text",
					"value":classAttendance.remarks,
					"disabled": ($("#userType").val() == 1 || $("#loggedInId").val() == $("#trainerId").val()  ? false : "disabled")}))));
}

function initApprovedTraineeTbl(tableCommand) {
	allowDropdownChange = 0;
	var classId = $("#classId").val();
	
	$("#attendancePagination1").pagination({     
        dataSource: "classinfo/"+classId+"/classattendance/pages",
        locator: "classAttendances",
        totalNumberLocator: function(response) {            
            return response.totalRecords;
        },
        ajax: {data: tableCommand, type: "GET"},
        className: "paginationjs-theme-blue",
        pageSize: 10,     
        callback: function(data) { 
        	$("#tblAttendance1Body").empty();
            classTraineeList = data;
            _.each(data, function(classAttendance) {
            	createClassroomTrainingRow(classAttendance);
            });
            //testListener();
            attachAttendanceStatusAndRemarksListener();
            allowDropdownChange = 1;
        }
    });	
}

function initCBTAttendanceTbl(tableCommand) {
	var classId = $("#classId").val();
	$("#attendancePagination2").pagination({     
        dataSource: "classinfo/"+classId+"/classattendance/pages",
        locator: "classAttendances",
        totalNumberLocator: function(response) {            
            return response.totalRecords;
        },
        ajax: {data: tableCommand, type: "GET"},
        className: "paginationjs-theme-blue",
        pageSize: 10,     
        callback: function(data) { 
        	$("#tblAttendance2Body").empty();
            classTraineeList = data;
            _.each(data, function(classAttendance) {
            	createClassAttendanceList(classAttendance);
            });
            attachAttendanceRemarksListener();
        }
    });
}

function createClassroomTrainingRow(classAttendance) {
	$("#tblAttendance1Body").append($("<tr/>")
		.append($("<td/>").attr({"class":"mid"}).html(classAttendance.fullName))
		.append($("<td/>").attr({"class":"mid"}).html(classAttendance.employeeCode))
		.append($("<td/>").attr({"class":"mid"}).html(classAttendance.userGroupName))
		.append($("<td/>").attr({"class":"mid"}).html(classAttendance.jobName))
		.append($("<td/>")
			.append($("<select/>")
				.attr({"id":"status_"+classAttendance.classEmployeeId,
					"class":"attendanceStatus",
					"data-attendanceid":+classAttendance.classEmployeeId,
					"data-id":+classAttendance.classEmployeeId,
					"disabled": ($("#userType").val() == 1 || $("#loggedInId").val() == $("#trainerId").val()  ? false : "disabled")})
				.append(new Option("",""))
				.append(new Option("Present", "PRESENT", true, classAttendance.attendanceStatus == "PRESENT" ? true : false))
				.append(new Option("Absent", "ABSENT", true, classAttendance.attendanceStatus == "ABSENT" ? true : false))
				.append(new Option("Excused", "EXCUSED", true, classAttendance.attendanceStatus == "EXCUSED" ? true : false))
				.append(new Option("Cancelled", "CANCELLED", true, classAttendance.attendanceStatus == "CANCELLED" ? true : false))
				)
				)
		.append($("<td/>")
			.append($("<input/>")
				.val(classAttendance.remarks)
				.attr({"id":"remarks_"+classAttendance.classEmployeeId,
					"class":"traineeRemarks",
					"data-id":+classAttendance.classEmployeeId,
					"type":"text",
					"disabled": ($("#userType").val() == 1 || $("#loggedInId").val() == $("#trainerId").val()  ? false : "disabled")}))));
	$("#status_"+classAttendance.classEmployeeId).select2({placeholder: "Choose an option"});
	$("#status_"+classAttendance.classEmployeeId).val(classAttendance.attendanceStatus).trigger('change');
}

function testListener() {
	
	if(classAttendanceList.length == 0){
		$(".attendanceStatus").val(null).trigger("change");
	} else {
		_.each(classAttendanceList, function(classAttendance) {
			$("#status_"+classAttendance.classEmployeeId).val(classAttendance.attendanceStatus).trigger("change");
			$("#remarks_"+classAttendance.classEmployeeId).val(classAttendance.remarks);
		});
	}
}

function attachAttendanceStatusAndRemarksListener() {
	var classAttendance = {};
	var classId = $("#classId").val();
	var listId = $("#dropdownScheduleList").val();

	$(".attendanceStatus").on("change",function() {
		if(allowDropdownChange != 0){
			var classEmployeeId = $(this).data("id");
			_.each(scheduledLists,function(scheduledList) {
				if(scheduledList.listId == listId){
					classAttendance.classId = scheduledList.classId;
					classAttendance.dateAccessed = scheduledList.dateAccessed;
					classAttendance.startTime = scheduledList.startTime;
					classAttendance.endTime = scheduledList.endTime;
					classAttendance.classEmployeeId = classEmployeeId;
					classAttendance.attendanceStatus = $("#status_"+classEmployeeId).val();
					
					$.when(ajax.customUpdate("classinfo/"+classId+"/classattendance/attendance_status",classAttendance)).done(function(response) {
						switch(response.status) {
							case HttpStatus.SUCCESS:
								alertify.success("Successfully Updated.");
								$("#dropdownScheduleList").trigger("change");
								break;
						}
					});
					classAttendance = {};
				}
			});
		}
	});
	$(".traineeRemarks").on("focusout",function() {
		var classEmployeeId = $(this).data("id");
		_.each(scheduledLists,function(scheduledList) {
			if(scheduledList.listId == listId){
				classAttendance.classId = scheduledList.classId;
				classAttendance.dateAccessed = scheduledList.dateAccessed;
				classAttendance.startTime = scheduledList.startTime;
				classAttendance.endTime = scheduledList.endTime;
				classAttendance.classEmployeeId = classEmployeeId;
				classAttendance.remarks = $("#remarks_"+classEmployeeId).val();

				$.when(ajax.customUpdate("classinfo/"+classId+"/classattendance/attendance_remarks",classAttendance)).done(function(response) {
					switch(response.status) {
						case HttpStatus.SUCCESS:
							alertify.success("Successfully Updated.");
							$("#dropdownScheduleList").trigger("change");
							break;
					}
				});
				classAttendance = {};
			}
		});
	});
}

function attachAttendanceRemarksListener() {
	var classAttendance = {};
	var classId = $("#classId").val();
	var listId = $("#dropdownScheduleList").val();
	
	$(".traineeRemarks").on("focusout",function() {
		var classEmployeeId = $(this).data("id");
		_.each(scheduledLists,function(scheduledList) {
			if(scheduledList.listId == listId){
				classAttendance.classId = scheduledList.classId;
				classAttendance.dateAccessed = scheduledList.dateAccessed;
				classAttendance.startTime = scheduledList.startTime;
				classAttendance.endTime = scheduledList.endTime;
				classAttendance.classEmployeeId = classEmployeeId;
				classAttendance.remarks = $("#remarks_"+classEmployeeId).val();

				$.when(ajax.customUpdate("classinfo/"+classId+"/classattendance/attendance_remarks",classAttendance)).done(function(response) {
					switch(response.status) {
						case HttpStatus.SUCCESS:
							alertify.success("Successfully Updated.");
							$("#dropdownScheduleList").trigger("change");
							break;
					}
				});
				classAttendance = {};
			}
		});
	});
}

function attachListenerToUploadPhotoLink() {
	$("#uploadPhotoLink").on("click",function() {
		$("#fileUpload").trigger("click");
	});
}

function attachListenerToUploadPhoto() {
	$("#fileUpload").on("change",function(e) {
		if(validateFile()) {
			handleRefreshEvent();
			updateClassPhoto();
		}
	});
}

function validateFile() {
	var result = true;
	if($("#fileUpload").get(0).files.length==0) {
		result = false;
	}
	if(!validateFileType("#fileUpload","IMAGE")) {
		alertify.warning("Invalid File Type. Please Try another one.");
		result = false;
	} 
	if(!validateFileSize("#fileUpload","IMAGE")) {
		result = false;
	}
	return result;
}

function updateClassPhoto() {
	blockUI("Uploading Media File..");
	var classId = $("#classId").val();
	$.when(ajax.upload("classinfo/"+classId+"/classPhotoUrl","uploadPhotoFrm")).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				alertify.success("Successfully Updated!");
				$("#classPhoto").attr("src",response.classInfo.classPhotoUrl);
				$(window).unbind('beforeunload');
				$.unblockUI();
				break;
		}
	});
}

function updateClassAttendance(classId,dateAccessed){
	var url = "classinfo/"+classId+"/classattendance";
	var classAttendance = {};
	classAttendance.dateAccessed = dateAccessed;
	$.when(ajax.customUpdate(url,classAttendance)).done(function(response){
		if(response.status == HttpStatus.SUCCESS){
			console.log("UPDATING CLASS ATTENDANCE SUCCESSFUL!");
		}else if(response.status == HttpStatus.NO_ATTENDANCES_FOUND){
			console.warn(Message.NO_ATTENDANCES_FOUND);
		}else{
			console.error(UNHANDLED_ERROR_MESSAGE);
		}		
	});
}