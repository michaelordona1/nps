/**
 * 
 */

$(function(){
	defineGlobalList();
	initClassFields();
	allowNumbersOnly();
	attachListenerToCourseDropdownChanged();
	attachListenerToScheduleTypeDropdownChanged();
	attachListenerToMonthlyRadioButtonChanged();
	attachListenerToRecurrenceDropdownChanged();
	attachListenerToOccurenceDropDown();
	attachListenerToAttendeeScale();
	attachListenerToAddSetSchedule();
	attachListenerToDaysCheckBox();
	attachListenerToSaveButton();
});

function defineGlobalList() {
	tempClassSchedules = [];
	tempDays = [];
	tempCheckedDays = [];
	tempSeriesParameter = [];
}


function initClassFields() {
	$.when(ajax.fetch("courseinfo/active/list")).done(function(response) {
		populateCourseDropdown(response.courseInfos);
	});
	$.when(ajax.fetch("locations/status/" + Status.ACTIVE)).done(function(response) {
		populateLocationDropdown(response.locations);
	});
	$.when(ajax.fetch("employeeinfo/trainers/status/"+Status.ACTIVE)).done(function(response) {
		populateEmployeeDropdown(response.employeeInfos);
	})
}

function populateCourseDropdown(courseInfos) {
	$("#courseDropdown").html("");
	$("#courseDropdown").append($("<option/>").attr({"selected":"","disabled":"disabled"}));
	_.each(courseInfos,function(courseInfo) {
	$("#courseDropdown").append($("<option/>").attr("value",courseInfo.courseId).html(courseInfo.courseName));
	})
}

function populateLocationDropdown(locations) {
	$("#locationDropdown").html("");
	$("#locationDropdown").append($("<option/>").attr({"selected":"","disabled":"disabled"}));
	_.each(locations,function(location) {
	$("#locationDropdown").append($("<option/>").attr("value",location.locationId).html(location.locationName));
	})
}

function populateEmployeeDropdown(employeeInfos) {
	$("#trainerDropdown").html("");
	$("#trainerDropdown").append($("<option/>").attr({"selected":"","disabled":"disabled"}));
	_.each(employeeInfos,function(employeeInfo) {
	$("#trainerDropdown").append($("<option/>").attr("value",employeeInfo.employeeId).html(employeeInfo.fullName));
	})
}

function populateClassFields(classDefault) {
	if(classDefault != null){
		$("#locationDropdown").val(classDefault.locationId).trigger("change");
		$("#trainerDropdown").val(classDefault.employeeId).trigger("change");
		if(classDefault.isSelfRegister == "1"){
			$("#selfRegCheckBox").prop("checked",true).trigger('change');
		} else if(classDefault.isSelfRegister == "0"){
			$("#selfRegCheckBox").prop("checked",false).trigger('change');
		}
		if(classDefault.withCertificate == "1"){
			$("#certificateCheckBox").prop("checked",true).trigger('change');
		} else if(classDefault.withCertificate == "0"){
			$("#certificateCheckBox").prop("checked",false).trigger('change');
		}
		if(classDefault.withExam == "1"){
			$("#examCheckbox").prop("checked",true).trigger('change');
		} else if(classDefault.withExam == "0"){
			$("#examCheckbox").prop("checked",false).trigger('change');
		}
		$("#minAttendees").val(classDefault.minAttendee);
		$("#maxAttendees").val(classDefault.maxAttendee);
		$("#scheduleTypeDropdown").val(classDefault.scheduleType).trigger("change");
	}else{
		$("#locationDropdown").val("").trigger("change");
		$("#trainerDropdown").val("").trigger("change");
		$("#selfRegCheckBox").prop("checked",false).trigger('change');
		$("#certificateCheckBox").prop("checked",false).trigger('change');
		$("#examCheckbox").prop("checked",false).trigger('change');
		$("#minAttendees").val("");
		$("#maxAttendees").val("");
		$("#scheduleTypeDropdown").val("").trigger("change");
	}
	
}

function attachListenerToCourseDropdownChanged() {
	$("#courseDropdown").on("change",function(e) {
		var courseId = parseInt($(this).val());
		var courseName = $("#courseDropdown option:selected").text();

		$.when(ajax.fetch("classinfo/classdefault/"+courseId)).done(function(response) {
			populateClassFields(response.classdefault);
			var courseInitial = convertCourseNameToInitials(courseName);
			$.when(ajax.fetch("classinfo/generate-classcode/"+courseInitial)).done(function(response) {
				
				var classInfo = response.classInfo;
				$("#btnClassSave").attr("data-id",classInfo.classId);
				$("#classCodeTxtbox").val(classInfo.classCode);
				
			});
		});
	});
}

function attachListenerToAddSetSchedule() {
	$("#addSetScheduleSubmit").on("click",function(e) {
		var incField = 0;
		if($("#setDateStartPicker").val().length == 0){
			$("#setScheduleStartLabel").css("color","red");
			incField = 1;
		}else{$("#setScheduleStartLabel").css("color","");}
		
		if($("#setDateEndPicker").val().length == 0){
			$("#setScheduleEndLabel").css("color","red");
			incField = 1;
		}else{$("#setScheduleEndLabel").css("color","");}
		if(incField == 1){
			alertify.dismissAll();
			alertify.warning("Please fill up the required fields.");
			$("#addSetScheduleSubmit").removeAttr("disabled");
			$("#addSetScheduleSubmit").html("<i class='material-icons left'>save</i>Save");
		}else{
			setSchedule = {};
			setSchedule.ScheduleDateStart = $("#setDateStartPicker").val();
			setSchedule.ScheduleDateEnd = $("#setDateEndPicker").val();
			setSchedule.ScheduleHrStart = $("#setStartHr").val();
			setSchedule.ScheduleMinStart = $("#setStartMin").val();
			setSchedule.ScheduleMeridiemStart = $("#setMeridiemFrom").val();
			setSchedule.ScheduleHrEnd = $("#setEndHr").val();
			setSchedule.ScheduleMinEnd = $("#setEndMin").val();
			setSchedule.ScheduleMeridiemEnd = $("#setMeridiemTo").val();
			setClassSchedule(setSchedule);
			createSetScheduleRow(setSchedule);
			
			$("#addScheduleModal").modal('close');
		}
	});
}

function attachListenerToScheduleTypeDropdownChanged() {
	$("#scheduleTypeDropdown").on("change",function(e) {
		if($(this).val() == "1"){
			$("#blockSchedule").css("display","");
			$("#setSchedule").css("display","none");
			$("#seriesSchedule").css("display","none");
		}else if($(this).val() == "2"){
			$("#blockSchedule").css("display","none");
			$("#setSchedule").css("display","");
			$("#seriesSchedule").css("display","none");
		}else if($(this).val() == "3"){
			$("#blockSchedule").css("display","none");
			$("#setSchedule").css("display","none");
			$("#seriesSchedule").css("display","");
		}
	});
}

function attachListenerToMonthlyRadioButtonChanged() {
	 $("input:radio[name=monthlyType]").change(function () {
         if (this.value == "Date") {
        	 $("#dateDiv").css("display","");
			 $("#dayDiv").css("display","none");
			 $("#dayTextBox").val("");
			 $("#offsetTextBox").val("");
         }
         if (this.value == "Day") {
        	 $("#dateDiv").css("display","none");
			 $("#dayDiv").css("display","");
			 $("#dayTextBox").val("");
			 $("dayDropDown").prop("selectedIndex",0).val();
			 $("weekDropDown").prop("selectedIndex",0).val();
			 $("#monthlyOffsetTextBox").val("");
         }
     });
}

function attachListenerToRecurrenceDropdownChanged() {
	$("#recurrenceDropdown").on("change",function(e) {
		if($(this).val() == "1"){
			$("#weeklyDiv").css("display","");
			$("#monthlyDiv").css("display","none");
			$("#dateDiv").css("display","none");
			$("#dayDiv").css("display","none");
			$("#dateRadio").prop("checked",false);
			$("#dayRadio").prop("checked",false);
		}else if($(this).val() == "2"){
			$("#weeklyDiv").css("display","none");
			$("#monthlyDiv").css("display","");
			$("#dateDiv").css("display","none");
			$("#dayDiv").css("display","none");
			$("#dateRadio").prop("checked",false);
			$("#dayRadio").prop("checked",false);
		}
	});
	$("#recurrenceDropdown").trigger("change");
}

function attachListenerToOccurenceDropDown() {
	$("#endTypeDropDown").on("change",function(e) {
		if($(this).val() == "endAfter"){
			$("#scheduleEndDate").hide();
			$("#noOfOccurrence").show();
			$("#scheduleEndDate").val("");
		}else if($(this).val() == "endBy"){
			$("#scheduleEndDate").show();
			$("#noOfOccurrence").hide();
			$("#noOfOccurrence").val(0);
		}
	});
}

function attachListenerToAttendeeScale() {
	$("#minAttendees").on("keydown",function(e) {
		//var numOnly = e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57);
		if(!((e.keyCode > 95 && e.keyCode < 106)
			      || (e.keyCode > 47 && e.keyCode < 58) 
			      || e.keyCode == 8)) {
			        return false;
	    }
		//var minScale = parseInt($("#minAttendees").val());
		//var maxScale = parseInt($("#maxAttendees").val());
		/*if(minScale > maxScale || minScale == maxScale || minScale < 0){
			$("#minAttendees").val(maxScale-1);
		}*/
	});
	
	$("#maxAttendees").on("keydown",function(e) {
		//var numOnly = e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57);
		if(!((e.keyCode > 95 && e.keyCode < 106)
			      || (e.keyCode > 47 && e.keyCode < 58) 
			      || e.keyCode == 8)) {
			        return false;
	    }
		//var minScale = parseInt($("#minAttendees").val());
		//var maxScale = parseInt($("#maxAttendees").val());
		/*if(minScale > maxScale || minScale == maxScale || minScale < 0){
			$("#maxAttendees").val(minScale+1);
		}*/
	});
}

function createSetScheduleRow(setSchedule) {	
	$("#setScheduleTblBody").append($("<tr/>")
		.append($("<td/>").attr({"class":"mid"}).html(setSchedule.ScheduleDateStart))
		.append($("<td/>").attr({"class":"mid"}).html(setSchedule.ScheduleDateEnd))
		.append($("<td/>").attr({"class":"mins"})
//		 .append($("<a/>").attr("href","#!")
//		  .append($("<i/>").attr("class","material-icons").html("edit")))
		 .append($("<a/>").attr("href","#!")
				 .append($("<i/>").attr("class","material-icons").html("delete"))
				 .bind("click", function(){
					$(this).closest('tr').remove();
				 })
	)));
}

function setClassSchedule(setSchedule) {
	var blockDate = {};
	var classId = parseInt($("#btnClassSave").attr("data-id"));
	var dateStart = setSchedule.ScheduleDateStart;
	var hrStart = parseInt(setSchedule.ScheduleHrStart);
	var minStart = setSchedule.ScheduleMinStart;
	var meridiemStart = setSchedule.ScheduleMeridiemStart;
	
	var dateEnd = setSchedule.ScheduleDateEnd;
	var hrEnd = parseInt(setSchedule.ScheduleHrEnd);
	var minEnd = setSchedule.ScheduleMinEnd;
	var meridiemEnd = setSchedule.ScheduleMeridiemEnd;
	
	if(meridiemStart == "PM" && hrStart < 12)
	{
		hrStart = hrStart + 12;
	}
	else if(meridiemStart == "AM" && hrStart == 12)
	{
		hrStart = hrStart - 12;
	}
	
	if(meridiemEnd == "PM" && hrEnd < 12)
	{
		hrEnd = hrEnd + 12;
	}
	else if(meridiemEnd == "AM" && hrEnd == 12)
	{
		hrEnd = hrEnd - 12;
	}

	var timeStart = hrStart+":"+minStart+":00";
	var timeEnd = hrEnd+":"+minEnd+":00";
	blockDate.classId = classId;
	blockDate.startDate = dateStart;
	blockDate.endDate = dateEnd;
	blockDate.startTime = timeStart;
	blockDate.endTime = timeEnd;
	
	tempClassSchedules.push(blockDate);
}

function attachListenerToSaveButton() {
	$("#btnClassSave").on("click",function(e) {
		$("#btnClassSave").attr("disabled","disabled");
		$("#btnClassSave").html("Saving...");
		
		validateClassInfoFields();
	});
}

function convertCourseNameToInitials(name) {
	var matches = name.match(/\b(\w)/g);
	var acronym = matches.join('');
	return acronym.toUpperCase();
}

function validateClassInfoFields() {
	var incField = 0;
	
	if($("#courseDropdown").val() == 0 || $("#courseDropdown").val() == null){
		$("#courseLabel").css("color","red");
		incField = 1;
	}else{$("#courseLabel").css("color","");}
	
	if($("#classCodeTxtbox").val().length == 0){
		$("#codeLabel").css("color","red");
		incField = 1;
	}else{$("#codeLabel").css("color","");}
	
	if($("#classNameTxtbox").val().length == 0){
		$("#nameLabel").css("color","red");
		incField = 1;
	}else{$("#nameLabel").css("color","");}
	
	if($("#locationDropdown").val() == 0 || $("#locationDropdown").val() == null){
		$("#locationLabel").css("color","red");
		incField = 1;
	}else{$("#locationLabel").css("color","");}
	
	if($("#trainerDropdown").val() == 0 || $("#trainerDropdown").val() == null){
		$("#trainerLabel").css("color","red");
		incField = 1;
	}else{$("#trainerLabel").css("color","");}
	
	if($("#minAttendees").val().length == 0){
		$("#minLabel").css("color","red");
		incField = 1;
	}else{$("#minLabel").css("color","");}
	
	if($("#maxAttendees").val().length == 0){
		$("#maxLabel").css("color","red");
		incField = 1;
	}else{$("#maxLabel").css("color","");}
	
	if($("#scheduleTypeDropdown").val() == 0 || $("#scheduleTypeDropdown").val() == null){
		$("#schedLabel").css("color","red");
		incField = 1;
	}else{$("#schedLabel").css("color","");}
	
	if(incField != 1){
		$("#blockSDLabel").css("color","");
		$("#blockEDLabel").css("color","");
		$("#setSchedLabel").css("color","");
		var minScale = parseInt($("#minAttendees").val());
		var maxScale = parseInt($("#maxAttendees").val());
		if(minScale > maxScale){
			$("#minLabel").css("color","red");
			$("#maxLabel").css("color","red");
			$("#minAttendees").val("");
			$("#maxAttendees").val("");
			alertify.dismissAll();
			alertify.warning("Minimum value cannot be greater than Maximum value.");
			$("#btnClassSave").removeAttr("disabled");
			$("#btnClassSave").html("<i class='material-icons left'>save</i>Save");
		} else if(minScale == maxScale){
			$("#minLabel").css("color","red");
			$("#maxLabel").css("color","red");
			$("#minAttendees").val("");
			$("#maxAttendees").val("");
			alertify.dismissAll();
			alertify.warning("Minimum and Maximum value cannot be equal.");
			$("#btnClassSave").removeAttr("disabled");
			$("#btnClassSave").html("<i class='material-icons left'>save</i>Save");
		} else if(minScale < 0 || maxScale < 0){
			$("#minLabel").css("color","red");
			$("#maxLabel").css("color","red");
			$("#minAttendees").val("");
			$("#maxAttendees").val("");
			alertify.dismissAll();
			alertify.warning("Minimum and Maximum value must be a positive number.");
			$("#btnClassSave").removeAttr("disabled");
			$("#btnClassSave").html("<i class='material-icons left'>save</i>Save");
		} else if($("#scheduleTypeDropdown").val() == "1" && $("#blockDateStartPicker").val() == ""){
			$("#blockSDLabel").css("color","red");
			alertify.dismissAll();
			alertify.warning("Please Specify Start Date.");
			$("#btnClassSave").removeAttr("disabled");
			$("#btnClassSave").html("<i class='material-icons left'>save</i>Save");
		} else if($("#scheduleTypeDropdown").val() == "1" && $("#blockDateEndPicker").val() == ""){
			$("#blockEDLabel").css("color","red");
			alertify.dismissAll();
			alertify.warning("Please Specify End Date.");
			$("#btnClassSave").removeAttr("disabled");
			$("#btnClassSave").html("<i class='material-icons left'>save</i>Save");
		} else if($("#scheduleTypeDropdown").val() == "2" && tempClassSchedules.length == 0){
			$("#setSchedLabel").css("color","red");
			alertify.dismissAll();
			alertify.warning("Please add at least 1 set schedule date.");
			$("#btnClassSave").removeAttr("disabled");
			$("#btnClassSave").html("<i class='material-icons left'>save</i>Save");
		} else if($("#scheduleTypeDropdown").val() == "3" && 
				($("#scheduleStartDate").val() == "" || $("#endTypeDropDown").val() == null)){
			$("#seriesScheduleStarts").css("color","red");
			alertify.dismissAll();
			alertify.warning("Please Specify Start Date and End Type.");
			$("#btnClassSave").removeAttr("disabled");
			$("#btnClassSave").html("<i class='material-icons left'>save</i>Save");
		} else if($("#scheduleTypeDropdown").val() == "3" && $("#endTypeDropDown").val() == "endBy" &&
				($("#scheduleStartDate").val() == "" || $("#scheduleEndDate").val() == "")){
			alertify.dismissAll();
			$("#seriesScheduleStarts").css("color","red");
			alertify.warning("Please Specify Start and End Date.");
			$("#btnClassSave").removeAttr("disabled");
			$("#btnClassSave").html("<i class='material-icons left'>save</i>Save");
		} else if($("#scheduleTypeDropdown").val() == "3" && 
				$("#recurrenceDropdown").val() == "1" && tempCheckedDays.length == 0){
			alertify.dismissAll();
			$("#seriesScheduleStarts").css("color","red");
			alertify.warning("Please Specify Start and End Date.");
			$("#btnClassSave").removeAttr("disabled");
			$("#btnClassSave").html("<i class='material-icons left'>save</i>Save");
		} else {
			if($("#scheduleTypeDropdown").val() == "1"){
				$("#blockSDLabel").css("color","");
				$("#blockEDLabel").css("color","");
				setSchedule = {};
				setSchedule.ScheduleDateStart = $("#blockDateStartPicker").val();
				setSchedule.ScheduleDateEnd = $("#blockDateEndPicker").val();
				setSchedule.ScheduleHrStart = $("#blockStartHr").val();
				setSchedule.ScheduleMinStart = $("#blockStartMin").val();
				setSchedule.ScheduleMeridiemStart = $("#blockMeridiemFrom").val();
				setSchedule.ScheduleHrEnd = $("#blockEndHr").val();
				setSchedule.ScheduleMinEnd = $("#blockEndMin").val();
				setSchedule.ScheduleMeridiemEnd = $("#blockMeridiemTo").val();
				setClassSchedule(setSchedule);
			} else if($("#scheduleTypeDropdown").val() == "3"){
				$("#seriesScheduleStarts").css("color","");
				setSchedule = {};
				setSchedule.ScheduleDateStart = $("#seriesStartDate").val();
				setSchedule.ScheduleEndType = $("#endTypeDropDown").val();
				setSchedule.ScheduleDateEnd = $("#seriesEndDate").val();
				setSchedule.ScheduleDateAfter = $("#seriesEndAfter").val();
				setSchedule.ScheduleHrStart = $("#seriesStartHr").val();
				setSchedule.ScheduleMinStart = $("#seriesStartMin").val();
				setSchedule.ScheduleMeridiemStart = $("#seriesMeridiemStartDropDown").val();
				setSchedule.ScheduleHrEnd = $("#seriesEndHr").val();
				setSchedule.ScheduleMinEnd = $("#seriesEndMin").val();
				setSchedule.ScheduleMeridiemEnd = $("#seriesMeridiemEndDropDown").val();
				setClassSeries(setSchedule);
			}
			submitClassDetails();
		}
		
	} else{
		alertify.dismissAll();
		alertify.warning("Please fill up the required fields.");
		$("#btnClassSave").removeAttr("disabled");
		$("#btnClassSave").html("<i class='material-icons left'>save</i>Save");
	}
}

function setClassInfo() {
	var classinfo = {};
	classinfo.classCode = $("#classCodeTxtbox").val();
	classinfo.courseId = parseInt($("#courseDropdown").val());
	classinfo.locationId = parseInt($("#locationDropdown").val());
	classinfo.className = $("#classNameTxtbox").val();
	var scheduleType = $("#scheduleTypeDropdown").val();
	
	if ($('#selfRegCheckBox').is(':checked')) {
		classinfo.isSelfRegister = 1;
	} else if(!$('#selfRegCheckBox').is(':checked')){
		classinfo.isSelfRegister = 0;
	}
	if ($('#certificateCheckBox').is(':checked')) {
		classinfo.withCertificate = 1;
	} else if(!$('#certificateCheckBox').is(':checked')){
		classinfo.withCertificate = 0;
	}
	if ($('#examCheckbox').is(':checked')) {
		classinfo.withExam = 1;
	} else if(!$('#examCheckbox').is(':checked')){
		classinfo.withExam = 0;
	}
	classinfo.minAttendee = parseInt($("#minAttendees").val());
	classinfo.maxAttendee = parseInt($("#maxAttendees").val());
	classinfo.scheduleType = scheduleType;

	return classinfo;
}

function setClassEmployee() {
	var classEmployee = {};
	classEmployee.classId = parseInt($("#btnClassSave").attr("data-id"));
	classEmployee.employeeId = parseInt($("#trainerDropdown").val());
	classEmployee.role = 1;
	return classEmployee;
}

function setDayToInteger(day) {
	var convertedValue;
	switch(day) {
		case "Monday":
			convertedValue = 1;
			break;
		case "Tuesday":
			convertedValue = 2;
			break;
		case "Wednesday":
			convertedValue = 3;
			break;
		case "Thursday":
			convertedValue = 4;
			break;
		case "Friday":
			convertedValue = 5;
			break;
		case "Saturday":
			convertedValue = 6;
			break;
		case "Sunday":
			convertedValue = 7;
			break;
	}
}

function setClassSeriesParameterWeekly(selectedDay) {
	var classSeriesParameter = {};
	classSeriesParameter.classId = parseInt($("#btnClassSave").attr("data-id"));
	classSeriesParameter.recurrencePattern = parseInt($(recurrenceDropdown).val());
	classSeriesParameter.noOfOccurrence = $("#noOfOccurrence").val();
	classSeriesParameter.day = selectedDay;
	classSeriesParameter.week = 0;
	classSeriesParameter.monthOffSet = 0;

	return classSeriesParameter;
}

function setClassSeriesParameterMonthly() {
	var monthlyType = $(`input[name='monthlyType']:checked`).val();
	var classSeriesParameter = {};
	classSeriesParameter.classId = parseInt($("#btnClassSave").attr("data-id"));
	classSeriesParameter.recurrencePattern = parseInt($(recurrenceDropdown).val());
	classSeriesParameter.noOfOccurrence = $("#noOfOccurrence").val();
	//check if empty
	if (monthlyType === "Date") {
		classSeriesParameter.day = $("#dayTextBox").val();
		classSeriesParameter.monthOffset = $("#offsetTextBox").val();
		classSeriesParameter.week = 0;
	} else if (monthlyType === "Day") {
		classSeriesParameter.day = $("#dayDropDown").val();
		classSeriesParameter.monthOffset = $("#monthlyOffsetTextBox").val();
		classSeriesParameter.week = $("#weekDropDown").val();
	}
	return classSeriesParameter;
}

function submitClassDetails() {
	//var classId = parseInt($("#btnClassSave").attr("data-id"));
	var classInfo = setClassInfo();
	var classEmployee = setClassEmployee();
	var classSchedules = tempClassSchedules;
	
	
	var scheduleType = "";
	if(classInfo.scheduleType == "1"){
		scheduleType = "block";
	}else if(classInfo.scheduleType == "2"){
		scheduleType = "set";
	}else if(classInfo.scheduleType == "3"){
		scheduleType = "series";
	}
	
	
	$.when(ajax.fetch("classinfo/check-classcode/"+classInfo.classCode)).done(function(response) {
		if(response.status == HttpStatus.SUCCESS){
			if(response.totalRecords > 0){
				$("#btnClassSave").removeAttr("disabled");
				$("#codeLabel").css("color","red");
				$("#btnClassSave").html("<i class='material-icons left'>save</i>Save");
				alertify.dismissAll();
				alertify.warning("Class Code already exists. Please enter a different Class Code.");
			} else {
				$.when(ajax.create("classinfo",classInfo)).done(function(classInfoResponse) {
					var classId = classInfoResponse.classInfo.classId;
					if(classInfoResponse.status == HttpStatus.SUCCESS){
						
						
						// Save Class Trainer
						classEmployee.classId = classId;
						ajax.create("classemployee",classEmployee);
						
						
						// Save Class Series Parameters
						if (scheduleType === "series") {
							var classSeriesParameterList = [];
							
							var recurrencePatternValue = $("#recurrenceDropdown").val();
							if (recurrencePatternValue === "1") {
								if (tempCheckedDays.length > 0) {

									_.each(tempCheckedDays, function(day) {
										var classSeriesWeeklyParam = setClassSeriesParameterWeekly(day.typeOfDay);
										classSeriesParameterList.push(classSeriesWeeklyParam);
									});
								} else {
									$("#btnClassSave").removeAttr("disabled");
									$("#codeLabel").css("color","red");
									$("#btnClassSave").html("<i class='material-icons left'>save</i>Save");
									alertify.dismissAll();
									alertify.warning("Empty selected check box!");
									return;
								}
							} else if (recurrencePatternValue === "2") {
								var classSeriesMonthlyParam = setClassSeriesParameterMonthly();
								classSeriesParameterList.push(classSeriesMonthlyParam);
								
							}
							var seriesSchedule = {};
							var seriesMeridiemStartValue = $("#seriesMeridiemStartDropDown").val();
							var seriesMeridiemEndValue = $("#seriesMeridiemEndDropDown").val();
							
							var startTimeHr = parseInt($("#seriesStartHr").val());

							if (seriesMeridiemStartValue === "AM") {
								if (startTimeHr === 12) {
									startTimeHr = startTimeHr - 12;
								}
							} else {
								startTimeHr = startTimeHr + 12;
							}
							
							var startTimeMin = $("#seriesStartMin").val();
							
							var endTimeHr = parseInt($("#seriesEndHr").val());
							if (seriesMeridiemEndValue === "AM") {
								if (endTimeHr === 12) {
									endTimeHr = endTimeHr - 12;
								}
							} else {
								if (endTimeHr !== 12) {
									endTimeHr = endTimeHr + 12;
								} 
							}
							var endTimeMin = $("#seriesEndMin").val();

							seriesSchedule.startDate = $("#scheduleStartDate").val();
							var seriesEndDate = $("#scheduleEndDate").val();
							if (!seriesEndDate) {
								seriesSchedule.endDate = null;
							} else {
								seriesSchedule.endDate = seriesEndDate;
							}
							seriesSchedule.startTime = `${startTimeHr}:${startTimeMin}`
							seriesSchedule.endTime = `${endTimeHr}:${endTimeMin}`
							seriesSchedule.classId = classId;
							classSchedules.push(seriesSchedule);
							
							_.each(classSeriesParameterList, function(parameter){parameter.classId = classId;})
							ajax.customUpdate("classinfo/classschedules/seriesparameter", classSeriesParameterList)
						}						
						
						
						// Add Course Evaluations to class
						var courseId = classInfoResponse.classInfo.courseId;
						var classEvaluation = {};
						classEvaluation.classId = classId;
						classEvaluation.courseId = courseId;
						ajax.create("classevaluations", classEvaluation);
						
						
						// Save Class Schedules
						_.each(classSchedules, function(classSchedule){classSchedule.classId = classId;})
						$.when(ajax.customUpdate("classinfo/"+classId+"/classschedules/"+scheduleType,classSchedules)).done(function(response) {
							if(response.status == HttpStatus.SUCCESS){
								alertify.dismissAll();
								alertify.success("Successfully Created.");
								setTimeout(function(){redirect("admin.classdashboard?classId="+classId);}, 3000);
							}
						});
					}
				});
			}
		}
	});
}

function checkIfDayExist(days,day) {
	var result = false;
	_.each(days,function(dayObj) {
		if(dayObj.typeOfDay == day) {
			result = true;
		}
	});
	return result;
}

function deleteDayFromCheckList(day) {
	var result = false;
	tempDays = tempCheckedDays;
	_.each(tempDays,function(dayObj) {
		if(dayObj.typeOfDay == day) {
			tempCheckedDays = deleteItem(day);
		}
	});
	return result;
}

function deleteItem(day) {
	var resultArray = [];
	_.each(tempCheckedDays,function(dayObj) {
		if(dayObj.typeOfDay != day) {
			resultArray.push(dayObj);
		}
	});
	return resultArray;
}

function attachListenerToDaysCheckBox(){
	$("#chMonSeries").on("change",function(e) {
		var checkedDay = 1;
		checkDays(this,checkedDay);
	});
	$("#chTueSeries").on("change",function(e) {
		var checkedDay = 2;
		checkDays(this,checkedDay);
	});
	$("#chWedSeries").on("change",function(e) {
		var checkedDay = 3;
		checkDays(this,checkedDay);
	});
	$("#chThuSeries").on("change",function(e) {
		var checkedDay = 4;
		checkDays(this,checkedDay);
	});
	$("#chFriSeries").on("change",function(e) {
		var checkedDay = 5;
		checkDays(this,checkedDay);
	});
	$("#chSatSeries").on("change",function(e) {
		var checkedDay = 6;
		checkDays(this,checkedDay);
	});
	$("#chSunSeries").on("change",function(e) {
		var checkedDay = 7;
		checkDays(this,checkedDay);
	});
}

function checkDays(dom,day) {
	if($(dom).prop("checked") != true) {
		if(checkIfDayExist(tempCheckedDays,day)) {
			deleteDayFromCheckList(day);
		}
	} else if(!checkIfDayExist(tempCheckedDays,day)) {
		tempcheckDay = {};
		tempcheckDay.typeOfDay = day;
		tempCheckedDays.push(tempcheckDay);
	}
	console.log(tempCheckedDays);
}

function setClassSeries(setSchedule) {
	var date1 = new Date(setSchedule.ScheduleDateStart);
	var date2 = new Date(setSchedule.ScheduleDateEnd);
	
	var endType = "";

	//Weekly Recurrence
	if($("#recurrenceDropdown").val() == "1"){
		
		for (var d = date1; d <= date2; d.setDate(d.getDate() + 1)) {
			var weekday=new Array(7);
			weekday[0]="Sunday";
			weekday[1]="Monday";
			weekday[2]="Tuesday";
			weekday[3]="Wednesday";
			weekday[4]="Thursday";
			weekday[5]="Friday";
			weekday[6]="Saturday";
			
			tempDays = tempCheckedDays;
			
			_.each(tempDays,function(dayObj) {
				
				if(weekday[d.getDay()] == dayObj.typeOfDay) {
					var blockDate = {};
					var monthNumber = d.getMonth()+1;
					var dateNumber = d.getDate();
					var classId = parseInt($("#btnClassSave").attr("data-id"));
					var dateStart = d.getFullYear()+"-"+monthNumber+"-"+dateNumber;
					var hrStart = parseInt(setSchedule.ScheduleHrStart);
					var minStart = setSchedule.ScheduleMinStart;
					var meridiemStart = setSchedule.ScheduleMeridiemStart;
					
					var dateEnd = setSchedule.ScheduleDateEnd;
					var hrEnd = parseInt(setSchedule.ScheduleHrEnd);
					var minEnd = setSchedule.ScheduleMinEnd;
					var meridiemEnd = setSchedule.ScheduleMeridiemEnd;
					
					if(meridiemStart == "PM" && hrStart < 12)
					{
						hrStart = hrStart + 12;
					}
					else if(meridiemStart == "AM" && hrStart == 12)
					{
						hrStart = hrStart - 12;
					}
					
					if(meridiemEnd == "PM" && hrEnd < 12)
					{
						hrEnd = hrEnd + 12;
					}
					else if(meridiemEnd == "AM" && hrEnd == 12)
					{
						hrEnd = hrEnd - 12;
					}
					
					var timeStart = hrStart+":"+minStart+":00";
					var timeEnd = hrEnd+":"+minEnd+":00";
					
					blockDate.classId = classId;
					blockDate.startDate = dateStart;
					blockDate.endDate = dateStart;
					blockDate.startTime = timeStart;
					blockDate.endTime = timeEnd;
					
					tempClassSchedules.push(blockDate);
				}
			});
		} 
	} else if($("#recurrenceDropdown").val() == "2"){
		//Monthly Recurrence
		for (var d = date1; d <= date2; d.setDate(d.getMonth() + 1)) {
			var weekday=new Array(7);
			weekday[0]="Sunday";
			weekday[1]="Monday";
			weekday[2]="Tuesday";
			weekday[3]="Wednesday";
			weekday[4]="Thursday";
			weekday[5]="Friday";
			weekday[6]="Saturday";
			
			tempDays = tempCheckedDays;
			
			_.each(tempDays,function(dayObj) {
				
				if(weekday[d.getDay()] == dayObj.typeOfDay) {
					var blockDate = {};
					var monthNumber = d.getMonth()+1;
					var dateNumber = d.getDate();
					var classId = parseInt($("#btnClassSave").attr("data-id"));
					var dateStart = d.getFullYear()+"-"+monthNumber+"-"+dateNumber;
					var hrStart = parseInt(setSchedule.ScheduleHrStart);
					var minStart = setSchedule.ScheduleMinStart;
					var meridiemStart = setSchedule.ScheduleMeridiemStart;
					
					var dateEnd = setSchedule.ScheduleDateEnd;
					var hrEnd = parseInt(setSchedule.ScheduleHrEnd);
					var minEnd = setSchedule.ScheduleMinEnd;
					var meridiemEnd = setSchedule.ScheduleMeridiemEnd;
					
					if(meridiemStart == "PM" && hrStart < 12)
					{
						hrStart = hrStart + 12;
					}
					else if(meridiemStart == "AM" && hrStart == 12)
					{
						hrStart = hrStart - 12;
					}
					
					if(meridiemEnd == "PM" && hrEnd < 12)
					{
						hrEnd = hrEnd + 12;
					}
					else if(meridiemEnd == "AM" && hrEnd == 12)
					{
						hrEnd = hrEnd - 12;
					}
					
					var timeStart = hrStart+":"+minStart+":00";
					var timeEnd = hrEnd+":"+minEnd+":00";
					
					blockDate.classId = classId;
					blockDate.startDate = dateStart;
					blockDate.endDate = dateStart;
					blockDate.startTime = timeStart;
					blockDate.endTime = timeEnd;
					
					tempClassSchedules.push(blockDate);
				}
			});
		} 
	}
}