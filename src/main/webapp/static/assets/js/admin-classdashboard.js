const RECURRENCE_PATTERN_WEEKLY = 1;
const RECURRENCE_PATTERN_MONTHLY = 2;
$(function () {
	defineVariables();
	getAllEnrolledTrainees();
	populateAllExamDropDown();
	populateScheduleDropdown();
	showCurrentDeliveryMethod();
	attachPresentTraineesDropdownCTListener();
	attachAverageScoreCTDropDownListener();
	attachAverageExamTimeCTDropDownListener();
	attachExamPassersCTDropDownListener();
	attachListenerToUploadPhotoLink();
	attachListenerToUploadPhoto();
});

function defineVariables() {
	scheduleSet = [];
	var totalAverageTime;
	classSeriesParam = [];
}

function showCurrentDeliveryMethod() {
	var deliveryMethod = parseInt($("#deliveryMethod").val());
	if (deliveryMethod === DeliveryMethod.INDIVIDUAL) {
		$("#modularblock").hide();
		$("#classroomtrainerblock").hide();
		$("#individualblock").show();
		initCompletionRate();
		attachIndividualDropDownListener();
	} else if (deliveryMethod === DeliveryMethod.MODULAR) {
		$("#modularblock").show();
		$("#classroomtrainerblock").hide();
		$("#individualblock").hide();
		initCompletionRate();
		initLearningPathTbl();
		attachIndividualDropDownListener();
	} else if (deliveryMethod === DeliveryMethod.CLASSTRAINING) {
		$("#checklistDiv").show();
		showChecklistTab();
		attachCheckBoxListener();
		$("#classroomtrainerblock").show();
		$("#modularblock").hide();
		$("#individualblock").hide();
	}
}

function getAllEnrolledTrainees() {
	var classId = parseInt(GetURLParameter('classId'));
	$.when(ajax.fetch(`classinfo/${classId}/fetchallenrolledtrainee`)).done(function (response) {
		var totalEnrolled = response.totalRecords;
		$("#totalEnrolledTraineeLbl").text(totalEnrolled);
		$("#idEnrolledTraineeLbl").html(totalEnrolled);
		$("#mdTotalEnrolledTraineeLbl").html(totalEnrolled);
	});
}

function initCompletionRate() {
	var classId = parseInt(GetURLParameter('classId'));
	$.when(ajax.fetch(`classinfo/fetchCompletionRate/${classId}`)).done(function (response) {
		var completionRate = response.totalAverageRecords;
		$("#idCompletionRateLbl").html(`${completionRate == "NaN" ? "0%" : completionRate.toFixed(2) + "%"}`);
		$("#mdCompletionRateLbl").html(`${completionRate == "NaN" ? "0%" : completionRate.toFixed(2) + "%"}`);
	});
}

function populateAllExamDropDown() {
	var classId = parseInt(GetURLParameter('classId'));
	var courseId = $("#courseId").val();
	$.when(ajax.fetch(`classexam/list?classId=${classId}`)).done(function (response) {
		_.each(response.classExams, function (exam) {
			$("#examAverageScoreCTDropDown").append($("<option/>").attr("value", exam.examId).html(exam.title));
			$("#examAverageTimeCTDropDown").append($("<option/>").attr("value", exam.examId).html(exam.title));
			$("#examPassersCTDropDown").append($("<option/>").attr("value", exam.examId).html(exam.title));
			$("#examAnalysisDropdownIndividual").append($("<option/>").attr("value", exam.examId).html(exam.title));
			$("#examAnalysisDropdownModular").append($("<option/>").attr("value", exam.examId).html(exam.title));

		});

		$("#examAverageScoreCTDropDown").trigger("contentChanged");
		$("#examAverageTimeCTDropDown").trigger("contentChanged");
		$("#examPassersCTDropDown").trigger("contentChanged");
		$("#examAnalysisDropdownIndividual").trigger("contentChanged");
		$("#examAnalysisDropdownModular").trigger("contentChanged");

	});
	
	
	$.when(ajax.fetch('courseinfo/'+courseId+'/courseexams')).done(function(response){
		_.each(response.courseExams, function(exam){
			$("#examAverageScoreCTDropDown").append($("<option/>").attr("value", exam.examId).html(exam.title));
			$("#examAverageTimeCTDropDown").append($("<option/>").attr("value", exam.examId).html(exam.title));
			$("#examPassersCTDropDown").append($("<option/>").attr("value", exam.examId).html(exam.title));
			$("#examAnalysisDropdownIndividual").append($("<option/>").attr("value", exam.examId).html(exam.title));
			$("#examAnalysisDropdownModular").append($("<option/>").attr("value", exam.examId).html(exam.title));
		})
		
		$("#examAverageScoreCTDropDown").trigger("contentChanged");
		$("#examAverageTimeCTDropDown").trigger("contentChanged");
		$("#examPassersCTDropDown").trigger("contentChanged");
		$("#examAnalysisDropdownIndividual").trigger("contentChanged");
		$("#examAnalysisDropdownModular").trigger("contentChanged");
	})
	
	
}

function populateScheduleDropdown() {
	var classId = parseInt(GetURLParameter('classId'));
	var classSchedule = "";
	var startDate = "";
	var endDate = "";
	var date1 = "";
	var date2 = "";
	var startTime = "";
	var endTime = "";

	var month = new Array();
	month[0] = "January"; month[1] = "February"; month[2] = "March"; month[3] = "April";
	month[4] = "May"; month[5] = "June"; month[6] = "July"; month[7] = "August";
	month[8] = "September"; month[9] = "October"; month[10] = "November";
	month[11] = "December";

	$.when(ajax.fetch("classinfo/" + classId + "/classschedule")).done(function (response) {
		if (response.status == HttpStatus.SUCCESS) {
			if ($("#scheduleType").val() == ScheduleType.BLOCK) {
				classSchedule = response.classInfo.classBlockSchedule;
				startDate = classSchedule.startDate;
				endDate = classSchedule.endDate;
				date1 = new Date(startDate);
				date2 = new Date(endDate);
				startTime = classSchedule.startTime;
				endTime = classSchedule.endTime;
				time1 = new Date(date1.toDateString() + " " + startTime);
				time2 = new Date(date2.toDateString() + " " + endTime);

				$("#classScheduleTblBody").html("");
				$("#idClassScheduleTblBody").html("");
				$("#mdClassScheduleTblBody").html("");
				var dateDiff = time2 - time1
				var mins = Math.floor(dateDiff / 60000);
				totalAverageTime = mins;
				showAverageTrainingTimeLabel();
				for (var d = date1; d <= date2; d.setDate(d.getDate() + 1)) {

					var dayStartTime = "";
					var dayEndTime = "";
					var startTimeHr = parseInt(startTime.substring(0, 2));
					var startTimeMin = startTime.substring(3, 5);
					var endTimeHr = parseInt(endTime.substring(0, 2));
					var endTimeMin = endTime.substring(3, 5);

					if (startTimeHr == 0) {
						dayStartTime = `12:${startTimeMin}:00 AM`;
					} else if (startTimeHr > 12) {
						dayStartTime = `${convertNumbertoString(startTimeHr - 12)}:${startTimeMin}:00 PM`;
					} else if (startTimeHr == 12) {
						dayStartTime = `${startTimeHr}:${startTimeMin}:00 PM`;
					} else {
						dayStartTime = `${convertNumbertoString(startTimeHr)}:${startTimeMin}:00 AM`
					}

					if (endTimeHr == 0) {
						dayEndTime = `12:${endTimeMin}:00 AM`
					} else if (endTimeHr > 12) {
						dayEndTime = `${convertNumbertoString(endTimeHr - 12)}:${endTimeMin}:00 PM`
					} else if (endTimeHr == 12) {
						dayEndTime = `${endTimeHr}:${endTimeMin}:00 PM`
					} else {
						dayEndTime = `${convertNumbertoString(endTimeHr)}:${endTimeMin}:00 AM`
					}
					var date = month[date1.getMonth()] + " " + date1.getDate() + ", " + date1.getFullYear();
					$
					var schedule = setSchedule(date, dayStartTime, dayEndTime);
					scheduleSet.push(schedule);
					$("#scheduleDropdown").append($("<option/>").attr("value", `${date1.getFullYear}-${date1.getMonth}-${date1.getDate}`).html(date));
				}
				initClassScheduleList();
				$("#scheduleDropdown").trigger("contentChanged");

			} else if ($("#scheduleType").val() == ScheduleType.SET) {
				classSchedule = response.classInfo.classSetSchedules;
				$("#classScheduleTblBody").html("");
				$("#idClassScheduleTblBody").html("");
				$("#mdClassScheduleTblBody").html("");

				_.each(classSchedule, function (classSetSchedule) {

					startDate = classSetSchedule.startDate;
					endDate = classSetSchedule.endDate;
					date1 = new Date(startDate);
					date2 = new Date(endDate);
					startTime = classSetSchedule.startTime;
					endTime = classSetSchedule.endTime;
					var dateDiff = endTime - startTime
					var mins = Math.floor(dateDiff / 60000);
					totalAverageTime = mins;
					showAverageTrainingTimeLabel();
					for (var d = date1; d <= date2; d.setDate(d.getDate() + 1)) {
						var dayStartTime = "";
						var dayEndTime = "";
						var startTimeHr = parseInt(startTime.substring(0, 2));
						var startTimeMin = startTime.substring(3, 5);
						var endTimeHr = parseInt(endTime.substring(0, 2));
						var endTimeMin = endTime.substring(3, 5);

						if (startTimeHr == 0) {
							dayStartTime = String(`12:${startTimeMin}:00 AM`);
						} else if (startTimeHr > 13) {
							dayStartTime = String(`0${convertNumbertoString(startTimeHr - 12)}:${startTimeMin}:00 PM`);
						} else if (startTimeHr == 12) {
							dayStartTime = String(`${startTimeHr}:${startTimeMin}:00 PM`);
						} else {
							dayStartTime = String(`${startTimeHr}:${startTimeMin}:00 AM`)
						}

						if (endTimeHr == 0) {
							dayEndTime = `12:${endTimeMin}:00 AM`
						} else if (endTimeHr > 13) {
							dayEndTime = `${convertNumbertoString(endTimeHr - 12)}:${endTimeMin}:00 PM`
						} else if (endTimeHr == 12) {
							dayEndTime = `${endTimeHr}:${endTimeMin}:00 PM`
						} else {
							dayEndTime = `${endTimeHr}:${endTimeMin}:00 AM`
						}
						var date = month[date1.getMonth()] + " " + date1.getDate() + ", " + date1.getFullYear();
						var schedule = setSchedule(date, dayStartTime, dayEndTime);
						$("#scheduleDropdown").append($("<option/>").attr("value", `${date1.getFullYear()}-${date1.getMonth()}-${date1.getDate()}`).html(date));

						scheduleSet.push(schedule);
					}
					initClassScheduleList();
					$("#scheduleDropdown").trigger("contentChanged");
				});

			} else if ($("#scheduleType").val() == ScheduleType.SERIES) {

				$.when(ajax.fetch("classinfo/" + classId + "/classscheduled")).done(function (response) {
					if (response.status == HttpStatus.SUCCESS) {
						if ($("#scheduleType").val() == ScheduleType.SERIES) {
							classSchedules = response.classInfo.classSeriesSchedules;
							$("#classScheduleTblBody").html("");
							$("#idClassScheduleTblBody").html("");
							$("#mdClassScheduleTblBody").html("");
							_.each(classSchedules,function(classSchedule) {
								startDate = classSchedule.startDate;
								endDate = classSchedule.endDate;
								date1 = new Date(startDate);
								date2 = new Date(endDate);
								startTime = classSchedule.startTime;
								endTime = classSchedule.endTime;
								time1 = new Date(date1.toDateString()+" "+startTime);
								time2 = new Date(date2.toDateString()+" "+endTime);
								
								var dayStartTime = "";
								var dayEndTime = "";
								var startTimeHr = parseInt(startTime.substring(0, 2));
								var startTimeMin = startTime.substring(3, 5);
								var endTimeHr = parseInt(endTime.substring(0, 2));
								var endTimeMin = endTime.substring(3, 5);

								if (startTimeHr == 0) {
									dayStartTime = `12:${startTimeMin}:00 AM`;
								} else if (startTimeHr > 12) {
									dayStartTime = `${convertNumbertoString(startTimeHr - 12)}:${startTimeMin}:00 PM`;
								} else if (startTimeHr == 12) {
									dayStartTime = `${startTimeHr}:${startTimeMin}:00 PM`;
								} else {
									dayStartTime = `${convertNumbertoString(startTimeHr)}:${startTimeMin}:00 AM`
								}

								if (endTimeHr == 0) {
									dayEndTime = `12:${endTimeMin}:00 AM`
								} else if (endTimeHr > 12) {
									dayEndTime = `${convertNumbertoString(endTimeHr - 12)}:${endTimeMin}:00 PM`
								} else if (endTimeHr == 12) {
									dayEndTime = `${endTimeHr}:${endTimeMin}:00 PM`
								} else {
									dayEndTime = `${convertNumbertoString(endTimeHr)}:${endTimeMin}:00 AM`
								}
								
								$("#classScheduleTblBody").append($("<tr/>")
									.append($("<td/>").html(month[date1.getMonth()]+" "+date1.getDate()+", "+date1.getFullYear()))
									.append($("<td/>").html(dayStartTime))
									.append($("<td/>").html(dayEndTime)));
							});
						}
					}
				});
			}
		}
	});
	
	
}

function attachPresentTraineesDropdownCTListener() {
	var classId = $("#classId").val();
	$("#scheduleDropdown").change(function () {
		var selectedValue = $("#scheduleDropdown :selected").val();

		$.when(ajax.fetch("classinfo/classattendance/" + classId + "?selectedDate=" + selectedValue)).done(function (response) {
			$("#presentTraineeLbl").html(checkIfNan(response.totalRecords));
		});
	});
}

function attachAverageScoreCTDropDownListener() {
	var classId = $("#classId").val();
	$("#examAverageScoreCTDropDown").on("change", function () {
		var selectedValue = $("#examAverageScoreCTDropDown :selected").val();
		$.when(ajax.fetch("classinfo/classexamaveragescore/" + classId + "?examId=" + selectedValue)).done(function (response) {
			$("#ctAverageScoreLbl").html(`${checkIfNan(response.totalDecimalRecords)}%`);
		});
	});
}

function attachAverageExamTimeCTDropDownListener() {
	var classId = $("#classId").val();
	$("#examAverageTimeCTDropDown").on("change", function () {
		var selectedValue = $("#examAverageTimeCTDropDown :selected").val();
		$.when(ajax.fetch("classinfo/classexamaveragetime/" + classId + "?examId=" + selectedValue)).done(function (response) {
			$("#ctAverageExamTime").html(checkIfNan(response.totalAverageValue));
		});
	});
}

function attachExamPassersCTDropDownListener() {
	var classId = $("#classId").val();
	$("#examPassersCTDropDown").on("change", function () {
		var selectedValue = $("#examPassersCTDropDown :selected").val();
		$.when(ajax.fetch("classinfo/classcountexampassers/" + classId + "?examId=" + selectedValue)).done(function (response) {
			var parsedValue = parseFloat(response.totalAverageValue);
			parsedValue = (Math.round(parsedValue * 100) / 100).toFixed(2);
			$("#ctExamPassersLbl").html(checkIfNan(parsedValue) + "%");
		});
	});
}

function attachIndividualDropDownListener() {
	var classId = $("#classId").val();
	var deliveryMethod = parseInt($("#deliveryMethod").val());
	var selector = "";

	if (deliveryMethod === DeliveryMethod.INDIVIDUAL) {
		selector = "examAnalysisDropdownIndividual";
	} else if (deliveryMethod === DeliveryMethod.MODULAR) {
		selector = "examAnalysisDropdownModular";
	}

	$(`#${selector}`).on("change", function () {
		var selectedValue = parseInt($(`#${selector} :selected`).val());
		$.when(ajax.fetch("classinfo/classcountexampassers/" + classId + "?examId=" + selectedValue)).done(function (response) {
			if (response.totalAverageValue === "NaN") {
				$("#idExamPassersLbl").html(`0%`);
				$("#mdExamPassersLbl").html(`0%`);

			} else {
				var parsedValue = parseFloat(response.totalAverageValue);
				$("#idExamPassersLbl").html(`${checkIfNan(parsedValue)}%`);
				$("#mdExamPassersLbl").html(`${checkIfNan(parsedValue)}%`);
			}
		});
		$.when(ajax.fetch("classinfo/classexamaveragescore/" + classId + "?examId=" + selectedValue)).done(function (response) {
			$("#idAveragescoreLbl").html(`${checkIfNan(response.totalDecimalRecords)}%`);
			$("#mdAverageScoreLbl").html(`${checkIfNan(response.totalDecimalRecords)}%`);
		});
		$.when(ajax.fetch("classinfo/classexamaveragetime/" + classId + "?examId=" + selectedValue)).done(function (response) {
			$("#idAverageExamTimeLbl").html(checkIfNan(response.totalAverageValue));
			$("#mdAverageExamTimeLbl").html(checkIfNan(response.totalAverageValue));
		});
		
		
		$(".generateExamAnalysisReport").attr({"href":"classinfo/"+classId+"/exam/"+selectedValue+"/report", "disabled" : false});
	

	});
}

function showChecklistTab() {
	var classId = $("#classId").val();
	$.when(ajax.fetch("classinfo/fetchclasschecklists/" + classId)).done(function (response) {
		_.each(response.classChecklists, function (checklist) {
			$("#checklistDiv")
				.append(`<p><label><input type="checkbox" id="checkbox_${checklist.classChecklistId}" ${checklist.isChecked === 1 ? "checked" : ""} 
					data-id=${checklist.classChecklistId} class="checklistFunc"><span style="color: #1e1e1e">${checklist.contentDesc}</span></label></p>`)

		});

	});
}

function showAverageTrainingTimeLabel() {
	var classId = $("#classId").val();
	$.when(ajax.fetch("classinfo/fetchclassaveragetrainingtime/" + classId)).done(function (response) {
		$("#mdAverageTrainingTimeLbl").text(checkIfNan(response.totalTimeSpent));
		$("#idAverageTrainingTimeLbl").text(checkIfNan(response.totalTimeSpent));
	});
}

function attachCheckBoxListener() {
	var classId = $("#classId").val();
	$("body").on("click", ".checklistFunc", function () {
		var getCheckListId = $(this).data("id");
		if ($(this).is(':checked')) {
			$.when(ajax.customUpdate(`classinfo/updateClassChecklist/${classId}?classChecklistId=${getCheckListId}&value=1`))
		} else {
			$.when(ajax.customUpdate(`classinfo/updateClassChecklist/${classId}?classChecklistId=${getCheckListId}&value=0`))
		}
	});
}

function populateLearningPathSection(learningPathSections) {
	$("#learningPathCon").html("");
	_.each(learningPathSections, function (learningPathSection) {
		$("#learningPathCon").append($("<div/>").css("margin-top", "20px")
			.append($("<table/>").addClass("highlight striped")
				.append($("<thead>")
					.append($("<tr/>")
						.append($("<th/>").addClass("large").attr("colspan", "2").css("text-align", "left")
							.html(learningPathSection.sectionTitle))
					))
				.append($("<tbody>").attr("id", "section_" + learningPathSection.learningPathSectionId))));
	});
}

function initLearningPathSections() {
	var result = $.Deferred();
	$.when(fetchLearningPathSection()).done(function (response) {
		switch (response.status) {
			case HttpStatus.SUCCESS:
				populateLearningPathSection(response.learningPathSections);
				learningPathSectionList = response.learningPathSections;
				result.resolve("Success");
				break;
		}
	});
	return result.promise();
}

function fetchLearningPathSection() {
	var courseId = $("#courseId").val();
	return $.ajax({
		url: "courseinfo/" + courseId + "/learningpathsections",
		type: "GET",
		dataType: "json",
		contentType: "application/json",
		mimeType: "application/json"
	}).done(function (response) {
		switch (response.status) {
			case HttpStatus.SESSION_EXPIRED:
				alertify.error(Message.SESSION_EXPIRED_MESSAGE);
				break;
			case HttpStatus.QUERY_FAILED:
				alertify.error(Message.QUERY_FAILED_MESSAGE);
				break;
			case HttpStatus.UNHANDLED_ERROR:
				alertify.error(Message.UNHANDLED_ERROR_MESSAGE);
				break;
		}
	});
}

function initLearningPathSubItems() {
	$.when(fetchLearningPathSubItems()).done(function (response) {
		switch (response.status) {
			case HttpStatus.SUCCESS:
				learningPathList = response.learningPaths;
				populateLearningPath(response.learningPaths);
				break;
		}
	});
}

function fetchLearningPathSubItems() {
	var courseId = $("#courseId").val();
	return $.ajax({
		url: "courseinfo/" + courseId + "/learningpaths",
		type: "GET",
		dataType: "json",
		contentType: "application/json",
		mimeType: "application/json"
	}).done(function (response) {
		switch (response.status) {
			case HttpStatus.SESSION_EXPIRED:
				alertify.error(Message.SESSION_EXPIRED_MESSAGE);
				break;
			case HttpStatus.QUERY_FAILED:
				alertify.error(Message.QUERY_FAILED_MESSAGE);
				break;
			case HttpStatus.UNHANDLED_ERROR:
				alertify.error(Message.UNHANDLED_ERROR_MESSAGE);
				break;
		}
	});
}

function initLearningPathTbl() {
	$.when(initLearningPathSections()).then(function (status) {
		initLearningPathSubItems();
	});
}

function populateLearningPath(learningPaths) {
	_.each(learningPaths, function (learningPath) {
		$("#section_" + learningPath.learningPathSectionId)
			.append($("<tr/>")
				.append($("<td/>").addClass("min").css("text-align", "left").html(
					(learningPath.itemType == ItemType.EXAM) ? learningPath.title : learningPath.fileLabel))
				.append($("<td/>").html(
					(learningPath.itemType == ItemType.EXAM) ?
						getExamTypeDesc(learningPath.examType) : getContentTypeDesc(learningPath.contentType)))
			);
	});
}

function getExamTypeDesc(examTypeValue) {
	var examTypeDesc = "";
	_.each(ExamTypes, function (examType) {
		if (examType.id == examTypeValue) {
			examTypeDesc = examType.description;
		}
	});
	return examTypeDesc;
}

function getContentTypeDesc(contentTypeValue) {
	var contentTypeDesc = "";
	_.each(ContentType.LIST, function (contentType) {
		if (contentType.id == contentTypeValue) {
			contentTypeDesc = contentType.description;
		}
	});
	return contentTypeDesc;
}

function initClassScheduleList() {
	var pageSize = 10;
	var deliveryMethod = parseInt($("#deliveryMethod").val());

	if (deliveryMethod === DeliveryMethod.INDIVIDUAL) {
		$("#idSchedulePagination").pagination({
			dataSource: scheduleSet,
			className: 'paginationjs-theme-blue',
			pageSize: pageSize,
			callback: function (data) {
				$("#idClassScheduleTblBody").empty();
				addSavedRecordValues(data);
			}
		});
	} else if (deliveryMethod === DeliveryMethod.MODULAR) {
		$("#mdSchedulePagination").pagination({
			dataSource: scheduleSet,
			className: 'paginationjs-theme-blue',
			pageSize: pageSize,
			callback: function (data) {
				$("#mdClassScheduleTblBody").empty();
				addSavedRecordValues(data);
			}
		});
	} else if (deliveryMethod === DeliveryMethod.CLASSTRAINING) {
		$("#ctSchedulePagination").pagination({
			dataSource: scheduleSet,
			className: 'paginationjs-theme-blue',
			pageSize: pageSize,
			callback: function (data) {
				$("#classScheduleTblBody").empty();
				addSavedRecordValues(data);
			}
		});
	}
}

function setSchedule(date, startTime, endTime) {
	schedule = {};
	schedule.date = date;
	schedule.startTime = startTime;
	schedule.endTime = endTime;

	return schedule;
}

function addSavedRecordValues(scheduleList) {
	var deliveryMethod = parseInt($("#deliveryMethod").val());
	_.each(scheduleList, function (schedule) {
		if (deliveryMethod === DeliveryMethod.INDIVIDUAL) {
			$("#idClassScheduleTblBody").append($("<tr/>")
				.append($("<td/>").html(schedule.date))
				.append($("<td/>").html(schedule.startTime))
				.append($("<td/>").html(schedule.endTime)));
		} else if (deliveryMethod === DeliveryMethod.MODULAR) {
			$("#mdClassScheduleTblBody").append($("<tr/>")
				.append($("<td/>").html(schedule.date))
				.append($("<td/>").html(schedule.startTime))
				.append($("<td/>").html(schedule.endTime)));
		} else if (deliveryMethod === DeliveryMethod.CLASSTRAINING) {
			$("#classScheduleTblBody").append($("<tr/>")
				.append($("<td/>").html(schedule.date))
				.append($("<td/>").html(schedule.startTime))
				.append($("<td/>").html(schedule.endTime)));
		}

	});
}

function convertNumbertoString(d) {
	return (d < 10) ? '0' + d.toString() : d.toString();
}

function checkIfNan(givenValue) {
	return isNaN(givenValue) || !givenValue ? "0" : givenValue.toFixed(2);
}

function attachListenerToUploadPhotoLink() {
	$("#uploadPhotoLink").on("click",function() {
		$("#fileUpload").trigger("click");
	});
}

function attachListenerToUploadPhoto() {
	$("#fileUpload").on("change",function(e) {
		if(validateFile()) {
			handleRefreshEvent();
			updateClassPhoto();
		}
	});
}

function validateFile() {
	var result = true;
	if($("#fileUpload").get(0).files.length==0) {
		result = false;
	}
	if(!validateFileType("#fileUpload","IMAGE")) {
		alertify.warning("Invalid File Type. Please Try another one.");
		result = false;
	} 
	if(!validateFileSize("#fileUpload","IMAGE")) {
		result = false;
	}
	return result;
}

function updateClassPhoto() {
	blockUI("Uploading Media File..");
	var classId = $("#classId").val();
	$.when(ajax.upload("classinfo/"+classId+"/classPhotoUrl","uploadPhotoFrm")).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				alertify.success("Successfully Updated!");
				$("#classPhoto").attr("src",response.classInfo.classPhotoUrl);
				$(window).unbind('beforeunload');
				$.unblockUI();
				break;
		}
	});
}
