$(function(){
	initReportTypes();
	initCourses();
	initUserGroups();
	initUsers();
	initTrainingCategories();
	attachListenerToResetBtn();
	attachListenerToSubmitBtn();
	attachListenerToReportDrpDwn();
})

function initReportTypes(){
	_.each(ReportType.LIST, function(reportType){
		$("#reportTypeDropdown").append(new Option(reportType.description, reportType.value));
	})
}

function initCourses(){
	$.when(ajax.fetch("courseinfo/active/list")).done(function(response){
		var courses = response.courseInfos;
		_.each(courses, function(course){
			$("#courseDropdown").append(new Option(course.courseName, course.courseId));
		})
	})
}

function initUserGroups(){
	$.when(ajax.fetch("usergroups")).done(function(response){
		var userGroups = response.usergroups;
		_.each(userGroups, function(userGroup){
			$("#userGroupDropdown").append(new Option(userGroup.userGroupName, userGroup.userGroupId));
		})		
	});
}

function initUsers(){
	$.when(ajax.fetch("users/userlist")).done(function(response){
		var users = response.users;
		_.each(users, function(user){
			$("#userDropdown").append(new Option(user.fullName, user.employeeCode));
		})			
	})
}

function initTrainingCategories(){
	$.when(ajax.fetch("trainingcategories")).done(function(response){
		var categories = response.trainingCategories;
		_.each(categories, function(category){
			$("#categoryDropdown").append(new Option(category.categoryName, category.categoryId))
		})
	})
}

function attachListenerToResetBtn(){
	$("#btnReset").click(function(e){
		e.preventDefault();
		$("#reportTypeDropdown").val(0).trigger('change');
		$("#courseDropdown").val('All').trigger('change');
		$("#userGroupDropdown").val('All').trigger('change');
		$("#userDropdown").val('All').trigger('change');
		$("#categoryDropdown").val('All').trigger("change");
		$("#startDate").val("");
		$("#endDate").val("");
	})
}

function attachListenerToSubmitBtn(){
	$("#btnSubmit").click(function(e){
		e.preventDefault();
		
		var reportsCommand = {};
		reportsCommand.reportType = $("#reportTypeDropdown").val() == "All" ? "" : $("#reportTypeDropdown").val();
		reportsCommand.courseId = $("#courseDropdown").val() == "All" ? 0 : parseInt($("#courseDropdown").val());
		reportsCommand.userGroupId = $("#userGroupDropdown").val() == "All" ? 0 : parseInt($("#userGroupDropdown").val());
		reportsCommand.employeeCode = $("#userDropdown").val() == "All" ? "" : $("#userDropdown").val();
		reportsCommand.categoryId = $("#categoryDropdown").val() == "All" ? 0 : $("#categoryDropdown").val();
		reportsCommand.startDate = $("#startDate").val().trim() == "" ? "" : $("#startDate").val() + " 00:00:00";
		reportsCommand.endDate = $("#endDate").val().trim() == "" ? "" : $("#endDate").val() + " 23:59:59";
		
		
		if($("#generatorTypeDropdown").val() == GeneratorType.PDF){
			$("#documentViewer").attr("src", "reports/pdf?reportType="+reportsCommand.reportType+"&courseId="+reportsCommand.courseId+
					 "&userGroupId="+reportsCommand.userGroupId+"&employeeCode="+reportsCommand.employeeCode+
					 "&categoryId="+reportsCommand.categoryId+"&startDate="+reportsCommand.startDate+
					 "&endDate="+reportsCommand.endDate);			
		}else{
			window.location.href = "reports/excel?reportType="+reportsCommand.reportType+"&courseId="+reportsCommand.courseId+
					 "&userGroupId="+reportsCommand.userGroupId+"&employeeCode="+reportsCommand.employeeCode+
					 "&categoryId="+reportsCommand.categoryId+"&startDate="+reportsCommand.startDate+
					 "&endDate="+reportsCommand.endDate; 		
		}
		
	})
}

function attachListenerToReportDrpDwn(){
	$("#reportTypeDropdown").change(function(e){
		e.preventDefault();
		
		if($(this).val() == ReportType.ORGANIC_AND_SUBCON_REPORT || $(this).val() == ReportType.FINANCIAL){
			$("#userDropdown").attr("disabled", true);
		}else{
			$("#userDropdown").attr("disabled", false);
		}
		
		if($(this).val() == ReportType.FINANCIAL){
			$("#userGroupDropdown").attr("disabled", true);
			$("#courseDropdown").attr("disabled", true);
		}else{
			$("#userGroupDropdown").attr("disabled", false);
			$("#courseDropdown").attr("disabled", false);
		}
		
		
		
	})
}
