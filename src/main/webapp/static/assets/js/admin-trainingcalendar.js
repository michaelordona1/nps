$(function() {
	initializeSchedules();
	initTrainerDropdown();
	initDeliveryMethodDropdown();
	initLocationDropDown();
	attachResetButtonListener();
	attachSearchButtonListener();
});


function initializeSchedules() {
	var trainerId = $("#trainerDropdown").val();
	var deliveryMethodId = $("#deliveryMethodDropdown").val();
	var locationId = $("#locationDropdown").val();
	var events = [];
	$.when(ajax.fetch(`classinfo/trainer/calendar?employeeId=${trainerId}&deliveryMethodId=${deliveryMethodId}&locationId=${locationId}`)).done(function(response) {
		if(response.status == HttpStatus.SUCCESS){
			var schedules = response.scheduleList;
			if (schedules.length == 0) {
				alertify.warning("No Data found on selected Values.");
			} else {
				_.each(schedules, function(schedule){
					var event = {};
					event.title = schedule.classCode;
					event.start = schedule.startDate;
					event.id = schedule.classId;
					event.color = schedule.color;
					events.push(event);
				});
				$("#calendar").empty();
				initializeCalendarWithListener("calendar", events);
			}
		}
	}); 
}



function initializeCalendarWithListener(dom, data) {
	var calendarDom = document.getElementById(dom);
	var calendar = new FullCalendar.Calendar(calendarDom, {
	  plugins: ['interaction', 'dayGrid', 'timeGrid'],
	  themeSystem: 'bootstrap',
	  defaultView: 'dayGridMonth',
	  header: {
		  left: 'prev,next today',
		  center: 'title',
		  right: 'dayGridMonth'
	  },
	  events: data,
	 /* eventRender: function(info){
		var $el = $(info.el).prepend("<span class='closeon'>&#10005;</span>");
	  },*/
	  eventClick: function(info) {
		$.when(ajax.fetch(`classinfo/${info.event.id}/fetchdetails`)).done(function(response) {
			var classInfo = response.classDetail;
			$("#trainerName").html(classInfo.trainerName);
			$("#locationName").html(classInfo.location);
			$("#availableSlots").html(classInfo.availableSlots);
			$("#status").html(classInfo.status);
			$("#redirectToClassInfoBtn").attr({"href": "admin.classdashboard?classId=" + classInfo.classId,
				"data-id":classInfo.classId
			})
		});
	  }
	});
	calendar.render();
}
function initTrainerDropdown() {
	$.when(ajax.fetch("employeeinfo/trainers")).done(function(response) {
		switch(response.status) {
			case HttpStatus.SUCCESS:
				populateTrainerDropdown(response.employeeInfos);
				break;
		}
	})
}

function populateTrainerDropdown(employeeInfos) {
	_.each(employeeInfos,function(employeeInfo) {
		$("#trainerDropdown").append($("<option/>").attr({"value":employeeInfo.employeeId})
			.html(employeeInfo.fullName))
			
	});
	$("#trainerDropdown").trigger("contentChanged");
}

function initDeliveryMethodDropdown() {
	_.each(DeliveryMethod.LIST,function(deliveryMethod) {
		$("#deliveryMethodDropdown").append($("<option/>").attr({"value":deliveryMethod.value})
			.html(deliveryMethod.name))
	});
}


function initLocationDropDown() {
	$.when(ajax.fetch("locations")).done(function(response) {
		var locations = [];
		locations = response.locations;
		_.each(locations, function(location){
			$("#locationDropdown").append($("<option/>").attr({"value":location.locationId})
			.html(location.locationName))
		});
	});
}

function attachResetButtonListener() {
	$("#resetBtn").on("click",function() {
		$("#trainerDropdown").val("").change();
		$("#deliveryMethodDropdown").val("").change();
		$("#locationDropdown").val("").change();
	});
}

function attachSearchButtonListener() {
	$("#searchBtn").on("click",function() {
		initializeSchedules();
	});
}
