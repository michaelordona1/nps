<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<link rel="icon" type="image/png" href="static/img/favicon.png">    

<link rel="stylesheet" type="text/css" href="static/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="static/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="static/css/popsurvey-main.css">
<link rel="stylesheet" type="text/css" href="static/css/media-queries.css">
<link rel="stylesheet" type="text/css" href="static/css/sidebar.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">

<!-- Alertify -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>

<!-- Links for datepicker -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

<script type="text/javascript" src="static/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="static/js/bootstrap.min.js"></script>

<script type="text/javascript" src="static/customjs/util.js"></script>
<script type="text/javascript" src="static/customjs/response.js"></script>

<!--  Data Tables -->
<link rel="stylesheet" type="text/css" href="static/vendor/datatables/DataTables-1.10.23/css/dataTables.bootstrap4.min.css">
<script type="text/javascript" src="static/vendor/datatables/DataTables-1.10.23/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="static/vendor/datatables/DataTables-1.10.23/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>

<!-- Generate excel file -->
<script src="static/js/FileSaver.min.js"></script>
<script src="static/js/xlsx.full.min.js"></script>
