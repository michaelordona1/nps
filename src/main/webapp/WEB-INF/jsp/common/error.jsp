<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/admin/common/admin-include.jsp"%>
<!DOCTYPE html>
<html>
   <head>
      <title>PAGSS</title>
      <meta name = "viewport" content = "width = device-width, initial-scale = 1">
      <!-- START: CSS-HEADER -->  
	  <jsp:include page="../portal/common/css-header.jsp" />
	  <!-- <link rel="stylesheet" type="text/css" href="static/assets/css/adminlogin.css"> -->
	  <link rel="stylesheet" type="text/css" href="static/assets/css/login.css"> 
      <!-- END: CSS-HEADER -->
       
      <!-- START: JS-HEADER -->
      <jsp:include page="../portal/common/js-header.jsp" />
      <!-- END: JS-HEADER -->
      
   </head>
  
   	<body>
		<div>
	         <div class="row">
	            <div class="fullView BGerror col m8 hide-on-small">
	               <div class="subContainer">
	                  <p><i class="large material-icons">autorenew</i><span style="font-size: 6rem;">404</span></p>
	                  <p style="font-size: 20px;">Your session has expired due to inactivity.</p>
	                  <p>Click <a href="#!">here</a> to login again.</p>
	               </div>
	            </div>
	            <div class="fullView col s12 m4" style="text-align: center;">
	               <img style="width:410px;height:auto;margin-bottom:20px;" src="../assets/images/login/PAGSS.jpg">
	            </div>
	         </div>
	      </div>
	</body>
</html>