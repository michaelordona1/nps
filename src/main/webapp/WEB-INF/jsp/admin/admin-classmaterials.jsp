<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/admin/common/admin-include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>PAGSS - CLASS MATERIALS</title>
	<!-- START: CSS-HEADER -->  
	<jsp:include page="../admin/common/admin-trainee-css-header.jsp" />
	<!-- END: CSS-HEADER -->
	<!-- START: JS-HEADER -->
	 <jsp:include page="../admin/common/admin-js-header.jsp" />
	 <script src="static/assets/js/admin-classmaterials.js"></script>
	<!-- END: JS-HEADER -->
	
	<style>
		#documentViewerCon {
		position:relative;
		padding-bottom:56.25%;
		padding-top:30px;
		height:0;
		overflow:hidden;
		}
		
		#documentViewerCon iframe, #documentViewerCon object, #documentViewerCon embed {
		position:absolute;
		top:0;
		left:0;
		width:100%;
		height:100%;
		}		
	</style>	
</head>
<body>
	<jsp:include page="../admin/common/admin-collapsibleheader.jsp" />

	<!-- Left Side bar -->
	<jsp:include page="../admin/common/admin-collapsiblesidebar.jsp" />
	
	<div class="container1">
          <div class="row">
          
		      <!-- left side card/panel includes photo, title, type and status -->
		      <jsp:include page="../admin/common/admin-classinfo-card.jsp" />
		      
            <div class="col s12 m9" style="padding: 5px;">
              <div class="navbar">
              	<a href="admin.classdashboard?classId=${classId}">Dashboard</a>
              	<a href="admin.classdetails?classId=${classId}">Details</a>
              	<a href="admin.classtrainee?classId=${classId}">Trainees</a>
              	<a href="admin.classattendance?classId=${classId}">Attendance</a>
              	<a href="admin.classsettings?classId=${classId}">Settings</a>
              	<a class="active" href="admin.classmaterials?classId=${classId}">Materials</a>
              	<a href="admin.classexam?classId=${classId}">Exams</a>
              	<a href="admin.classfinancial?classId=${classId}">Financials</a>
              	<a href="admin.classevaluation?classId=${classId}">Evaluation</a>
              </div>
              <!-- content starts here -->
              <div class="divcard">

			    <c:if test="${sessionScope.user.userTypeId eq 1}">
                <div class="row">
                  <div class="right">
                    <a href="#!" class="btn bttn waves-light waves-effect" type="submit" style="margin-right: 5px;"><i class="material-icons left">arrow_back</i>Back</a>
                   <!--  <a type="submit" id="copyBtn" style="margin-right: 5px;" class="btn bttn waves-light waves-effect"><i class="material-icons left"></i>Copy Link URL on trainee</a> -->
                    <a data-target="learningMaterialModal" class="modal-trigger btn bttn waves-light waves-effect" type="submit" style="margin-right: 5px;"><i class="material-icons left">add_circle</i>Add Materials</button>
                    <a id="btnDeleteMaterials" class="btn bttn waves-light waves-effect" type="submit" style="margin-right: 5px;"><i class="material-icons left">delete</i>Delete Materials</a>
                  </div>
                </div>
                </c:if>

                <div style="margin-top: 20px;padding: 10px;">
                  <span><b>Class Materials</b></span>
                </div>

                <div>
                  <table class="striped highlight">
                    <tbody id="classMaterialTblBody">
                    </tbody>
                  </table>
                </div>
                <div id="pagealign">
	               <div id="classMaterialTblPagination">
	                  <!--Pagination-->
	               </div>
              	</div>

                <div style="margin-top: 20px;padding: 10px;">
                  <span><b>Course Materials</b></span>
                </div>

                <div>
                  <table class="striped highlight">
                    <tbody id="courseMaterialTblBody">
                    </tbody>
                  </table>
                  <div id="pagealign">
	               <div id="courseMaterialTblPagination">
	                  <!--Pagination-->
	               </div>
              	</div>
                </div>
                

              </div>
              <!-- content ends here -->
            </div>
          </div>
        </div>

        <!--Modal learningMaterialModal starts here-->

        <div id="learningMaterialModal" class="modal modal-fixed-footer fixed-modal">
            <div class="modal-header">
              <h5>Add Learning Material</h5>
            </div>
            <div class="modal-content">
              <div class="content-row">
                <div>
                  <b>Type*</b>
                    <select id="contentTypeDropdown">
                      <option disabled selected value="0">Choose an option</option>
                    </select>
                  <div id="fileName"> 
                  <b>File</b>
                    <input type="text" id="fileLabel" class="validate">
                  </div> 
                </div>
              </div>
              <div class="content-row" id="mediaUploadDiv">
              	<form id="mediaUploadFrm">
                <div class="file-field input-field" style="margin-top: 0 !important;">
                  <div class="btn bttn right" style="margin-top: 20px;">
                    <span>Browse</span>
                    <input type="file" name="mediaFileUpload" id="mediaFileUpload">
                  </div>
                  <div class="file-path-wrapper" style="padding-top: 0 !important;padding-left:0 !important;">
                    <b>Browse File</b>
                    <input class="file-path validate" type="text" id="fileupload">
                  </div>
                </div>
                </form>
              </div>
              <div class="content-row" id="urlDiv">
                <b>URL*</b>
                    <input type="text" id="urlTextBox" class="validate">
              </div>
              <div class="content-row" style="margin: 10px 0 20px 0;">
                <b>Viewable by Trainees</b>
                <label style="margin-left:10px !important;">
                  <input type="checkbox" id="viewStatus"/>
                  <span>Yes</span>
                </label>
              </div>
            </div>
            
            <div class="modal-footer">
            <div id="materialsModalBtn">
              <a class="modal-close btn waves-effect waves-light bttn" type="button" id="cancelBtn" name="action"><i class="material-icons left">cancel</i>Cancel</a>
              <a class="btn waves-effect waves-light bttn" type="submit" name="action" id="saveClassMaterials"><i class="material-icons left">save</i>Save</a>
            </div>
            <div id="progressCon">
	            <b id="percentValue">0%</b>
	            <div class="progress">
	                <div id="progressBar" class="determinate"></div>
	            </div>
            </div>
            <input type="hidden" id="courseId" value="${courseId}">
        </div>
	</div>
	
	
    <div id="viewEmbeddedPowerPoint" class="modal modal-fixed-footer">
        <div class="modal-header">
          <h5 id="embeddedPowerPointTitle"></h5>
        </div>
        <div class="modal-content">
          <div class="content-row">
            <div class="row" id="documentViewerCon">
				<center><iframe id="documentViewerFrame" allowfullscreen webkitallowfullscreen></iframe></center>
             	<div style="width: 50px; height: 50px; position: absolute; opacity: 0; right: 13px; top: 10px;"> </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
         	<a class="modal-close btn waves-effect waves-light bttn" type="button" name="action" id="closeBtn"><i class="material-icons left">cancel</i>Close</a>
       </div>
    </div>       
         
         	
	<input type="hidden" id="classId" value="${classInfo.classId}">
        <input type="hidden" id="tbCourseId" value="${classInfo.courseId}">
        <input type="hidden" id="deliveryMethod" value="${classInfo.deliveryMethod}">
        <input type="hidden" id="scheduleType" value="${classInfo.scheduleType}">
        <input type="hidden" id="userType" value="${sessionScope.user.userTypeId}">
        <!--Modal learningMaterialModal ends here-->
        
</body>
</html>