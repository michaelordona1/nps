<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/admin/common/admin-include.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1" name="viewport" content="width=device-width, initial-scale=1.0">
	<title>PAGSS - Home</title>
	<!-- START: CSS-HEADER -->  
	<jsp:include page="../admin/common/admin-css-header.jsp" />
	<link rel="stylesheet" type="text/css" href="static/assets/css/page.css">
	<!-- END: CSS-HEADER -->
	<!-- START: JS-HEADER -->
	 <jsp:include page="../admin/common/admin-js-header.jsp" />
	<!-- END: JS-HEADER -->
	
	<script>
		$(function(){
			initializeSettings();
		})
		var HIT_COUNT = ${user.hitCount};
		function initializeSettings(){
			var description = "In compliance with Data Privacy Act of 2012 (DPA) (R.A. 10173) and its Implementing Rules and Regulations (DPA-IRR). <br/><br/> PAGSS-TDD sets our data protection practices to safeguard intellectual properties of the company and the personal data of employees and individuals responsible to training matters. The policy includes information on employees training history (current, past, and prospective), how it will use and process, keep and secure, and how it will be disposed when no longer needed.";
			
			if(HIT_COUNT == 0){
				alertify.alert("", description, function(){
					ajax.create("user/update-hitcount");
				}).set('label', 'I Agree');
			}
		}
	</script>	

	
</head>
<body>
<!-- Header -->
	<jsp:include page="../admin/common/admin-header.jsp" />
	
<!-- Left Side bar -->
	<jsp:include page="../admin/common/admin-sidebar.jsp" />
	
<!-- Content Page -->

</body>
</html>