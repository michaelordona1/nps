<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/admin/common/admin-include.jsp"%>

            <div class="col s12 m3" style="padding-top: 20px;">
              <div class="card">
                <div class="card-image">
                  <c:choose>
                    <c:when test="${classInfo.classPhotoUrl==null}">
                        <img id="classPhoto" class="fixImg" src="static/assets/images/usersimage/users.png">
                    </c:when>  
                    <c:otherwise>
                        <img id="classPhoto" class="fixImg" src="${photoUrl}">
                    </c:otherwise>
                  </c:choose>
                </div>
                <c:if test="${sessionScope.user.userTypeId eq 1}">
                <div class="card-fab">
                    <a id="uploadPhotoLink" class="btn-floating btn-large halfway-fab waves-effect waves-light red bttn">
                    <i class="material-icons">add</i></a>
                </div>
                <form id="uploadPhotoFrm">
                	<input type="file" style="visibility:hidden;" id="fileUpload" name="fileUpload">
                </form>
                </c:if>
              </div>
              <div>
                <h6><b>${classInfo.courseName}</b></h6>
                <p><span>${classInfo.className}</span></p>
                <c:choose>
		            <c:when test="${classInfo.deliveryMethod==1}">
		              <p><span>CBT (Individual)</span></p>
		            </c:when>
		            <c:when test="${classInfo.deliveryMethod==2}">
		              <p><span>CBT (Modular)</span></p>
		            </c:when>
		            <c:otherwise>
		              <p><span>Classroom Training</span></p>
		            </c:otherwise>
		        </c:choose>
                <c:choose>
					<c:when test="${classInfo.completionStatus==1}">
				       <p><span>Completed</span></p>
				    </c:when>
				    <c:otherwise>
				       <p><span>Incomplete</span></p>
				    </c:otherwise>
				</c:choose>
              </div>
            </div>
            
<script type="text/javascript">
$(function(){ updateClassStatus(); })

function updateClassStatus(){
	ajax.customUpdate("classinfo/${classInfo.classId}/class-status");
}


</script>