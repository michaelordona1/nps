<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/admin/common/admin-include.jsp"%>
<div class="navbar-fixed" style="z-index:4 !important;">
   <nav>
      <div class="nav-wrapper">
         <a href="#" class="sidenav-trigger hide-on-large-only" data-target="slide-out"><i class="material-icons">menu</i></a>
         <ul class="hide-on-med-and-down" style="margin-right: 10px;">
           <li class="left">
             <ul>
               <li><a data-target="slide-out" class="sidenav-trigger"><i class="material-icons" style="padding: 0 15px">menu</i></a></li>
               <li><a href="admin.home" class="compname">Learning Management System</a></li>
             </ul>
           </li>
           <li class="right">
             <ul>
               <li>
                 <a href="admin.viewuser?id=${sessionScope.user.userId}" class="profset circle">
                   <img id="profile" src="static/assets/images/usersimage/user.png">
                 </a>
               </li>
               <li>
               	   <c:if test="${sessionScope.user.userTypeId ne 3}">
               	  	 <a class="dropdown-trigger" data-target="dropOption" style="height: 74px;">
               	   </c:if>	
	                   <span style="margin-top:-10px;display:block;height:20px;color: #1e1e1e; font-weight: bold; padding-right: 20px;">${sessionScope.user.fullName}</span>
	                   <label>${sessionScope.user.jobName}</label>
	              <c:if test="${sessionScope.user.userTypeId ne 3}">    
                   	</a>
                  </c:if>	
               </li>
               <li>
                 <a href="#!"><i style="font-size: 30px !important;" class="material-icons account_circle">notifications</i></a>
               </li>
               <li>
                 <a href="logout"><i style="font-size: 30px !important;" class="material-icons account_circle">power_settings_new</i></a>
               </li>
             </ul>
           </li>
         </ul>
      </div>
   </nav>
</div>
<ul id="dropOption" class="dropdown-content">
   <li><a href="admin.home">Admin/Trainer</a></li>
   <li><a href="mytraining-inprogress">Trainee User</a></li>
</ul>