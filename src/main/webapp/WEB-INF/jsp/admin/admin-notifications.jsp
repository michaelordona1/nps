<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/admin/common/admin-include.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>PAGSS - NOTIFICATIONS</title>
<!-- START: CSS-HEADER -->
<jsp:include page="../admin/common/admin-css-header.jsp" />
<!-- END: CSS-HEADER -->
<!-- START: JS-HEADER -->

<script src="static/assets/vendor/jquery/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.0.2/sockjs.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script>
<script src="static/assets/js/admin-notifications.js"></script>
</head>
<body>

	  <div id="main-content" class="container">
	    <div class="row">
	      <div class="col-md-12 space-bottom10">
	          <button id="connect"
			  class="btn btn-default"
			  type="submit">Connect</button>
	          <button id="disconnect"
			  class="btn btn-default"
			  type="submit"
			  disabled="disabled">Disconnect</button>
			  <button id="start"
			  class="btn btn-default"
			  type="submit">Start</button>
	          <button id="stop"
			  class="btn btn-default"
			  type="submit">Stop</button>
	      </div>
	    </div>
	    <div class="row">
	      <div class="col-md-12">
	        <table id="conversation" class="table table-striped">
	          <thead>
	            <tr>
		      <th width="10%">From</th>
	              <th width="15%">Topic</th>
		      <th width="60%">Message</th>
		      <th width="10%">Time</th>
	            </tr>
	          </thead>
	          <tbody id="messages">
	          </tbody>
	        </table>
	      </div>
	    </div>
	  </div>
	        
</body>
</html>