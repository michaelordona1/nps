<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/admin/common/admin-include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>PAGSS - CLASS TRAINEE</title>
	<!-- START: CSS-HEADER -->  
	<jsp:include page="../admin/common/admin-trainee-css-header.jsp" />
	<!-- END: CSS-HEADER -->
	<!-- START: JS-HEADER -->
	 <jsp:include page="../admin/common/admin-js-header.jsp" />
	<script src="static/assets/js/admin-classtrainee.js"></script>
	<!-- END: JS-HEADER -->
</head>
<body>
	<!-- Header -->
	<jsp:include page="../admin/common/admin-collapsibleheader.jsp" />

	<!-- Left Side bar -->
	<jsp:include page="../admin/common/admin-collapsiblesidebar.jsp" />
	
	<div class="container1">
          <div class="row">
          
		      <!-- left side card/panel includes photo, title, type and status -->
		      <jsp:include page="../admin/common/admin-classinfo-card.jsp" />
		      
            <div class="col s12 m9" style="padding: 5px;">
              <div class="navbar">
              	<a href="admin.classdashboard?classId=${classId}">Dashboard</a>
              	<a href="admin.classdetails?classId=${classId}">Details</a>
              	<a class="active" href="admin.classtrainee?classId=${classId}">Trainees</a>
              	<a href="admin.classattendance?classId=${classId}">Attendance</a>
              	<a href="admin.classsettings?classId=${classId}">Settings</a>
              	<a href="admin.classmaterials?classId=${classId}">Materials</a>
              	<a href="admin.classexam?classId=${classId}">Exams</a>
              	<a href="admin.classfinancial?classId=${classId}">Financials</a>
              	<a href="admin.classevaluation?classId=${classId}">Evaluation</a>
              </div>
              <!-- content starts here -->
              <div class="divcard">

				
                <div>
                  <div class="right">
                  	<c:if test="${sessionScope.user.userTypeId eq 1 || sessionScope.user.userId eq trainer.employeeId}">
                    	<a href="admin.class" class="btn bttn waves-light waves-effect" type="submit" style="margin-right: 5px;"><i class="material-icons left">arrow_back</i>Back</a>
                    	<a data-target="addTraineesModal" class="modal-trigger btn bttn waves-light waves-effect" type="submit" style="margin-right: 5px;"><i class="material-icons left">add_circle</i>Add Trainees</a>
                   </c:if>
                   <c:if test="${countTrainees > 0 && classInfo.withCertificate == 1 && trainer.employeeId == sessionScope.user.userId || sessionScope.user.userTypeId eq 1}">
                    	<a href="#!" class="btn bttn waves-light waves-effect" id="generateCertificatesBtn">Generate Certificates</a>
                   </c:if>
                   
                   
                    <c:choose>
					    <c:when test="${classInfo.deliveryMethod==3}">
					       <a href="#!" id="setAsCompletedBtn" class="btn bttn waves-light waves-effect" type="submit" style="margin-right: 5px;">Set Status as Completed</a>
					    </c:when>
					</c:choose>
                    <!--<a href="#!" class="btn bttn waves-light waves-effect" type="submit" style="margin-right: 5px;"><i class="material-icons left">save</i>Save</a>-->
                  </div>
                </div>
				
				
                <div class="row" style="margin:60px 0 10px 0;">
                  <div class="col s6 m6" style="padding-left:0;">
                    <span class="left"><b>Trainee List</b></span>
                  </div>
                  <div class="col s6 m6">
                    <a href="#!" id="printApprovedTraineeBtn"><i class="material-icons right">print</i></a>
                  </div>
                </div>

                <div class="row">
                  <table class="highlight striped">
                    <thead>
                        <tr>
                            <th>Action</th>
                            <th>Name<a href="#!"><a href="#!" class="sort-by" id="sortByNameApproved"></a></th>
                            <th class="min">Job<a href="#!" class="sort-by" id="sortByJobApproved"></a></th>
                            <th>User Group<a href="#!" class="sort-by" id="sortByUserGroupApproved"></a></th>
                            <th>Date Enrolled<a href="#!" class="sort-by" id="sortByDateEnrolledApproved"></a></th>
                            <th>Date Completed<a href="#!" class="sort-by" id="sortByDateCompletedApproved"></a></th>
                            <th>Training Status<a href="#!" class="sort-by" id="sortByTrainingStatusApproved"></a></th>
                            <th>Comment<a href="#!"></a></th>
                        </tr>
                      </thead>

                      <tbody id="ApprovedTraineeTblBody">
                        <!-- Dynamically populated -->
                      </tbody>
                  </table>
                   <div id="pagealign">
	              <div id="AppprovedTraineePagination">
	                <!-- Pagination -->
	              </div>
              </div>
                </div>

                <div class="row" style="margin-top: 20px;padding: 10px;">
                  <span><b>For Approval</b></span>
                  <table class="highlight striped">
                    <thead>
                        <tr>
                            <th>Action</th>
                            <th>Name<a href="#!" class="sort-by" id="sortByNamePending"></a></th>
                            <th class="mins">Job<a href="#!" class="sort-by" id="sortByJobPending"></a></th>
                            <th>User Group<a href="#!" class="sort-by" id="sortByUserGroupPending"></a></th>
                            <th>Date of Request<a href="#!" class="sort-by" id="sortByRequestDatePending"></a></th>
                            <th>Date Approved<a href="#!" class="sort-by" id="sortByApprovedDatePending"></a></th>
                            <th>Approved Status<a href="#!" class="sort-by" id="sortByApprovedStatusPending"></a></th>
                        </tr>
                      </thead>

                      <tbody id="pendingTraineeTblBody">
                        <!-- Dynamically populated -->
                      </tbody>
                  </table>
                  <div id="pagealign">
	                  <div id="pendingTraineePagination">
		                <!-- Pagination -->
		              </div>
	              </div>
                </div>

                <div class="row" style="margin-top: 20px;padding: 10px;">
                  <span><b>Trainees' Grade Sheet</b></span>
                  <a href="#!" class="right" id="printGradingSheetBtn"><i class="material-icons">print</i></a>
				<div class="table-wrapper">
                  <table class="highlight striped">
                  	  <thead id="gradingSheetTblHeader">
                  	  	<tr id="gradingSheetTblHeader">
                  	  	</tr>
                  	  </thead>
                      <tbody id="gradingSheetTblBody">
                      </tbody>
                  </table>
                  <div id="pagealign">
	                  <div id="gradingSheetPagination">
		                <!-- Pagination -->
		              </div>
	              </div>
                </div>  
                </div>
              </div>
              <!-- content ends here -->
            </div>
          </div>
        </div>

        <!--Modal addtrainees starts here-->

        <div id="addTraineesModal" class="modal modal-fixed-footer">
            <div class="modal-header">
              <h5>Add Trainees</h5>
            </div>
            <div id="TraineesModal" class="modal-content">
              <div>
                <span><b>Filter</b></span>
              </div>
              <div>
                <div class="row">
                  <div class="col s6 m4" style="padding-top: 15px;">
                    <label style="margin-left:10px !important;">
                      <input name="filterGroup" id="jobRoleRdBtn" type="radio" checked />
                      <span>Job Role</span>
                    </label>
                  </div>
                  <div class="col s6 m8">
                    <select id="jobRoleDropdown">
                    </select>
                  </div>
                </div>
              </div>
              <div>
                <div class="row">
                  <div class="col s6 m4" style="padding-top: 15px;">
                    <label style="margin-left:10px !important;">
                      <input name="filterGroup" id="userGroupRdBtn" type="radio" />
                      <span>User Group</span>
                    </label>
                  </div>
                  <div class="col s6 m8">
                   <select id="userGroupDropdown" disabled>
                    </select>
                  </div>
                </div>
              </div>
              <div style="padding-top:5px;" class="nav-wrapper col s12 m7 row">
                   <div class="row">
                     <div class="col m6 hide-on-med-and-down">
                     </div>
                     <div class="input-field col s12 m6">
                       <input style="padding-right: 0 !important;" id="searchTrainee" type="search" placeholder="Search">
                         <label class="label-icon" for="search"><i style="margin-top:4px;" class="material-icons">search</i></label>
                       <i id="clearField" style="margin-top:1px;" class="material-icons">close</i>
                     </div>
                   </div>
              </div>
              <div>
                <table class="highlight responsive-table">
                  <thead id="traineeTblHeader">
                  </thead>
                  <tbody id="traineeTblBody">
                  </tbody>
                </table>
                <div id="pagealign">
	              <div id="traineePagination">
	                <!-- Pagination -->
	              </div>
              </div>
              <br><br>
              </div>
            </div>
            <div class="modal-footer">
              <a class="modal-close btn waves-effect waves-light bttn" type="button" id="cancelTraineeBtn"><i class="material-icons left">cancel</i>Cancel</a>
              <a class="btn waves-effect waves-light bttn" type="submit" name="action" id="addTraineeBtn"><i class="material-icons left">save</i>Save</a>
            </div>
        </div>

        <!--Modal addtrainees ends here-->
        <input type="hidden" id="classId" value="${classId}">
        <input type="hidden" id="courseId" value="${classInfo.courseId}">
        <input type="hidden" id="scheduleType" value="${classInfo.scheduleType}">
        <input type="hidden" id="classPhotoUrl" value="${classInfo.classPhotoUrl}">
        <input type="hidden" id="deliveryMethod" value="${classInfo.deliveryMethod}">
        <input type="hidden" id="countTrainees" value="${countTrainees}">
        <input type="hidden" id="countIncompleteTrainees" value="${countIncompleteTrainees}">
        <input type="hidden" id="userType" value="${sessionScope.user.userTypeId}">
        <input type="hidden" id="loggedInId" value="${sessionScope.user.userId}">
        <input type="hidden" id="trainerId" value="${trainer.employeeId}">        
</body>
</html>