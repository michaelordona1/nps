<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/admin/common/admin-include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="ISO-8859-1" name="viewport" content="width=device-width, initial-scale=1.0">
	<title>PAGSS - VIEW USER (USER GROUP)</title>
	<!-- START: CSS-HEADER -->  
	<jsp:include page="../admin/common/admin-trainee-css-header.jsp" />
	<!-- END: CSS-HEADER -->
	<!-- START: JS-HEADER -->
	 <jsp:include page="../admin/common/admin-js-header.jsp" />
	 <script src="static/assets/js/admin-classdetails.js"></script>
	<!-- END: JS-HEADER -->
</head>
<body>
	<!-- Header -->
	<jsp:include page="../admin/common/admin-collapsibleheader.jsp" />

	<!-- Left Side bar -->
	<jsp:include page="../admin/common/admin-collapsiblesidebar.jsp" />
	
	<div class="container1">
          <div class="row">
		      
		      <!-- left side card/panel includes photo, title, type and status -->
		      <jsp:include page="../admin/common/admin-classinfo-card.jsp" />
		      
            <div class="col s12 m9" style="padding: 5px;">
              <div class="navbar">
              	<a href="admin.classdashboard?classId=${classId}">Dashboard</a>
              	<a class="active" href="admin.classdetails?classId=${classId}">Details</a>
              	<a href="admin.classtrainee?classId=${classId}">Trainees</a>
              	<a href="admin.classattendance?classId=${classId}">Attendance</a>
              	<a href="admin.classsettings?classId=${classId}">Settings</a>
              	<a href="admin.classmaterials?classId=${classId}">Materials</a>
              	<a href="admin.classexam?classId=${classId}">Exams</a>
              	<a href="admin.classfinancial?classId=${classId}">Financials</a>
              	<a href="admin.classevaluation?classId=${classId}">Evaluation</a>
              </div>
              <!-- content starts here -->
              <div class="divcard">
                <div class="row">

				  <c:if test="${sessionScope.user.userTypeId eq 1 || sessionScope.user.userId eq trainer.employeeId}">
                  <div class="right">
                    <a href="admin.class" class="btn bttn waves-light waves-effect" type="submit" style="margin-right: 5px;"><i class="material-icons left">arrow_back</i>Back</a>
                    <a href="#!" class="btn bttn waves-light waves-effect" type="submit" style="margin-right: 5px;"><i class="material-icons left">not_interested</i>Cancel Class</a>
                    <a id="btnClassSave" class="btn bttn waves-light waves-effect" type="submit" style="margin-right: 5px;"><i class="material-icons left">save</i>Save</a>
                  </div>
                </div>
                </c:if>

                <div class="row" style="margin-top: 10px;">
                  <div class="col s12 m6">
                    <b>Course</b>
                      <input id="courseTxtbox" type="text" disabled name="" value="${classInfo.courseName}">
                  </div>
                  <div class="col s12 m6">
                    <b>Class Code</b>
                      <input id="classCodeTxtbox" type="text" disabled name="" value="${classInfo.classCode}">
                  </div>
                </div>

                <div class="row">
                  <div class="col s12 m6">
                    <b id="nameLabel">Class Name</b>
                      <input id="classNameTxtbox" type="text" name="" value="${classInfo.className}" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                  </div>
                  <div class="col s12 m6">
                    <b id="locationLabel">Location</b>
                    <span class="selectMarginOnly">
                      <select id="locationDropdown" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                      </select>
                    </span>
                  </div>
                </div>

                <div class="row">
                  <div class="col s12 m6">
                    <b id="trainerLabel">Trainer</b>
                      <select id="trainerDropdown" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                      </select>
                  </div>
                  <div class="col s12 m6">
                    <div class="row switch">
                      <div class="col s4 m4" style="text-align: center;">
                        <b>Self Registration</b>
                        <label>
                          <input id="selfRegCheckBox"  type="checkbox" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                          <span class="lever leverMargin"></span>
                        </label>
                      </div>
                      <div class="col s4 m4" style="text-align: center;">
                        <b>Send Certificate</b>
                        <label>
                          <input id="certificateCheckBox" type="checkbox" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                          <span class="lever leverMargin"></span>
                        </label>
                      </div>
                      <div class="col s4 m4" style="text-align: center;">
                        <b>Exam</b><br>
                        <label>
                          <input id="examCheckbox" type="checkbox" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                          <span class="lever leverMargin" style="margin-left: 15px !important;"></span>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col s12 m6">
                    <b id="minLabel">Minimum Attendees</b>
                      <input class="numbersOnly" id="minAttendees" type="number" value="${classInfo.minAttendee}" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                  </div>
                  <div class="col s12 m6">
                    <b id="maxLabel">Maximum Attendees</b>
                      <input class="numbersOnly" id="maxAttendees" type="number" value="${classInfo.maxAttendee}" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                  </div>
                </div>

                <div class="row">
                  <div class="col s12 m6">
                    <b id="schedLabel">Schedule Type</b>
                     <select disabled id="scheduleTypeDropdown">
                      	<option disabled selected></option>
                      	<option value="1">Block Schedule</option>
                      	<option value="2">Set Schedule</option>
                      	<option value="3">Series Schedule</option>
                     </select>
                  </div>
                  <div class="col m6 hide-on-small-only">
                  </div>
                </div>
                

                <!-- blockSchedule type starts here -->
		          <div id="blockSchedule" class="blockSchedule showOpt">
		            <div class="divcard">
		                <legend style="margin-bottom: 10px;"><b>Block Schedule</b></legend>
		
		                <div class="row">
		                  <div class="col s12 m6">
		                    <b>Start Date*</b>
		                    <input id="blockDateStartPicker" type="date"  name="" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
		                    <label></label>
		                  </div>
		                  <div class="col s12 m6">
		                    <b>End Date*</b>
		                    <input id="blockDateEndPicker" type="date"  name="" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
		                    <label></label>
		                  </div>
		                </div>

                <div class="row">
                  <div class="col s12 m6">
                    <b>Start Time*</b><br>
                    <div>
                      <div style="padding-right: 5px;">
                        <i class="material-icons" style="vertical-align: middle;">access_time</i>
                        <input id="blockStartHr" class="numberOnly" value="12" min="1" max="12" type="number" name="" style="width: 50px;" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                        <span>:</span>
                        <input id="blockStartMin" class="numberOnly" value="00" min="0" max="59" type="number" name="" style="width: 50px;" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                      	<span class="selectWidth">
                      		<select id="blockMeridiemFrom" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
	                            <option disabled></option>
	                            <option value="AM">AM</option>
	                            <option value="PM">PM</option>
	                          </select>
                      	</span>
                      </div>
                    </div>
                  </div>
                  <div class="col s12 m6">
                    <b>End Time*</b><br>
                    <div class="row">
                      <div style="padding-right: 5px;">
                        <i class="material-icons" style="vertical-align: middle;">access_time</i>
                        <input id="blockEndHr" class="numberOnly" value="12" min="1" max="12" type="number" name="" style="width: 50px;" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                        <span>:</span>
                        <input id="blockEndMin" class="numberOnly" value="00" min="0" max="59" type="number" name="" style="width: 50px;" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                        <span class="selectWidth">
                        	<select id="blockMeridiemTo" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
	                            <option disabled></option>
	                            <option value="AM">AM</option>
	                            <option value="PM">PM</option>
	                          </select>
                        </span>
                      </div>
                    </div>
                  </div>
		            </div>
		          </div>

		          <!-- blockSchedule type ends here -->
		        </div>
		        <!-- setSchedule type starts here -->
		          <div id="setSchedule" class="setSchedule showOpt">
		            <div class="divcard">
		              <legend style="margin-bottom: 10px;"><b>Set Schedule</b></legend>
		              <div>
		                <a data-target="addScheduleModal" class="modal-trigger bttn btn waves-effect waves-light right" type="submit"><i class="material-icons left">add_circle</i>Add Schedule</a>
		              </div>
		              <div style="margin-top: 20px;">
		                <table class="highlight striped">
		                  <thead>
		                    <tr>
		                      <th class="mid">Event Schedule Start</th>
		                      <th class="mid">Event Schedule End</th>
		                      <th class="mins"></th>
		                    </tr>
		                  </thead>
		                  <tbody id="setScheduleTblBody">
		                  </tbody>
		                </table>
		              </div>
		            </div>
		          </div>
		          <!-- setSchedule type ends here -->
		
		          <!-- seriesSchedule type starts here
		          DESCRIPTION: I put this old series schedule into comment for ID references
		           -->
		          <!-- <div id="seriesSchedule" class="seriesSchedule endAfter endBy showOpt">
		            <div class="divcard">
		              <legend style="margin-bottom: 10px;"><b>Series Schedule</b></legend>
		              <div class="row">
		                <div class="col s12 m4">
			                <b id="seriesScheduleStarts">Schedule Start</b>
	                		<input id="scheduleStartDate" type="date" class="datepicker">
	                		<label></label>
		                </div>
		
		                <div class="col s12 m4">
		                	<span>&nbsp</span>	
		                	<input type="date" class="datepicker" name="">
		                    <label></label>
		                </div>
		                <div class="col s12 m4">
			                <span><b>Recurrence Pattern</b></span>
			                <span class="selectMarginOnly">
				                <select id="recurrenceDropdown">
			                      <option selected value="1">Weekly</option>
			                      <option value="2">Monthly</option>
				                 </select>
			                 </span>
		                </div>
		              </div>
		              weekly option starts here
		              <div id="weeklyDiv" style="padding:10px 10px 20px 10px;'">
		              <div id="pagealign" style="padding:10px 10px 20px 10px;">
		              	<label>
		                  <input type="checkbox" id="chMonSeries">
		                  <span style="color: #1e1e1e">Monday</span>
		                </label>
		                <label style="margin-left: 15px;">
		                  <input type="checkbox" id="chTueSeries">
		                  <span style="color: #1e1e1e">Tuesday</span>
		                </label>
		                <label style="margin-left: 15px;">
		                  <input type="checkbox" id="chWedSeries">
		                  <span style="color: #1e1e1e">Wednesday</span>
		                </label>
		                <label style="margin-left: 15px;">
		                  <input type="checkbox" id="chThuSeries">
		                  <span style="color: #1e1e1e">Thursday</span>
		                </label>
		                <label style="margin-left: 15px;">
		                  <input type="checkbox" id="chFriSeries">
		                  <span style="color: #1e1e1e">Friday</span>
		                </label>
		                <label style="margin-left: 15px;">
		                  <input type="checkbox" id="chSatSeries">
		                  <span style="color: #1e1e1e">Saturday</span>
		                </label>
		                <label style="margin-left: 15px;">
		                  <input type="checkbox" id="chSunSeries">
		                  <span style="color: #1e1e1e">Sunday</span>
		                </label>
		              </div>
		              weekly option ends here
		              monthly option starts here
		              	<div id="monthlyDiv" class="row" style="padding:10px 10px 20px 10px; display:none;'">
		              		<div class="col s12 m3">
		              			<label>
				                    <input id="dateRadio" type="radio" name="monthlyType" class="with-gap" value="Date">
				                    <span style="color: #1e1e1e"><b>Date</b></span>
			                    </label><br>
			                    <label>
				                    <input id="dayRadio" type="radio" name="monthlyType" class="with-gap" value="Day">
				                    <span style="color: #1e1e1e"><b>Day</b></span>
			                    </label>
		              		</div>
		              		<div class="col s12 m9">
		              			When date is chosen starts here
		              			<span id="dateDiv">Day <input type="text" style="width:20px;height:20px;"> of every <input type="text" style="width:20px;height:20px;"> month(s)</span><br>
		              			When day is chosen starts here
		              			<span id="dayDiv" class="selectWidth">The 
			              			<select>
			              				<option>1st</option>
			              			</select> 
			              			<select>
			              				<option>Monday</option>
			              			</select>
			              			of every <input type="text" style="width:20px;height:20px;"> month(s)
		              			</span>
		              		</div>
		              	</div>
		              monthly option ends here
		              <div class="row">
		                <div class="col s12 m6">
		                  <b>Schedule Start</b>
		                    <input id="seriesStartDate" type="date" class="datepicker" name="">
		                    <label></label>
		                </div>
		                <div class="col s12 m6">
		                  <div class="row">
		                    <div class="col s4 m4">
		                      <span>&nbsp</span>
		                      <span class="selectMarginOnly">
			                      <select id="endTypeDropDown">
			                        <option disabled selected></option>
			                        <option value="endAfter">End after</option>
			                        <option value="endBy">End By</option>
			                      </select>
		                      </span>
		                    </div>
		                    <div class="col s8 m8">
		                      <div class="endAfter showOpt">
		                        <span>&nbsp</span>
		                        <input id="seriesEndDate" type="date" class="datepicker" name="">
		                        <label></label>
		                      </div>
		                      <div class="endBy showOpt">
		                        <span>&nbsp</span>
		                        <input id="seriesEndAfter" type="text" name="" placeholder="Number of occurences">
		                      </div>
		                    </div>
		                  </div>
		                </div>
		              </div>
		              <div class="row">
		                  <div class="col s12 m6">
		                    <b>Start Time*</b><br>
		                    <div class="row">
		                      <div style="padding-right: 5px;">
		                        <i class="material-icons" style="vertical-align: middle;">access_time</i>
		                        <input id="seriesStartHr" class="numberOnly" value="12" min="1" max="12" type="number" name="" style="width: 50px;">
		                        <span>:</span>
		                        <input id="seriesStartMin" class="numberOnly" value="00" min="0" max="59" type="number" name="" style="width: 50px;">
		                        <span class="selectWidth"> 
			              			<select id="seriesMeridiemStartDropDown">
			                            <option disabled selected></option>
			                            <option value="AM">AM</option>
			                            <option value="PM">PM</option>
			              			</select> 
			              		</span>
		                      </div>
		                    </div>
		                  </div>
		                  <div class="col s12 m6">
		                    <b>End Time*</b><br>
		                    <div class="row">
		                      <div style="padding-right: 5px;">
		                        <i class="material-icons" style="vertical-align: middle;">access_time</i>
		                        <input id="seriesEndHr" class="numberOnly" value="12" min="1" max="12" type="number" name="" style="width: 50px;">
		                        <span>:</span>
		                        <input id="seriesEndMin" class="numberOnly" value="00" min="0" max="59" type="number" name="" style="width: 50px;">
		                        <span class="selectWidth"> 
			              			<select id="seriesMeridiemEndDropDown">
			                            <option disabled selected></option>
			                            <option value="AM">AM</option>
			                            <option value="PM">PM</option>
			              			</select> 
			              		</span>
		                      </div>
		                    </div>
		                  </div>
		                </div>
		              </div>
		            </div>
		          </div> -->
		          <!-- seriesSchedule type ends here -->
		        
		        <!-- seriesSchedule type starts here -->
                <div id="seriesSchedule" class="seriesSchedule endAfter endBy showOpt" style="display:none;">
                   <div class="divcard">
                      <legend style="margin-bottom: 10px;"><b>Series Schedule</b></legend>
                      <div class="row">
                         <div class="col s12 m3">
	                        <b id="seriesScheduleStarts">Schedule Start</b>
	                        <input id="scheduleStartDate" type="date" class="datepicker" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
	                        <label></label>
                         </div>
                         <div class="col s12 m3">
                            <span>&nbsp;</span>
                            <span class="selectMarginOnly">
                            <select id="endTypeDropDown" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                               <option disabled selected></option>
                               <option value="endAfter">End after</option>
                               <option value="endBy">End By</option>
                            </select>
                            </span>
                         </div>
                         <div class="col s12 m3">
                            <span>&nbsp;</span>	
                            <input id="scheduleEndDate" type="date" class="datepicker" style="display:none;" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                            <span>&nbsp;</span>	
                            <input id="noOfOccurrence" type="number" class="datepicker" value="1" style="display:none;" minlength="1" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                         </div>
                         <div class="col s12 m3">
                            <span><b>Recurrence Pattern</b></span>
                            <select id="recurrenceDropdown" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                               <option value="1">Weekly</option>
                               <option value="2">Monthly</option>
                            </select>
                         </div>
                      </div>
                      <!-- weekly option starts here -->
                      <div id="weeklyDiv" class="row" style="padding:10px 10px 20px 10px;">
                      <div id="pagealign" class="row" style="padding:10px 10px 20px 10px;">
                         <label>
                            <input type="checkbox" id="chMonSeries" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                            <span style="color: #1e1e1e">Monday</span>
                         </label>
                         <label style="margin-left: 15px;">
                            <input type="checkbox" id="chTueSeries" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                            <span style="color: #1e1e1e">Tuesday</span>
                         </label>
                         <label style="margin-left: 15px;">
                            <input type="checkbox" id="chWedSeries" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                            <span style="color: #1e1e1e">Wednesday</span>
                         </label>
                         <label style="margin-left: 15px;">
                            <input type="checkbox" id="chThuSeries" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                            <span style="color: #1e1e1e">Thursday</span>
                         </label>
                         <label style="margin-left: 15px;">
                            <input type="checkbox" id="chFriSeries" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                            <span style="color: #1e1e1e">Friday</span>
                         </label>
                         <label style="margin-left: 15px;">
                            <input type="checkbox" id="chSatSeries" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                            <span style="color: #1e1e1e">Saturday</span>
                         </label>
                         <label style="margin-left: 15px;">
                            <input type="checkbox" id="chSunSeries" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                            <span style="color: #1e1e1e">Sunday</span>
                         </label>
                      </div>
                      </div>
                      <!-- weekly option ends here -->
                      <!-- monthly option starts here -->
                      <div id="monthlyDiv" class="row" style="padding:10px 10px 20px 10px; ">
                         <div class="col s12 m3">
                            <label>
                               <input id="dateRadio" type="radio" name="monthlyType" class="with-gap" value="Date" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                               <span style="color: #1e1e1e"><b>Date</b></span>
                            </label><br>
                            <label>
                               <input id="dayRadio" type="radio" name="monthlyType" class="with-gap" value="Day" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                               <span style="color: #1e1e1e;margin-top:18px;"><b>Day</b></span>
                            </label>
                      </div>
                      <div class="col s12 m9">
                         <!-- When date is chosen starts here -->
                         <span id="dateDiv">Day <input id="dayTextBox" value="1" type="text" style="width:20px;height:20px;"> of every <input id="offsetTextBox" type="text" value="1" style="width:20px;height:20px;"> month(s)</span><br>
                         <!-- When day is chosen starts here -->
                         <span id="dayDiv" class="selectWidth">The 
                         <select id="weekDropDown" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                            <option selected value="1">1st</option>
                            <option value="2">2nd</option>
                            <option value="3">3rd</option>
                            <option value="4">4th</option>
                            <option value="5">5th</option>
                         </select> 
                         <select id="dayDropDown" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                            <option selected value="1">Monday</option>
                            <option value="2">Tuesday</option>
                            <option value="3">Wednesday</option>
                            <option value="4">Thursday</option>
                            <option value="5">Friday</option>
                            <option value="6">Saturday</option>
                            <option value="7">Sunday</option>
                         </select>
                            of every <input id="monthlyOffsetTextBox"type="text" value="1" style="width:20px;height:20px;" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}> month(s)
                         </span>
                      </div>
                      </div>
                      <!-- monthly option ends here -->
                      <div class="row">
                         <div class="col s12 m6">
                            <b>Start Time*</b><br>
                            <div class="row">
                               <div style="padding-right: 5px;">
                                  <i class="material-icons" style="vertical-align: middle;">access_time</i>
                                  <input id="seriesStartHr" class="numberOnly" value="8" min="1" max="12" type="number" name="" style="width: 50px;" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                                  <span>:</span>
                                  <input id="seriesStartMin" class="numberOnly" value="00" min="0" max="59" type="number" name="" style="width: 50px;" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                                  <span class="selectWidth"> 
                                     <select id="seriesMeridiemStartDropDown" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                                        <option selected value="AM">AM</option>
                                        <option value="PM">PM</option>
                                     </select> 
                                  </span>
                               </div>
                            </div>
                         </div>
                         <div class="col s12 m6">
                            <b>End Time*</b><br>
                            <div class="row">
                               <div style="padding-right: 5px;">
                                  <i class="material-icons" style="vertical-align: middle;">access_time</i>
                                  <input id="seriesEndHr" class="numberOnly" value="12" min="1" max="12" type="number" name="" style="width: 50px;" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                                  <span>:</span>
                                  <input id="seriesEndMin" class="numberOnly" value="00" min="0" max="59" type="number" name="" style="width: 50px;" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                                  <span class="selectWidth"> 
                                  <select id="seriesMeridiemEndDropDown" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                                     <option value="AM">AM</option>
                                     <option selected value="PM">PM</option>
                                  </select> 
                                  </span>
                               </div>
                            </div>
                         </div>
                      </div>
                   </div>
                </div>
                <!-- seriesSchedule type ends here -->
		        
        <!-- addScheduleModal starts here -->
          <div id="addScheduleModal" class="modal modal-fixed-footer" style="height: 320px;" style="display:none;">
            <div class="modal-header">
              <h5>Add Schedule</h5>
            </div>
            <div class="modal-content">
              <div class="content-row">
              	<div class="row">
              		<div class="col s12 m6">
              			<div>
		                  <b id="setScheduleStartLabel">Schedule Start*</b>
		                    <input id="setDateStartPicker" type="date" class="datepicker" name="" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
		                    <label></label>
		                </div>
		                <div>
		                	<b>Start Time</b><br>
		                   	<i class="material-icons" style="vertical-align: middle;">access_time</i>
		                    <input id="setStartHr" class="numberOnly" value="12" min="1" max="12" type="number" name="" style="width: 50px;" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
		                    <span>:</span>
		                    <input id="setStartMin" class="numberOnly" value="00" min="0" max="59" type="number" name="" style="width: 50px;" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
		                    <span class="selectWidth">
		                    	<select id="setMeridiemFrom" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
			                      <option disabled selected></option>
			                      <option selected value="AM">AM</option>
			                      <option value="PM">PM</option>
			                    </select>
		                    </span>
		            	</div>
              		</div>
              		<div class="col s12 m6">
              			<div>
		                  <b id="setScheduleEndLabel">Schedule End*</b>
		                    <input id="setDateEndPicker" type="date" class="datepicker" name="" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
		                    <label></label>
		                </div>
		                <div>
		                	<b>End Time</b><br>
		                   	<i class="material-icons" style="vertical-align: middle;">access_time</i>
		                    <input id="setEndHr" class="numberOnly" value="12" min="1" max="12" type="number" name="" style="width: 50px;" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
		                    <span>:</span>
		                    <input id="setEndMin" class="numberOnly" value="00" min="0" max="59" type="number" name="" style="width: 50px;" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
		            		<span class="selectWidth">
		            			<select id="setMeridiemTo" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
			                      <option disabled selected></option>
			                      <option selected value="AM">AM</option>
			                      <option value="PM">PM</option>
			                    </select>
		            		</span>
		            	</div>
              		</div>
	            </div>
              </div>
            </div>
            <c:if test="${sessionScope.user.userTypeId eq 1 || sessionScope.user.userId eq trainer.employeeId}">
            <div class="modal-footer">
              <a class="modal-close btn waves-effect waves-light bttn" type="button" name="action"><i class="material-icons left">cancel</i>Cancel</a>
              <a id="addSetScheduleSubmit" class="btn waves-effect waves-light bttn" type="submit" name="action"><i class="material-icons left">save</i>Save</a>
            </div>
            </c:if>
          </div>
          <!-- addScheduleModal ends here -->

                <div class="row" style="margin: 5px 5px 0 5px;">
                  <div class="divcard">
                    <div class="row" style="padding: 0 15px;">
                      <legend style="margin-bottom: 10px;"><b>Class Schedule</b></legend>
                    </div>
<!--                     <div class="row" style="padding: 0 15px;"> -->
<!--                       <a href="#!" class="right bttn btn waves-effect waves-light"><i class="material-icons left">add_circle</i>Add</a> -->
<!--                     </div> -->
                    <div>
                    	<table class="highlight striped">
                    		<thead>
	                    		<tr>
	                    			<th>Date</th>
	                    			<th>Start Time</th>
	                    			<th>End Time</th>
	                    		</tr>
                    		</thead>
                    		<tbody id="classScheduleTblBody">
                    			<tr>
                    				<td>ad</td>
                    				<td>ad</td>
                    				<td>ad</td>
                    			</tr>
                    		</tbody>
                    	</table>
                    </div>
                  </div>
                </div>

                <div id="mainChecklist" class="row" style="margin: 5px 5px 0 5px;">
                  <div class="divcard">
                  	<c:if test="${sessionScope.user.userTypeId eq 1 || sessionScope.user.userId eq trainer.employeeId}">
                  	<div>
                  		<a id="" href="#!" class="btn bttn waves-light waves-effect right"><i class="material-icons left">add_circle</i>Add</a>
                  	</div>
                  	</c:if>
                    <div>
                      <legend style="margin-bottom: 10px;"><b>Checklist</b></legend>
                    </div>
                    <div class="row">
                      <div class="col s10 m11">
                        <input type="text" name="" ${sessionScope.user.userTypeId == 1 || sessionScope.user.userId eq trainer.employeeId ? '' : 'disabled'}>
                      </div>
                      <div class="col s2 m1" style="padding-top: 10px;">
                      	<c:if test="${sessionScope.user.userTypeId eq 1 || sessionScope.user.userId eq trainer.employeeId}">
                        <a href="#!"><i class="material-icons">delete</i></a>
                        </c:if>
                        <!-- <a href="#!"><i class="material-icons">add_circle</i></a> -->
                      </div>
                    </div>
                  </div>
                </div>
                
                <div id="mainLearningPath" class="row" style="margin: 5px 5px 0 5px;">
                  <div class="divcard">
                    <div class="row" style="padding: 0 15px;">
                      <legend style="margin-bottom: 10px;"><b>Learning Path</b></legend>
                    </div>
<!--                     <div class="row" style="padding: 0 15px;"> -->
<!--                       <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> -->
<!--                     </div> -->
					<div id="learningPathCon">
		               
		            </div>
                  </div>
                </div>

                <!-- only shows on modular and hide in individual -->
                <div class="row" style="margin: 5px 5px 0 5px;">
                  <div class="divcard">
                    <div class="row" style="padding: 0 15px;">
                      <legend style="margin-bottom: 10px;"><b>Course Description</b></legend>
                    </div>
                    <div id="divCourseDescription" class="row" style="padding: 0 15px;">
                      
                    </div>
                  </div>
                </div>
                <!-- up to here -->

                <div class="row" style="margin: 5px 5px 0 5px;">
                  <div class="divcard">
                    <div class="row" style="padding: 0 15px;">
                      <legend style="margin-bottom: 10px;"><b>Course Objectives</b></legend>
                    </div>
                    <div id="divCourseObjective" class="row" style="padding: 0 15px;">
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                  </div>
                </div>

              </div>
              <!-- content ends here -->
            </div>
          </div>
        </div>
        
        <input type="hidden" id="classId" value="${classInfo.classId}">
        <input type="hidden" id="locationId" value="${classInfo.locationId}">
        <input type="hidden" id="courseId" value="${classInfo.courseId}">
        <input type="hidden" id="trainerId" value="${classInfo.courseId}">
        <input type="hidden" id="isSelfRegister" value="${classInfo.isSelfRegister}">
        <input type="hidden" id="withCertificate" value="${classInfo.withCertificate}">
        <input type="hidden" id="exam" value="${classInfo.withExam}">
        <input type="hidden" id="deliveryMethod" value="${classInfo.deliveryMethod}">
        <input type="hidden" id="scheduleType" value="${classInfo.scheduleType}">
        <input type="hidden" id="userType" value="${sessionScope.user.userTypeId}">
        <input type="hidden" id="loggedInId" value="${sessionScope.user.userId}">
        <input type="hidden" id="trainerId" value="${trainer.employeeId}">        
</body>
</html>