<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/admin/common/admin-include.jsp"%>
<!DOCTYPE html>
<html>
   <head>
      <title>PAGSS</title>
      <meta name = "viewport" content = "width = device-width, initial-scale = 1">
      <!-- START: CSS-HEADER -->  
	  <jsp:include page="../portal/common/css-header.jsp" />
	  <!-- <link rel="stylesheet" type="text/css" href="static/assets/css/adminlogin.css"> -->
	  <!-- css for new login -->
	  <link rel="stylesheet" type="text/css" href="static/assets/css/login.css">  
      <!-- END: CSS-HEADER -->
       
      <!-- START: JS-HEADER -->
      <jsp:include page="../portal/common/js-header.jsp" />
      <script src="static/assets/js/portal-resetpassword.js"></script>
      <!-- END: JS-HEADER -->
      </head>
<body>
<!-- <div class="mother-container">

        <div class="subContainer">
          <div class="container">
            <div id="container-2">
              <div id="small">
                <img style="width:150px;height:150px;" src="static/assets/images/usersimage/user.png">
                <h5 style="color:#1e1e1e !important">Change Password</h5>
                <form action="save-password" method="post" role="form">
                <input type="password" class="validate text-input" placeholder="Old Password">
                <input name="newPassword" type="password" class="validate text-input" placeholder="New Password">
                <input name="confirmPassword" type="password" class="validate text-input" placeholder="Confirm Password" required>
                <button class="btn waves-effect waves-light bttn" type="submit" name="action" style="margin-top: 10px;">Submit</button>
            	<button id="fontcolor" class="btn waves-effect waves-light bttn" type="submit" name="action">Submit</button>
            	</form>
            </div>
          </div>
        </div>

      </div>
   </div> --> 
   
   	  <!-- new login starts here -->
      <div>
      	<div class="row">
            <div class="fullView col s12 m4">
            	<form action="save-password" method="post" role="form">
	            	<div style="display:block;padding:20px;">
		            	<img style="width:150px;height:auto;margin-bottom:20px;" src="static/assets/images/usersimage/user.png">
		                <h5 style="color:#1e1e1e !important">Change Password</h5>
		                <input style="width:90%;padding: 0 5px;background-color: rgb(232, 240, 254) !important;" type="password" class="validate text-input" placeholder="Old Password">
		                <input style="width:90%;padding: 0 5px;background-color: rgb(232, 240, 254) !important;" name="newPassword" type="password" class="validate text-input" placeholder="New Password">
		                <input style="width:90%;padding: 0 5px;background-color: rgb(232, 240, 254) !important;" name="confirmPassword" type="password" class="validate text-input" placeholder="Confirm Password" required>
		                <!-- <button class="btn waves-effect waves-light bttn" type="submit" name="action" style="margin-top: 10px;">Submit</button> -->
		            	<button style="width: 93%;" id="fontcolor" class="btn waves-effect waves-light bttn" type="submit" name="action">Submit</button>
	            	</div>
           		</form>
            </div>
      		<div class="bg col m8" style="padding: 0 !important;">
               
            </div>
      	</div>
      </div>
      <!-- new login ends here -->  
</body>
</html>