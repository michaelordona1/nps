<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/admin/common/admin-include.jsp"%>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>PAGSS - TRAINING CALENDAR</title>
	<!-- START: CSS-HEADER -->
  <jsp:include page="../admin/common/admin-css-header.jsp" />

	<!-- END: CSS-HEADER -->
	<!-- START: JS-HEADER -->
	<jsp:include page="../admin/common/admin-js-header.jsp" />
  <script src="static/assets/js/admin-trainingcalendar.js"></script>
	<!-- END: JS-HEADER -->
</head>
<body>
	<!-- Header -->
	<jsp:include page="../admin/common/admin-header.jsp" />

	<!-- Left Side bar -->
	<jsp:include page="../admin/common/admin-sidebar.jsp" />
    <div class="container2">
          <div class="row">
            <div class="col s12 m12">
               <h4>Training Calendar</h4>
            </div>
          </div>

          <div class="row">
            <hr style="width:100%;">
          </div>

          <!--Search Filter starts here-->
          <div class="row">
            <div class="divcard">
              <div class="row">
                <div class="col s6 m4">
                  <b>Trainer</b>
                  <select id="trainerDropdown">
                    <option selected value="" >All</option>
                  </select>
                </div>
                <div class="col s6 m4">
                  <b>Delivery Method</b>
                  <select id="deliveryMethodDropdown">
                    <option selected value="" >All</option>
                  </select>
                </div>
                <div class="col s10 m4">
                  <b>Location</b>
                  <select id="locationDropdown">
                    <option selected="" value="">All</option>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="right buttonright">
                  <a class="btn waves-effect waves-light bttn" type="button" name="action" id="resetBtn"><i class="material-icons left">refresh</i>Reset</a>
                  <a class="btn waves-effect waves-light bttn" type="submit" name="action" id="searchBtn"><i class="material-icons left">search</i>Search</a>
                </div>
              </div>
            </div>
          </div>
          <!--Search Filter ends here-->

          <div class="row">
            <div class="divcard">
              <!-- Calendar starts here -->
                <div class="row">
	                <div class="col s12 m3">
	                  <div class="divcard" style="padding: 10px;">
                        <legend style="margin-bottom: 10px;"><b>Class Details</b></legend>
                        <p><b>Trainer Name:</b> <span id="trainerName"></span></p>
                        <p><b>Location:</b> <span id="locationName"></span></p>
                        <p><b>Avaiable slots:</b> <span id="availableSlots"></span></p>
                        <p><b>Status:</b> <span id="status"></span></p>
                        <a id="redirectToClassInfoBtn" class="btn waves-effect waves-light bttn" style="width: 100%"><i class="material-icons left">search</i>Go to Class</a>
	                  </div>
	                </div>
	                <div class="col s12 m9">
	                  <div id="calendar" class="fc fc-unthemed fc-ltr"></div>
	                </div>
	              </div>
              <!-- Calendar ends here -->
            </div>
          </div>
    </div>
</body>
</html>