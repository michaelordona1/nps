<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/admin/common/admin-include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="ISO-8859-1" name="viewport" content="width=device-width, initial-scale=1.0">
	<title>PAGSS - VIEW USER (USER GROUP)</title>
	<!-- START: CSS-HEADER -->  
	<jsp:include page="../admin/common/admin-trainee-css-header.jsp" />
	<!-- END: CSS-HEADER -->
	<!-- START: JS-HEADER -->
	 <jsp:include page="../admin/common/admin-js-header.jsp" />
	 <script src="static/assets/js/admin-classattendance.js"></script>
	<!-- END: JS-HEADER -->
</head>
<body>
	<!-- Header -->
	<jsp:include page="../admin/common/admin-collapsibleheader.jsp" />

	<!-- Left Side bar -->
	<jsp:include page="../admin/common/admin-collapsiblesidebar.jsp" />
	
	<div class="container1">
          <div class="row">
          	
		      <!-- left side card/panel includes photo, title, type and status -->
		      <jsp:include page="../admin/common/admin-classinfo-card.jsp" />
			
            <div class="col s12 m9" style="padding: 5px;">
              <div class="navbar">
              	<a href="admin.classdashboard?classId=${classId}">Dashboard</a>
              	<a href="admin.classdetails?classId=${classId}">Details</a>
              	<a href="admin.classtrainee?classId=${classId}">Trainees</a>
              	<a class="active" href="admin.classattendance?classId=${classId}">Attendance</a>
              	<a href="admin.classsettings?classId=${classId}">Settings</a>
              	<a href="admin.classmaterials?classId=${classId}">Materials</a>
              	<a href="admin.classexam?classId=${classId}">Exams</a>
              	<a href="admin.classfinancial?classId=${classId}">Financials</a>
              	<a href="admin.classevaluation?classId=${classId}">Evaluation</a>
              </div>
              <!-- content starts here -->
              <div class="divcard">
                <div class="row">
                	<c:if test="${sessionScope.user.userTypeId eq 1 || sessionScope.user.userId eq trainer.employeeId}">
                  <div class="right">
                    <a href="admin.class" class="btn bttn waves-light waves-effect" type="submit" style="margin-right: 5px;"><i class="material-icons left">arrow_back</i>Back</a>
<!--                     <a href="#!" class="btn bttn waves-light waves-effect" type="submit" style="margin-right: 5px;"><i class="material-icons left">save</i>Save Attendance</a> -->
                  </div>
                  	</c:if>
                </div>

                <div>
                  <span>Schedule List</span>
                </div>

                <div class="row">
                  <div class="col s6 m6">
                    <select id="dropdownScheduleList">
                      <option>asd</option>
                      <option>asd</option>
                      <option>asd</option>
                    </select>
                  </div>
                  <div class="col s6 m6" style="padding-top: 15px;">
                    <a href="#!" class="right" id="printClassAttendanceBtn"><i class="material-icons">print</i></a>
                  </div>
                </div>

                <div id="divAttendance1" class="row">
                  <table class="highlight striped">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Employee Code</th>
                        <th>Group</th>
                        <th>Job Role</th>
                        <th>Attendance Status</th>
                        <th>Remarks</th>
                      </tr>
                    </thead>
                    <tbody id="tblAttendance1Body">
                    </tbody>
                  </table>
                  	<div id="pagealign">
              			<div id="attendancePagination1">
                  		<!--Pagination-->
               			</div>
             		</div>
                </div>
                
                <!-- Show only on cbt modular and indiviual attendance -->
                <div id="divAttendance2">
                  <table class="highlight striped">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Employee Code</th>
                        <th>Group</th>
                        <th>Job Role</th>
                        <th>Status</th>
                        <th>Remarks</th>
                      </tr>
                    </thead>
                    <tbody id="tblAttendance2Body">
                    </tbody>
                  </table>
                  	<div id="pagealign">
              			<div id="attendancePagination2">
                  		<!--Pagination-->
               			</div>
             		</div>
                </div>
                <!-- up to here -->

              </div>
              <!-- content ends here -->
            </div>
          </div>
        </div>
        
        <input type="hidden" id="classId" value="${classInfo.classId}">
        <input type="hidden" id="courseId" value="${classInfo.courseId}">
        <input type="hidden" id="deliveryMethod" value="${classInfo.deliveryMethod}">
        <input type="hidden" id="scheduleType" value="${classInfo.scheduleType}">
        <input type="hidden" id="userType" value="${sessionScope.user.userTypeId}">
        <input type="hidden" id="loggedInId" value="${sessionScope.user.userId}">
        <input type="hidden" id="trainerId" value="${trainer.employeeId}">        
        
        
</body>
</html>