<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/admin/common/admin-include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>PAGSS - VIEW USER (USER GROUP)</title>
	<!-- START: CSS-HEADER -->  
	<jsp:include page="../admin/common/admin-trainee-css-header.jsp" />
	<!-- END: CSS-HEADER -->
	<!-- START: JS-HEADER -->
	 <jsp:include page="../admin/common/admin-js-header.jsp" />
	 <script src="static/assets/js/admin-classfinancial.js"></script>
	<!-- END: JS-HEADER -->
</head>
<body>
	<jsp:include page="../admin/common/admin-collapsibleheader.jsp" />

	<!-- Left Side bar -->
	<jsp:include page="../admin/common/admin-collapsiblesidebar.jsp" />
	
	<div class="container1">
          <div class="row">
          
		      <!-- left side card/panel includes photo, title, type and status -->
		      <jsp:include page="../admin/common/admin-classinfo-card.jsp" />
		      
            <div class="col s12 m9" style="padding: 5px;">
              <div class="navbar">
              	<a href="admin.classdashboard?classId=${classId}">Dashboard</a>
              	<a href="admin.classdetails?classId=${classId}">Details</a>
              	<a href="admin.classtrainee?classId=${classId}">Trainees</a>
              	<a href="admin.classattendance?classId=${classId}">Attendance</a>
              	<a href="admin.classsettings?classId=${classId}">Settings</a>
              	<a href="admin.classmaterials?classId=${classId}">Materials</a>
              	<a href="admin.classexam?classId=${classId}">Exams</a>
              	<a class="active" href="admin.classfinancial?classId=${classId}">Financials</a>
              	<a href="admin.classevaluation?classId=${classId}">Evaluation</a>
              </div>
              <!-- content starts here -->
              <div class="divcard">

				<c:if test="${sessionScope.user.userTypeId eq 1 || sessionScope.user.userId eq trainer.employeeId}">
                <div class="row" style="margin-bottom: 40px;">
                  <div class="right">
                    <a href="#!" class="btn bttn waves-light waves-effect" style="margin-right: 5px;"><i class="material-icons left">arrow_back</i>Back</a>
                    <a id="btnSaveClassFinancial" class="btn bttn waves-light waves-effect" style="margin-right: 5px;"><i class="material-icons left">save</i>Save</a>
                  </div>
                </div>
                </c:if>

                <!-- Trainers section starts here -->                
                <div class="row" style="margin-bottom:10px;">
                	<div class="col s12 m6">
                		<h6 style="margin-top: 10px;"><b>Trainers</b></h6>
                	</div>
                	<div class="col s12 m6">
                		<c:if test="${sessionScope.user.userTypeId eq 1 || sessionScope.user.userId eq trainer.employeeId}">
                		<a id="addTrainerBtn" href="#!" class="btn bttn waves-light waves-effect right"><i class="material-icons left">add_circle</i>Add</a>
                		</c:if>
                	</div>
                </div>
				
                <div class="row" style="padding: 10px;margin-bottom:50px;">
                  <table>
                    <thead>
                      <tr>
                        <th>Trainer</th>
                        <th>Rate</th>
                        <th></th>
                        <th>No. of Days</th>
                        <th></th>
                        <th>Total Cost</th>
                        <th></th>
                      </tr>
                    </thead>

                    <tbody id="trainerTblBody">
                    </tbody>
                  </table>
                </div>
                <!-- trainers section ends here -->

                <!-- Materials section starts here -->
                <div class="row" style="margin-bottom:10px;">
                	<div class="col s12 m6">
                		<h6 style="margin-top: 10px;"><b>Materials</b></h6>
                	</div>
                	<div class="col s12 m6">
                		<c:if test="${sessionScope.user.userTypeId eq 1 || sessionScope.user.userId eq trainer.employeeId}">
                		<a id="addMaterialBtn" href="#!" class="btn bttn waves-light waves-effect right"><i class="material-icons left">add_circle</i>Add</a>
                		</c:if>
                	</div>
                </div>

                <div class="row" style="padding: 10px;margin-bottom:50px;">
                  <table>
                    <thead>
                      <tr>
                        <th>Material</th>
                        <th>Price</th>
                        <th></th>
                        <th>Quantity</th>
                        <th></th>
                        <th>Total Cost</th>
                        <th></th>
                      </tr>
                    </thead>

                    <tbody id="materialTblBody">
                    </tbody>
                  </table>
                </div>
                <!-- Materials section ends here -->

                <!-- Others section starts here -->
                <div class="row" style="margin-bottom:10px;">
                	<div class="col s12 m6">
                		<h6 style="margin-top: 10px;"><b>Others</b></h6>
                	</div>
                	<div class="col s12 m6">
                		<c:if test="${sessionScope.user.userTypeId eq 1 || sessionScope.user.userId eq trainer.employeeId}">
                		<a id="addOtherBtn" href="#!" class="btn bttn waves-light waves-effect right"><i class="material-icons left">add_circle</i>Add</a>
                		</c:if>
                	</div>
                </div>
                

                <div class="row" style="padding: 10px;">
                  <table>
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Price</th>
                        <th></th>
                        <th>Quantity</th>
                        <th></th>
                        <th>Total Cost</th>
                        <th></th>
                      </tr>
                    </thead>

                    <tbody id="otherTblBody">
                    </tbody>
                  </table>
                </div>
                <!-- Others section ends here -->

                <div class="row" style="padding: 10px;">
                  <div class="right">
                    <b>Grand Total Cost:</b>
                    <input id="tbFinalTotalCost" type="text" name="" value="0.00" disabled style="font-weight: bolder;">
                  </div>
                </div>

              </div>
              <!-- content ends here -->
            </div>
          </div>
        </div>
        
        <input type="hidden" id="classId" value="${classInfo.classId}">
        <input type="hidden" id="courseId" value="${classInfo.courseId}">
        <input type="hidden" id="deliveryMethod" value="${classInfo.deliveryMethod}">
        <input type="hidden" id="scheduleType" value="${classInfo.scheduleType}">
        <input type="hidden" id="userType" value="${sessionScope.user.userTypeId}">
        <input type="hidden" id="loggedInId" value="${sessionScope.user.userId}">
        <input type="hidden" id="trainerId" value="${trainer.employeeId}">        
</body>
</html>