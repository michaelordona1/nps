<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/admin/common/admin-include.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>PAGSS - REPORTS</title>
<!-- START: CSS-HEADER -->
<jsp:include page="../admin/common/admin-css-header.jsp" />
<!-- END: CSS-HEADER -->
<!-- START: JS-HEADER -->
<jsp:include page="../admin/common/admin-js-header.jsp" />
<script src="static/assets/js/admin-reports.js"></script>
<!-- END: JS-HEADER -->
</head>
<body>
	<!-- Header -->
	<jsp:include page="../admin/common/admin-header.jsp" />

	<!-- Left Side bar -->
	<jsp:include page="../admin/common/admin-sidebar.jsp" />

	<div class="container2">
          <div class="row">
            <div class="col s12 m9">
               <h4>Reports</h4>
            </div>
          </div>

          <div class="row">
            <hr style="width:100%;">
          </div>

          <div class="row">
            <div class="divcard">
                 <legend style="margin-bottom: 10px;"><b>&nbspSearch Filter&nbsp</b></legend>
                 <div class="row">
                   <div class="col s12 m4">
                     <b>Types of Report</b>
                     <select id="reportTypeDropdown">
                     </select>
                   </div>
                   <div class="col s12 m4">
                     <b>Course</b>
                     <select id="courseDropdown">
                       <option>All</option>
                     </select>
                   </div>
                   <div class="col s12 m4">
                     <b>User Group</b>
                     <select id="userGroupDropdown">
                       <option>All</option>
                     </select>
                   </div>
                 </div>
                 <div class="row">
					<div class="col s12 m4">
						<b>Users</b> 
						 <select id="userDropdown">
	                       <option>All</option>
	                     </select>
					</div>
					<div class="col s12 m4">
						<b>Category</b>
						 <select id="categoryDropdown">
	                       <option>All</option>
	                     </select>						
					</div>
					<div class="col s12 m2">
						<b>Start Date</b>
						<input type="date"  id="startDate">
					</div>
					<div class="col s12 m2">
						<b>End Date</b>
						<input type="date"  id="endDate">
					</div>					
				</div>
                 <div class="row">
                 	<div>
	                 	<div class="col s12 m2">
							<b>Generate as</b>
							<select id="generatorTypeDropdown">
		                       <option value="1">PDF</option>
		                       <option value="2">Excel</option>
		                    </select>
						</div>
                 	</div>
                 
					<div class="right buttonright">
						<a id="btnReset" class="btn waves-effect waves-light bttn"
							type="button" name="action"><i class="material-icons left">refresh</i>Reset</a>
						<a id="btnSubmit" class="btn waves-effect waves-light bttn"
							type="submit" name="action"><i class="material-icons left">search</i>Generate</a>
					</div>
				</div>
           </div>
           </div>



           <div class="row">
	          <div class="divcard" id="iframeContainer" style="height:600px">
	          	<center><iframe id="documentViewer" style="position: relative; height: 570px; width: 100%;"></iframe></center>
              </div>
           </div>
            
        </div>
</body>
</html>