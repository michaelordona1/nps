<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/admin/common/admin-include.jsp"%>
<!DOCTYPE html>
<html>
   <head>
       <title>PAGSS</title>
      <meta name = "viewport" content = "width = device-width, initial-scale = 1">
      <!-- START: CSS-HEADER -->  
	  <jsp:include page="../portal/common/css-header.jsp" />
      <!-- END: CSS-HEADER -->
       
      <!-- START: JS-HEADER -->
      <jsp:include page="../admin/common/admin-js-header.jsp" />
      <jsp:include page="../admin/common/admin-tinymce-header.jsp" />
      <script src="static/assets/js/utility/tinymce-util.js"></script>
      <script src="static/assets/js/admin-classessay.js"></script>
      <!-- END: JS-HEADER -->
   </head>
	<body>
		<!-- Header -->
		<jsp:include page="../portal/common/portal-header.jsp" />	
		<!-- Left Side bar -->
		<%-- <jsp:include page="../portal/common/portal-sidenav.jsp" />	 --%>
		<div class="container" style="margin-top: 20px;">
			<div class="divcard">
			
			      <div class="row">
			      	<div class="col s12 m3">
			      		<div>
		                  <div class="divcard" style="margin-bottom: 20px;">
		                    <table>
		                      <tbody id="questionNumbersContainer">
		                      </tbody>
		                    </table>
		                  </div>	
		                  <div class="col s12 m6">
	                  		  <a href="admin.classexam?classId=${classInfo.classId}" class="btn bttn waves-effect waves-light"  style="width: 100%;">Back</a>
	                	  </div>
	                	  <div class="col s12 m6">
	                  		  <a href="#!" class="btn bttn waves-effect waves-light" id="submitBtn" style="width: 100%;">Submit</a>
	                	  </div>	                  
			      		</div>
			      	</div>
			      	
			      	<div class="col s12 m9">
			      		<div class="divcontrol">
		                  <div>
		                    <a href="#!" id="prevBtn" class="btn bttn waves-effect waves-effect"><i class="material-icons left">keyboard_arrow_left</i>Previous Question</a>
		                  </div>
		                  <div style="padding:10px;">
	                    	<span><b>Question <span id="currentQuestionNo"></span> out of <span id="totalQuestions"></span></b></span>
	                  	  </div>	
		                  <div>
		                    <a href="#!" id="nextBtn" class="btn bttn waves-effect waves-effect"><i class="material-icons right">keyboard_arrow_right</i>Next Question</a>
		                  </div>	                  	  
			      		</div>
			      		
			      		
			      		<div class="mydivs">
			                  <div id="essayCon">
			                    <div>
			                      <span id="essayQuestionCon"> </span>
			                    </div>
			                    
			                    <div class="row">
			                      <div class="col s12 m7">
			                        <form class="formstyle" id="essayForm">
			                        </form>
			                      </div>
			                      <div class="col s12 m5 center" >
			                        <img class="fixSizeimg questionPic" id="essayMedia" src="">
			                      </div>
			                    </div>
			                  </div>			                  			                 
			      		</div>
			      		
			      		
			      		
			      		<div>
			      			<p>Criteria for checking the Essay Exam: </p>
			      			<table>
			      				<thead>
			      					<tr>
			      						<th>Criteria</th>
			      						<th>Description</th>
			      						<th>Percentage</th>
			      					</tr>
			      				</thead>
			      				<tbody>
			      					<tr>
			      						<td>CONTENTS</td>
			      						<td>refers to the presentation of information in an orderly manner and with considerable substance or depth</td>
			      						<td>50%</td>
			      					</tr>
			      					<tr>
			      						<td>ORGANIZATION OF THOUGHTS</td>
			      						<td>refers to the ability to organize ideas to specific context</td>
			      						<td>15%</td>
			      					</tr>
			      					<tr>
			      						<td>GRAMMAR</td>
			      						<td>refers to the ability to write effectively with correct grammar usage and spelling</td>
			      						<td>20%</td>
			      					</tr>
			      					<tr>
			      						<td>STRUCTURE</td>
			      						<td>refers to the presentation of an essay in a coherent manner : the introduction, body and conclusion</td>
			      						<td>15%</td>
			      					</tr>
			      				</tbody>
			      			</table>
			      			<p>*** The Trainers shall be using  the given average percentage to arrive at the most appropriate significant ratings for Essay exams</p>
			      		</div>
			      		
			      	</div>
			      	
			      	
			      </div>
			</div>
		</div>
		<input type="hidden" id="hdnSectionOrder" value="${sectionOrder}">
		<input type="hidden" id="hdnExamId" value="${examId}">
		<input type="hidden" id="hdnClassId" value="${classInfo.classId}">
		<input type="hidden" id="hdnCourseId" value="${courseId}">
		<input type="hidden" id="hdnEmployeeId" value="${employeeId}">
		<input type="hidden" id="hdnRetakeNo" value="${retakeNo}">
		
	    <input type="hidden" id="hdnQuestionId">
	    <input type="hidden" id="hdnQuestionType">
	    <input type="hidden" id="hdnIsRandomized">		
	</body>
</html>