<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/admin/common/admin-include.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>PAGSS - QUESTIONS</title>
	<!-- START: CSS-HEADER -->
	<jsp:include page="../admin/common/admin-css-header.jsp" />
	<!-- END: CSS-HEADER -->
	<!-- START: JS-HEADER -->
	<jsp:include page="../admin/common/admin-js-header.jsp" />
	<script src="static/assets/js/admin-questions.js"></script>
	<!-- END: JS-HEADER -->
</head>
<body>
	<!-- Header -->
	<jsp:include page="../admin/common/admin-header.jsp" />

	<!-- Left Side bar -->
	<jsp:include page="../admin/common/admin-sidebar.jsp" />
	<div class="container2">
          <div class="row">
            <div class="col s12 m9">
               <h4>Questions</h4>
            </div>
            <div id="navpad" class="nav-wrapper col s12 m3 row">
              <div class="row">
                 <div class="input-field col s12 m12">
                    <a href="admin.question?action=create" id="add-location" style="width:100%;text-transform:capitalize;" class="btn modal-trigger waves-effect waves-light bttn" type="submit" name="" ><i class="material-icons left">add_circle</i>Add Question</a>
                 </div>
              </div>
            </div>
          </div>

          <div class="row">
            <hr style="width:100%;">
          </div>
            
          <div class="row">
            <div class="divcard">
               <legend style="margin-bottom: 10px;"><b>Search Filter</b></legend>
               <div class="row">
                 <div class="col s12 m6">
                   <b>Type</b>
                   <select id="questionTypeDropDown">
                     <option selected value="0">All</option>
                   </select>
                 </div>
                 <div class="col s12 m6">
                   <b>Topic</b>
                   <select id="topicDropDown">
                     <option selected value="0">All</option>
                   </select>
                 </div>
                 <div class="col s12 m6">
                   <b>Difficulty Level</b>
                   <select id="difficultyDropDown">
                     <option selected value="0">All</option>
                   </select>
                 </div>
                 <div class="col s12 m6">
                   <b>Status</b>
                   <select id="statusDropDown">
                     <option value="2">All</option>
                     <option value="1" selected>Active</option>
                     <option value="0">Inactive</option>
                   </select>
                 </div>
                 <div class="right buttonright">
                    <a class="btn waves-effect waves-light bttn" id= "resetBtn" type="button" name="action"><i class="material-icons left">refresh</i>Reset</a>
                    <a id="searchQuestionBtn" class="btn waves-effect waves-light bttn"><i class="material-icons left">search</i>Search</a>
                 </div>
               </div>
            </div>
          </div>

          <div class="row">
            <div class="divcard">
              <table class="highlight ">
                <thead>
                  <tr>
                      <th>Actions</th>
                      <th>Question<a href="" class="sort sort-by" data-sortname="label" data-sortdir="DESC"></a><a href="#" class="sort" data-sortname="label" data-sortdir="ASC"></a></th>
                      <th>Topic<a href="" class="sort sort-by" data-sortname="topicDesc" data-sortdir="DESC"></a><a href="#" class="sort" data-sortname="topicDesc" data-sortdir="ASC"></a></th>
                      <th>Difficulty Level<a href="" class="sort sort-by" data-sortname="difficultyName" data-sortdir="DESC"></a><a href="#" class="sort" data-sortname="difficultyName" data-sortdir="ASC"></a></th>
                      <th>Type<a href="" class="sort sort-by" data-sortname="questionTypeDesc" data-sortdir="DESC"></a><a href="#" class="sort" data-sortname="questionTypeDesc" data-sortdir="ASC"></a></th>
                      <th>Status<a name="sort"  class="sort sort-by" data-sortname="status" data-sortdir="DESC"></a><a href="#" class="sort" data-sortname="status" data-sortdir="ASC"></a></th>
                  </tr>
                </thead>
                <tbody id="questionsTblBody">
                  <!-- Dynamically Populated -->
                </tbody>
                
              </table>
              <div id="pagealign">
	              <div id="questionsPagination">
	              </div>
              </div>
            </div>
          </div>
          
          <!-- Question Preview with image -->
          <div id="viewQuestionModalwithPicture" class="modal modal-fixed-footer" oncontextmenu='return false;'>
              <div class="modal-header">
                <h5>Question</h5>
              </div>
              <div class="modalQuestion">
                <div class="content-row">
                  <div class="row">
                    <div class="col s12 m8">
                      <div id="questionContentwithPicture">
                      </div>
                      <!-- Multiple choice starts here -->
                      <div id="choiceDivwithPicture" class="divcard questionsDiv">
                      	<p>Type of Question: <b>Multiple Choice</b></p>
                      	<p>Answers:</p>
						<div id="multipleChoiceDivwithPicture">
						</div>
                      </div>
                      <!-- Multiple Choice Ends here -->
                      <!-- Fill in the blanks starts here -->
                      <div id="fillBlanksDivwithPicture" class="divcard questionsDiv">
                      	<p>Answers (in order): <b id="trueFalsewithPicture"></b></p>
						<div id="fillBlanksDivAnswerwithPicture">
						</div>
                      </div>
                      <!-- Fill in the blanks ends here -->
                      <!-- True or false starts here -->
                      <div id="trueFalseDivwithPicture" class="divcard questionsDiv">
                      	<p>Type of Question: <b>True or False</b></p>
						<p>Answers (in order): <b id="trueFalsewithPicture"></b></p>
                      </div>
                      <!-- True or false ends here -->
                      <!-- Essay starts here -->
                      <div id="essayDivwithPicture" class="divcard questionsDiv">
						<p>Type of Question: <b>Essay</b></p>
                      </div>
                      <!-- Essay ends here -->
                      <!-- Identification starts here -->
                      <div id="identificationDivwithPicture" class="divcard questionsDiv">
						<p>Type of Question: <b>Identification</b></p>
						<p>Answer: <b id="identificationwithPicture">answer</b></p>
                      </div>
                      <!-- Identification ends here -->
                      <!-- Enumeration starts here -->
                      <div id="enumerationDivwithPicture" class="divcard questionsDiv">
                      	<p>Type of Question: <b>Enumeration</b></p>
						<p>Answers: <b id=""></b></p>
						<div id="enumerationDivAnswerwithPicture">
						</div>
                      </div>
                      <!-- Enumeration ends here -->
                      <!-- Ordering starts here -->
                      <div class="divcard questionsDiv" id="orderingDivwithPicture" >
						<p>Type of Question: <b>Ordering</b></p>
						<p>Answers in order: <b id=""></b></p>
						<div id="orderingDivAnswerswithPicture">
						</div>
                      </div>
                      <!-- Ordering ends here -->
                      <!-- Matching starts here -->
                      <div class="divcard questionsDiv" id="matchingDivwithPicture">
						<p>Type of Question: <b>Matching</b></p>
						<p>Answers: <b id=""></b></p>
						<div id="matchingDivAnswerswithPicture">
						</div>
                      </div>
                      <!-- Matching ends here -->
                      <!-- Assessment starts here -->
                      <div id="assessmentCriteriawithPicture" class="divcard questionsDiv">
						<div>
							<input type="text">
							<textarea rows="30" cols="30" style="resize: none !important;"></textarea>
						</div>
                      </div>
                      <!-- Assessment ends here -->
                    </div>
                    <div class="col s12 m4" id="modalPicCon">
                    </div>
                  </div>
                </div>
              </div>
          </div>
          
          <!-- Question preview without Image -->
          <div id="viewQuestionModalwithoutPicture" class="modal modal-fixed-footer">
              <div class="modal-header">
                <h5>Question</h5>
              </div>
              <div class="modalQuestion">
                <div class="content-row">
                  <div class="row">
					<div id="questionContentwithoutPicture">
                   	</div>
                     <!-- Multiple choice starts here -->
                      <div id="choiceDivwithoutPicture" class="divcard questionsDiv">
                      	<p>Type of Question: <b>Multiple Choice</b></p>
                      	<p>Answers: </p>
                        <div id="multipleChoiceDivwithoutPicture">
                        </div>
                      </div>
                      <!-- Multiple Choice Ends here -->
                      <!-- Fill in the blanks starts here -->
                      <div id="fillBlanksDivwithoutPicture" class="divcard questionsDiv">
						<p>Answers (in order): <b id="trueFalsewithPicture"></b></p>
						<div id="fillBlanksDivAnswerwithoutPicture">
						</div>
                      </div>
                      <!-- Fill in the blanks ends here -->
                      <!-- True or false starts here -->
                      <div id="trueFalseDivwithoutPicture" class="divcard questionsDiv">
						<p>Type of Question: <b>True or False</b></p>
						<p>Answer: <b id="trueFalsewithoutPicture">answer</b></p>
                      </div>
                      <!-- True or false ends here -->
                      <!-- Essay starts here -->
                      <div id="essayDivwithoutPicture" class="divcard questionsDiv">
						<p>Type of Question: <b>Essay</b></p>
                      </div>
                      <!-- Essay ends here -->
                      <!-- Identification starts here -->
                      <div id="identificationDivwithoutPicture" class="divcard questionsDiv">
						<p>Type of Question: <b>Identification</b></p>
						<p>Answer: <b id="identificationwithoutPicture">answer</b></p>
                      </div>
                      <!-- Identification ends here -->
                      <!-- Enumeration starts here -->
                      <div id="enumerationDivwithoutPicture" class="divcard questionsDiv">
                      	<p>Type of Question: <b>Enumeration</b></p>
						<p>Answers: <b id=""></b></p>
						<div id="enumerationDivAnswerwithoutPicture">
						</div>
                      </div>
                      <!-- Enumeration ends here -->
                      <!-- Ordering starts here -->
                      <div class="divcard questionsDiv" id="orderingDivwithoutPicture" >
						<p>Type of Question: <b>Ordering</b></p>
						<p>Answers in order: <b id=""></b></p>
						<div id="orderingDivAnswerswithoutPicture">
						</div>
                      </div>
                      <!-- Ordering ends here -->
                      <!-- Matching starts here -->
                      <div class="divcard questionsDiv" id="matchingDivwithoutPicture">
						<p>Type of Question: <b>Matching</b></p>
						<p>Answers: <b id=""></b></p>
						<div id="matchingDivAnswerswithoutPicture">
						</div>
                      </div>
                      <!-- Matching ends here -->
                      <!-- Assessment starts here -->
                      <div id="assessmentCriteriawithoutPicture" class="divcard questionsDiv">
						<div>
							<input type="text">
							<textarea rows="30" cols="30" style="resize: none !important;"></textarea>
						</div>
                      </div>
                      <!-- Assessment ends here -->
                  </div>
                </div>
              </div>
          </div>
        </div>
	</body>
</html>