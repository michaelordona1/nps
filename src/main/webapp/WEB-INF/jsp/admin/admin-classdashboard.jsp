<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/admin/common/admin-include.jsp"%>
<!DOCTYPE html>
<html>

<head>
  <meta charset="ISO-8859-1" name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PAGSS - CLASS DASHBOARD</title>
  <!-- START: CSS-HEADER -->
  <jsp:include page="../admin/common/admin-trainee-css-header.jsp" />
  <!-- END: CSS-HEADER -->
  <!-- START: JS-HEADER -->
  <jsp:include page="../admin/common/admin-js-header.jsp" />
  <script src="static/assets/js/admin-classdashboard.js"></script>
  <!-- END: JS-HEADER -->
</head>

<body>
  <!-- Header -->
  <jsp:include page="../admin/common/admin-collapsibleheader.jsp" />

  <!-- Left Side bar -->
  <jsp:include page="../admin/common/admin-collapsiblesidebar.jsp" />

  <div class="container1">
    <div class="row">
    
      <!-- left side card/panel includes photo, title, type and status -->
      <jsp:include page="../admin/common/admin-classinfo-card.jsp" />
    
      <div class="col s12 m9" style="padding: 5px;">
        <div class="navbar">
          <a class="active" href="admin.classdashboard?classId=${classId}">Dashboard</a>
          <a href="admin.classdetails?classId=${classId}">Details</a>
          <a href="admin.classtrainee?classId=${classId}">Trainees</a>
          <a href="admin.classattendance?classId=${classId}">Attendance</a>
          <a href="admin.classsettings?classId=${classId}">Settings</a>
          <a href="admin.classmaterials?classId=${classId}">Materials</a>
          <a href="admin.classexam?classId=${classId}">Exams</a>
          <a href="admin.classfinancial?classId=${classId}">Financials</a>
          <a href="admin.classevaluation?classId=${classId}">Evaluation</a>
        </div>
        <!-- content starts here -->
        <div class="divcard" id="classroomtrainerblock">
          <div class="row">
            <div class="col s12 m8">
              <div>
              	<a href="classinfo/${classId}/dashboard/report" class="btn waves-effect waves-light bttn generateBtn">Generate Dashboard Report</a>
              </div>
              <div class="divcard">
                <legend style="margin-bottom: 10px;"><b>Schedule</b></legend>
                <table class="highlight striped">
                  <thead>
                    <tr>
                      <th>Date</th>
                      <th>Start Time</th>
                      <th>End Time</th>
                    </tr>
                  </thead>
                  <tbody id="classScheduleTblBody">
                    <tr>
                      <td>ad</td>
                      <td>ad</td>
                      <td>ad</td>
                    </tr>
                  </tbody>
                </table>
                <div id="pagealign">
                  <div id="ctSchedulePagination"></div>
                </div>	
              </div>
              <div style="margin-top: 20px;">
                Location: <span>${classInfo.locationName}</span>
              </div>
              <div>
                <p><b>Trainer</b></p>
              </div>
              <div class="row" style="margin-bottom: 20px;">
                <div class="col s12 m4" style="text-align: center;">
                  <img src="static/assets/images/usersimage/user.png" style="height: 150px;width: 150px;">
                </div>
                <div class="col s12 m8">
                  <p>${classInfo.trainerName}</p>
                </div>
              </div>
              <div class="divcard" id="checklistDiv">
                <legend style="margin-bottom: 10px;"><b>Checklist</b></legend>
                <div id="checklistsCol">
                </div>
              </div>
            </div>
            <div class="col s12 m4">
              <div class="divcard" style="clear: both;">
                <h3 id="totalEnrolledTraineeLbl" class="left" style="margin: 0;">0</h3>
                <h5 style="text-align: center;vertical-align: middle;">Enrolled Trainees</h5>
              </div>
              <div class="divcard" style="clear: both;">
                <select id="scheduleDropdown" class="presentTraineeDrpDwn">
                  <option disabled selected>Choose an Option</option>
                </select>
                <label></label>
                <h3 id="presentTraineeLbl" class="left" style="margin: 0;">0</h3>
                <h5 style="text-align: center;vertical-align: middle;">Present Trainees</h5>
              </div>
              <div class="divcard" style="clear: both;">
                <select id="examAverageScoreCTDropDown">
                  <option disabled selected>Choose an Option</option>
                </select>
                <h3 id="ctAverageScoreLbl" class="left" style="margin: 0;">0%</h3>
                <h5 style="text-align: center;vertical-align: middle;">Average Score</h5>
              </div>
              <div class="divcard" style="clear: both;">
                <select id="examAverageTimeCTDropDown">
                  <option disabled selected>Choose an Option</option>
                </select>
                <h3 id="ctAverageExamTime" class="left" style="margin-top: 10px;">0</h3>
                <span style="color: #aeaeae">minutes</span>
                <h5 style="text-align: center;vertical-align: middle;">Average Exam Time</h5>
              </div>
              <div class="divcard" style="clear: both;">
                <select id="examPassersCTDropDown">
                  <option disabled selected>Choose an Option</option>
                </select>
                <h3 id="ctExamPassersLbl" class="left" style="margin: 0;">0%</h3>
                <h5 style="text-align: center;vertical-align: middle;">Exam Passers</h5>
              </div>
            </div>
          </div>
        </div>
        <!-- content ends here -->
        <!-- Individual dashboard content starts here -->
        <div class="divcard" id="individualblock">
          <div class="row">
            <div class="col s12 m6">
              <div>
              	<a href="classinfo/${classId}/dashboard/report" class="btn waves-effect waves-light bttn generateBtn">Generate Dashboard Report</a>
              </div>
              <div class="divcard">
                <legend style="margin-bottom: 10px;"><b>Schedule</b></legend>
                <table class="highlight striped">
                  <thead>
                    <tr>
                      <th>Date</th>
                      <th>Start Time</th>
                      <th>End Time</th>
                    </tr>
                  </thead>
                  <tbody id="idClassScheduleTblBody">
                    <tr>
                      <td>ad</td>
                      <td>ad</td>
                      <td>ad</td>
                    </tr>
                  </tbody>
                </table>
                <div id="pagealign">
                  <div id="idSchedulePagination"></div>
                </div>	
              </div>
              <div class="divcard">
                <h3 id="idEnrolledTraineeLbl" class="left" style="margin: 0;">0<i class="material-icons" style="font-size: 40px;">group</i>
                </h3>
                <h5 style="text-align: center;vertical-align: middle;">Enrolled Trainees</h5>
              </div>
              <div class="divcard">
                <h3 id="idCompletionRateLbl" class="left" style="margin: 0;">0%</h3>
                <h5 style="text-align: center;vertical-align: middle;">Completion Rate</h5>
              </div>
              <div class="divcard">
                <div class="left">
                  <h3 id="idAverageTrainingTimeLbl" style="margin-top: 0;margin-bottom: 0;">0<i class="material-icons"
                      style="font-size: 40px;">schedule</i></h3>
                  <span style="color: #aeaeae">minutes</span>
                </div>
                <h5 style="text-align: center;vertical-align: middle;">Average Training Time</h5>
              </div>
            </div>
            <div class="col s12 m6">
              <div>
                Duration: <span>${classInfo.classDuration}</span>
              </div>
              <div>
                <p><b>Trainer</b></p>
              </div>
              <div class="row" style="margin-bottom: 20px;">
                <div class="col s12 m4" style="text-align: center;">
                  <img src="static/assets/images/usersimage/user.png" style="height: 150px;width: 150px;">
                </div>
                <div class="col s12 m8">
                  <p>${classInfo.trainerName}</p>
                </div>
              </div>
              <div class="divcard examAnalysis">
                <legend style="margin-bottom: 10px;"><b>Exam Analysis</b></legend>
                <div style="margin-bottom: 20px;">
                  <select id="examAnalysisDropdownIndividual" class="singleDropDownFunc">
                    <option disabled selected>Choose an Option</option>
                  </select>
                </div>
                <div style="margin-bottom: 20px">
                	<a href="#!" class="btn waves-effect waves-light bttn generateExamAnalysisReport" disabled>Generate Exam Analysis Report</a>
                </div>
                <div class="divcard">
                  <h3 id="idExamPassersLbl" class="left" style="margin: 0;">0%</h3>
                  <h5 style="text-align: center;vertical-align: middle;">Exams Passers</h5>
                </div>
                <div class="divcard">
                  <h3 id="idAveragescoreLbl" class="left" style="margin: 0;">0%</h3>
                  <h5 style="text-align: center;vertical-align: middle;">Average Score</h5>
                </div>
                <div class="divcard">
                  <div class="left">
                    <h3 id="idAverageExamTimeLbl" style="margin-top: 0;margin-bottom: 0;">0<i class="material-icons"
                        style="font-size: 40px;">schedule</i></h3>
                    <span style="color: #aeaeae">minutes</span>
                  </div>
                  <h5 style="text-align: center;vertical-align: middle;">Average Exam Time</h5>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content ends here -->
        <!-- Modular dashboard content starts here -->
        <div class="divcard" id="modularblock">
          <div class="row">
            <div class="col s12 m6">
              <div>
              	<a href="classinfo/${classId}/dashboard/report" class="btn waves-effect waves-light bttn generateBtn">Generate Dashboard Report</a>
              </div>
              <div>
                Duration: <span>${classInfo.classDuration}</span>
              </div>
              <div>
                <p><b>Trainer</b></p>
              </div>
              <div class="row" style="margin-bottom: 20px;">
                <div class="col s12 m4" style="text-align: center;">
                  <img src="static/assets/images/usersimage/user.png" style="height: 150px;width: 150px;">
                </div>
                <div class="col s12 m8">
                  <p>${classInfo.trainerName}</p>
                 
                </div>
              </div>
              <div class="divcard">
                <legend style="margin-bottom: 10px;"><b>Schedule</b></legend>
                <table class="highlight striped">
                  <thead>
                    <tr>
                      <th>Date</th>
                      <th>Start Time</th>
                      <th>End Time</th>
                    </tr>
                  </thead>
                  <tbody id="mdClassScheduleTblBody">
                    <tr>
                      <td>ad</td>
                      <td>ad</td>
                      <td>ad</td>
                    </tr>
                  </tbody>
                </table>
                <div id="pagealign">
                  <div id="mdSchedulePagination"></div>
                </div>	
              </div>
              <div class="divcard">
                <h3 id="mdTotalEnrolledTraineeLbl" class="left" style="margin: 0;">0<i class="material-icons" style="font-size: 40px;">group</i>
                </h3>
                <h5 style="text-align: center;vertical-align: middle;">Enrolled Trainees</h5>
              </div>
              <div class="divcard">
                <h3 id="mdCompletionRateLbl" class="left" style="margin: 0;">0%</h3>
                <h5 style="text-align: center;vertical-align: middle;">Completion Rate</h5>
              </div>
              <div class="divcard">
                <div class="left">
                  <h3 id="mdAverageTrainingTimeLbl" style="margin-top: 0;margin-bottom: 0;">0<i class="material-icons"
                      style="font-size: 40px;">schedule</i></h3>
                  <span style="color: #aeaeae">minutes</span>
                </div>
                <h5 style="text-align: center;vertical-align: middle;">Average Training Time</h5>
              </div>
            </div>
            <div class="col s12 m6">
              <div class="divcard">
                <legend style="margin-bottom: 10px;"><b>Learning Path</b></legend>
                <div class="row" id="learningPathCon">
                </div>
              </div>
              <div class="divcard examAnalysis">
                <legend style="margin-bottom: 10px;"><b>Exam Analysis</b></legend>
                <div style="margin-bottom: 20px;">
                  <select id="examAnalysisDropdownModular" class="singleDropDownFunc">
                    <option disabled selected>Choose an Option</option>
                  </select>
                </div>
                <div style="margin-bottom: 20px">
                	<a href="#!" class="btn waves-effect waves-light bttn generateExamAnalysisReport" disabled>Generate Exam Analysis Report</a>
                </div>
                <div class="divcard">
                  <h3 id="mdExamPassersLbl" class="left" style="margin: 0;">0%</h3>
                  <h5 style="text-align: center;vertical-align: middle;">Exams Passers</h5>
                </div>
                <div class="divcard">
                  <h3 id="mdAverageScoreLbl" class="left" style="margin: 0;">0%</h3>
                  <h5 style="text-align: center;vertical-align: middle;">Average Score</h5>
                </div>
                <div class="divcard">
                  <div class="left">
                    <h3 id="mdAverageExamTimeLbl" style="margin-top: 0;margin-bottom: 0;">0<i class="material-icons"
                        style="font-size: 40px;">schedule</i></h3>
                    <span style="color: #aeaeae">minutes</span>
                  </div>
                  <h5 style="text-align: center;vertical-align: middle;">Average Exam Time</h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <input type="hidden" id="classId" value="${classId}">
  <input type="hidden" id="scheduleType" value="${classInfo.scheduleType}">
  <input type="hidden" id="deliveryMethod" value="${classInfo.deliveryMethod}">
  <input type="hidden" id="courseId" value="${classInfo.courseId}">
</body>

</html>