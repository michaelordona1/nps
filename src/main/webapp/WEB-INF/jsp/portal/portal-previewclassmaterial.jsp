<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/portal/common/include.jsp"%>
<!DOCTYPE html>
<html>
   <head>
       <title>PAGSS</title>
      <meta name = "viewport" content = "width = device-width, initial-scale = 1">
      <!-- START: CSS-HEADER -->  
	  <jsp:include page="../portal/common/css-header.jsp" />
      <!-- END: CSS-HEADER -->
       
      <!-- START: JS-HEADER -->
      <jsp:include page="../portal/common/js-header.jsp" />
      <script src="static/assets/js/portal-previewclassmaterial.js"></script>
      <!-- END: JS-HEADER -->
      <style type="text/css" media="print"> * { display: none; }</style>
   </head>
  
   <body>
     <!-- Header -->
	<jsp:include page="../portal/common/portal-header.jsp" />

	<!-- Left Side bar -->
	<jsp:include page="../portal/common/portal-sidenav.jsp" />
	
	<div class="container1" style="margin-top: 20px;">
          <div class="divcard">
            <div class="row" >
              <div class="col s12 m4" style="text-align: center;">
                <c:choose>
                	<c:when test="${classPhotoUrl != null}">
                		<img class="fixSizeimg" src="${classPhotoUrl}">
                	</c:when>
                	<c:otherwise>
                     	 <img class="fixSizeimg" src="static/assets/images/usersimage/users.png">
                    </c:otherwise>
                </c:choose>
              </div>
              <div class="col s12 m4">
                <span style="color:#1e88e5;font-family: 'Staatliches', cursive;font-size: 1.64rem; ">${classInfo.courseName}</span> <span>(${classInfo.courseCode})</span>
                <hr>
                <div>
                  <span>
                  <c:choose>
                  	<c:when test="${classInfo.deliveryMethod eq 1}">
                  		Individual	
                  	</c:when>
                  	<c:when test="${classInfo.deliveryMethod eq 2}">
                  		Modular
                  	</c:when>
                  	<c:otherwise>
                  		Classroom Training
                  	</c:otherwise>
                  </c:choose>
                  </span><br>
                  <span>${classInfo.categoryName}</span><br>
                  <span>${classInfo.className}</span><br>
                  <span>Location: ${classInfo.locationName}</span><br>
                  <span>Trainer: ${classInfo.trainerName}</span>
                </div>
              </div>
              <div class="col s12 m4">
                <div class="divcard">
                  <legend style="margin-bottom: 10px;"><b>Class Schedule</b></legend>
                  <div id="classScheduleDiv">
                  </div>
                </div>
              </div>
            </div>

            <div class="row" style="margin-top: 20px;">
              <div class="col s12 m6">
                <div class="divcard">
                  <legend style="margin-bottom: 10px;"><b>Course Description</b></legend>
                  <p>${classInfo.description}</p>
                </div>
              </div>
              <div class="col s12 m6">
                <div class="divcard">
                  <legend style="margin-bottom: 10px;"><b>Course Objectives</b></legend>
                  <p>${classInfo.objective}</p>
                </div>
              </div>
            </div>

            <!-- Hidden in take the exams and in evaluation class starts here -->
	        <div class="row" id="documentViewerCon">
	        	<center><iframe id="documentViewerFrame" width='800' height='500' allowfullscreen webkitallowfullscreen></iframe></center>
	        </div>
	        <!-- Hidden in take the exams ends here -->

            <div class="row">
              <a href="#!" id="finishBtn" class="bttn btn waves-effect waves-light" style="width: 100%;">Finish</a>
          </div> 
        </div>
        </div>
        <input type="hidden" id="classId" value="${classInfo.classId}">
        <input type="hidden" id="courseId" value="${classInfo.courseId}">
        <input type="hidden" id="learningPathId" value="${learningPathId}">
        <input type="hidden" id="contentUrl" value="${contentUrl}">
        <input type="hidden" id="contentType" value="${contentType}">
        </body>
        </html>