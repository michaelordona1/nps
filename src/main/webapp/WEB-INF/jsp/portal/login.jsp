<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/admin/common/admin-include.jsp"%>
<!DOCTYPE html>
<html>
   <head>
      <title>PAGSS</title>
      <meta name = "viewport" content = "width = device-width, initial-scale = 1">
      <!-- START: CSS-HEADER -->  
	  <jsp:include page="../portal/common/css-header.jsp" />
	  <!-- <link rel="stylesheet" type="text/css" href="static/assets/css/adminlogin.css"> -->
	  <!-- css for new login -->
	  <link rel="stylesheet" type="text/css" href="static/assets/css/login.css"> 
      <!-- END: CSS-HEADER -->
       
      <!-- START: JS-HEADER -->
      <jsp:include page="../portal/common/js-header.jsp" />
      <script src="static/assets/js/portal-login.js"></script>
      <!-- END: JS-HEADER -->
      
   </head>
  
   <body>
      <!-- <div class="mother-container">

        <div class="subContainer">
          <div class="container">
          <form action="login" method="post" role="form">
            <div id="container-2">
              <div id="small">

                <img style="width:290px;height:80px;margin-bottom:20px;" src="static/assets/images/login/PAGSS.jpg">

                 <input name="user" type="text" class="validate text-input" placeholder="Enter Username">
                    <input name="pass" type="password" class="validate text-input" placeholder="Password" required>
                 <button id="fontcolor" class="btn waves-effect waves-light bttn" type="submit" name="action">Submit</button>
                <p>
                  <a href="/" style="color:#0e0e0e;text-decoration:underline;" class="right">Forgot Password</a><br>
                </p>
              </div>
            </div>
            </form>
          </div>
        </div>

      </div> -->
      
      <!-- new login starts here -->
      <div>
         <div class="row">
            <div class="fullView signinStyle col s12 m4">
                <form action="login" method="post" role="form">
                    <div style="display:block;padding:20px;">
                        <img style="width:410px;height:auto;margin-bottom:20px;" src="static/assets/images/login/PAGSS.jpg">
                        <input style="padding: 0 5px;width:90%;" name="user" type="text" class="validate text-input" placeholder="Enter Username">
                        <input style="padding: 0 5px;width:90%;" name="pass" type="password" class="validate text-input" placeholder="Password" required>
                        <button style="width: 93%;" id="fontcolor" class="btn waves-effect waves-light bttn" type="submit" name="action">Submit</button>
                        <p>
<!--                           <a href="/" style="color:#0e0e0e;text-decoration:underline;" class="right">Forgot Password</a><br> -->
                          <a data-target="chgePassModal" class="modalBttnlogin modal-trigger right">Forgot Password</a>
                        </p>
                    </div>
                </form>
            </div>
            <div class="fullview col m8">
               <div class="row rowHeight">
                     <div class="carousel">
                        <a class="carousel-item" href="#ten!"><img src="static/assets/images/login/8.jpg"></a>
                        <a class="carousel-item" href="#nine!"><img src="static/assets/images/login/9.jpg"></a>
                        <a class="carousel-item" href="#eight!"><img src="static/assets/images/login/10.jpg"></a>
                        <a class="carousel-item" href="#one!"><img src="static/assets/images/login/1.jpg"></a>
                        <a class="carousel-item" href="#three!"><img src="static/assets/images/login/3.jpg"></a>
                        <a class="carousel-item" href="#four!"><img src="static/assets/images/login/4.jpg"></a>
                        <a class="carousel-item" href="#five!"><img src="static/assets/images/login/5.jpg"></a>
                        <a class="carousel-item" href="#six!"><img class="sixImage" src="static/assets/images/login/6.jpg"></a>
                     </div>
               </div>
               <div class="row">
                  <div class="bg">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- new login ends here -->
      <!-- Change password modal -->
      <div id="chgePassModal" class="modal modal-fixed-footer">
         <div class="modal-content">
            <h5 style="text-align:center;margin:0 0 25px !important;">Forgot your password?</h5>
            <b>Username:</b>
            <input type="text" id="username" class="inputPass validate">
         </div>
         <div class="modal-footer">
            <!-- <a class="modal-close btn waves-effect waves-light bttn" type="button" name="action"><i class="material-icons left">cancel</i>Cancel</a> -->
            <a class="btn waves-effect waves-light bttn" type="submit" name="action" id="resetBtn">Reset Password</a>
         </div>
      </div>
   </body>
</html>