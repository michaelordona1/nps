<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/admin/common/admin-include.jsp"%>
<!DOCTYPE html>
<html>

<head>
  <title>PAGSS</title>
  <meta name="viewport" content="width = device-width, initial-scale = 1">
</head>
<body oncontextmenu='return false;'>
	<video id="materialVideo" src="${material.contentUrl}" autoplay controls controlsList='nodownload' style="height:100vh; width:100%; background-color:black;"></video>
</body>
</html>