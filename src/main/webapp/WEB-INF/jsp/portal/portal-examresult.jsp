<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/admin/common/admin-include.jsp"%>
<!DOCTYPE html>
<html>
   <head>
       <title>PAGSS</title>
      <meta name = "viewport" content = "width = device-width, initial-scale = 1">
      <!-- START: CSS-HEADER -->  
	  <jsp:include page="../portal/common/css-header.jsp" />
      <!-- END: CSS-HEADER -->
       
      <!-- START: JS-HEADER -->
      <jsp:include page="../portal/common/js-header.jsp" />
      <script src="static/assets/js/portal-examresult.js"></script>
      <!-- END: JS-HEADER -->
   </head>
  
   <body >
     <!-- Header -->
	<jsp:include page="../portal/common/portal-header.jsp" />

	<!-- Left Side bar -->
	<%-- <jsp:include page="../portal/common/portal-sidenav.jsp" /> --%>
	<div class="container" style="margin-top: 20px;">
          <div class="divcard">

            <!-- Breadcrumbs starts here -->
            <div class="row">
              <nav>
                <div>
                  <div class="col s12">
                    <div class="col s12">
                  <a href="#!" style="color:black;" class="breadcrumb">${classInfo.courseName}</a> 
                    <a href="#!" style="color:black;" class="breadcrumb">></a> 
                    <a href="#!" style="color:black;" class="breadcrumb">${classInfo.className}</a>
                    <a href="#!" style="color:black;" class="breadcrumb">></a> 
                    <a href="#!" style="color:black;" class="breadcrumb">${examInfo.title}</a>
                </div>
                  </div>
                </div>
              </nav>
            </div>
            <!-- Breadcrumbs ends here -->

            <div class="row" style="margin-top: 20px;">
              <div class="col s12 m6">
                <div class="row">
                  <h6><b>${employeeInfo.fullName} (${status}) (${examInfo.title})</b></h6>
                </div>
                <div class="row" style="margin-top: 25px;">
                  <div id="scoreCon"><p>Score: <span>${totalScore}/${totalItems} (${percentage}%)</span></p></div>
                  <div class="divcard" id="showBreakDownCon">
                    <legend style="margin-bottom: 10px;"><b>Score Breakdown</b></legend>
                    <table class="striped highlight">
                      <thead>
                        <tr>
                          <th>Section</th>
                          <th>Total Items</th>
                          <th>Score</th>
                        </tr>
                      </thead>
                      <tbody id="scoreBreakdownTblBody">
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="row" style="margin-top: 15px;" id="officialScoreCon">
                  <p>Official Score: <span><span id="officialScore"></span>/${totalItems} (<span id="officialPercentage"></span>)</span></p>
                  <p>Official Exam Status:<span id="officialStatus"></span></p>
                  <div class="divcard">
                    <legend style="margin-bottom: 10px;"><b>Score Breakdown</b></legend>
                    <table class="striped highlight">
                      <thead>
                        <tr>
                          <th>Section</th>
                          <th>Total Items</th>
                          <th>Score</th>
                        </tr>
                      </thead>
                      <tbody id="OfficialScoreBreakdownTblBody">
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="col s12 m6">
                <div class="row">
                  <div class="col s6 m6" id="reviewBtnCon">
                  </div>
                  <div class="col s6 m6" id="retakeExamBtnCon">
                    	<c:if test="${retakesLeft > 0}">
                    	<a href="#!" class="bttn btn waves-light waves-effect" id="retakeExamBtn" style="width: 100%;">Retake Exam</a>
                    	</c:if>
                  </div>
                </div>
                <div>
                <div class="row" style="padding-left: 10px;margin-top: 10px;">
                  <span id="retakeTxt"></span><br>
                  <div id="retakePropCon">
                  <span id="conditionTxt">Condition:</span><br>
                  <span id="retakesLeftTxt">No. of Retakes Left: ${retakesLeft}</span></div><br>
                </div>
                <div class="row" style="padding-left: 10px;margin-top: 10px;" id="retakeCon">
                  <div class="divcard">
                    <legend style="margin-bottom: 10px;"><b>Attempt History</b></legend>
                    <table class="striped highlight">
                      <thead>
                        <tr>
                          <th>Date of Attempt</th>
                          <th>Score</th>
                          <th>Percentage</th>
                          <th>Status</th>
                        </tr>
                      </thead>
                      <tbody id="attemptHistoryTblBody">
                      </tbody>
                    </table>
                  </div>
                </div>
                </div>
                
              </div>
            </div>

            <div class="row" id="attachmentCon">
              <div class="file-field input-field" style="margin-top: 0 !important;">
                <div class="btn bttn right" style="margin-top: 20px;">
                  <span>Browse</span>
                  <input type="file">
                </div>
                <div class="file-path-wrapper" style="padding-top: 0 !important;">
                  <span>&nbsp</span>
                  <input class="file-path validate" type="text" id="fileupload">
                </div>
              </div>
            </div>

            <div class="row">
              <a href="preview-class?classId=${classInfo.classId}" class="bttn btn waves-effect waves-light" type="submit" style="width: 100%;">Back to Class</a>
            </div>

          </div>
        </div>

      </div>
	<input type="hidden" id="hdnClassId" value="${classInfo.classId}">
	<input type="hidden" id="hdnCourseId" value="${classInfo.courseId}">
	<input type="hidden" id="hdnExamId" value="${examInfo.examId}">
	<input type="hidden" id="hdnStatus" value="${status}">
   </body>
</html>