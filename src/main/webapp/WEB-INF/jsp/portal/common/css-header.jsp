<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/admin/common/admin-include.jsp"%>

<link rel="stylesheet" href="static/assets/vendor/materialize/icons/material-icon.css">
<link rel="stylesheet" href="static/assets/vendor/materialize/css/materialize.min.css">
<link rel="stylesheet" href="static/assets/vendor/pagination/pagination.css">
<link rel="stylesheet" href="static/assets/vendor/lightbox/lightbox.css">

<link href="https://fonts.googleapis.com/css?family=Staatliches&display=swap" rel="stylesheet">
<link rel="stylesheet" href="static/assets/vendor/alertify/alertify.min.css">
<link rel="stylesheet" href="static/assets/vendor/alertify/default.min.css">
<link rel="stylesheet" href="static/assets/vendor/slick-1.8.1/slick.css">
<link rel="stylesheet" href="static/assets/vendor/slick-1.8.1/slick-theme.css">
<link rel="stylesheet" href="static/assets/vendor/alertify/default.min.css">
<link rel="stylesheet" href="static/assets/vendor/fullcalendar/daygrid/main.min.css"/>
<link rel="stylesheet" href="static/assets/vendor/fullcalendar/core/main.min.css"/>
<link rel="stylesheet" href="static/assets/vendor/fullcalendar/timegrid/main.min.css"/>
<link rel="stylesheet" type="text/css" href="static/assets/css/page.css">
