<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/admin/common/admin-include.jsp"%>

<div class="navbar-fixed" style="z-index: 3;">
	<nav>
		<div class="nav-wrapper">
           <a href="#" class="sidenav-trigger hide-on-large-only" data-target="slide-out"><i class="material-icons">menu</i></a>
           <ul class="hide-on-med-and-down" style="margin-right: 10px;">
           <li class="left">
             <ul>
               <!-- <li><a data-target="slide-out" class="sidenav-trigger show-on-large"><i class="material-icons" style="padding: 0 15px">menu</i></a></li> -->
               <li><a href="mytraining-inprogress" class="compname">Learning Management System</a></li>
             </ul>
           </li>
           <li class="right">
             <ul>
               <li>
                 <a href="#!" class="profset">
                   <img id="profile" src="static/assets/images/usersimage/user.png">
                 </a>
               </li>
               <li>
               	   <c:if test="${sessionScope.user.userTypeId ne 3}">
               	  	 <a class="dropdown-trigger" data-target="dropOption" style="height: 74px;">
               	   </c:if>	
	                   <span style="margin-top:-10px;display:block;height:20px;color: #1e1e1e; font-weight: bold; padding-right: 20px;">${sessionScope.user.fullName}</span>
	                   <label>${sessionScope.user.jobName}</label>
	              <c:if test="${sessionScope.user.userTypeId ne 3}">    
                   	</a>
                  </c:if>	
               </li>
               <li>
                 <a class="dropdown-trigger" data-target="dropOption2" href="#!">
                 	<i style="font-size: 30px !important;" class="material-icons account_circle badge notif">notifications</i>
                 	<small id="notificationBadge" class="notification-badge"></small>
               	</a>
               </li>
               <li>
                 <a id="logoutBtn" href="#!"><i style="font-size: 30px !important;" class="material-icons account_circle">power_settings_new</i></a>
               </li>
             </ul>
           </li>
         </ul>
		</div>
	</nav>
</div>
<ul id="dropOption" class="dropdown-content">
   <li><a href="admin.home">Admin/Trainer</a></li>
   <li><a href="mytraining-inprogress">Trainee User</a></li>
</ul>
<div id="dropOption2" class="dropdown-content collection with-header">
	<h4 class="collection-header">Notifications</h4>
	<ul id="notificationUl">
	</ul>
</div>



<script>
	const JOB_ROLE_ID = `${sessionScope.user.jobroleId}`;
	const USER_GROUP_ID = `${sessionScope.user.userGroupId}`;
	const USER_ID = `${sessionScope.user.userId}`;
</script>
<script src="static/assets/js/notifications.js"></script>