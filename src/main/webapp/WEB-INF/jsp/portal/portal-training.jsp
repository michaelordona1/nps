<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/admin/common/admin-include.jsp"%>
<!DOCTYPE html>
<html>
   <head>
       <title>PAGSS</title>
      <meta name = "viewport" content = "width = device-width, initial-scale = 1">
      <!-- START: CSS-HEADER -->  
	  <jsp:include page="../portal/common/css-header.jsp" />
      <!-- END: CSS-HEADER -->
       
      <!-- START: JS-HEADER -->
      <jsp:include page="../portal/common/js-header.jsp" />
      <script src="static/assets/js/portal-training.js"></script>
      <!-- END: JS-HEADER -->
      
	<script>
		$(function(){
			initializeSettings();
		})
		var HIT_COUNT = ${user.hitCount};
		function initializeSettings(){
			var description = "In compliance with Data Privacy Act of 2012 (DPA) (R.A. 10173) and its Implementing Rules and Regulations (DPA-IRR). <br/><br/> PAGSS-TDD sets our data protection practices to safeguard intellectual properties of the company and the personal data of employees and individuals responsible to training matters. The policy includes information on employees training history (current, past, and prospective), how it will use and process, keep and secure, and how it will be disposed when no longer needed.";
			
			if(HIT_COUNT == 0){
				alertify.alert("", description, function(){
					ajax.create("user/update-hitcount");
				}).set('label', 'I Agree');
			}
		}
	</script>	      
   </head>
  
   <body >
     <!-- Header -->
	<jsp:include page="../portal/common/portal-header.jsp" />

	<!-- Left Side bar -->
	<%-- <jsp:include page="../portal/common/portal-sidenav.jsp" /> --%>
        <div class="container">
          <div class="row">
            <div class="col s12 m12">
               <h4>My Trainings</h4>
            </div>
          </div>

          <div class="row">
            <hr style="width:100%;">
          </div>

          
          <div class="navbarTrainee" style="margin-bottom: 20px;">
            <a href="mytraining-calendar">Calendar</a>
            <a href="mytraining-inprogress" id="inProgressHref"></a>
            <a href="mytraining-completed" id="completeHref"></a>
            <a href="mytraining-catalog">Training Catalog</a>
          </div>

          <div class="row" id="classDiv">
            <!--<div class="col s12 m4">
              <div class="divcard">
                <h6 style="margin: 0;"><b>Passenger Handling</b></h6>
                <p><label><span>Class 101</span></label></p>
                <p><label><span>Classroom Training</span></label></p>
                <a href="#!" class="btn bttn waves-effect waves-light" type="submit" style="width: 100%;">Start</a>
              </div>
            </div>
            
            <div class="col s12 m4">
              <div class="divcard">
                <h6 style="margin: 0;"><b>Passenger Handling</b></h6>
                <p><label><span>Class 101</span></label></p>
                <p><label><span>CBT (Individual)</span></label></p>
                <a href="#!" class="btn bttn waves-effect waves-light" type="submit" style="width: 100%;">Resume</a>
              </div>
            </div>

            <div class="col s12 m4">
              <div class="divcard">
                <h6 style="margin: 0;"><b>Passenger Handling</b></h6>
                <p><label><span>Class 101</span></label></p>
                <p><label><span>CBT (Modular)</span></label></p>
                <a href="#!" class="btn bttn waves-effect waves-light" type="submit" style="width: 100%;">Resume</a>
              </div>
            </div>
          </div>-->
        </div>
        <div id="classDivPagination"></div>
	<input type="hidden" id="userId" value="${user.userId}"/>
	<input type="hidden" id="status" value="${status}"/>
   </body>
</html>