<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/admin/common/admin-include.jsp"%>
<!DOCTYPE html>
<html>
   <head>
       <title>PAGSS</title>
      <meta name = "viewport" content = "width = device-width, initial-scale = 1">
      <!-- START: CSS-HEADER -->  
	  <jsp:include page="../portal/common/css-header.jsp" />
      <!-- END: CSS-HEADER -->
       
      <!-- START: JS-HEADER -->
      <jsp:include page="../portal/common/js-header.jsp" />
      <jsp:include page="../admin/common/admin-tinymce-header.jsp" />
      <script src="static/assets/js/utility/tinymce-util.js"></script>
      <script src="static/assets/js/portal-examreview.js"></script>
      <!-- END: JS-HEADER -->
   </head>
	<body>
		<!-- Header -->
		<jsp:include page="../portal/common/portal-header.jsp" />	
		<!-- Left Side bar -->
		<%-- <jsp:include page="../portal/common/portal-sidenav.jsp" />	 --%>
		<div class="container" style="margin-top: 20px;">
			<div class="divcard">
			      <!-- Breadcrumbs starts here -->
			      <div class="row">
		              <nav>
		                <div class="col s12">
		                  <a href="#!" style="color:black;" class="breadcrumb">${classInfo.courseName}</a> 
		                    <a href="#!" style="color:black;" class="breadcrumb">></a> 
		                    <a href="#!" style="color:black;" class="breadcrumb">${classInfo.className}</a>
		                    <a href="#!" style="color:black;" class="breadcrumb">></a> 
		                    <a href="#!" style="color:black;" class="breadcrumb">${examInfo.title}</a>
		                    <a href="#!" style="color:black;" class="breadcrumb">></a> 
		                  <a href="#!" style="color:black;" class="breadcrumb">${examQuestion.sectionName}</a>
		                </div>
		              </nav>
			      </div>	
			      
			      <div class="row">
			      	<div class="col s12 m3">
			      		<div>
		                  <div class="divcard" style="margin-bottom: 20px;">
		                    <table>
		                      <tbody id="questionNumbersContainer">
		                      </tbody>
		                    </table>
		                  </div>	
		                  <div>
	                  		  <a href="exam-review-home?classId=${classInfo.classId}&courseId=${courseId}&examId=${examId}" class="btn bttn waves-effect waves-light" id="back" style="width: 100%;">Back</a>
	                	  </div>	                  
			      		</div>
			      	</div>
			      	
			      	<div class="col s12 m9">
			      		<div class="divcontrol">
		                  <div>
		                    <a href="#!" id="prevBtn" class="btn bttn waves-effect waves-effect"><i class="material-icons left">keyboard_arrow_left</i>Previous Question</a>
		                  </div>
		                  <div style="padding:10px;">
	                    	<span><b>Question <span id="currentQuestionNo"></span> out of <span id="totalQuestions"></span></b></span>
	                  	  </div>	
		                  <div>
		                    <a href="#!" id="nextBtn" class="btn bttn waves-effect waves-effect"><i class="material-icons right">keyboard_arrow_right</i>Next Question</a>
		                  </div>	                  	  
			      		</div>
			      		
			      		
			      		<div class="mydivs">
			      			<div id="multipleChoiceCon">
								<div class="row">
									<span id="multipleChoiceQuestion"></span>
				                      <div class="col s12 m7" style="overflow-x:auto;">
				                        <form class="formstyle" id="multipleChoiceChoices"> 
				                        </form>
				                          <input type="hidden" id="hdnExamQuestionId">                        
				                      </div>
				                      <div class="col s12 m5 center" id="multipleChoicePicCon">
				                      </div>
			                    </div>			      			
			      			</div>
			      			
			      			<div id="trueOrFalseCon">
			      				<span id="trueOrFalseQuestionCon"></span>
									<div class="row s12 m12" style="overflow-x:auto;">
				                      <label>
				                        <input type="radio" name="trueOrFalseRdBtn" value="true" id="trueRdBtn">
				                        <span id="trueSpan">True</span>
				                      </label><br>
				                      <label>
				                        <input type="radio" name="trueOrFalseRdBtn" value="false" id="falseRdBtn">
				                        <span id="falseSpan">False</span>
				                      </label>										
									</div>		  
				                    <div class="col s12 m5 center" id="trueFalsePicCon">
					                </div>			                          			
			      			</div>
			      			
		                  <div id="identificationCon">
		                      <div id="identificationQuestion"></div>
		                     <div class="row">
		                      <div class="col s12 m7" style="overflow-x:auto;">
		                        <div class="row" style="padding-top: 20px;">
		                          <div class="col s12 m2" style="padding-top: 15px;">
		                            <span id="answerSpan"><b>Answer:</b></span> 
		                          </div>
		                          <div class="col s12 m10">
		                             <input type="text" name="" id="identificationAnswerTxtBox">
		                          </div>
		                        </div>
			                        <div class="row" style="padding-top: 20px">
			                        	<div class="col s12 m12">
			                        		<span id="correctSpan"></span>
			                        	</div>
			                       </div>			                       
		                       </div>
		                       
	                       
		                      <div class="col s12 m5 center" id="identificationPicCon">
		                       </div>
		                     </div>
		                   </div>
			      			
			      			<div id="enumerationCon">
			                    <span id="enumerationContent"> </span>
			                    <div class="row">
			                      <div class="col s12 m7" style="overflow-x:auto;">
			                        <div class="row" style="padding-top: 20px;">
			                          <div class="col s12 m2" style="padding-top: 15px;">
			                            <span><b>Answer:</b></span> 
			                          </div>
			                          <div class="col s12 m10" id="enumerationColumn">
			                              <input type="text" name="">
			                              <input type="text" name="">
			                              <input type="text" name="">
			                          </div>
			                        </div>
			                      </div>
			                        <div class="row" style="padding-top: 20px">
			                        	<div class="col s12 m12">
			                        		<span id="correctEnumerationSpan"></span>
			                        	</div>
			                       </div>				                      
			                      
			                      <div class="col s12 m5 center" id="enumerationPicCon">
			                      </div>
			                    </div>			      			
			      			</div>
			      			
			      			<div id="orderingCon">
			                    <span id="orderingContent"> </span>
			                    <div class="row">
			                      <div class="col s12 m7" style="overflow-x:auto;">
			                        <ul id="sortable" class="container">
			                        </ul>
			                      </div>
			                        <div class="row" style="padding-top: 20px">
			                        	<div class="col s12 m12">
			                        		<span id="correctOrderingSpan"></span>
			                        	</div>
			                       </div>				                      
			                      <div class="col s12 m5 center" id="orderingPicCon">
			                      </div>
			                    </div>			      			
			      			</div>
			      			
			      			
			      			<div id="matchingCon">
			                    <span id="matchingQuestionCon"></span>
			                    <div class="row">
			                       <div class="col s12 m2" style="overflow-x:auto;">
			                          <ol id="matchingQuestions">
			                          </ol>
			                       </div>
			                       <div class="col s12 m4">
			                          <ol type="A" id="matchingQuestionsCompare" class="SpaceBttm right">
			                          </ol>
			                       </div>
			                       <div class="col s12 m6 center" id="matchingPicCon">
			                       </div>	                       
			                    </div>
			                 </div>
			                 
			                  <!-- Fill in the blanks -->                  
			                  <div id="fillInTheBlanksCon">
			                    <div id="fillInTheBlanksSpan">
			                    </div>
			                    <div class="row">
			                      <div class="col s12 m7" style="overflow-x:auto;">
			                        <form class="formstyle" id="fillInTheBlanksQuestions">
			                        </form>                        
			                      </div>
			                      <div class="col s12 m5 center" id="fillInTheBlanksPicCon">
			                      </div>
			                    </div>
			                  </div>
			                  
			                  <div id="essayCon">
			                    <div>
			                      <span id="essayQuestionCon"> </span>
			                    </div>
			                    
			                    <div class="row">
			                      <div class="col s12 m7" style="overflow-x:auto;">
			                        <form class="formstyle" id="essayForm">
			                        </form>
			                      </div>
			                      <div class="col s12 m5 center" id="essayPicCon">
			                      </div>
			                    </div>
			                  </div>			                  			                 
			                 
			      		</div>
			      	
			      	</div>
			      	
			      	
			      </div>
			</div>
		</div>
		<input type="hidden" id="hdnSectionOrder" value="${sectionOrder}">
		<input type="hidden" id="hdnExamId" value="${examId}">
		<input type="hidden" id="hdnClassId" value="${classInfo.classId}">
		<input type="hidden" id="hdnCourseId" value="${courseId}">
		
	    <input type="hidden" id="hdnQuestionId">
	    <input type="hidden" id="hdnQuestionType">
	    <input type="hidden" id="hdnIsRandomized">		
	</body>
</html>