<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/portal/common/include.jsp"%>
<!DOCTYPE html>
<html>
   <head>
      <title>PAGSS</title>
      <meta name = "viewport" content = "width = device-width, initial-scale = 1">
      <!-- START: CSS-HEADER -->  
	  <jsp:include page="../portal/common/css-header.jsp" />
      <!-- END: CSS-HEADER -->
       
      <!-- START: JS-HEADER -->
      <jsp:include page="../portal/common/js-header.jsp" />
      <script src="static/assets/js/portal-examreview-home.js"></script>
      <!-- END: JS-HEADER -->
   </head>
<body>
     <!-- Header -->
	<jsp:include page="../portal/common/portal-header.jsp" />

	<!-- Left Side bar -->
	<jsp:include page="../portal/common/portal-sidenav.jsp" />
	<div class="container1" style="margin-top: 20px;">
          <div class="divcard">

            <!-- Breadcrumbs starts here -->
            <div class="row">
              <nav>
                <div>
                  <div class="col s12">
                    <a href="#!" style="color:black;" class="breadcrumb">${classInfo.courseName}</a> 
                     <a href="#!" style="color:black;" class="breadcrumb">></a> 
                    <a href="#!" style="color:black;" class="breadcrumb">${classInfo.className}</a>
                    <a href="#!" style="color:black;" class="breadcrumb">></a> 
                    <a href="#!" style="color:black;" class="breadcrumb">${examInfo.title}</a>
                  </div>
                </div>
              </nav>
            </div>
            <!-- Breadcrumbs ends here -->

            <div class="row">
            	<div class="col s2">
                	<a href="exam-summary?classId=${classInfo.classId}&courseId=${courseId}&examId=${examInfo.examId}" class="bttn btn waves-effect waves-light" type="submit" style="width: 100%;">Back to result</a>
              	</div>
            </div>

            <div class="row">
              <div class="col s12 m6">
                <span><b style="font-size: 20px">${examInfo.title}</b>&nbsp(<span id="examType"></span>)</span>
              </div>
              <div class="col s12 m6">
                <span class="right"><i class="material-icons left">schedule</i>&nbsp${examInfo.duration} Minutes</span>
              </div>
            </div>

            <div>
              <hr>
              <p>${examInfo.description}</p>
              <table class="striped highlights">
                <thead>
                  <tr>
                    <th>Section</th>
                    <th>Instruction</th>
                    <th>Total Points</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody id="examSectionTbl">
                </tbody>
              </table>
            </div>

          </div>
        </div>

      </div>
	<input type="hidden" id="hdnExamType" value="${examInfo.examType}">
	<input type="hidden" id="hdnClassId" value="${classInfo.classId}">
	<input type="hidden" id="hdnExamId"  value="${examInfo.examId}">
</body>
</html>