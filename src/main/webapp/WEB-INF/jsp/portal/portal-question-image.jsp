<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/admin/common/admin-include.jsp"%>
<!DOCTYPE html>
<html>

<head>
  <title>PAGSS</title>
  <meta name="viewport" content="width = device-width, initial-scale = 1">
  
	<link rel="stylesheet" href="static/assets/vendor/imgViewer2/leaflet.css">
	<link rel="stylesheet" href="static/assets/vendor/jquery-ui/jquery-ui.min.css" media="screen">

	<script src="static/assets/vendor/imgViewer2/leaflet.js"></script>
	<script src="static/assets/vendor/jquery/jquery.min.js"></script>
	<script src="static/assets/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="static/assets/vendor/imgViewer2/imgViewer2.js"></script>
  
  <style>
	.center {
	  justify-content: center;
	  display: flex;
	  align-items: center;
	  height: 100%;
	}
	
	img {
		max-width: 100%;
   		height: 700px;
    	width: auto\9; /* ie8 */
	}
  </style>
  
</head>
<body oncontextmenu='return false;' style="background-color:black">
	<div class="col s12 m12 center">
		<img src="${questionImage.mediaUrl}" id="theImage">
	</div>
	
	
	
	
	<script>
		$(function(){
			$("#theImage").imgViewer2();
		})
	</script>
	
</body>
</html>